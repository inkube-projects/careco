<!doctype html>
   <html lang="es-ES" prefix="og: http://ogp.me/ns# og: http://ogp.me/ns#" >
      <head>
         <!-- Meta Tags -->
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
         <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
         <!--Shortcut icon-->
         <title>Contacto - Careco</title>

         <meta property="og:title" content="Contacto" />
         <meta property="og:description" content="&hellip;" />
         <meta property="og:url" content="http://www.careco.es/?page_id=5288" />
         <meta property="og:type" content="article" />
         <meta property="og:site_name" content="Careco" />
         <meta property="article:published_time" content="2015-07-28T20:22:47+00:00" />
         <meta property="article:modified_time" content="2017-03-02T16:38:21+00:00" />
         <meta property="og:updated_time" content="2017-03-02T16:38:21+00:00" />

         <!-- This site is optimized with the Yoast SEO plugin v4.4 - https://yoast.com/wordpress/plugins/seo/ -->
         <link rel="canonical" href="contact.php" />
         <meta property="og:locale" content="es_ES" />
         <meta property="og:type" content="article" />
         <meta property="og:title" content="Contacto - Careco" />
         <meta property="og:url" content="http://www.careco.es/?page_id=5288" />
         <meta property="og:site_name" content="Careco" />
         <meta name="twitter:card" content="summary" />
         <meta name="twitter:title" content="Contacto - Careco" />
         <!-- / Yoast SEO plugin. -->

         <link rel='dns-prefetch' href='//fonts.googleapis.com' />
         <link rel='dns-prefetch' href='//s.w.org' />
         <link rel="alternate" type="application/rss+xml" title="Careco &raquo; Feed" href="http://www.careco.es/?feed=rss2" />
         <script type="text/javascript">
            window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/www.careco.es\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.7"}};
            !function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
         </script>
         <style type="text/css">
            img.wp-smiley,
            img.emoji {
            	display: inline !important;
            	border: none !important;
            	box-shadow: none !important;
            	height: 1em !important;
            	width: 1em !important;
            	margin: 0 .07em !important;
            	vertical-align: -0.1em !important;
            	background: none !important;
            	padding: 0 !important;
            }
         </style>
         <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
         <link rel='stylesheet' id='contact-form-7-css'  href='resources_2/css/styles.4.6.1.css' type='text/css' media='all' />
         <link rel='stylesheet' id='mediaelement-css'  href='resources_2/css/mediaelementplayer.min.2.22.0.css' type='text/css' media='all' />
         <link rel='stylesheet' id='wp-mediaelement-css'  href='resources_2/css/wp-mediaelement.min.4.7.css' type='text/css' media='all' />
         <link rel='stylesheet' id='rgs-css'  href='resources_2/css/rgs.6.0.1.css' type='text/css' media='all' />
         <link rel='stylesheet' id='font-awesome-css'  href='plugins/font-awesome/web-fonts-with-css/css/fontawesome-all.min.css' type='text/css' media='all' />
         <link rel='stylesheet' id='main-styles-css'  href='resources_2/css/style.7.6.css' type='text/css' media='all' />
         <link rel='stylesheet' id='pretty_photo-css'  href='resources_2/css/prettyPhoto.7.0.1.css' type='text/css' media='all' />
<!--[if lt IE 9]>
<link rel='stylesheet' id='nectar-ie8-css'  href='http://www.careco.es/wp-content/themes/salient/css/ie8.css?ver=4.7' type='text/css' media='all' />
<![endif]-->
<link rel='stylesheet' id='responsive-css'  href='resources_2/css/responsive.7.6.css' type='text/css' media='all' />
<link rel='stylesheet' id='select2-css'  href='resources_2/css/select2-6.2.css' type='text/css' media='all' />
<link rel='stylesheet' id='skin-ascend-css'  href='resources_2/css/ascend7.6.css' type='text/css' media='all' />
<link rel='stylesheet' id='easy-social-share-buttons-css'  href='resources_2/css/easy-social-share-buttons.min.4.1.css' type='text/css' media='all' />
<link rel='stylesheet' id='js_composer_front-css'  href='resources_2/css/js_composer.min.4.12.1.css' type='text/css' media='all' />
<link rel='stylesheet' id='redux-google-fonts-salient_redux-css'  href='http://fonts.googleapis.com/css?family=Montserrat%3A400%2C700%7CLato%3A900%2C400&#038;subset=latin&#038;ver=1509443950' type='text/css' media='all' />
<script type='text/javascript' src='resources_2/js/jquery.1.12.4.js'></script>
<script type='text/javascript' src='resources_2/js/jquery-migrate.min.1.4.1.js'></script>
<script type='text/javascript' src='resources_2/js/modernizr.2.6.2.js'></script>
<link rel='https://api.w.org/' href='http://www.careco.es/?rest_route=/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.careco.es/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.careco.es/wp-includes/wlwmanifest.xml" />
<meta name="generator" content="WordPress 4.7" />
<link rel='shortlink' href='http://www.careco.es/?p=5288' />
<link rel="alternate" type="application/json+oembed" href="http://www.careco.es/?rest_route=%2Foembed%2F1.0%2Fembed&#038;url=http%3A%2F%2Fwww.careco.es%2F%3Fpage_id%3D5288" />
<link rel="alternate" type="text/xml+oembed" href="http://www.careco.es/?rest_route=%2Foembed%2F1.0%2Fembed&#038;url=http%3A%2F%2Fwww.careco.es%2F%3Fpage_id%3D5288&#038;format=xml" />
<link rel="stylesheet" href="resources_2/css/main-contact.css">
<link rel="stylesheet" href="resources_2/css/css-style.css">
<script type="text/javascript">var essb_settings = {"ajax_url":"http:\/\/www.careco.es\/wp-admin\/admin-ajax.php","essb3_nonce":"6e97810b1e","essb3_plugin_url":"http:\/\/www.careco.es\/wp-content\/plugins\/easy-social-share-buttons3","essb3_facebook_total":true,"essb3_admin_ajax":false,"essb3_internal_counter":false,"essb3_stats":false,"essb3_ga":false,"essb3_ga_mode":"simple","essb3_counter_button_min":0,"essb3_counter_total_min":0,"blog_url":"http:\/\/www.careco.es\/","ajax_type":"wp","essb3_postfloat_stay":false,"essb3_no_counter_mailprint":false,"essb3_single_ajax":false,"twitter_counter":"self","post_id":5288};</script><meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]>
   <link rel="stylesheet" type="text/css" href="resources_2/css/vc_lte_ie9.min.css" media="screen">
<![endif]-->
<!--[if IE  8]>
   <link rel="stylesheet" type="text/css" href="resources_2/css/vc-ie8.min.css" media="screen">
<![endif]-->
<style type="text/css" data-type="vc_custom-css">.slider {
    min-height:440px!important;
    max-height:440px!important;
    }


   .faja {
    min-height:220px!important;
    max-height:220px!important;
    }

     .texto {
    padding-right:25%!important;
    padding-top:3%!important;
    }
</style>
<style type="text/css" data-type="vc_shortcodes-custom-css">
   .vc_custom_1488448051959{margin-top: 20px !important;}.vc_custom_1481627956764{padding-top: 20px !important;padding-bottom: 20px !important;}
   body[data-form-style="minimal"] input[type="text"], body[data-form-style="minimal"] textarea, body[data-form-style="minimal"] input[type="email"], body[data-form-style="minimal"] .container-wrap .span_12.light input[type="email"],
   body[data-form-style="minimal"] input[type=password], body[data-form-style="minimal"] input[type=tel], body[data-form-style="minimal"] input[type=url],
   body[data-form-style="minimal"] input[type=search], body[data-form-style="minimal"] input[type=date] {
   	background-color: transparent;
   	box-shadow: none;
   	-webkit-box-shadow: none;
   	border: none!important;
   	position: relative;
   	margin: 0;
   	font-size: 14px;
   	padding-top: 13px!important;
   	padding-bottom: 13px!important;
   	padding-left: 0!important;
   	padding-right: 0!important;
   }
</style>
<noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>


</head>


<body class="page-template-default page page-id-5288 ascend nectar-auto-lightbox wpb-js-composer js-comp-ver-4.12.1 vc_responsive" data-footer-reveal="false" data-header-format="default" data-footer-reveal-shadow="none" data-dropdown-style="classic" data-cae="linear" data-cad="500" data-aie="none" data-ls="pretty_photo" data-apte="standard" data-hhun="0" data-fancy-form-rcs="1" data-form-style="minimal" data-form-submit="default" data-is="minimal" data-button-style="default" data-header-inherit-rc="false" data-header-search="false" data-animated-anchors="true" data-ajax-transitions="true" data-full-width-header="true" data-slide-out-widget-area="true" data-slide-out-widget-area-style="fullscreen" data-user-set-ocm="1" data-loading-animation="none" data-bg-header="false" data-ext-responsive="false" data-header-resize="0" data-header-color="custom" data-transparent-header="false" data-smooth-scrolling="1" data-permanent-transparent="false" data-responsive="1" >



	<div id="header-secondary-outer" data-full-width="true" data-permanent-transparent="false" >
		<div class="container">
			<nav>


			</nav>
		</div>
	</div>

 <!-- <div id="header-space" data-header-mobile-fixed='1'></div> -->

<? include('resources_2/includes/header.php'); ?>



<div id="search-outer" class="nectar">

	<div id="search">

		<div class="container">

		     <div id="search-box">

		     	<div class="col span_12">
			      	<form action="http://www.careco.es" method="GET">
			      		<input type="text" name="s" id="s" value="Start Typing..." data-placeholder="Start Typing..." />
			      	</form>
			      			        </div><!--/span_12-->

		     </div><!--/search-box-->

		     <div id="close"><a href="#"><span class="icon-salient-x" aria-hidden="true"></span></a></div>

		 </div><!--/container-->

	</div><!--/search-->

</div><!--/search-outer-->

<div id="mobile-menu" data-mobile-fixed="1">

	<div class="container">
		<ul>
			<li><a href="">No menu assigned!</a></li>
		</ul>
	</div>

</div>


<div id="ajax-loading-screen" data-disable-fade-on-click="0" data-effect="standard" data-method="standard">

			<div class="loading-icon none">
			<span class="default-loading-icon spin"></span>
		</div>
	</div>

<div id="ajax-content-wrap">

<div class="blurred-wrap">

<div class="container-wrap">

	<div class="container main-content">

		<div class="row">

			<p id="breadcrumbs"><span xmlns:v="http://rdf.data-vocabulary.org/#"><span typeof="v:Breadcrumb"><a href="index.php" rel="v:url" property="v:title">Inicio</a> » <span class="breadcrumb_last">Contacto</span></span></span></p>
		<div id="fws_5a77a663d4060"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-content  vc_row-o-equal-height vc_row-flex standard_section   "  style="padding-top: 0px; padding-bottom: 0px; "><div class="row-bg-wrap"> <div class="row-bg  using-bg-color  "  style="background-color: #fffcfc; " data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div> </div><div class="col span_12 dark left">
	<div style=" background-image: url('http://www.careco.es/wp-content/uploads/2015/07/slider-contact-01-1.jpg'); " class="vc_col-sm-6 wpb_column column_container vc_column_container col no-extra-padding" data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-color="#ffffff" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">
				<div class="img-with-aniamtion-wrap right" data-max-width="100%"><div class="inner"><img data-shadow="none" data-shadow-direction="middle" class="img-with-animation " data-delay="0" height="350" width="479" data-animation="fade-in" src="http://www.careco.es/wp-content/uploads/2016/12/01.jpg" alt="" /></div></div>
			</div>
		</div>
	</div>

	<div style=" background-image: url('http://www.careco.es/wp-content/uploads/2015/07/slider-contact-02.jpg'); " class="vc_col-sm-6 wpb_column column_container vc_column_container col no-extra-padding" data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-color="#e3e3e1" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">
				<div class="img-with-aniamtion-wrap " data-max-width="100%"><div class="inner"><img data-shadow="none" data-shadow-direction="middle" class="img-with-animation " data-delay="0" height="730" width="1969" data-animation="fade-in" src="http://www.careco.es/wp-content/uploads/2015/07/slider-contact-02.jpg" alt="" /></div></div>
			</div>
		</div>
	</div>
</div></div>
		<div id="fws_5a77a663d56e6"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-content  vc_row-o-equal-height vc_row-flex standard_section  faja hide "  style="padding-top: 0px; padding-bottom: 2%; "><div class="row-bg-wrap"> <div class="row-bg  using-bg-color  "  style="background-color: #5b4254; " data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div> </div><div class="col span_12 dark left">
	<div  class="vc_col-sm-2 vc_hidden-xs wpb_column column_container vc_column_container col padding-6-percent"  data-bg-cover="" data-padding-pos="bottom" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

			</div>
		</div>
	</div>

	<div style="margin-top: 150px; " class="vc_col-sm-2 texto2 wpb_column column_container vc_column_container col padding-2-percent"  data-bg-cover="" data-padding-pos="left" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<h2 style="text-align: right;"><span style="color: #ffffff;">Contacto &gt;<br />
</span></h2>

		</div>
	</div>

			</div>
		</div>
	</div>

	<div  class="vc_col-sm-2 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

			</div>
		</div>
	</div>

	<div  class="vc_col-sm-2 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

			</div>
		</div>
	</div>

	<div  class="vc_col-sm-2 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

			</div>
		</div>
	</div>

	<div  class="vc_col-sm-2 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

			</div>
		</div>
	</div>
</div></div>
		<div id="fws_5a77a663d64c0"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-content standard_section   "  style="padding-top: 0px; padding-bottom: 0px; "><div class="row-bg-wrap"> <div class="row-bg    "  style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div> </div><div class="col span_12 dark left">
	<div  class="vc_col-sm-4 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">
				<div id="map_5a77a663d6900" style="height: 600px;" class="nectar-google-map" data-dark-color-scheme="" data-ultra-flat="" data-greyscale="" data-extra-color="" data-enable-animation="false" data-enable-zoom="1" data-zoom-level="14" data-center-lat="39.473440" data-center-lng="-0.371401" data-marker-img=""></div><div class="map_5a77a663d6900 map-marker-list"><div class="map-marker" data-lat="39.473440" data-lng="-0.371401" data-mapinfo=""></div></div>
			</div>
		</div>
	</div>

	<div style="" class="vc_col-sm-4 wpb_column column_container vc_column_container col padding-3-percent" data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-color="#cddddc" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

	<div class="wpb_text_column wpb_content_element  texto " >
		<div class="wpb_wrapper">
			<h4></h4>
<h4><span style="color: #658181;">CONTACTA CON NOSOTROS</span></h4>
<p>&nbsp;</p>
<p><span style="color: #658181;">En Careco llevamos desde 1947 anticipándonos al cambio. Creemos que sólo así se puede alcanzar la calidad de vida, por eso, diseñamos, construimos y promovemos viviendas y construcciones tecnológicamente avanzadas que han sido pensadas para disfrutar del mañana. Hoy por hoy, vamos por delante.</span></p>

		</div>
	</div>

	<div class="wpb_text_column wpb_content_element  vc_custom_1488448051959" >
		<div class="wpb_wrapper">
			<h4><span style="color: #658181;">SEDE CENTRAL:</span></h4>
<p><span style="color: #658181;">Carrer del Mar, 44</span><br />
<span style="color: #658181;">46003 València, Valencia</span></p>
<p><span style="color: #658181;">T. +34 96 315 43 80<br />
M. +34 687 715 531</span><br />
<span style="color: #658181;">email: careco@careco.es</span></p>

		</div>
	</div>

			</div>
		</div>
	</div>

	<div  class="vc_col-sm-4 wpb_column column_container vc_column_container col padding-3-percent"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">
				<div role="form" class="wpcf7" id="wpcf7-f9399-p5288-o1" lang="es-ES" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/?page_id=5288#wpcf7-f9399-p5288-o1" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="9399" />
<input type="hidden" name="_wpcf7_version" value="4.6.1" />
<input type="hidden" name="_wpcf7_locale" value="es_ES" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f9399-p5288-o1" />
<input type="hidden" name="_wpnonce" value="f84312017d" />
</div>
<p><label> Nombre (requerido)<br />
    <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span> </label></p>
<p><label> Tu correo electrónico (requerido)<br />
    <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" /></span> </label></p>
<p><label> Teléfono <span class="wpcf7-form-control-wrap tel-751"><input type="tel" name="tel-751" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel" aria-invalid="false" /></span><br />
    </label></p>
<p><label> Mensaje<br />
    <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea></span> </label></p>
<p><input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" /></p>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
			</div>
		</div>
	</div>
</div></div>
		</div><!--/row-->

	</div><!--/container-->

</div><!--/container-wrap-->


<?php include('resources_2/includes/footer.php') ?>


</div><!--blurred-wrap-->
	<div id="slide-out-widget-area-bg" class="fullscreen dark"></div>
	<div id="slide-out-widget-area" class="fullscreen" data-back-txt="Back">

		<div class="inner-wrap">
		<div class="inner">

		  <a class="slide_out_area_close" href="#"><span class="icon-salient-x icon-default-style"></span></a>


		   		 	 <div class="off-canvas-menu-container">
		  		<ul class="menu">
					    <li id="menu-item-7082" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-7082"><a href="index.php">HOME</a></li>
<li id="menu-item-7560" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-7560"><a href="current-promotion-sales.php">PROMOCIONES EN CURSO</a>
<ul class="sub-menu">
	<li id="menu-item-8176" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-8176"><a href="#">VENTA</a>
	<ul class="sub-menu">
		<li id="menu-item-8181" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8181"><a href="current-promotion-sales.php">AVD. FRANCIA</a></li>
		<li id="menu-item-8180" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8180"><a href="current-promotion-sales.php">PICANYA</a></li>
	</ul>
</li>
	<li id="menu-item-8177" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-8177"><a href="#">ALQUILER</a>
	<ul class="sub-menu">
		<li id="menu-item-8185" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8185"><a href="current-promotion-rental.php">AVD. FRANCIA</a></li>
		<li id="menu-item-8184" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8184"><a href="current-promotion-rental.php">PICANYA</a></li>
	</ul>
</li>
</ul>
</li>
<li id="menu-item-7797" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7797"><a href="next-promotions.php">PRÓXIMAS PROMOCIONES</a></li>
<li id="menu-item-7799" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-7799"><a href="about-us.php">CARECO, CASAS COMO TÚ</a>
<ul class="sub-menu">
	<li id="menu-item-8246" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8246"><a href="about-us.php">ASI SOMOS</a></li>
	<li id="menu-item-7800" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7800"><a href="our-milestones.php">NUESTROS HITOS</a></li>
	<li id="menu-item-9375" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9375"><a href="our-works.php">NUESTRAS OBRAS</a></li>
</ul>
</li>
<li id="menu-item-8216" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-5288 current_page_item menu-item-8216"><a href="contact.php">CONTACTO</a></li>

				</ul>
		    </div>


		</div>

		<div class="bottom-meta-wrap"><p class="bottom-text" data-has-desktop-social="true">Derechos reservados CARECO. 2016</p></div><!--/bottom-meta-wrap--></div> <!--/inner-wrap-->
	</div>


</div> <!--/ajax-content-wrap-->


	<a id="to-top" class=""><i class="fa fa-angle-up"></i></a>
<script type="text/javascript">function showhide_toggle(a,b,d,f){var e=jQuery("#"+a+"-link-"+b),c=jQuery("a",e),g=jQuery("#"+a+"-content-"+b);a=jQuery("#"+a+"-toggle-"+b);e.toggleClass("sh-show sh-hide");g.toggleClass("sh-show sh-hide").toggle();"true"===c.attr("aria-expanded")?c.attr("aria-expanded","false"):c.attr("aria-expanded","true");a.text()===d?a.text(f):a.text(d)};</script>
<link rel='stylesheet' id='sbvcbgslider-style-css'  href='resources_2/css/style.4.7.css' type='text/css' media='all' />
<script type='text/javascript' src='resources_2/js/core.min.1.11.4.js'></script>
<script type='text/javascript' src='resources_2/js/widget.min.1.11.4.js'></script>
<script type='text/javascript' src='resources_2/js/position.min.1.11.4.js'></script>
<script type='text/javascript' src='resources_2/js/menu.min.1.11.4.js'></script>
<script type='text/javascript' src='resources_2/js/wp-a11y.min.4.7.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var uiAutocompleteL10n = {"noResults":"Sin resultados.","oneResult":"1 resultado encontrado. Utiliza las teclas de flecha arriba y abajo para navegar.","manyResults":"%d resultados encontrados. Utiliza las teclas arriba y abajo para navegar.","itemSelected":"Elemento seleccionado."};
/* ]]> */
</script>
<script type='text/javascript' src='resources_2/js/autocomplete.min.1.11.4.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var MyAcSearch = {"url":"http:\/\/www.careco.es\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='resources_2/js/wpss-search-suggest.js'></script>
<script type='text/javascript' src='http://www.careco.es/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpcf7 = {"recaptcha":{"messages":{"empty":"Por favor, prueba que no eres un robot."}}};
/* ]]> */
</script>
<script type='text/javascript' src='resources_2/js/scripts.4.6.1.js'></script>
<script type='text/javascript' src='resources_2/js/nicescroll.3.5.4.js'></script>
<script type='text/javascript' src='resources_2/js/prettyPhoto.7.0.1.js'></script>
<script type='text/javascript' src='resources_2/js/midnight.1.0.js'></script>
<script type='text/javascript' src='resources_2/js/superfish.1.4.8.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var nectarLove = {"ajaxurl":"http:\/\/www.careco.es\/wp-admin\/admin-ajax.php","postID":"5288","rooturl":"http:\/\/www.careco.es","pluginPages":[],"disqusComments":"false","loveNonce":"93d1ba00d9","mapApiKey":"AIzaSyA44aW6ukgUoBzZd5Dn5bsJloe5v0u-5rw"};
/* ]]> */
</script>
<script type='text/javascript' src='resources_2/js/init.7.6.js'></script>
<script type='text/javascript' src='resources_2/js/infinitescroll.1.1.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var mejsL10n = {"language":"es-ES","strings":{"Close":"Cerrar","Fullscreen":"Pantalla completa","Turn off Fullscreen":"Salir de pantalla completa","Go Fullscreen":"Ver en pantalla completa","Download File":"Descargar archivo","Download Video":"Descargar v\u00eddeo","Play":"Reproducir","Pause":"Pausa","Captions\/Subtitles":"Pies de foto \/ Subt\u00edtulos","None":"None","Time Slider":"Control de tiempo","Skip back %1 seconds":"Retroceder %1 segundos","Video Player":"Reproductor de v\u00eddeo","Audio Player":"Reproductor de audio","Volume Slider":"Control de volumen","Mute Toggle":"Desactivar sonido","Unmute":"Activar sonido","Mute":"Silenciar","Use Up\/Down Arrow keys to increase or decrease volume.":"Utiliza las teclas de flecha arriba\/abajo para aumentar o disminuir el volumen.","Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.":"Usa las teclas de direcci\u00f3n izquierda\/derecha para avanzar un segundo, y las flechas arriba\/abajo para avanzar diez segundos."}};
var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/"};
/* ]]> */
</script>
<script type='text/javascript' src='resources_2/js/mediaelement-and-player.min.2.22.0.js'></script>
<script type='text/javascript' src='resources_2/js/wp-mediaelement.min.4.7.js'></script>
<script type='text/javascript' src='resources_2/js/flickity.min.1.1.1.js'></script>
<script type='text/javascript' src='resources_2/js/select2.min.3.5.2.js'></script>
<script type='text/javascript' src='resources_2/js/wp-embed.min.4.7.js'></script>
<script type='text/javascript' src='resources_2/js/js_composer_front.min.4.12.1.js'></script>
<script type='text/javascript' src='resources_2/js/jquery.backstretch.min.1.1.js'></script>
<script type='text/javascript' src='resources_2/js/jquery.flexverticalcenter.1.1.js'></script>
<script type='text/javascript' src='resources_2/js/script.1.1.js'></script>
<script type="text/javascript" src="plugins/bootstrap/js/bootstrap.js">
<script type='text/javascript' src='resources_2/js/map.7.0.7.js'></script>
<div class="essb_mailform"><div class="essb_mailform_content"><p>Send this to friend</p><label class="essb_mailform_content_label">Your email</label><input type="text" id="essb_mailform_from" class="essb_mailform_content_input" placeholder="Your email"/><label class="essb_mailform_content_label">Recipient email</label><input type="text" id="essb_mailform_to" class="essb_mailform_content_input" placeholder="Recipient email"/><div class="essb_mailform_content_buttons"><button id="essb_mailform_btn_submit" class="essb_mailform_content_button" onclick="essb_mailform_send();">Send</button><button id="essb_mailform_btn_cancel" class="essb_mailform_content_button" onclick="essb_close_mailform(); return false;">Cancel</button></div><input type="hidden" id="essb_mail_salt" value="1362929168"/><input type="hidden" id="essb_mail_instance" value=""/><input type="hidden" id="essb_mail_post" value=""/></div></div><div class="essb_mailform_shadow"></div><link rel="stylesheet" id="essb-cct-style"  href="http://www.careco.es/wp-content/plugins/easy-social-share-buttons3/lib/modules/click-to-tweet/assets/css/styles.css" type="text/css" media="all" /><script type="text/javascript">var essb_window = function(oUrl, oService, oInstance) { var element = jQuery('.essb_'+oInstance); var instance_post_id = jQuery(element).attr("data-essb-postid") || ""; var instance_position = jQuery(element).attr("data-essb-position") || ""; var wnd; var w = 800 ; var h = 500; if (oService == "twitter") { w = 500; h= 300; } var left = (screen.width/2)-(w/2); var top = (screen.height/2)-(h/2); if (oService == "twitter") { wnd = window.open( oUrl, "essb_share_window", "height=300,width=500,resizable=1,scrollbars=yes,top="+top+",left="+left ); } else { wnd = window.open( oUrl, "essb_share_window", "height=500,width=800,resizable=1,scrollbars=yes,top="+top+",left="+left ); } if (typeof(essb_settings) != "undefined") { if (essb_settings.essb3_stats) { if (typeof(essb_handle_stats) != "undefined") { essb_handle_stats(oService, instance_post_id, oInstance); } } if (essb_settings.essb3_ga) { essb_ga_tracking(oService, oUrl, instance_position); } } essb_self_postcount(oService, instance_post_id); if (typeof(essb_abtesting_logger) != "undefined") { essb_abtesting_logger(oService, instance_post_id, oInstance); } var pollTimer = window.setInterval(function() { if (wnd.closed !== false) { window.clearInterval(pollTimer); essb_smart_onclose_events(oService, instance_post_id); } }, 200); }; var essb_self_postcount = function(oService, oCountID) { if (typeof(essb_settings) != "undefined") { oCountID = String(oCountID); jQuery.post(essb_settings.ajax_url, { 'action': 'essb_self_postcount', 'post_id': oCountID, 'service': oService, 'nonce': essb_settings.essb3_nonce }, function (data) { if (data) { }},'json'); } }; var essb_smart_onclose_events = function(oService, oPostID) { if (oService == "subscribe" || oService == "comments") return; if (typeof (essbasc_popup_show) == 'function') { essbasc_popup_show(); } if (typeof essb_acs_code == 'function') { essb_acs_code(oService, oPostID); } if (typeof(after_share_easyoptin) != "undefined") { essb_toggle_subscribe(after_share_easyoptin); } }; var essb_tracking_only = function(oUrl, oService, oInstance, oAfterShare) { var element = jQuery('.essb_'+oInstance); if (oUrl == "") { oUrl = document.URL; } var instance_post_id = jQuery(element).attr("data-essb-postid") || ""; var instance_position = jQuery(element).attr("data-essb-position") || ""; if (typeof(essb_settings) != "undefined") { if (essb_settings.essb3_stats) { if (typeof(essb_handle_stats) != "undefined") { essb_handle_stats(oService, instance_post_id, oInstance); } } if (essb_settings.essb3_ga) { essb_ga_tracking(oService, oUrl, instance_position); } } essb_self_postcount(oService, instance_post_id); if (oAfterShare) { essb_smart_onclose_events(oService, instance_post_id); } }; var essb_pinterest_picker = function(oInstance) { essb_tracking_only('', 'pinterest', oInstance); var e=document.createElement('script'); e.setAttribute('type','text/javascript'); e.setAttribute('charset','UTF-8'); e.setAttribute('src','//assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e); };var essb_mailform_opened = false; function essb_open_mailform(unique_id) { jQuery.fn.extend({ center: function () { return this.each(function() { var top = (jQuery(window).height() - jQuery(this).outerHeight()) / 2; var left = (jQuery(window).width() - jQuery(this).outerWidth()) / 2; jQuery(this).css({position:'fixed', margin:0, top: (top > 0 ? top : 0)+'px', left: (left > 0 ? left : 0)+'px'}); }); } }); if (essb_mailform_opened) { essb_close_mailform(unique_id); return; } var sender_element = jQuery(".essb_"+unique_id); if (!sender_element.length) return; var sender_post_id = jQuery(sender_element).attr("data-essb-postid") || ""; jQuery("#essb_mail_instance").val(unique_id); jQuery("#essb_mail_post").val(sender_post_id); var win_width = jQuery( window ).width(); var win_height = jQuery(window).height(); var doc_height = jQuery('document').height(); var base_width = 300; if (win_width < base_width) { base_width = win_width - 30; } var height_correction = 20; var element_class = ".essb_mailform"; var element_class_shadow = ".essb_mailform_shadow"; jQuery(element_class).css( { width: base_width+'px'}); var popup_height = jQuery(element_class).outerHeight(); if (popup_height > (win_height - 30)) { jQuery(element_class).css( { height: (win_height - height_correction)+'px'}); } jQuery("#essb_mailform_from").val(""); jQuery("#essb_mailform_to").val(""); if (jQuery("#essb_mailform_c").length) jQuery("#essb_mailform_c").val(""); jQuery(element_class_shadow).css( { height: (win_height)+'px'}); jQuery(element_class).center(); jQuery(element_class).slideDown(200); jQuery(element_class_shadow).fadeIn(200); essb_mailform_opened = true; essb_tracking_only("", "mail", unique_id); }; function essb_close_mailform() { var element_class = ".essb_mailform"; var element_class_shadow = ".essb_mailform_shadow"; jQuery(element_class).fadeOut(200); jQuery(element_class_shadow).fadeOut(200); essb_mailform_opened = false; }; function essb_mailform_send() { var sender_email = jQuery("#essb_mailform_from").val(); var recepient_email = jQuery("#essb_mailform_to").val(); var captcha_validate = jQuery("#essb_mailform_c").length ? true : false; var captcha = captcha_validate ? jQuery("#essb_mailform_c").val() : ""; var custom_message = jQuery("#essb_mailform_custom").length ? jQuery("#essb_mailform_custom").val() : ""; if (sender_email == "" || recepient_email == "" || (captcha == "" && captcha_validate)) { alert("Please fill all fields in form!"); return; } var mail_salt = jQuery("#essb_mail_salt").val(); var instance_post_id = jQuery("#essb_mail_post").val(); console.log("mail salt = " + mail_salt); if (typeof(essb_settings) != "undefined") { jQuery.post(essb_settings.ajax_url, { "action": "essb_mail_action", "post_id": instance_post_id, "from": sender_email, "to": recepient_email, "c": captcha, "cu": custom_message, "salt": mail_salt, "nonce": essb_settings.essb3_nonce }, function (data) { if (data) { console.log(data); alert(data["message"]); if (data["code"] == "1") essb_close_mailform(); }},'json'); } };</script>

</body>
</html>
