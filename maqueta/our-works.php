<!doctype html>
<html lang="es-ES" prefix="og: http://ogp.me/ns# og: http://ogp.me/ns#" >
   <head>
      <!-- Meta Tags -->
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
      <!--Shortcut icon-->
      <title>NUESTRAS OBRAS - Careco</title>

      <meta property="og:title" content="NUESTRAS OBRAS" />
      <meta property="og:description" content="&hellip;" />
      <meta property="og:url" content="" /> <!-- Pagina actual -->
      <meta property="og:type" content="article" />
      <meta property="og:site_name" content="Careco" />
      <meta property="article:published_time" content="2017-02-15T12:28:17+00:00" />
      <meta property="article:modified_time" content="2017-03-27T09:38:54+00:00" />
      <meta property="og:updated_time" content="2017-03-27T09:38:54+00:00" />

      <!-- This site is optimized with the Yoast SEO plugin v4.4 - https://yoast.com/wordpress/plugins/seo/ -->
      <link rel="canonical" href="" /> <!-- Pagina actual -->
      <meta property="og:locale" content="es_ES" />
      <meta property="og:type" content="article" />
      <meta property="og:title" content="NUESTRAS OBRAS - Careco" />
      <meta property="og:url" content="" /> <!-- Pagina actual -->
      <meta property="og:site_name" content="Careco" />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:title" content="NUESTRAS OBRAS - Careco" />

      <!-- / Yoast SEO plugin. -->
      <link rel='dns-prefetch' href='//fonts.googleapis.com' />
      <link rel='dns-prefetch' href='//s.w.org' />
      <link rel="alternate" type="application/rss+xml" title="Careco &raquo; Feed" href="http://www.careco.es/?feed=rss2" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/www.careco.es\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.7"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
         img.wp-smiley,
         img.emoji {
         	display: inline !important;
         	border: none !important;
         	box-shadow: none !important;
         	height: 1em !important;
         	width: 1em !important;
         	margin: 0 .07em !important;
         	vertical-align: -0.1em !important;
         	background: none !important;
         	padding: 0 !important;
         }
      </style>
      <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
      <link rel='stylesheet' id='contact-form-7-css'  href='resources_2/css/styles.4.6.1.css' type='text/css' media='all' />
      <link rel='stylesheet' id='mediaelement-css'  href='resources_2/css/mediaelementplayer.min.2.22.0.css' type='text/css' media='all' />
      <link rel='stylesheet' id='wp-mediaelement-css'  href='resources_2/css/wp-mediaelement.min.4.7.css' type='text/css' media='all' />
      <link rel='stylesheet' id='rgs-css'  href='resources_2/css/rgs.6.0.1.css' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-css'  href='plugins/font-awesome/web-fonts-with-css/css/fontawesome-all.min.css' type='text/css' media='all' />
      <link rel='stylesheet' id='main-styles-css'  href='resources_2/css/style.7.6.css' type='text/css' media='all' />
      <link rel='stylesheet' id='pretty_photo-css'  href='resources_2/css/prettyPhoto.7.0.1.css' type='text/css' media='all' />
      <!--[if lt IE 9]>
         <link rel='stylesheet' id='nectar-ie8-css'  href='resources_2/css/ie8.4.7.css' type='text/css' media='all' />
      <![endif]-->
      <link rel='stylesheet' id='responsive-css'  href='resources_2/css/responsive.7.6.css' type='text/css' media='all' />
      <link rel='stylesheet' id='nectar-portfolio-css'  href='resources_2/css/portfolio.7.6.css' type='text/css' media='all' />
      <link rel='stylesheet' id='select2-css'  href='resources_2/css/select2-6.2.css' type='text/css' media='all' />
      <link rel='stylesheet' id='skin-ascend-css'  href='resources_2/css/ascend7.6.css' type='text/css' media='all' />
      <link rel='stylesheet' id='easy-social-share-buttons-css'  href='resources_2/css/easy-social-share-buttons.min.4.1.css' type='text/css' media='all' />
      <link rel='stylesheet' id='js_composer_front-css'  href='resources_2/css/js_composer.min.4.12.1.css' type='text/css' media='all' />
      <link rel='stylesheet' id='redux-google-fonts-salient_redux-css'  href='http://fonts.googleapis.com/css?family=Montserrat%3A400%2C700%7CLato%3A900%2C400&#038;subset=latin&#038;ver=1509443950' type='text/css' media='all' />
      <script type='text/javascript' src='resources_2/js/jquery.1.12.4.js'></script>
      <script type='text/javascript' src='resources_2/js/jquery-migrate.min.1.4.1.js'></script>
      <script type='text/javascript' src='resources_2/js/modernizr.2.6.2.js'></script>
      <link rel='https://api.w.org/' href='http://www.careco.es/?rest_route=/' />
      <link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.careco.es/xmlrpc.php?rsd" />
      <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.careco.es/wp-includes/wlwmanifest.xml" />
      <meta name="generator" content="WordPress 4.7" />
      <link rel='shortlink' href='http://www.careco.es/?p=9256' />
      <link rel="alternate" type="application/json+oembed" href="http://www.careco.es/?rest_route=%2Foembed%2F1.0%2Fembed&#038;url=http%3A%2F%2Fwww.careco.es%2F%3Fpage_id%3D9256" />
      <link rel="alternate" type="text/xml+oembed" href="http://www.careco.es/?rest_route=%2Foembed%2F1.0%2Fembed&#038;url=http%3A%2F%2Fwww.careco.es%2F%3Fpage_id%3D9256&#038;format=xml" />
      <link rel="stylesheet" href="resources_2/css/main-our-work.css">
      <link rel="stylesheet" href="resources_2/css/css-style.css">
      <script type="text/javascript">
         var essb_settings = {
            "ajax_url":"http:\/\/www.careco.es\/wp-admin\/admin-ajax.php",
            "essb3_nonce":"9c8aa7b10a",
            "essb3_plugin_url":"http:\/\/www.careco.es\/wp-content\/plugins\/easy-social-share-buttons3",
            "essb3_facebook_total":true,
            "essb3_admin_ajax":false,
            "essb3_internal_counter":false,
            "essb3_stats":false,
            "essb3_ga":false,
            "essb3_ga_mode":"simple",
            "essb3_counter_button_min":0,
            "essb3_counter_total_min":0,
            "blog_url":"http:\/\/www.careco.es\/",
            "ajax_type":"wp",
            "essb3_postfloat_stay":false,
            "essb3_no_counter_mailprint":false,
            "essb3_single_ajax":false,
            "twitter_counter":"self",
            "post_id":9256
         };
      </script>
      <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
      <!--[if lte IE 9]>
         <link rel="stylesheet" type="text/css" href="http://www.careco.es/wp-content/plugins/js_composer_salient/assets/css/vc_lte_ie9.min.css" media="screen">
      <![endif]-->
      <!--[if IE  8]>
         <link rel="stylesheet" type="text/css" href="http://www.careco.es/wp-content/plugins/js_composer_salient/assets/css/vc-ie8.min.css" media="screen">
      <![endif]-->
      <style type="text/css" data-type="vc_custom-css">
         .derechapad {
            padding-right:10%!important;
            padding-top:10%!important;
         }
         .derechapad2 {
            padding-right:10%!important;
            padding-top:10%!important;
         }
         .rectangulo {
             min-height:125px!important;
             max-height:125px!important;
         }
         .izquierdapad {
            padding-left:10%!important;
            padding-top:10%!important;
         }
         .filete .divider {
            border-right: 2px solid #fff;
         }
      </style>
      <style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1481627956764{padding-top: 20px !important;padding-bottom: 20px !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
   </head>

   <body class="page-template-default page page-id-9256 ascend nectar-auto-lightbox wpb-js-composer js-comp-ver-4.12.1 vc_responsive"
      data-footer-reveal="false"
      data-header-format="default"
      data-footer-reveal-shadow="none"
      data-dropdown-style="classic"
      data-cae="linear"
      data-cad="500"
      data-aie="none"
      data-ls="pretty_photo"
      data-apte="standard"
      data-hhun="0"
      data-fancy-form-rcs="1"
      data-form-style="minimal"
      data-form-submit="default"
      data-is="minimal"
      data-button-style="default"
      data-header-inherit-rc="false"
      data-header-search="false"
      data-animated-anchors="true"
      data-ajax-transitions="true"
      data-full-width-header="true"
      data-slide-out-widget-area="true"
      data-slide-out-widget-area-style="fullscreen"
      data-user-set-ocm="1"
      data-loading-animation="none"
      data-bg-header="false"
      data-ext-responsive="false"
      data-header-resize="0"
      data-header-color="custom"
      data-transparent-header="false"
      data-smooth-scrolling="1"
      data-permanent-transparent="false"
      data-responsive="1">



	<div id="header-secondary-outer" data-full-width="true" data-permanent-transparent="false" >
		<div class="container">
			<nav>


			</nav>
		</div>
	</div>

   <? include('resources_2/includes/header.php'); ?>

<div id="search-outer" class="nectar">

	<div id="search">

		<div class="container">

		     <div id="search-box">

		     	<div class="col span_12">
			      	<form action="http://www.careco.es" method="GET">
			      		<input type="text" name="s" id="s" value="Start Typing..." data-placeholder="Start Typing..." />
			      	</form>
			      			        </div><!--/span_12-->

		     </div><!--/search-box-->

		     <div id="close"><a href="#"><span class="icon-salient-x" aria-hidden="true"></span></a></div>

		 </div><!--/container-->

	</div><!--/search-->

</div><!--/search-outer-->

<div id="mobile-menu" data-mobile-fixed="1">

	<div class="container">
		<ul>
			<li><a href="">No menu assigned!</a></li>
		</ul>
	</div>

</div>


<div id="ajax-loading-screen" data-disable-fade-on-click="0" data-effect="standard" data-method="standard">

			<div class="loading-icon none">
			<span class="default-loading-icon spin"></span>
		</div>
	</div>

<div id="ajax-content-wrap">

<div class="blurred-wrap">

<div class="container-wrap">

	<div class="container main-content">

		<div class="row">

			<p id="breadcrumbs"><span xmlns:v="http://rdf.data-vocabulary.org/#"><span typeof="v:Breadcrumb"><a href="index.php" rel="v:url" property="v:title">Inicio</a> » <span class="breadcrumb_last">NUESTRAS OBRAS</span></span></span></p>
		<div id="fws_5a6e41487cbf7"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-content standard_section   "  style="padding-top: 0px; padding-bottom: 0px; "><div class="row-bg-wrap"> <div class="row-bg using-image   "  style="background-image: url(http://www.careco.es/wp-content/uploads/2016/11/nuestros_hitos.jpg); background-position: center center; background-repeat: no-repeat; " data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div> </div><div class="col span_12 dark left">
	<div  class="vc_col-sm-2 vc_hidden-sm vc_hidden-xs wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

			</div>
		</div>
	</div>

	<div  class="vc_col-sm-2 vc_hidden-sm vc_hidden-xs wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

			</div>
		</div>
	</div>

	<div  class="vc_col-sm-2 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

			</div>
		</div>
	</div>

	<div  class="vc_col-sm-6 vc_hidden-sm vc_hidden-xs wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">
				<div class="divider-wrap"><div style="height: 200px;" class="divider"></div></div>
	<div class="wpb_text_column wpb_content_element  bloque-inicio" >
		<div class="wpb_wrapper">
			<h1><a href="index.php"><span style="font-size: 32px; padding: 5px 10px 10px 0px; color: #fff;">CARECO</span></a></h1>
<h1><span style="font-size: 32px; background-color: #7fa9ae; padding: 5px 10px 5px 10px; color: #fff;">CASAS COMO TÚ</span></h1>

		</div>
	</div>
<div class="divider-wrap"><div style="height: 120px;" class="divider"></div></div>
			</div>
		</div>
	</div>
</div></div>
		<div id="fws_5a6e41487da6f"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-content  vc_row-o-equal-height vc_row-flex standard_section  rectangulo hide "  style="padding-top: 0px; padding-bottom: 0px; "><div class="row-bg-wrap"> <div class="row-bg  using-bg-color  "  style="background-color: #606060; " data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div> </div><div class="col span_12 dark left">
	<div  class="vc_col-sm-2 vc_hidden-xs wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

			</div>
		</div>
	</div>

	<div  class="vc_col-sm-2 texto2 wpb_column column_container vc_column_container col padding-2-percent"  data-bg-cover="" data-padding-pos="left" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

			</div>
		</div>
	</div>

	<div  class="vc_col-sm-2 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

			</div>
		</div>
	</div>

	<div  class="vc_col-sm-2 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

			</div>
		</div>
	</div>

	<div  class="vc_col-sm-2 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

			</div>
		</div>
	</div>

	<div  class="vc_col-sm-2 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

			</div>
		</div>
	</div>
</div></div>
		<div id="fws_5a6e41487e523"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-content standard_section   "  style="padding-top: 0px; padding-bottom: 0px; "><div class="row-bg-wrap"> <div class="row-bg  using-bg-color  "  style="background-color: #baddda; " data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div> </div><div class="col span_12 dark left">
	<div style="" class="vc_col-sm-4 vc_hidden-xs wpb_column column_container vc_column_container col no-extra-padding" data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-color="#cddddc" data-bg-opacity="1" data-hover-bg="#000000" data-hover-bg-opacity="0.7" data-animation="" data-delay="0"><a class="column-link" href="about-us.php"></a>
		<div class="vc_column-inner">
			<div class="wpb_wrapper">
				<div class="divider-wrap"><div style="height: 20px;" class="divider"></div></div>
	<div class="wpb_text_column wpb_content_element  right" >
		<div class="wpb_wrapper">
			<h4 style="text-align: right;"><span style="color: #a1bfbd;">ASÍ SOMOS            <span style="color: rgba(100, 100, 100, 0);"> ·</span></span></h4>

		</div>
	</div>
<div class="divider-wrap"><div style="height: 20px;" class="divider"></div></div>
			</div>
		</div>
	</div>

	<div style="" class="vc_col-sm-4 vc_hidden-xs wpb_column column_container vc_column_container col no-extra-padding" data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-color="#c7d2d6" data-bg-opacity="1" data-hover-bg="#000000" data-hover-bg-opacity="0.7" data-animation="" data-delay="0"><a class="column-link" href="our-milestones.php"></a>
		<div class="vc_column-inner">
			<div class="wpb_wrapper">
				<div class="divider-wrap"><div style="height: 20px;" class="divider"></div></div>
	<div class="wpb_text_column wpb_content_element  right" >
		<div class="wpb_wrapper">
			<h4 style="text-align: center;"><span style="color: #a1bfbd;">NUESTROS HITOS</span></h4>

		</div>
	</div>
<div class="divider-wrap"><div style="height: 20px;" class="divider"></div></div>
			</div>
		</div>
	</div>

	<div style="" class="vc_col-sm-4 wpb_column column_container vc_column_container col no-extra-padding" data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-color="#adc0c0" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">
				<div class="divider-wrap"><div style="height: 20px;" class="divider"></div></div>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<h4 style="text-align: left;"><span style="color: #a1bfbd;"> ·           <span style="color: rgba(100, 100, 100, 0);"> </span></span><span style="color: #ffffff;">NUESTRAS OBRAS</span></h4>

		</div>
	</div>
<div class="divider-wrap"><div style="height: 20px;" class="divider"></div></div>
			</div>
		</div>
	</div>
</div></div>
		<div id="fws_5a6e41487f0dc"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row standard_section   "  style="padding-top: 0px; padding-bottom: 0px; "><div class="row-bg-wrap"> <div class="row-bg    "  style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div> </div><div class="col span_12 dark left">
	<div  class="vc_col-sm-12 portfolio-obras wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">
						<div class="portfolio-filters-inline full-width-content " data-alignment="default" data-color-scheme="default">
			<div class="container">
				 <span id="current-category">All</span> 				<ul>
				    <li id="sort-label">Sort Portfolio:</li> 				   <li><a href="#" data-filter="*">All</a></li>
               	   <li><a href="#" data-filter=".alquiler">ALQUILER</a><ul class='children'>
<li><a href="#" data-filter=".francia-alquiler">Francia</a></li>
<li><a href="#" data-filter=".picanya-alquiler">Picanya</a></li>
</ul>
</li>
<li><a href="#" data-filter=".edificio-aceiteras-casanova-avd-puerto">Edificio Aceiteras Casanova</a></li>
<li><a href="#" data-filter=".edificio-azul-almazora">Edificio Azul</a></li>
<li><a href="#" data-filter=".edificio-berlanga">Edificio Berlanga</a></li>
<li><a href="#" data-filter=".edificio-miradores-calle-finlandia">Edificio Miradores</a></li>
<li><a href="#" data-filter=".malilla">Malilla</a></li>
<li><a href="#" data-filter=".masia-les-palmes-picanya">Masia Les Palmes</a></li>
<li><a href="#" data-filter=".urbanizacion-la-mezquida-javea">Urbanización La Mezquida</a></li>
<li><a href="#" data-filter=".venta">VENTA</a><ul class='children'>
<li><a href="#" data-filter=".francia">Francia</a></li>
<li><a href="#" data-filter=".picanya">Picanya</a></li>
</ul>
</li>
				</ul>
				<div class="clear"></div>
			</div>
		</div>




	<div class="portfolio-wrap ">


			<span class="portfolio-loading  none">   </span>



			<div class="row portfolio-items masonry-items  "  data-masonry-type="default" data-ps="2" data-starting-filter="default" data-gutter="5px" data-categories-to-show="edificio-aceiteras-casanova-avd-puerto,edificio-azul-almazora,edificio-berlanga,edificio-miradores-calle-finlandia,malilla,masia-les-palmes-picanya,urbanizacion-la-mezquida-javea" data-col-num="elastic">


					<div class="col elastic-portfolio-item wide_tall element edificio-berlanga "  data-project-cat="edificio-berlanga " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="1000" height="1000" src="images/portfolio/others/MG_7916-1000x1000.jpg" class="attachment-wide_tall size-wide_tall wp-post-image" alt="" title="" srcset="images/portfolio/others/MG_7916-1000x1000.jpg 1000w, images/portfolio/others/MG_7916-50x50.jpg 50w, images/portfolio/others/MG_7916-100x100.jpg 100w, images/portfolio/others/MG_7916-500x500.jpg 500w" sizes="(max-width: 1000px) 100vw, 1000px" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/1.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>Edificio Berlanga, Avenida de Francia</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item tall element edificio-miradores-calle-finlandia "  data-project-cat="edificio-miradores-calle-finlandia " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="1000" src="images/portfolio/others/17_WEB-500x1000.jpg" class="attachment-tall size-tall wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/2.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>EDIFICIO MIRADORES, CALLE FINLANDIA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item wide element edificio-berlanga "  data-project-cat="edificio-berlanga " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="1000" height="500" src="images/portfolio/others/PB_berlanga_03-1000x500.jpg" class="attachment-wide size-wide wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/3.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>Edificio Berlanga, Avenida de Francia</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item regular element urbanizacion-la-mezquida-javea "  data-project-cat="urbanizacion-la-mezquida-javea " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="500" src="images/portfolio/others/20090918111446090_0001_WEB-500x500.jpg" class="attachment-regular size-regular wp-post-image" alt="" title="" srcset="images/portfolio/others/20090918111446090_0001_WEB-500x500.jpg 500w, images/portfolio/others/20090918111446090_0001_WEB-50x50.jpg 50w, images/portfolio/others/20090918111446090_0001_WEB-100x100.jpg 100w, images/portfolio/others/20090918111446090_0001_WEB-1000x1000.jpg 1000w" sizes="(max-width: 500px) 100vw, 500px" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/4.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>URBANIZACION LA MEZQUIDA, JAVEA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item wide element edificio-miradores-calle-finlandia "  data-project-cat="edificio-miradores-calle-finlandia " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="1000" height="500" src="images/portfolio/others/14_WEB-1000x500.jpg" class="attachment-wide size-wide wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/5.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>EDIFICIO MIRADORES, CALLE FINLANDIA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item regular element edificio-berlanga "  data-project-cat="edificio-berlanga " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="500" src="http://www.careco.es/wp-content/uploads/2017/03/MG_8220-500x500.jpg" class="attachment-regular size-regular wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/03/MG_8220-500x500.jpg 500w, http://www.careco.es/wp-content/uploads/2017/03/MG_8220-50x50.jpg 50w, http://www.careco.es/wp-content/uploads/2017/03/MG_8220-100x100.jpg 100w, http://www.careco.es/wp-content/uploads/2017/03/MG_8220-1000x1000.jpg 1000w" sizes="(max-width: 500px) 100vw, 500px" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/6.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>Edificio Berlanga, Avenida de Francia</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item tall element edificio-berlanga "  data-project-cat="edificio-berlanga " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="1000" src="http://www.careco.es/wp-content/uploads/2017/03/03-500x1000.jpg" class="attachment-tall size-tall wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/7.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>Edificio Berlanga, Avenida de Francia</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item regular element edificio-aceiteras-casanova-avd-puerto "  data-project-cat="edificio-aceiteras-casanova-avd-puerto " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="500" src="http://www.careco.es/wp-content/uploads/2017/02/aceite0000-500x500.jpg" class="attachment-regular size-regular wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/02/aceite0000-500x500.jpg 500w, http://www.careco.es/wp-content/uploads/2017/02/aceite0000-50x50.jpg 50w, http://www.careco.es/wp-content/uploads/2017/02/aceite0000-100x100.jpg 100w" sizes="(max-width: 500px) 100vw, 500px" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/8.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>EDIFICIO ACEITERAS CASANOVA, AVD. PUERTO</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item wide element urbanizacion-la-mezquida-javea "  data-project-cat="urbanizacion-la-mezquida-javea " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="1000" height="500" src="http://www.careco.es/wp-content/uploads/2017/02/20090918110945478_0002-1000x500.jpg" class="attachment-wide size-wide wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/10.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>URBANIZACION LA MEZQUIDA, JAVEA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item tall element edificio-miradores-calle-finlandia "  data-project-cat="edificio-miradores-calle-finlandia " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="756" src="http://www.careco.es/wp-content/uploads/2017/02/12-500x756.jpg" class="attachment-tall size-tall wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/11.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>EDIFICIO MIRADORES, CALLE FINLANDIA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item wide element edificio-miradores-calle-finlandia "  data-project-cat="edificio-miradores-calle-finlandia " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="1000" height="500" src="http://www.careco.es/wp-content/uploads/2017/02/EDIFICIO-FINLANDIA-01-WEB-1000x500.jpg" class="attachment-wide size-wide wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/12.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>EDIFICIO MIRADORES, CALLE FINLANDIA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item wide element edificio-berlanga "  data-project-cat="edificio-berlanga " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="1000" height="500" src="http://www.careco.es/wp-content/uploads/2016/11/cMG_5932-1000x500.jpg" class="attachment-wide size-wide wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/13.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>Edificio Berlanga, Avenida de Francia</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item tall element edificio-miradores-calle-finlandia "  data-project-cat="edificio-miradores-calle-finlandia " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="1000" src="http://www.careco.es/wp-content/uploads/2017/03/DSCF9875_web-500x1000.jpg" class="attachment-tall size-tall wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/14.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>EDIFICIO MIRADORES, CALLE FINLANDIA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item regular element masia-les-palmes-picanya "  data-project-cat="masia-les-palmes-picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="450" src="http://www.careco.es/wp-content/uploads/2017/02/027-500x450.jpg" class="attachment-regular size-regular wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/9.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>MASIA LES PALMES, PICANYA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item regular element urbanizacion-la-mezquida-javea "  data-project-cat="urbanizacion-la-mezquida-javea " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="500" src="http://www.careco.es/wp-content/uploads/2017/03/IMG_9192_WEB-500x500.jpg" class="attachment-regular size-regular wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/03/IMG_9192_WEB-500x500.jpg 500w, http://www.careco.es/wp-content/uploads/2017/03/IMG_9192_WEB-50x50.jpg 50w, http://www.careco.es/wp-content/uploads/2017/03/IMG_9192_WEB-100x100.jpg 100w, http://www.careco.es/wp-content/uploads/2017/03/IMG_9192_WEB-1000x1000.jpg 1000w" sizes="(max-width: 500px) 100vw, 500px" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/15.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>URBANIZACION LA MEZQUIDA, JAVEA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item tall element edificio-berlanga "  data-project-cat="edificio-berlanga " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="1000" src="http://www.careco.es/wp-content/uploads/2016/11/MG_7936-500x1000.jpg" class="attachment-tall size-tall wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/16.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>Edificio Berlanga, Avenida de Francia</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item regular element masia-les-palmes-picanya "  data-project-cat="masia-les-palmes-picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="500" src="http://www.careco.es/wp-content/uploads/2017/02/5MG_7416-500x500.jpg" class="attachment-regular size-regular wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/02/5MG_7416-500x500.jpg 500w, http://www.careco.es/wp-content/uploads/2017/02/5MG_7416-50x50.jpg 50w, http://www.careco.es/wp-content/uploads/2017/02/5MG_7416-100x100.jpg 100w" sizes="(max-width: 500px) 100vw, 500px" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/17.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>MASIA LES PALMES, PICANYA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item tall element edificio-miradores-calle-finlandia "  data-project-cat="edificio-miradores-calle-finlandia " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="1000" src="http://www.careco.es/wp-content/uploads/2017/03/DSCF9871_web-500x1000.jpg" class="attachment-tall size-tall wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/18.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>EDIFICIO MIRADORES, CALLE FINLANDIA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item regular element edificio-aceiteras-casanova-avd-puerto "  data-project-cat="edificio-aceiteras-casanova-avd-puerto " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="500" src="http://www.careco.es/wp-content/uploads/2017/02/aceite0002-500x500.jpg" class="attachment-regular size-regular wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/02/aceite0002-500x500.jpg 500w, http://www.careco.es/wp-content/uploads/2017/02/aceite0002-50x50.jpg 50w, http://www.careco.es/wp-content/uploads/2017/02/aceite0002-100x100.jpg 100w" sizes="(max-width: 500px) 100vw, 500px" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/19.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>EDIFICIO ACEITERAS CASANOVA, AVD. PUERTO</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item wide_tall element masia-les-palmes-picanya "  data-project-cat="masia-les-palmes-picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="1000" height="1000" src="http://www.careco.es/wp-content/uploads/2016/11/03-vivienda-unifamiliar-picanya-restaurante-1000x1000.jpg" class="attachment-wide_tall size-wide_tall wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2016/11/03-vivienda-unifamiliar-picanya-restaurante-1000x1000.jpg 1000w, http://www.careco.es/wp-content/uploads/2016/11/03-vivienda-unifamiliar-picanya-restaurante-50x50.jpg 50w, http://www.careco.es/wp-content/uploads/2016/11/03-vivienda-unifamiliar-picanya-restaurante-100x100.jpg 100w, http://www.careco.es/wp-content/uploads/2016/11/03-vivienda-unifamiliar-picanya-restaurante-500x500.jpg 500w" sizes="(max-width: 1000px) 100vw, 1000px" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/21.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>MASIA LES PALMES, PICANYA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item tall element edificio-miradores-calle-finlandia "  data-project-cat="edificio-miradores-calle-finlandia " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="900" src="http://www.careco.es/wp-content/uploads/2017/02/11P6190085-500x900.jpg" class="attachment-tall size-tall wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/22.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>EDIFICIO MIRADORES, CALLE FINLANDIA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item tall element malilla "  data-project-cat="malilla " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="800" src="http://www.careco.es/wp-content/uploads/2017/02/20090918111525463_0001-500x800.jpg" class="attachment-tall size-tall wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/23.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>Malilla</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item regular element urbanizacion-la-mezquida-javea "  data-project-cat="urbanizacion-la-mezquida-javea " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="500" src="http://www.careco.es/wp-content/uploads/2017/02/IMG_9190-1-500x500.jpg" class="attachment-regular size-regular wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/02/IMG_9190-1-500x500.jpg 500w, http://www.careco.es/wp-content/uploads/2017/02/IMG_9190-1-50x50.jpg 50w, http://www.careco.es/wp-content/uploads/2017/02/IMG_9190-1-100x100.jpg 100w" sizes="(max-width: 500px) 100vw, 500px" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/20.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>URBANIZACION LA MEZQUIDA, JAVEA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item wide element masia-les-palmes-picanya "  data-project-cat="masia-les-palmes-picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="1000" height="500" src="http://www.careco.es/wp-content/uploads/2017/02/14MG_7494-1000x500.jpg" class="attachment-wide size-wide wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/26.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>MASIA LES PALMES, PICANYA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item wide_tall element edificio-berlanga "  data-project-cat="edificio-berlanga " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="1000" height="1000" src="http://www.careco.es/wp-content/uploads/2016/11/MG_7911-1000x1000.jpg" class="attachment-wide_tall size-wide_tall wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2016/11/MG_7911-1000x1000.jpg 1000w, http://www.careco.es/wp-content/uploads/2016/11/MG_7911-50x50.jpg 50w, http://www.careco.es/wp-content/uploads/2016/11/MG_7911-100x100.jpg 100w, http://www.careco.es/wp-content/uploads/2016/11/MG_7911-500x500.jpg 500w, http://www.careco.es/wp-content/uploads/2016/11/MG_7911-130x130.jpg 130w" sizes="(max-width: 1000px) 100vw, 1000px" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/28.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>Edificio Berlanga, Avenida de Francia</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item regular element urbanizacion-la-mezquida-javea "  data-project-cat="urbanizacion-la-mezquida-javea " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="500" src="http://www.careco.es/wp-content/uploads/2017/03/IMG_9185_WEB-500x500.jpg" class="attachment-regular size-regular wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/03/IMG_9185_WEB-500x500.jpg 500w, http://www.careco.es/wp-content/uploads/2017/03/IMG_9185_WEB-50x50.jpg 50w, http://www.careco.es/wp-content/uploads/2017/03/IMG_9185_WEB-100x100.jpg 100w, http://www.careco.es/wp-content/uploads/2017/03/IMG_9185_WEB-1000x1000.jpg 1000w" sizes="(max-width: 500px) 100vw, 500px" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/24.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>URBANIZACION LA MEZQUIDA, JAVEA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item regular element masia-les-palmes-picanya "  data-project-cat="masia-les-palmes-picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="500" src="http://www.careco.es/wp-content/uploads/2017/03/web_P1080724-500x500.jpg" class="attachment-regular size-regular wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/03/web_P1080724-500x500.jpg 500w, http://www.careco.es/wp-content/uploads/2017/03/web_P1080724-50x50.jpg 50w, http://www.careco.es/wp-content/uploads/2017/03/web_P1080724-100x100.jpg 100w" sizes="(max-width: 500px) 100vw, 500px" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/48.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>MASIA LES PALMES, PICANYA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item tall element edificio-aceiteras-casanova-avd-puerto "  data-project-cat="edificio-aceiteras-casanova-avd-puerto " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="900" src="http://www.careco.es/wp-content/uploads/2017/02/PB113309-500x900.jpg" class="attachment-tall size-tall wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/25.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>EDIFICIO ACEITERAS CASANOVA, AVD. PUERTO</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item wide element masia-les-palmes-picanya "  data-project-cat="masia-les-palmes-picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="849" height="378" src="http://www.careco.es/wp-content/uploads/2017/02/DSCF0263_edited.jpg" class="attachment-wide size-wide wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/02/DSCF0263_edited.jpg 849w, http://www.careco.es/wp-content/uploads/2017/02/DSCF0263_edited-282x126.jpg 282w, http://www.careco.es/wp-content/uploads/2017/02/DSCF0263_edited-768x342.jpg 768w" sizes="(max-width: 849px) 100vw, 849px" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="http://www.careco.es/wp-content/uploads/2017/02/DSCF0263_edited.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>MASIA LES PALMES, PICANYA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item regular element malilla "  data-project-cat="malilla " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="500" src="http://www.careco.es/wp-content/uploads/2017/02/20090918111600744_0001-500x500.jpg" class="attachment-regular size-regular wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/02/20090918111600744_0001-500x500.jpg 500w, http://www.careco.es/wp-content/uploads/2017/02/20090918111600744_0001-50x50.jpg 50w, http://www.careco.es/wp-content/uploads/2017/02/20090918111600744_0001-100x100.jpg 100w" sizes="(max-width: 500px) 100vw, 500px" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/27.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>Malilla</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item tall element edificio-miradores-calle-finlandia "  data-project-cat="edificio-miradores-calle-finlandia " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="1000" src="http://www.careco.es/wp-content/uploads/2017/02/15-WEB-500x1000.jpg" class="attachment-tall size-tall wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/29.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>EDIFICIO MIRADORES, CALLE FINLANDIA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item regular element edificio-azul-almazora "  data-project-cat="edificio-azul-almazora " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="500" src="http://www.careco.es/wp-content/uploads/2017/02/20090918111624988_0001-500x500.jpg" class="attachment-regular size-regular wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/02/20090918111624988_0001-500x500.jpg 500w, http://www.careco.es/wp-content/uploads/2017/02/20090918111624988_0001-50x50.jpg 50w, http://www.careco.es/wp-content/uploads/2017/02/20090918111624988_0001-100x100.jpg 100w" sizes="(max-width: 500px) 100vw, 500px" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/31.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>Edificio azul, almazora</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item wide_tall element edificio-miradores-calle-finlandia "  data-project-cat="edificio-miradores-calle-finlandia " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="1000" height="1000" src="http://www.careco.es/wp-content/uploads/2017/03/DSCF9864_web-1000x1000.jpg" class="attachment-wide_tall size-wide_tall wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/03/DSCF9864_web-1000x1000.jpg 1000w, http://www.careco.es/wp-content/uploads/2017/03/DSCF9864_web-50x50.jpg 50w, http://www.careco.es/wp-content/uploads/2017/03/DSCF9864_web-100x100.jpg 100w, http://www.careco.es/wp-content/uploads/2017/03/DSCF9864_web-500x500.jpg 500w" sizes="(max-width: 1000px) 100vw, 1000px" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/32.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>EDIFICIO MIRADORES, CALLE FINLANDIA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item regular element masia-les-palmes-picanya "  data-project-cat="masia-les-palmes-picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="500" src="http://www.careco.es/wp-content/uploads/2017/03/web_P1080605-500x500.jpg" class="attachment-regular size-regular wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/03/web_P1080605-500x500.jpg 500w, http://www.careco.es/wp-content/uploads/2017/03/web_P1080605-50x50.jpg 50w, http://www.careco.es/wp-content/uploads/2017/03/web_P1080605-100x100.jpg 100w" sizes="(max-width: 500px) 100vw, 500px" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="http://www.careco.es/wp-content/uploads/2017/03/web_P1080605.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>MASIA LES PALMES, PICANYA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item wide_tall element urbanizacion-la-mezquida-javea "  data-project-cat="urbanizacion-la-mezquida-javea " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="1000" height="900" src="http://www.careco.es/wp-content/uploads/2017/02/IMG_9195-1000x900.jpg" class="attachment-wide_tall size-wide_tall wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/34.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>URBANIZACION LA MEZQUIDA, JAVEA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item tall element edificio-aceiteras-casanova-avd-puerto "  data-project-cat="edificio-aceiteras-casanova-avd-puerto " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="800" src="http://www.careco.es/wp-content/uploads/2017/02/PB113310-500x800.jpg" class="attachment-tall size-tall wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/35.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>EDIFICIO ACEITERAS CASANOVA, AVD. PUERTO</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item regular element masia-les-palmes-picanya "  data-project-cat="masia-les-palmes-picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="500" src="http://www.careco.es/wp-content/uploads/2017/03/web_P1080610-500x500.jpg" class="attachment-regular size-regular wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/03/web_P1080610-500x500.jpg 500w, http://www.careco.es/wp-content/uploads/2017/03/web_P1080610-50x50.jpg 50w, http://www.careco.es/wp-content/uploads/2017/03/web_P1080610-100x100.jpg 100w" sizes="(max-width: 500px) 100vw, 500px" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/36.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>MASIA LES PALMES, PICANYA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item wide element edificio-miradores-calle-finlandia "  data-project-cat="edificio-miradores-calle-finlandia " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="1000" height="500" src="http://www.careco.es/wp-content/uploads/2017/02/17-1000x500.jpg" class="attachment-wide size-wide wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/37.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>EDIFICIO MIRADORES, CALLE FINLANDIA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item regular element masia-les-palmes-picanya "  data-project-cat="masia-les-palmes-picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="500" src="http://www.careco.es/wp-content/uploads/2017/02/13MG_7485-500x500.jpg" class="attachment-regular size-regular wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/02/13MG_7485-500x500.jpg 500w, http://www.careco.es/wp-content/uploads/2017/02/13MG_7485-50x50.jpg 50w, http://www.careco.es/wp-content/uploads/2017/02/13MG_7485-100x100.jpg 100w" sizes="(max-width: 500px) 100vw, 500px" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/38.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>MASIA LES PALMES, PICANYA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item regular element urbanizacion-la-mezquida-javea "  data-project-cat="urbanizacion-la-mezquida-javea " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="500" src="http://www.careco.es/wp-content/uploads/2017/02/20090918111418063_0001-500x500.jpg" class="attachment-regular size-regular wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/02/20090918111418063_0001-500x500.jpg 500w, http://www.careco.es/wp-content/uploads/2017/02/20090918111418063_0001-50x50.jpg 50w, http://www.careco.es/wp-content/uploads/2017/02/20090918111418063_0001-100x100.jpg 100w" sizes="(max-width: 500px) 100vw, 500px" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/39.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>URBANIZACION LA MEZQUIDA, JAVEA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item regular element edificio-azul-almazora "  data-project-cat="edificio-azul-almazora " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="463" height="500" src="http://www.careco.es/wp-content/uploads/2017/02/20090918111624988_PCC_001-463x500.jpg" class="attachment-regular size-regular wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/40.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>Edificio azul, almazora</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item wide element masia-les-palmes-picanya "  data-project-cat="masia-les-palmes-picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="1000" height="500" src="http://www.careco.es/wp-content/uploads/2016/11/02-vivienda-unifamiliar-picanya-cocina-restaurante-1000x500.jpg" class="attachment-wide size-wide wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/41.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>MASIA LES PALMES, PICANYA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item tall element urbanizacion-la-mezquida-javea "  data-project-cat="urbanizacion-la-mezquida-javea " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="800" src="http://www.careco.es/wp-content/uploads/2017/02/IMG_9199-500x800.jpg" class="attachment-tall size-tall wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/42.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>URBANIZACION LA MEZQUIDA, JAVEA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item wide element edificio-miradores-calle-finlandia "  data-project-cat="edificio-miradores-calle-finlandia " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="1000" height="500" src="http://www.careco.es/wp-content/uploads/2017/02/06-1000x500.jpg" class="attachment-wide size-wide wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="http://www.careco.es/wp-content/uploads/2017/02/06.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>EDIFICIO MIRADORES, CALLE FINLANDIA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item tall element edificio-azul-almazora "  data-project-cat="edificio-azul-almazora " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="800" src="http://www.careco.es/wp-content/uploads/2017/02/34380004-500x800.jpg" class="attachment-tall size-tall wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/44.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>Edificio azul, almazora</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item wide_tall element edificio-aceiteras-casanova-avd-puerto "  data-project-cat="edificio-aceiteras-casanova-avd-puerto " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="1000" height="890" src="http://www.careco.es/wp-content/uploads/2017/02/PB113342-1000x890.jpg" class="attachment-wide_tall size-wide_tall wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/45.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>EDIFICIO ACEITERAS CASANOVA, AVD. PUERTO</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item wide element masia-les-palmes-picanya "  data-project-cat="masia-les-palmes-picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="1000" height="500" src="http://www.careco.es/wp-content/uploads/2017/03/027-WEB-1000x500.jpg" class="attachment-wide size-wide wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/46.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>MASIA LES PALMES, PICANYA</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col elastic-portfolio-item regular element edificio-azul-almazora "  data-project-cat="edificio-azul-almazora " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="fade_in">


							<div class="work-item style-2" data-custom-content="off">

								<img width="500" height="500" src="http://www.careco.es/wp-content/uploads/2017/02/20090918111645501_0001-500x500.jpg" class="attachment-regular size-regular wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/02/20090918111645501_0001-500x500.jpg 500w, http://www.careco.es/wp-content/uploads/2017/02/20090918111645501_0001-50x50.jpg 50w, http://www.careco.es/wp-content/uploads/2017/02/20090918111645501_0001-100x100.jpg 100w" sizes="(max-width: 500px) 100vw, 500px" />
								<div class="work-info-bg"></div>
								<div class="work-info">




									        	<a href="images/portfolio/47.jpg"  class="pretty_photo"></a>



									<div class="vert-center">

											<h3>Edificio azul, almazora</h3>
																				</div><!--/vert-center-->

								</div>
							</div><!--work-item-->



					</div><!--/inner-wrap-->
					</div><!--/col-->


			</div><!--/portfolio-->
	   </div><!--/portfolio wrap-->


			</div>
		</div>
	</div>
</div></div>
		<div id="fws_5a6e41489599f"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row standard_section  hide "  style="padding-top: 0px; padding-bottom: 0px; "><div class="row-bg-wrap"> <div class="row-bg    "  style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div> </div><div class="col span_12 dark left">
	<div  class="vc_col-sm-12 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">




	<div class="portfolio-wrap ">


			<span class="portfolio-loading  none">   </span>



			<div class="row portfolio-items no-masonry  " data-rcp="false" data-masonry-type="default" data-ps="1" data-starting-filter="" data-gutter="default" data-categories-to-show="" data-col-num="cols-3">


					<div class="col span_4  element picanya "  data-project-cat="picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img src="http://www.careco.es/wp-content/themes/salient/img/no-portfolio-item-small.jpg" alt="no image added yet." />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="" title="5" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=picanya-pp-53">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya PP-53</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9421" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-berlanga "  data-project-cat="edificio-berlanga " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/03/MG_7916-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/03/MG_7916-600x403.jpg 600w, http://www.careco.es/wp-content/uploads/2017/03/MG_7916-282x188.jpg 282w" sizes="(max-width: 600px) 100vw, 600px" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/1.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-berlanga-avenida-francia-4">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Edificio Berlanga, Avenida de Francia</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9835" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-miradores-calle-finlandia "  data-project-cat="edificio-miradores-calle-finlandia " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/17_WEB-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/2.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-miradores-calle-finlandia-10">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">EDIFICIO MIRADORES, CALLE FINLANDIA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9855" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-berlanga "  data-project-cat="edificio-berlanga " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2016/11/PB_berlanga_03-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/3.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-berlanga-avenida-francia-3">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Edificio Berlanga, Avenida de Francia</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9507" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element urbanizacion-la-mezquida-javea "  data-project-cat="urbanizacion-la-mezquida-javea " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/03/20090918111446090_0001_WEB-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/4.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=urbanizacion-la-mezquida-javea-8">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">URBANIZACION LA MEZQUIDA, JAVEA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9867" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-miradores-calle-finlandia "  data-project-cat="edificio-miradores-calle-finlandia " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/14_WEB-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/5.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-miradores-calle-finlandia-8">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">EDIFICIO MIRADORES, CALLE FINLANDIA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9853" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-berlanga "  data-project-cat="edificio-berlanga " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/03/MG_8220-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/03/MG_8220-600x403.jpg 600w, http://www.careco.es/wp-content/uploads/2017/03/MG_8220-282x188.jpg 282w" sizes="(max-width: 600px) 100vw, 600px" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/6.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-berlanga-avenida-francia-5">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Edificio Berlanga, Avenida de Francia</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9834" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-berlanga "  data-project-cat="edificio-berlanga " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/03/03-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/7.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-berlanga-avenida-francia-6">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Edificio Berlanga, Avenida de Francia</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9836" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-aceiteras-casanova-avd-puerto "  data-project-cat="edificio-aceiteras-casanova-avd-puerto " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/aceite0000-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/02/aceite0000-600x403.jpg 600w, http://www.careco.es/wp-content/uploads/2017/02/aceite0000-282x190.jpg 282w, http://www.careco.es/wp-content/uploads/2017/02/aceite0000-768x517.jpg 768w, http://www.careco.es/wp-content/uploads/2017/02/aceite0000-1024x689.jpg 1024w, images/portfolio/8.jpg 1200w" sizes="(max-width: 600px) 100vw, 600px" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/8.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-aceiteras-casanova-avd-puerto">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">EDIFICIO ACEITERAS CASANOVA, AVD. PUERTO</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9258" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element urbanizacion-la-mezquida-javea "  data-project-cat="urbanizacion-la-mezquida-javea " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/20090918110945478_0002-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/02/20090918110945478_0002-600x403.jpg 600w, http://www.careco.es/wp-content/uploads/2017/02/20090918110945478_0002-282x190.jpg 282w" sizes="(max-width: 600px) 100vw, 600px" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/10.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=urbanizacion-la-mezquida-javea-2">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">URBANIZACION LA MEZQUIDA, JAVEA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9338" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-miradores-calle-finlandia "  data-project-cat="edificio-miradores-calle-finlandia " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/12-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/11.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-miradores-calle-finlandia-3">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">EDIFICIO MIRADORES, CALLE FINLANDIA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9322" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-miradores-calle-finlandia "  data-project-cat="edificio-miradores-calle-finlandia " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/EDIFICIO-FINLANDIA-01-WEB-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/02/EDIFICIO-FINLANDIA-01-WEB-600x403.jpg 600w, http://www.careco.es/wp-content/uploads/2017/02/EDIFICIO-FINLANDIA-01-WEB-282x189.jpg 282w, http://www.careco.es/wp-content/uploads/2017/02/EDIFICIO-FINLANDIA-01-WEB-768x514.jpg 768w" sizes="(max-width: 600px) 100vw, 600px" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/12.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-miradores-calle-finlandia-11">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">EDIFICIO MIRADORES, CALLE FINLANDIA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9864" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-berlanga "  data-project-cat="edificio-berlanga " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2016/11/cMG_5932-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/13.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=diu-iufdig-d-dfgdf">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Edificio Berlanga, Avenida de Francia</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9430" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-miradores-calle-finlandia "  data-project-cat="edificio-miradores-calle-finlandia " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/03/DSCF9875_web-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/14.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-miradores-calle-finlandia-6">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">EDIFICIO MIRADORES, CALLE FINLANDIA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9846" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element masia-les-palmes-picanya "  data-project-cat="masia-les-palmes-picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/027-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/9.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=masia-les-palmes-picanya-2">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">MASIA LES PALMES, PICANYA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9364" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element urbanizacion-la-mezquida-javea "  data-project-cat="urbanizacion-la-mezquida-javea " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/03/IMG_9192_WEB-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/15.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=urbanizacion-la-mezquida-javea-6">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">URBANIZACION LA MEZQUIDA, JAVEA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9868" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-berlanga "  data-project-cat="edificio-berlanga " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2016/11/MG_7936-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/16.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-berlanga-avenida-francia-2">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Edificio Berlanga, Avenida de Francia</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9506" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element masia-les-palmes-picanya "  data-project-cat="masia-les-palmes-picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/5MG_7416-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/17.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=masia-les-palmes-picanya-4">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">MASIA LES PALMES, PICANYA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9368" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-miradores-calle-finlandia "  data-project-cat="edificio-miradores-calle-finlandia " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/03/DSCF9871_web-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/18.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-miradores-calle-finlandia-4">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">EDIFICIO MIRADORES, CALLE FINLANDIA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9324" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-aceiteras-casanova-avd-puerto "  data-project-cat="edificio-aceiteras-casanova-avd-puerto " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/aceite0002-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/19.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-aceiteras-casanova-avd-puerto-2">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">EDIFICIO ACEITERAS CASANOVA, AVD. PUERTO</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9276" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element masia-les-palmes-picanya "  data-project-cat="masia-les-palmes-picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2016/11/03-vivienda-unifamiliar-picanya-restaurante-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/21.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=masia-les-palmes-picanya-11">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">MASIA LES PALMES, PICANYA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9894" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">2</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-miradores-calle-finlandia "  data-project-cat="edificio-miradores-calle-finlandia " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/11P6190085-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/22.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-miradores-calle-finlandia-5">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">EDIFICIO MIRADORES, CALLE FINLANDIA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9327" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element malilla "  data-project-cat="malilla " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="518" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/20090918111525463_0001-518x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/23.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=malilla-2">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Malilla</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9353" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element urbanizacion-la-mezquida-javea "  data-project-cat="urbanizacion-la-mezquida-javea " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/IMG_9190-1-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/20.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=urbanizacion-la-mezquida-javea-3">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">URBANIZACION LA MEZQUIDA, JAVEA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9330" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element masia-les-palmes-picanya "  data-project-cat="masia-les-palmes-picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/14MG_7494-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/26.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=masia-les-palmes-picanya">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">MASIA LES PALMES, PICANYA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9356" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-berlanga "  data-project-cat="edificio-berlanga " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2016/11/MG_7911-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2016/11/MG_7911-600x403.jpg 600w, http://www.careco.es/wp-content/uploads/2016/11/MG_7911-282x188.jpg 282w" sizes="(max-width: 600px) 100vw, 600px" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/28.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-berlanga-avenida-francia">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Edificio Berlanga, Avenida de Francia</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9437" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element urbanizacion-la-mezquida-javea "  data-project-cat="urbanizacion-la-mezquida-javea " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/03/IMG_9185_WEB-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/24.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=urbanizacion-la-mezquida-javea-7">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">URBANIZACION LA MEZQUIDA, JAVEA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9866" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element masia-les-palmes-picanya "  data-project-cat="masia-les-palmes-picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/03/web_P1080724-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/48.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=masia-les-palmes-picanya-7">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">MASIA LES PALMES, PICANYA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9876" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-aceiteras-casanova-avd-puerto "  data-project-cat="edificio-aceiteras-casanova-avd-puerto " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/PB113309-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/25.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-aceiteras-casanova-avd-puerto-3">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">EDIFICIO ACEITERAS CASANOVA, AVD. PUERTO</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9279" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element masia-les-palmes-picanya "  data-project-cat="masia-les-palmes-picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="378" src="http://www.careco.es/wp-content/uploads/2017/02/DSCF0263_edited-600x378.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="http://www.careco.es/wp-content/uploads/2017/02/DSCF0263_edited.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=masia-les-palmes-picanya-5">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">MASIA LES PALMES, PICANYA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9370" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element malilla "  data-project-cat="malilla " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/20090918111600744_0001-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/27.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=malilla">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Malilla</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9349" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-miradores-calle-finlandia "  data-project-cat="edificio-miradores-calle-finlandia " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/15-WEB-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/02/15-WEB-600x403.jpg 600w, http://www.careco.es/wp-content/uploads/2017/02/15-WEB-282x188.jpg 282w" sizes="(max-width: 600px) 100vw, 600px" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/29.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-miradores-calle-finlandia-9">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">EDIFICIO MIRADORES, CALLE FINLANDIA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9854" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-azul-almazora "  data-project-cat="edificio-azul-almazora " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="523" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/20090918111624988_0001-523x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/31.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-azul-almazora-3">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Edificio azul, almazora</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9302" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-miradores-calle-finlandia "  data-project-cat="edificio-miradores-calle-finlandia " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/03/DSCF9864_web-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/32.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-miradores-calle-finlandia-7">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">EDIFICIO MIRADORES, CALLE FINLANDIA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9845" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element masia-les-palmes-picanya "  data-project-cat="masia-les-palmes-picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/03/web_P1080605-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="http://www.careco.es/wp-content/uploads/2017/03/web_P1080605.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=masia-les-palmes-picanya-9">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">MASIA LES PALMES, PICANYA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9878" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element urbanizacion-la-mezquida-javea "  data-project-cat="urbanizacion-la-mezquida-javea " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/IMG_9195-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/34.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=urbanizacion-la-mezquida-javea-4">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">URBANIZACION LA MEZQUIDA, JAVEA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9341" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-aceiteras-casanova-avd-puerto "  data-project-cat="edificio-aceiteras-casanova-avd-puerto " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/PB113310-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/35.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-aceiteras-casanova-avd-puerto-4">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">EDIFICIO ACEITERAS CASANOVA, AVD. PUERTO</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9284" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element masia-les-palmes-picanya "  data-project-cat="masia-les-palmes-picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/03/web_P1080610-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/36.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=masia-les-palmes-picanya-8">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">MASIA LES PALMES, PICANYA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9877" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-miradores-calle-finlandia "  data-project-cat="edificio-miradores-calle-finlandia " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/17-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/37.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-miradores-calle-finlandia">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">EDIFICIO MIRADORES, CALLE FINLANDIA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9314" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element masia-les-palmes-picanya "  data-project-cat="masia-les-palmes-picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/13MG_7485-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/38.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=masia-les-palmes-picanya-3">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">MASIA LES PALMES, PICANYA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9366" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element urbanizacion-la-mezquida-javea "  data-project-cat="urbanizacion-la-mezquida-javea " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/20090918111418063_0001-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/39.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=urbanizacion-la-mezquida-javea">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">URBANIZACION LA MEZQUIDA, JAVEA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9331" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-azul-almazora "  data-project-cat="edificio-azul-almazora " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="463" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/20090918111624988_PCC_001-463x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/40.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-azul-almazora-2">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Edificio azul, almazora</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9300" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element masia-les-palmes-picanya "  data-project-cat="masia-les-palmes-picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2016/11/02-vivienda-unifamiliar-picanya-cocina-restaurante-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/41.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=masia-les-palmes-picanya-10">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">MASIA LES PALMES, PICANYA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9895" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element urbanizacion-la-mezquida-javea "  data-project-cat="urbanizacion-la-mezquida-javea " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/IMG_9199-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/42.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=urbanizacion-la-mezquida-javea-5">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">URBANIZACION LA MEZQUIDA, JAVEA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9343" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-miradores-calle-finlandia "  data-project-cat="edificio-miradores-calle-finlandia " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/06-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/02/06-600x403.jpg 600w, http://www.careco.es/wp-content/uploads/2017/02/06-282x188.jpg 282w" sizes="(max-width: 600px) 100vw, 600px" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="http://www.careco.es/wp-content/uploads/2017/02/06.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-miradores-calle-finlandia-2">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">EDIFICIO MIRADORES, CALLE FINLANDIA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9313" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-azul-almazora "  data-project-cat="edificio-azul-almazora " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="531" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/34380004-531x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/44.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-azul-almazora-4">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Edificio azul, almazora</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9304" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-aceiteras-casanova-avd-puerto "  data-project-cat="edificio-aceiteras-casanova-avd-puerto " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/PB113342-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/45.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-aceiteras-casanova-avd-puerto-5">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">EDIFICIO ACEITERAS CASANOVA, AVD. PUERTO</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9287" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element masia-les-palmes-picanya "  data-project-cat="masia-les-palmes-picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/03/027-WEB-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/46.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=masia-les-palmes-picanya-6">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">MASIA LES PALMES, PICANYA</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9875" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element edificio-azul-almazora "  data-project-cat="edificio-azul-almazora " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2017/02/20090918111645501_0001-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2017/02/20090918111645501_0001-600x403.jpg 600w, http://www.careco.es/wp-content/uploads/2017/02/20090918111645501_0001-282x190.jpg 282w, http://www.careco.es/wp-content/uploads/2017/02/20090918111645501_0001-1024x690.jpg 1024w" sizes="(max-width: 600px) 100vw, 600px" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="images/portfolio/47.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=edificio-azul-almazora">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Edificio azul, almazora</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9298" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element picanya "  data-project-cat="picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img src="http://www.careco.es/wp-content/themes/salient/img/no-portfolio-item-small.jpg" alt="no image added yet." />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="" title="22" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=picanya-sg59-a22-2">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya SG59-A22</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-10274" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element picanya "  data-project-cat="picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img src="http://www.careco.es/wp-content/themes/salient/img/no-portfolio-item-small.jpg" alt="no image added yet." />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=picanya-sg59-a15">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya SG59-A15</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-10033" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element picanya "  data-project-cat="picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img src="http://www.careco.es/wp-content/themes/salient/img/no-portfolio-item-small.jpg" alt="no image added yet." />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=picanya-sg59-a17">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya SG59-A17</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9614" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element picanya "  data-project-cat="picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img src="http://www.careco.es/wp-content/themes/salient/img/no-portfolio-item-small.jpg" alt="no image added yet." />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="" title="a2" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=picanya-ms-22-p17-o6-alq">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya MS-22 P17 O6 alq</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-10282" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element picanya "  data-project-cat="picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img src="http://www.careco.es/wp-content/themes/salient/img/no-portfolio-item-small.jpg" alt="no image added yet." />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="" title="42" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=picanya-ms-24-ll3-alq">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya MS-24 LL3 alq</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-10133" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element picanya "  data-project-cat="picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img src="http://www.careco.es/wp-content/themes/salient/img/no-portfolio-item-small.jpg" alt="no image added yet." />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="" title="a2" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=picanya-ms-22-p12-i5-alq">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya MS-22 P12 I5 alq</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-10130" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element picanya "  data-project-cat="picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img src="http://www.careco.es/wp-content/themes/salient/img/no-portfolio-item-small.jpg" alt="no image added yet." />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="" title="2" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=picanya-ms-22-p12-i5">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya MS-22 P12 I5</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-10058" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element picanya venta "  data-project-cat="picanya venta " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img src="http://www.careco.es/wp-content/themes/salient/img/no-portfolio-item-small.jpg" alt="no image added yet." />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=picanya-pp-47-cb">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya PP-51-CB</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-10003" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element picanya venta "  data-project-cat="picanya venta " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img src="http://www.careco.es/wp-content/themes/salient/img/no-portfolio-item-small.jpg" alt="no image added yet." />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=8143">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya PP-49-CB</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-8143" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element picanya "  data-project-cat="picanya " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img src="http://www.careco.es/wp-content/themes/salient/img/no-portfolio-item-small.jpg" alt="no image added yet." />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=picanya-pp-47">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya PP-47</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-8136" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element picanya venta "  data-project-cat="picanya venta " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img src="http://www.careco.es/wp-content/themes/salient/img/no-portfolio-item-small.jpg" alt="no image added yet." />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=8123">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya MS2-CB</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-8123" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element picanya venta "  data-project-cat="picanya venta " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img src="http://www.careco.es/wp-content/themes/salient/img/no-portfolio-item-small.jpg" alt="no image added yet." />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=picanya-ms-6">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya MS-6</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-8116" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element picanya venta "  data-project-cat="picanya venta " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2016/11/007-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="http://www.careco.es/wp-content/uploads/2016/11/007.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=picanya-jr-3-sb">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya JR-3-SB</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-8105" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element picanya venta "  data-project-cat="picanya venta " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img src="http://www.careco.es/wp-content/themes/salient/img/no-portfolio-item-small.jpg" alt="no image added yet." />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="" title="1" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=picanya-jr-1-cb">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya JR-1-CB</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-10044" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element picanya venta "  data-project-cat="picanya venta " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img src="http://www.careco.es/wp-content/themes/salient/img/no-portfolio-item-small.jpg" alt="no image added yet." />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=8094">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya JR-3-CB</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-8094" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element picanya venta "  data-project-cat="picanya venta " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img src="http://www.careco.es/wp-content/themes/salient/img/no-portfolio-item-small.jpg" alt="no image added yet." />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=picanya-jr-7-cb">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya JR-7-CB</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-8086" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element picanya venta "  data-project-cat="picanya venta " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2016/11/027-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="http://www.careco.es/wp-content/uploads/2016/11/027.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=adosado-en-picanya-jr-7-cb-a">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya JR-7-CB-A</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-8052" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element venta "  data-project-cat="venta " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2016/11/1MG_7501-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="http://www.careco.es/wp-content/uploads/2016/11/1MG_7501.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=picanya-sg59-a01-alq">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya SG59-A01 alq</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-10107" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element venta "  data-project-cat="venta " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2016/11/1MG_7501-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="http://www.careco.es/wp-content/uploads/2016/11/1MG_7501.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=picanya-sg59-a01">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya SG59-A01</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-10062" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element venta "  data-project-cat="venta " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2016/11/1MG_7501-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="http://www.careco.es/wp-content/uploads/2016/11/1MG_7501.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=picanya-sg59-a14">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya SG59-A14</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-8032" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element alquiler picanya-alquiler "  data-project-cat="alquiler picanya-alquiler " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img src="http://www.careco.es/wp-content/themes/salient/img/no-portfolio-item-small.jpg" alt="no image added yet." />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=picanya-ms-22-f6">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya MS 22 F6</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9643" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element picanya venta "  data-project-cat="picanya venta " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img src="http://www.careco.es/wp-content/themes/salient/img/no-portfolio-item-small.jpg" alt="no image added yet." />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=picanya-sg59-a22">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya SG59-A22</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-8016" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element alquiler picanya-alquiler "  data-project-cat="alquiler picanya-alquiler " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2016/11/7MG_7349-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2016/11/7MG_7349-600x403.jpg 600w, http://www.careco.es/wp-content/uploads/2016/11/7MG_7349-282x188.jpg 282w" sizes="(max-width: 600px) 100vw, 600px" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="http://www.careco.es/wp-content/uploads/2016/11/7MG_7349.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=picanya-ms22-a2">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya MS26-A2</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-7967" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element alquiler picanya-alquiler "  data-project-cat="alquiler picanya-alquiler " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img src="http://www.careco.es/wp-content/themes/salient/img/no-portfolio-item-small.jpg" alt="no image added yet." />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=picanya-ms-22-r6">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya MS 22 R6</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9911" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element alquiler picanya-alquiler "  data-project-cat="alquiler picanya-alquiler " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img src="http://www.careco.es/wp-content/themes/salient/img/no-portfolio-item-small.jpg" alt="no image added yet." />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=picanya-ms-22-h6">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya MS 22 H6</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9642" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element alquiler picanya-alquiler "  data-project-cat="alquiler picanya-alquiler " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img src="http://www.careco.es/wp-content/themes/salient/img/no-portfolio-item-small.jpg" alt="no image added yet." />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=picanya-ms-24-d4">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Picanya MS 24 D4</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-7944" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element alquiler francia-alquiler "  data-project-cat="alquiler francia-alquiler " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2016/11/37MG_5798-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2016/11/37MG_5798-600x403.jpg 600w, http://www.careco.es/wp-content/uploads/2016/11/37MG_5798-282x188.jpg 282w" sizes="(max-width: 600px) 100vw, 600px" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="http://www.careco.es/wp-content/uploads/2016/11/37MG_5798.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=avda-francia-p36-2">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Avda. Francia P36</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9189" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element alquiler francia-alquiler "  data-project-cat="alquiler francia-alquiler " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2016/11/37MG_5798-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2016/11/37MG_5798-600x403.jpg 600w, http://www.careco.es/wp-content/uploads/2016/11/37MG_5798-282x188.jpg 282w" sizes="(max-width: 600px) 100vw, 600px" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="http://www.careco.es/wp-content/uploads/2016/11/37MG_5798.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=avda-francia-comercial">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Avda. Francia Bajo Comercial</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-9178" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element francia venta "  data-project-cat="francia venta " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2016/11/37MG_5798-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2016/11/37MG_5798-600x403.jpg 600w, http://www.careco.es/wp-content/uploads/2016/11/37MG_5798-282x188.jpg 282w" sizes="(max-width: 600px) 100vw, 600px" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="http://www.careco.es/wp-content/uploads/2016/11/37MG_5798.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=avda-francia-bajo-comercial">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Avda. Francia Bajo Comercial</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-8869" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element francia venta "  data-project-cat="francia venta " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2016/11/37MG_5798-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2016/11/37MG_5798-600x403.jpg 600w, http://www.careco.es/wp-content/uploads/2016/11/37MG_5798-282x188.jpg 282w" sizes="(max-width: 600px) 100vw, 600px" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="http://www.careco.es/wp-content/uploads/2016/11/37MG_5798.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=avda-francia-p36">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Avda. Francia P36</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-8853" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element alquiler francia "  data-project-cat="alquiler francia " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2016/11/37MG_5809-1-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="http://www.careco.es/wp-content/uploads/2016/11/37MG_5809-1.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=avda-francia-37-copy">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Avda. Francia P37</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-7933" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element alquiler francia-alquiler "  data-project-cat="alquiler francia-alquiler " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2016/11/37MG_5798-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" srcset="http://www.careco.es/wp-content/uploads/2016/11/37MG_5798-600x403.jpg 600w, http://www.careco.es/wp-content/uploads/2016/11/37MG_5798-282x188.jpg 282w" sizes="(max-width: 600px) 100vw, 600px" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="http://www.careco.es/wp-content/uploads/2016/11/37MG_5798.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=avda-francia-copy-2">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Avda. Francia P34</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-7888" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element alquiler francia-alquiler "  data-project-cat="alquiler francia-alquiler " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2016/08/027b-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="http://www.careco.es/wp-content/uploads/2016/08/027b.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=avda-francia-copy">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Avda. Francia cvb</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-7852" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element francia venta "  data-project-cat="francia venta " data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2016/11/P1090308-1-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="http://www.careco.es/wp-content/uploads/2016/11/P1090308-1.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=avda-francia">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Avda. Francia P28</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-7844" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element "  data-project-cat="" data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2016/08/027b-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="http://www.careco.es/wp-content/uploads/2016/08/027b.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=uni-picanya">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Unifamiliar en Picanya</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-6001" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element "  data-project-cat="" data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2016/08/Exterior-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="http://www.careco.es/wp-content/uploads/2016/08/Exterior.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=piso-en-penya-roja">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Promoción en Penya-Roja</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-6148" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->



					<div class="col span_4  element "  data-project-cat="" data-default-color="true" data-title-color="" data-subtitle-color="">

						<div class="inner-wrap animated" data-animation="none">


							<div class="work-item style-1" data-custom-content="off">

								<img width="600" height="403" src="http://www.careco.es/wp-content/uploads/2016/08/web_P1080672-600x403.jpg" class="attachment-portfolio-thumb size-portfolio-thumb wp-post-image" alt="" title="" />
								<div class="work-info-bg"></div>
								<div class="work-info">


										<div class="vert-center">
											<a href="http://www.careco.es/wp-content/uploads/2016/08/web_P1080672.jpg" class="pretty_photo default-link">View Larger</a> <a class="default-link" href="http://www.careco.es/?portfolio=promocion-en-picanya-2">More Details</a>
										</div><!--/vert-center-->
									</div>
								</div><!--work-item-->

								<div class="work-meta">
									<h4 class="title">Promoción en Picanya II</h4>


								</div>
								<div class="nectar-love-wrap">
									<a href="#" class="nectar-love" id="nectar-love-6189" title="Love this"> <div class="heart-wrap"><i class="icon-salient-heart-2"></i></div><span class="nectar-love-count">0</span></a>								</div><!--/nectar-love-wrap-->



					</div><!--/inner-wrap-->
					</div><!--/col-->


			</div><!--/portfolio-->
	   </div><!--/portfolio wrap-->


			</div>
		</div>
	</div>
</div></div>

		</div><!--/row-->

	</div><!--/container-->

</div><!--/container-wrap-->


<?php include('resources_2/includes/footer.php') ?>


</div><!--blurred-wrap-->
	<div id="slide-out-widget-area-bg" class="fullscreen dark"></div>
	<div id="slide-out-widget-area" class="fullscreen" data-back-txt="Back">

		<div class="inner-wrap">
		<div class="inner">

		  <a class="slide_out_area_close" href="#"><span class="icon-salient-x icon-default-style"></span></a>


		   		 	 <div class="off-canvas-menu-container">
		  		<ul class="menu">
					    <li id="menu-item-7082" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-7082"><a href="index.php">HOME</a></li>
<li id="menu-item-7560" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-7560"><a href="current-promotion-sales.php">PROMOCIONES EN CURSO</a>
<ul class="sub-menu">
	<li id="menu-item-8176" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-8176"><a href="#">VENTA</a>
	<ul class="sub-menu">
		<li id="menu-item-8181" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8181"><a href="current-promotion-sales.php">AVD. FRANCIA</a></li>
		<li id="menu-item-8180" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8180"><a href="current-promotion-sales.php">PICANYA</a></li>
	</ul>
</li>
	<li id="menu-item-8177" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-8177"><a href="#">ALQUILER</a>
	<ul class="sub-menu">
		<li id="menu-item-8185" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8185"><a href="current-promotion-rental.php">AVD. FRANCIA</a></li>
		<li id="menu-item-8184" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8184"><a href="current-promotion-rental.php">PICANYA</a></li>
	</ul>
</li>
</ul>
</li>
<li id="menu-item-7797" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7797"><a href="next-promotions.php">PRÓXIMAS PROMOCIONES</a></li>
<li id="menu-item-7799" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-7799"><a href="about-us.php">CARECO, CASAS COMO TÚ</a>
<ul class="sub-menu">
	<li id="menu-item-8246" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8246"><a href="about-us.php">ASI SOMOS</a></li>
	<li id="menu-item-7800" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7800"><a href="our-milestones.php">NUESTROS HITOS</a></li>
	<li id="menu-item-9375" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-9256 current_page_item menu-item-9375"><a href="our-works.php">NUESTRAS OBRAS</a></li>
</ul>
</li>
<li id="menu-item-8216" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8216"><a href="contact.php">CONTACTO</a></li>

				</ul>
		    </div>


		</div>

		<div class="bottom-meta-wrap"><p class="bottom-text" data-has-desktop-social="true">Derechos reservados CARECO. 2016</p></div><!--/bottom-meta-wrap--></div> <!--/inner-wrap-->
	</div>


</div> <!--/ajax-content-wrap-->


	<a id="to-top" class=""><i class="fas fa-angle-up"></i></a>
      <script type="text/javascript">
         function showhide_toggle(a,b,d,f){var e=jQuery("#"+a+"-link-"+b),c=jQuery("a",e),g=jQuery("#"+a+"-content-"+b);a=jQuery("#"+a+"-toggle-"+b);e.toggleClass("sh-show sh-hide");g.toggleClass("sh-show sh-hide").toggle();"true"===c.attr("aria-expanded")?c.attr("aria-expanded","false"):c.attr("aria-expanded","true");a.text()===d?a.text(f):a.text(d)};
      </script>
   	<link rel='stylesheet' id='sbvcbgslider-style-css'  href='resources_2/css/style.4.7.css' type='text/css' media='all' />
      <script type='text/javascript' src='resources_2/js/core.min.1.11.4.js'></script>
      <script type='text/javascript' src='resources_2/js/widget.min.1.11.4.js'></script>
      <script type='text/javascript' src='resources_2/js/position.min.1.11.4.js'></script>
      <script type='text/javascript' src='resources_2/js/menu.min.1.11.4.js'></script>
      <script type='text/javascript' src='resources_2/js/wp-a11y.min.4.7.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
            var uiAutocompleteL10n = {
               "noResults":"Sin resultados.",
               "oneResult":"1 resultado encontrado. Utiliza las teclas de flecha arriba y abajo para navegar.",
               "manyResults":"%d resultados encontrados. Utiliza las teclas arriba y abajo para navegar.",
               "itemSelected":"Elemento seleccionado."
            };
         /* ]]> */
      </script>
      <script type='text/javascript' src='resources_2/js/autocomplete.min.1.11.4.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
            var MyAcSearch = {"url":"http:\/\/www.careco.es\/wp-admin\/admin-ajax.php"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='resources_2/js/wpss-search-suggest.js'></script>
      <script type='text/javascript' src='resources_2/js/jquery.form.min.3.51.0-2014.06.20.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
            var _wpcf7 = {"recaptcha":{"messages":{"empty":"Por favor, prueba que no eres un robot."}}};
         /* ]]> */
      </script>
      <script type='text/javascript' src='resources_2/js/scripts.4.6.1.js'></script>
      <script type='text/javascript' src='resources_2/js/nicescroll.3.5.4.js'></script>
      <script type='text/javascript' src='resources_2/js/prettyPhoto.7.0.1.js'></script>
      <script type='text/javascript' src='resources_2/js/isotope.min.7.6.js'></script>
      <script type='text/javascript' src='resources_2/js/midnight.1.0.js'></script>
      <script type='text/javascript' src='resources_2/js/superfish.1.4.8.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
            var nectarLove = {
               "ajaxurl":"http:\/\/www.careco.es\/wp-admin\/admin-ajax.php",
               "postID":"9256",
               "rooturl":"http:\/\/www.careco.es",
               "pluginPages":[],
               "disqusComments":"false",
               "loveNonce":"d9b98b20d9",
               "mapApiKey":"AIzaSyA44aW6ukgUoBzZd5Dn5bsJloe5v0u-5rw"
            };
         /* ]]> */
      </script>
      <script type='text/javascript' src='resources_2/js/init.7.6.js'></script>
      <script type='text/javascript' src='resources_2/js/infinitescroll.1.1.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
            var mejsL10n = {
               "language":"es-ES",
               "strings":{
                  "Close":"Cerrar",
                  "Fullscreen":"Pantalla completa",
                  "Turn off Fullscreen":"Salir de pantalla completa",
                  "Go Fullscreen":"Ver en pantalla completa",
                  "Download File":"Descargar archivo",
                  "Download Video":"Descargar v\u00eddeo",
                  "Play":"Reproducir",
                  "Pause":"Pausa",
                  "Captions\/Subtitles":"Pies de foto \/ Subt\u00edtulos",
                  "None":"None",
                  "Time Slider":"Control de tiempo",
                  "Skip back %1 seconds":"Retroceder %1 segundos",
                  "Video Player":"Reproductor de v\u00eddeo",
                  "Audio Player":"Reproductor de audio",
                  "Volume Slider":"Control de volumen",
                  "Mute Toggle":"Desactivar sonido",
                  "Unmute":"Activar sonido",
                  "Mute":"Silenciar",
                  "Use Up\/Down Arrow keys to increase or decrease volume.":"Utiliza las teclas de flecha arriba\/abajo para aumentar o disminuir el volumen.",
                  "Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.":"Usa las teclas de direcci\u00f3n izquierda\/derecha para avanzar un segundo, y las flechas arriba\/abajo para avanzar diez segundos."
               }
            };
            var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='resources_2/js/mediaelement-and-player.min.2.22.0.js'></script>
      <script type='text/javascript' src='resources_2/js/wp-mediaelement.min.4.7.js'></script>
      <script type='text/javascript' src='resources_2/js/flickity.min.1.1.1.js'></script>
      <script type='text/javascript' src='resources_2/js/touchswipe.min.1.0.js'></script>
      <script type='text/javascript' src='resources_2/js/select2.min.3.5.2.js'></script>
      <script type='text/javascript' src='resources_2/js/wp-embed.min.4.7.js'></script>
      <script type='text/javascript' src='resources_2/js/js_composer_front.min.4.12.1.js'></script>
      <script type='text/javascript' src='resources_2/js/jquery.backstretch.min.1.1.js'></script>
      <script type='text/javascript' src='resources_2/js/jquery.flexverticalcenter.1.1.js'></script>
      <script type='text/javascript' src='resources_2/js/script.1.1.js'></script>
      <script type="text/javascript" src="plugins/bootstrap/js/bootstrap.js"></script>
      <div class="essb_mailform">
         <div class="essb_mailform_content">
            <p>Send this to friend</p>
            <label class="essb_mailform_content_label">Your email</label>
            <input type="text" id="essb_mailform_from" class="essb_mailform_content_input" placeholder="Your email"/>
            <label class="essb_mailform_content_label">Recipient email</label>
            <input type="text" id="essb_mailform_to" class="essb_mailform_content_input" placeholder="Recipient email"/>
            <div class="essb_mailform_content_buttons">
               <button id="essb_mailform_btn_submit" class="essb_mailform_content_button" onclick="essb_mailform_send();">Send</button>
               <button id="essb_mailform_btn_cancel" class="essb_mailform_content_button" onclick="essb_close_mailform(); return false;">Cancel</button>
            </div>
            <input type="hidden" id="essb_mail_salt" value="1362929168"/>
            <input type="hidden" id="essb_mail_instance" value=""/>
            <input type="hidden" id="essb_mail_post" value=""/>
         </div>
      </div>
      <div class="essb_mailform_shadow"></div>
      <link rel="stylesheet" id="essb-cct-style"  href="resources_2/css/styles.css" type="text/css" media="all" />
      <script type="text/javascript">
         var essb_window = function(oUrl, oService, oInstance) { var element = jQuery('.essb_'+oInstance); var instance_post_id = jQuery(element).attr("data-essb-postid") || ""; var instance_position = jQuery(element).attr("data-essb-position") || ""; var wnd; var w = 800 ; var h = 500; if (oService == "twitter") { w = 500; h= 300; } var left = (screen.width/2)-(w/2); var top = (screen.height/2)-(h/2); if (oService == "twitter") { wnd = window.open( oUrl, "essb_share_window", "height=300,width=500,resizable=1,scrollbars=yes,top="+top+",left="+left ); } else { wnd = window.open( oUrl, "essb_share_window", "height=500,width=800,resizable=1,scrollbars=yes,top="+top+",left="+left ); } if (typeof(essb_settings) != "undefined") { if (essb_settings.essb3_stats) { if (typeof(essb_handle_stats) != "undefined") { essb_handle_stats(oService, instance_post_id, oInstance); } } if (essb_settings.essb3_ga) { essb_ga_tracking(oService, oUrl, instance_position); } } essb_self_postcount(oService, instance_post_id); if (typeof(essb_abtesting_logger) != "undefined") { essb_abtesting_logger(oService, instance_post_id, oInstance); } var pollTimer = window.setInterval(function() { if (wnd.closed !== false) { window.clearInterval(pollTimer); essb_smart_onclose_events(oService, instance_post_id); } }, 200); }; var essb_self_postcount = function(oService, oCountID) { if (typeof(essb_settings) != "undefined") { oCountID = String(oCountID); jQuery.post(essb_settings.ajax_url, { 'action': 'essb_self_postcount', 'post_id': oCountID, 'service': oService, 'nonce': essb_settings.essb3_nonce }, function (data) { if (data) { }},'json'); } }; var essb_smart_onclose_events = function(oService, oPostID) { if (oService == "subscribe" || oService == "comments") return; if (typeof (essbasc_popup_show) == 'function') { essbasc_popup_show(); } if (typeof essb_acs_code == 'function') { essb_acs_code(oService, oPostID); } if (typeof(after_share_easyoptin) != "undefined") { essb_toggle_subscribe(after_share_easyoptin); } }; var essb_tracking_only = function(oUrl, oService, oInstance, oAfterShare) { var element = jQuery('.essb_'+oInstance); if (oUrl == "") { oUrl = document.URL; } var instance_post_id = jQuery(element).attr("data-essb-postid") || ""; var instance_position = jQuery(element).attr("data-essb-position") || ""; if (typeof(essb_settings) != "undefined") { if (essb_settings.essb3_stats) { if (typeof(essb_handle_stats) != "undefined") { essb_handle_stats(oService, instance_post_id, oInstance); } } if (essb_settings.essb3_ga) { essb_ga_tracking(oService, oUrl, instance_position); } } essb_self_postcount(oService, instance_post_id); if (oAfterShare) { essb_smart_onclose_events(oService, instance_post_id); } }; var essb_pinterest_picker = function(oInstance) { essb_tracking_only('', 'pinterest', oInstance); var e=document.createElement('script'); e.setAttribute('type','text/javascript'); e.setAttribute('charset','UTF-8'); e.setAttribute('src','//assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e); };var essb_mailform_opened = false; function essb_open_mailform(unique_id) { jQuery.fn.extend({ center: function () { return this.each(function() { var top = (jQuery(window).height() - jQuery(this).outerHeight()) / 2; var left = (jQuery(window).width() - jQuery(this).outerWidth()) / 2; jQuery(this).css({position:'fixed', margin:0, top: (top > 0 ? top : 0)+'px', left: (left > 0 ? left : 0)+'px'}); }); } }); if (essb_mailform_opened) { essb_close_mailform(unique_id); return; } var sender_element = jQuery(".essb_"+unique_id); if (!sender_element.length) return; var sender_post_id = jQuery(sender_element).attr("data-essb-postid") || ""; jQuery("#essb_mail_instance").val(unique_id); jQuery("#essb_mail_post").val(sender_post_id); var win_width = jQuery( window ).width(); var win_height = jQuery(window).height(); var doc_height = jQuery('document').height(); var base_width = 300; if (win_width < base_width) { base_width = win_width - 30; } var height_correction = 20; var element_class = ".essb_mailform"; var element_class_shadow = ".essb_mailform_shadow"; jQuery(element_class).css( { width: base_width+'px'}); var popup_height = jQuery(element_class).outerHeight(); if (popup_height > (win_height - 30)) { jQuery(element_class).css( { height: (win_height - height_correction)+'px'}); } jQuery("#essb_mailform_from").val(""); jQuery("#essb_mailform_to").val(""); if (jQuery("#essb_mailform_c").length) jQuery("#essb_mailform_c").val(""); jQuery(element_class_shadow).css( { height: (win_height)+'px'}); jQuery(element_class).center(); jQuery(element_class).slideDown(200); jQuery(element_class_shadow).fadeIn(200); essb_mailform_opened = true; essb_tracking_only("", "mail", unique_id); }; function essb_close_mailform() { var element_class = ".essb_mailform"; var element_class_shadow = ".essb_mailform_shadow"; jQuery(element_class).fadeOut(200); jQuery(element_class_shadow).fadeOut(200); essb_mailform_opened = false; }; function essb_mailform_send() { var sender_email = jQuery("#essb_mailform_from").val(); var recepient_email = jQuery("#essb_mailform_to").val(); var captcha_validate = jQuery("#essb_mailform_c").length ? true : false; var captcha = captcha_validate ? jQuery("#essb_mailform_c").val() : ""; var custom_message = jQuery("#essb_mailform_custom").length ? jQuery("#essb_mailform_custom").val() : ""; if (sender_email == "" || recepient_email == "" || (captcha == "" && captcha_validate)) { alert("Please fill all fields in form!"); return; } var mail_salt = jQuery("#essb_mail_salt").val(); var instance_post_id = jQuery("#essb_mail_post").val(); console.log("mail salt = " + mail_salt); if (typeof(essb_settings) != "undefined") { jQuery.post(essb_settings.ajax_url, { "action": "essb_mail_action", "post_id": instance_post_id, "from": sender_email, "to": recepient_email, "c": captcha, "cu": custom_message, "salt": mail_salt, "nonce": essb_settings.essb3_nonce }, function (data) { if (data) { console.log(data); alert(data["message"]); if (data["code"] == "1") essb_close_mailform(); }},'json'); } };
      </script>
   </body>
</html>
