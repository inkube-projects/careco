<!doctype html>
   <html lang="es-ES" prefix="og: http://ogp.me/ns# og: http://ogp.me/ns#" >
      <head>
         <!-- Meta Tags -->
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
         <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
         <!--Shortcut icon-->
         <title>Promociones en curso AVD. FRANCIA venta - Careco</title>

         <meta property="og:title" content="Promociones en curso AVD. FRANCIA venta" />
         <meta property="og:description" content="&hellip;" />
         <meta property="og:url" content="" /> <!-- pagina actual-->
         <meta property="og:type" content="article" />
         <meta property="og:site_name" content="Careco" />
         <meta property="article:published_time" content="2016-11-10T16:47:07+00:00" />
         <meta property="article:modified_time" content="2017-12-15T07:57:59+00:00" />
         <meta property="og:updated_time" content="2017-12-15T07:57:59+00:00" />

         <!-- This site is optimized with the Yoast SEO plugin v4.4 - https://yoast.com/wordpress/plugins/seo/ -->
         <link rel="canonical" href="" /> <!-- pagina actual-->
         <meta property="og:locale" content="es_ES" />
         <meta property="og:type" content="article" />
         <meta property="og:title" content="Promociones en curso AVD. FRANCIA venta - Careco" />
         <meta property="og:url" content="" /> <!-- pagina actual-->
         <meta property="og:site_name" content="Careco" />
         <meta property="og:image" content="images/iconos-localidad-1.png" />
         <meta name="twitter:card" content="summary" />
         <meta name="twitter:title" content="Promociones en curso AVD. FRANCIA venta - Careco" />
         <meta name="twitter:image" content="images/iconos-localidad-1.png" />
         <!-- / Yoast SEO plugin. -->

         <link rel='dns-prefetch' href='//fonts.googleapis.com' />
         <link rel='dns-prefetch' href='//s.w.org' />
         <link rel="alternate" type="application/rss+xml" title="Careco &raquo; Feed" href="http://www.careco.es/?feed=rss2" />
            <script type="text/javascript">
            window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/www.careco.es\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.7"}};
            !function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
         </script>
         <style type="text/css">
            img.wp-smiley,
            img.emoji {
               display: inline !important;
               border: none !important;
               box-shadow: none !important;
               height: 1em !important;
               width: 1em !important;
               margin: 0 .07em !important;
               vertical-align: -0.1em !important;
               background: none !important;
               padding: 0 !important;
            }
         </style>
         <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
         <link rel='stylesheet' id='contact-form-7-css'  href='resources_2/css/styles.4.6.1.css' type='text/css' media='all' />
         <link rel='stylesheet' id='mediaelement-css'  href='resources_2/css/mediaelementplayer.min.2.22.0.css' type='text/css' media='all' />
         <link rel='stylesheet' id='wp-mediaelement-css'  href='resources_2/css/wp-mediaelement.min.4.7.css' type='text/css' media='all' />
         <link rel='stylesheet' id='rgs-css'  href='resources_2/css/rgs.6.0.1.css' type='text/css' media='all' />
         <link rel='stylesheet' id='font-awesome-css'  href='plugins/font-awesome/web-fonts-with-css/css/fontawesome-all.min.css' type='text/css' media='all' />
         <link rel='stylesheet' id='main-styles-css'  href='resources_2/css/style.7.6.css' type='text/css' media='all' />
         <link rel='stylesheet' id='pretty_photo-css'  href='resources_2/css/prettyPhoto.7.0.1.css' type='text/css' media='all' />
         <!--[if lt IE 9]>
            <link rel='stylesheet' id='nectar-ie8-css'  href='css\ie8.4.7.css' type='text/css' media='all' />
         <![endif]-->
         <link rel='stylesheet' id='responsive-css'  href='resources_2/css/responsive.7.6.css' type='text/css' media='all' />
         <link rel='stylesheet' id='nectarslider-css'  href='resources_2/css/nectar-slider4.7.css' type='text/css' media='all' />
         <link rel='stylesheet' id='select2-css'  href='resources_2/css/select2-6.2.css' type='text/css' media='all' />
         <link rel='stylesheet' id='skin-ascend-css'  href='resources_2/css/ascend7.6.css' type='text/css' media='all' />
         <link rel='stylesheet' id='easy-social-share-buttons-css'  href='resources_2/css/easy-social-share-buttons.min.4.1.css' type='text/css' media='all' />
         <link rel='stylesheet' id='js_composer_front-css'  href='resources_2/css/js_composer.min.4.12.1.css' type='text/css' media='all' />
         <link rel='stylesheet' id='redux-google-fonts-salient_redux-css'  href='http://fonts.googleapis.com/css?family=Montserrat%3A400%2C700%7CLato%3A900%2C400&#038;subset=latin&#038;ver=1509443950' type='text/css' media='all' />
         <script type='text/javascript' src='resources_2/js/jquery.1.12.4.js'></script>
         <script type='text/javascript' src='resources_2/js/jquery-migrate.min.1.4.1.js'></script>
         <script type='text/javascript' src='resources_2/js/modernizr.2.6.2.js'></script>
         <link rel='https://api.w.org/' href='http://www.careco.es/?rest_route=/' />
         <link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.careco.es/xmlrpc.php?rsd" />
         <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.careco.es/wp-includes/wlwmanifest.xml" />
         <meta name="generator" content="WordPress 4.7" />
         <link rel='shortlink' href='' /> <!-- Pagina actual -->
         <link rel="alternate" type="application/json+oembed" href="http://www.careco.es/?rest_route=%2Foembed%2F1.0%2Fembed&#038;url=http%3A%2F%2Fwww.careco.es%2F%3Fpage_id%3D7471" />
         <link rel="alternate" type="text/xml+oembed" href="http://www.careco.es/?rest_route=%2Foembed%2F1.0%2Fembed&#038;url=http%3A%2F%2Fwww.careco.es%2F%3Fpage_id%3D7471&#038;format=xml" />
         <link rel="stylesheet" href="resources_2/css/main-promotion-sale.css">
         <link rel="stylesheet" href="resources_2/css/css-style.css">
         <script type="text/javascript">
            var essb_settings = {
               "ajax_url":"http:\/\/www.careco.es\/wp-admin\/admin-ajax.php",
               "essb3_nonce":"dd1aeca4cb",
               "essb3_plugin_url":"http:\/\/www.careco.es\/wp-content\/plugins\/easy-social-share-buttons3",
               "essb3_facebook_total":true,
               "essb3_admin_ajax":false,
               "essb3_internal_counter":false,
               "essb3_stats":false,
               "essb3_ga":false,
               "essb3_ga_mode":"simple",
               "essb3_counter_button_min":0,
               "essb3_counter_total_min":0,
               "blog_url":"http:\/\/www.careco.es\/",
               "ajax_type":"wp",
               "essb3_postfloat_stay":false,
               "essb3_no_counter_mailprint":false,
               "essb3_single_ajax":false,
               "twitter_counter":"self",
               "post_id":7471
            };
         </script>
         <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
         <!--[if lte IE 9]>
            <link rel="stylesheet" type="text/css" href="resources_2/css/vc_lte_ie9.min.css" media="screen">
         <![endif]-->
         <!--[if IE  8]>
            <link rel="stylesheet" type="text/css" href="resources_2/css/vc-ie8.min.css" media="screen">
         <![endif]-->
         <style type="text/css" data-type="vc_custom-css">
            .rectangulo {
                min-height:400px!important;
                max-height:400px!important;
                padding-right:12.6%!important;
                padding-left:12%!important;

            }
            .pad {
                padding-left:20%!important;

            }
            .padr {
                padding-right:10%!important;
            }

            .block {
                display:block!important;
            }
            .promo .wpb_wrapper > div {margin-bottom: 0px;}
            div#links-link-7471, div#links2-link-7471 {
                margin-bottom: 10px;
            }
            .vc_col-sm-4.textobajo.wpb_column.column_container.col.centered-text.padding-4-percent.instance-0 {
               padding: 0%;
            }
         </style>
         <style type="text/css" data-type="vc_shortcodes-custom-css">
            .vc_custom_1481628275977{
               padding-top: 30px !important;
               padding-bottom: 15px !important;
               background-color: #e2e2e2 !important;
            }
            .vc_custom_1481742481819{
               padding-bottom: 20px !important;
            }
            .vc_custom_1482324452088{
               margin-top: 45px !important;
               padding-bottom: 23px !important;
            }
            .vc_custom_1481627956764{
               padding-top: 20px !important;
               padding-bottom: 20px !important;
            }
         </style>
         <noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
      </head>

      <body class="page-template-default page page-id-7471 ascend nectar-auto-lightbox wpb-js-composer js-comp-ver-4.12.1 vc_responsive"
         data-footer-reveal="false"
         data-header-format="default"
         data-footer-reveal-shadow="none"
         data-dropdown-style="classic"
         data-cae="linear"
         data-cad="500"
         data-aie="none"
         data-ls="pretty_photo"
         data-apte="standard"
         data-hhun="0"
         data-fancy-form-rcs="1"
         data-form-style="minimal"
         data-form-submit="default"
         data-is="minimal"
         data-button-style="default"
         data-header-inherit-rc="false"
         data-header-search="false"
         data-animated-anchors="true"
         data-ajax-transitions="true"
         data-full-width-header="true"
         data-slide-out-widget-area="true"
         data-slide-out-widget-area-style="fullscreen"
         data-user-set-ocm="1"
         data-loading-animation="none"
         data-bg-header="false"
         data-ext-responsive="false"
         data-header-resize="0"
         data-header-color="custom"
         data-transparent-header="false"
         data-smooth-scrolling="1"
         data-permanent-transparent="false"
         data-responsive="1">



	<div id="header-secondary-outer" data-full-width="true" data-permanent-transparent="false" >
		<div class="container">
			<nav>


			</nav>
		</div>
	</div>

   <? include('resources_2/includes/header.php'); ?>



<div id="search-outer" class="nectar">

	<div id="search">

		<div class="container">

		     <div id="search-box">

		     	<div class="col span_12">
			      	<form action="http://www.careco.es" method="GET">
			      		<input type="text" name="s" id="s" value="Start Typing..." data-placeholder="Start Typing..." />
			      	</form>
			      			        </div><!--/span_12-->

		     </div><!--/search-box-->

		     <div id="close"><a href="#"><span class="icon-salient-x" aria-hidden="true"></span></a></div>

		 </div><!--/container-->

	</div><!--/search-->

</div><!--/search-outer-->

<div id="mobile-menu" data-mobile-fixed="1">

	<div class="container">
		<ul>
			<li><a href="">No menu assigned!</a></li>
		</ul>
	</div>

</div>


<div id="ajax-loading-screen" data-disable-fade-on-click="0" data-effect="standard" data-method="standard">

			<div class="loading-icon none">
			<span class="default-loading-icon spin"></span>
		</div>
	</div>

<div id="ajax-content-wrap">

<div class="blurred-wrap">

<div class="container-wrap">

	<div class="container main-content">

		<div class="row">

			<p id="breadcrumbs"><span xmlns:v="http://rdf.data-vocabulary.org/#"><span typeof="v:Breadcrumb"><a href="index.php" rel="v:url" property="v:title">Inicio</a> » <span class="breadcrumb_last">Promociones en curso AVD. FRANCIA venta</span></span></span></p>
		<div id="fws_5a6fd1b8c4cb1"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-section  vc_row-o-equal-height vc_row-flex standard_section  promo " data-using-ctc="true" style="padding-top: 0px; padding-bottom: 0px; color: #ffffff; "><div class="row-bg-wrap"> <div class="row-bg    "  style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div> </div><div class="col span_12 custom left">
	<div style="" class="vc_col-sm-4 textobajo wpb_column column_container vc_column_container col centered-text padding-4-percent" data-using-bg="true" data-bg-cover="" data-padding-pos="right" data-has-bg-color="true" data-bg-color="#eeeeee" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

	<div class="wpb_text_column wpb_content_element  vc_custom_1481628275977 pad" >
		<div class="wpb_wrapper">
			<h2 style="text-align: left;"><a href="next-promotions.php"><span style="color: #6b6c6b;">PROMOCIONES</span></a></h2>
<h2 style="text-align: left;"><a href="next-promotions.php"><span style="color: #6b6c6b;">EN CURSO</span></a></h2>

		</div>
	</div>

	<div class="wpb_raw_code wpb_content_element wpb_raw_html">
		<div class="wpb_wrapper">
			<div class="arrow"></div>
		</div>
	</div>
<div class="divider-wrap"><div style="height: 45px;" class="divider"></div></div>
	<div class="wpb_text_column wpb_content_element  vc_custom_1481742481819 pad activo" >
		<div class="wpb_wrapper">
			<p class="activo">AVD. FRANCIA (Valencia)</p>
<p><strong><div id="links2-link-7471" class="sh-link links2-link sh-hide"><a href="#" onclick="showhide_toggle('links2', 7471, 'PICANYA', 'PICANYA'); return false;" aria-expanded="false"><span id="links2-toggle-7471">PICANYA</span></a></div><div id="links2-content-7471" class="sh-content links2-content sh-hide" style="display: none;"></strong></p>
<p style="text-align: left;"><a href="current-promotion-sales.php">VENTA</a> | <a href="current-promotion-rental.php">ALQUILER</a></p>
<p></div>

		</div>
	</div>

	<div class="wpb_text_column wpb_content_element  vc_custom_1482324452088 pad" >
		<div class="wpb_wrapper">
			<h2 style="text-align: left; font-size: 14px;"><span style="color: #ffffff;"><a style="color: #ffffff;" href="index.php">&lt; Volver</a></span></h2>

		</div>
	</div>

			</div>
		</div>
	</div>

	<div  class="vc_col-sm-8 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

	<div id="fws_5a6fd1b8c5c9e" data-midnight="" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row  vc_row-o-equal-height vc_row-flex  vc_row-o-content-middle standard_section   "  style="padding-top: 0px; padding-bottom: 0px; "><div class="row-bg-wrap"> <div class="row-bg   "  style=""></div> </div><div class="col span_12  left">
	<div  class="vc_col-sm-12 wpb_column column_container vc_column_container col padding-3-percent"  data-bg-cover="" data-padding-pos="right" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
		<div class="wpb_wrapper">

	<div class="wpb_gallery wpb_content_element clearfix">
		<div class="wpb_wrapper"><div class="wpb_gallery_slidesnectarslider_style" data-interval="5"><div class="nectar-slider-wrap" style="height: 305px" data-flexible-height="" data-fullscreen="false"  data-full-width="false" data-parallax="false" data-autorotate="5500" id="ns-id-5a6fd1b8c6743"><div class="swiper-container" style="height: 305px"  data-loop="true" data-height="305" data-bullets="" data-bullet_style="see_through" data-arrows="false" data-bullets="false" data-desktop-swipe="true" data-settings=""><div class="swiper-wrapper"><div class="swiper-slide" data-bg-alignment="center" data-color-scheme="light" data-x-pos="centered" data-y-pos="middle"><div class="image-bg" style="background-image: url(http://www.careco.es/wp-content/uploads/2016/11/MG_7914.jpg);"></div><span class="ie-fix"></span> </div><!--/swiper-slide--></div><div class="nectar-slider-loading"></div></div></div></div>
		</div>
	</div>
		</div>
	</div>
	</div>
</div></div>
			</div>
		</div>
	</div>
</div></div>
		<div id="fws_5a6fd1b8c744e"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row  vc_row-o-equal-height vc_row-flex standard_section   "  style="padding-top: 0px; padding-bottom: 0px; "><div class="row-bg-wrap"> <div class="row-bg    "  style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div> </div><div class="col span_12 dark center">
	<div style="" class="vc_col-sm-4 wpb_column column_container vc_column_container col no-extra-padding" data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-color="#cddddc" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

			</div>
		</div>
	</div>

	<div style="" class="vc_col-sm-4 wpb_column column_container vc_column_container col no-extra-padding" data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-color="#adc0c0" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">
				<div class="divider-wrap"><div style="height: 20px;" class="divider"></div></div>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<h4 style="text-align: center;"><span style="color: #ffffff;">EN VENTA</span></h4>

		</div>
	</div>
<div class="divider-wrap"><div style="height: 20px;" class="divider"></div></div>
			</div>
		</div>
	</div>

	<div style="" class="vc_col-sm-4 wpb_column column_container vc_column_container col no-extra-padding" data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-color="#cddddc" data-bg-opacity="1" data-hover-bg="#000000" data-hover-bg-opacity="0.7" data-animation="" data-delay="0"><a class="column-link" href="current-promotion-rental.php"></a>
		<div class="vc_column-inner">
			<div class="wpb_wrapper">
				<div class="divider-wrap"><div style="height: 20px;" class="divider"></div></div>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<h4 style="text-align: center;"><span style="color: #a1bfbd;">EN ALQUILER</span></h4>

		</div>
	</div>
<div class="divider-wrap"><div style="height: 13px;" class="divider"></div></div>
			</div>
		</div>
	</div>
</div></div>
		<div id="fws_5a6fd1b8c7e7e"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row standard_section   "  style="padding-top: 0px; padding-bottom: 0px; "><div class="row-bg-wrap"> <div class="row-bg    "  style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div> </div><div class="col span_12 dark left">
	<div  class="vc_col-sm-12 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">
				<div class="divider-wrap"><div style="height: 40px;" class="divider"></div></div>
			</div>
		</div>
	</div>
</div></div>
		<div id="fws_5a6fd1b8c8379"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row standard_section   "  style="padding-top: 0px; padding-bottom: 0px; "><div class="row-bg-wrap"> <div class="row-bg    "  style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div> </div><div class="col span_12 dark left">
	<div  class="vc_col-sm-4 wpb_column column_container vc_column_container col padding-4-percent"  data-bg-cover="" data-padding-pos="right" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<h6>450.000 €</h6>
<h4><strong>PISO EN CALLE</strong></h4>
<h4><strong>LUIS GARCIA BERLANGA</strong></h4>
<p>GB9 P28 8P</p>

		</div>
	</div>
<div class="divider-wrap"><div style="height: 8px;" class="divider"></div></div><div class="img-with-aniamtion-wrap " data-max-width="100%"><div class="inner"><img data-shadow="none" data-shadow-direction="middle" class="img-with-animation " data-delay="0" height="127" width="870" data-animation="none" src="http://www.careco.es/wp-content/uploads/2016/11/iconos_p28.png" alt="" /></div></div><div class="divider-wrap"><div style="height: 8px;" class="divider"></div></div><div class="img-with-aniamtion-wrap " data-max-width="100%"><div class="inner"><a href="http://www.careco.es/?portfolio=avda-francia" target="_self" class=""><img data-shadow="none" data-shadow-direction="middle" class="img-with-animation " data-delay="0" height="38" width="166" data-animation="none" src="http://www.careco.es/wp-content/uploads/2016/11/MAS-INFO-01-1.jpg" alt="" /></a></div></div>
			</div>
		</div>
	</div>

	<div  class="vc_col-sm-4 wpb_column column_container vc_column_container col padding-4-percent"  data-bg-cover="" data-padding-pos="right" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<h4>CARACTERÍSTICAS</h4>

		</div>
	</div>
<div class="divider-wrap"><div style="height: 8px;" class="divider"></div></div>
	<div class="wpb_text_column wpb_content_element  padr" >
		<div class="wpb_wrapper">
			<p>Vivienda de 136 m² en 8ª planta, dispone de 3 dormitorios todos con armarios empotrados y 2 baños con suelo radiante calefactable. El dormitorio principal muy amplio admite King Size, con zona de vestidor y 3 amplios armarios, con baño integrado y terraza exterior. Todas las habitaciones a fachada&#8230;[<a href="http://www.careco.es/?portfolio=avda-francia">Más info</a>]</p>

		</div>
	</div>

			</div>
		</div>
	</div>

	<div  class="vc_col-sm-4 block wpb_column column_container vc_column_container col padding-4-percent"  data-bg-cover="" data-padding-pos="left" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

	<div class="wpb_gallery wpb_content_element clearfix">
		<div class="wpb_wrapper"><div class="wpb_gallery_slides wpb_flexslider flex-gallery flexslider" data-interval="5" data-flex_fx="fade"><ul class="slides"><li><img class="" src="http://www.careco.es/wp-content/uploads/2016/11/VENDIDO-600x400.jpg" width="600" height="400" alt="VENDIDO" title="VENDIDO" /></li></ul></div>
		</div>
	</div>
			</div>
		</div>
	</div>
</div></div>
		<div id="fws_5a6fd1b8cab97"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row standard_section   "  style="padding-top: 0px; padding-bottom: 0px; "><div class="row-bg-wrap"> <div class="row-bg    "  style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div> </div><div class="col span_12 dark left">
	<div  class="vc_col-sm-12 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">
				<div class="divider-wrap"><div style="height: 40px;" class="divider"></div></div>
			</div>
		</div>
	</div>
</div></div>
		<div id="fws_5a6fd1b8cb018"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row standard_section   "  style="padding-top: 0px; padding-bottom: 0px; "><div class="row-bg-wrap"> <div class="row-bg    "  style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div> </div><div class="col span_12 dark left">
	<div  class="vc_col-sm-4 wpb_column column_container vc_column_container col padding-4-percent"  data-bg-cover="" data-padding-pos="right" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<h6>450.000 €</h6>
<h4><strong>LOCAL COMERCIAL</strong></h4>
<h4><strong>LUIS GARCIA BERLANGA</strong></h4>
<p>GB9 PB</p>

		</div>
	</div>
<div class="divider-wrap"><div style="height: 8px;" class="divider"></div></div><div class="img-with-aniamtion-wrap " data-max-width="100%"><div class="inner"><img data-shadow="none" data-shadow-direction="middle" class="img-with-animation " data-delay="0" height="30" width="350" data-animation="none" src="http://www.careco.es/wp-content/uploads/2016/11/BERLANGA-3.png" alt="" /></div></div><div class="divider-wrap"><div style="height: 8px;" class="divider"></div></div><div class="img-with-aniamtion-wrap " data-max-width="100%"><div class="inner"><a href=" http://www.careco.es/?portfolio=avda-francia-comercial" target="_self" class=""><img data-shadow="none" data-shadow-direction="middle" class="img-with-animation " data-delay="0" height="38" width="166" data-animation="none" src="http://www.careco.es/wp-content/uploads/2016/11/MAS-INFO-01-1.jpg" alt="" /></a></div></div>
			</div>
		</div>
	</div>

	<div  class="vc_col-sm-4 wpb_column column_container vc_column_container col padding-4-percent"  data-bg-cover="" data-padding-pos="right" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<h4>CARACTERÍSTICAS</h4>

		</div>
	</div>
<div class="divider-wrap"><div style="height: 8px;" class="divider"></div></div>
	<div class="wpb_text_column wpb_content_element  padr" >
		<div class="wpb_wrapper">
			<p>Magnífico local comercial de 180 m2 en finca nueva para acondicionar. Dispone de 3 amplias puertas a la calle y salida de humos. Así como aislamiento acústico con las viviendas (dB(a) 62). 4 meses gratis si el cliente realiza los trabajos de acondicionamiento del local. Alquiler a 7 €/m2. [<a href="http://www.careco.es/?portfolio=avda-francia-bajo-comercial">Más info</a>]</p>

		</div>
	</div>

			</div>
		</div>
	</div>

	<div  class="vc_col-sm-4 block wpb_column column_container vc_column_container col padding-4-percent"  data-bg-cover="" data-padding-pos="left" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

	<div class="wpb_gallery wpb_content_element clearfix">
		<div class="wpb_wrapper"><div class="wpb_gallery_slides wpb_flexslider flex-gallery flexslider" data-interval="5" data-flex_fx="fade"><ul class="slides"><li><img class="" src="http://www.careco.es/wp-content/uploads/2016/11/PB_berlanga_01-600x400.jpg" width="600" height="400" alt="PB_berlanga_01" title="PB_berlanga_01" /></li><li><img class="" src="http://www.careco.es/wp-content/uploads/2016/11/PB_berlanga_02-600x400.jpg" width="600" height="400" alt="PB_berlanga_02" title="PB_berlanga_02" /></li><li><img class="" src="http://www.careco.es/wp-content/uploads/2016/11/PB_berlanga_03-600x400.jpg" width="600" height="400" alt="PB_berlanga_03" title="PB_berlanga_03" /></li><li><img class="" src="http://www.careco.es/wp-content/uploads/2016/11/MG_7911_fachada-berlanga-alameda-600x400.jpg" width="600" height="400" alt="_MG_7911_fachada berlanga alameda" title="_MG_7911_fachada berlanga alameda" /></li><li><img class="" src="http://www.careco.es/wp-content/uploads/2016/11/cMG_5932-600x400.jpg" width="600" height="400" alt="_cMG_5932" title="_cMG_5932" /></li><li><img class="" src="http://www.careco.es/wp-content/uploads/2016/11/MG_7914-600x400.jpg" width="600" height="400" alt="_MG_7914" title="_MG_7914" /></li></ul></div>
		</div>
	</div>
			</div>
		</div>
	</div>
</div></div>
		<div id="fws_5a6fd1b8cec08"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row standard_section   "  style="padding-top: 0px; padding-bottom: 0px; "><div class="row-bg-wrap"> <div class="row-bg    "  style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div> </div><div class="col span_12 dark left">
	<div  class="vc_col-sm-12 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">
				<div class="divider-wrap"><div style="height: 40px;" class="divider"></div></div>
			</div>
		</div>
	</div>
</div></div>
		<div id="fws_5a6fd1b8cf071"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row standard_section  hide "  style="padding-top: 0px; padding-bottom: 0px; "><div class="row-bg-wrap"> <div class="row-bg    "  style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div> </div><div class="col span_12 dark left">
	<div  class="vc_col-sm-4 wpb_column column_container vc_column_container col padding-4-percent"  data-bg-cover="" data-padding-pos="right" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<h6>790.000 €</h6>
<h4><strong>PISO EN CALLE</strong></h4>
<h4><strong>LUIS GARCIA BERLANGA</strong></h4>
<p>GB9 P36 11-12P</p>

		</div>
	</div>
<div class="divider-wrap"><div style="height: 8px;" class="divider"></div></div><div class="img-with-aniamtion-wrap " data-max-width="100%"><div class="inner"><img data-shadow="none" data-shadow-direction="middle" class="img-with-animation " data-delay="0" height="30" width="350" data-animation="none" src="http://www.careco.es/wp-content/uploads/2016/11/Berlanga-2.png" alt="" /></div></div><div class="divider-wrap"><div style="height: 8px;" class="divider"></div></div><div class="img-with-aniamtion-wrap " data-max-width="100%"><div class="inner"><a href="http://www.careco.es/?portfolio=avda-francia-p36" target="_self" class=""><img data-shadow="none" data-shadow-direction="middle" class="img-with-animation " data-delay="0" height="38" width="166" data-animation="none" src="http://www.careco.es/wp-content/uploads/2016/11/MAS-INFO-01-1.jpg" alt="" /></a></div></div>
			</div>
		</div>
	</div>

	<div  class="vc_col-sm-4 wpb_column column_container vc_column_container col padding-4-percent"  data-bg-cover="" data-padding-pos="right" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<h4>CARACTERÍSTICAS</h4>

		</div>
	</div>
<div class="divider-wrap"><div style="height: 8px;" class="divider"></div></div>
	<div class="wpb_text_column wpb_content_element  padr" >
		<div class="wpb_wrapper">
			<p>Atico duplex de 183m² en 11ª y 12ª planta, dispone de 4 dormitorios, 2 baños y 1 aseo, amplia terraza y cocina muy grande, todas las habitaciones disponen de armarios empotrados y balcón. Sistema de aspiración centralizada anti alergia-ácaros. Climatización con control independiente en cada estancia frío y calor. Aislamiento acústico al exterior y al interior del edificio superando la normativa&#8230;[<a href="http://www.careco.es/?portfolio=avda-francia-p36">Más info</a>]</p>

		</div>
	</div>

			</div>
		</div>
	</div>

	<div  class="vc_col-sm-4 block wpb_column column_container vc_column_container col padding-4-percent"  data-bg-cover="" data-padding-pos="left" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">

	<div class="wpb_gallery wpb_content_element clearfix">
		<div class="wpb_wrapper"><div class="wpb_gallery_slides wpb_flexslider flex-gallery flexslider" data-interval="5" data-flex_fx="fade"><ul class="slides"><li><img class="" src="http://www.careco.es/wp-content/uploads/2016/11/MG_7180-600x400.jpg" width="600" height="400" alt="_MG_7180" title="_MG_7180" /></li><li><img class="" src="http://www.careco.es/wp-content/uploads/2016/11/MG_7200-600x400.jpg" width="600" height="400" alt="_MG_7200" title="_MG_7200" /></li><li><img class="" src="http://www.careco.es/wp-content/uploads/2016/11/MG_7233-600x400.jpg" width="600" height="400" alt="_MG_7233" title="_MG_7233" /></li></ul></div>
		</div>
	</div>
			</div>
		</div>
	</div>
</div></div>
		<div id="fws_5a6fd1b8d17ef"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-content  vc_row-o-equal-height vc_row-flex standard_section   "  style="padding-top: 0px; padding-bottom: 0px; "><div class="row-bg-wrap"> <div class="row-bg  using-bg-color  "  style="background-color: #adc0c0; " data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div> </div><div class="col span_12 dark left">
	<div  class="vc_col-sm-8 mapa wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="left" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">
				<div id="map_5a6fd1b8d1c41" style="height: 400px;" class="nectar-google-map" data-dark-color-scheme="" data-ultra-flat="" data-greyscale="" data-extra-color="" data-enable-animation="false" data-enable-zoom="" data-zoom-level="14" data-center-lat="39.459072" data-center-lng="-0.350573" data-marker-img=""></div><div class="map_5a6fd1b8d1c41 map-marker-list"><div class="map-marker" data-lat="" data-lng="" data-mapinfo=""></div></div>
			</div>
		</div>
	</div>

	<div  class="vc_col-sm-4 padr wpb_column column_container vc_column_container col padding-4-percent"  data-bg-cover="" data-padding-pos="left" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">
				<div class="divider-wrap"><div style="height: 70px;" class="divider"></div></div>
	<div class="wpb_text_column wpb_content_element  padr" >
		<div class="wpb_wrapper">
			<h5 style="text-align: left;"><span style="color: #ffffff;">Localidad</span></h5>
<h2 style="text-align: left;"><span style="color: #ffffff;">Valencia</span></h2>
<p><img class="alignleft size-full wp-image-7465" src="images/iconos-localidad-1.png" alt="iconos localidad" width="269" height="32" /></p>

		</div>
	</div>
<div class="divider-wrap"><div style="height: 40px;" class="divider"></div></div>
	<div class="wpb_text_column wpb_content_element  padr" >
		<div class="wpb_wrapper">
			<p><span style="color: #ffffff;">En la ciudad de la Ciencias</span><br />
<span style="color: #ffffff;">Zona consolidada</span><br />
<span style="color: #ffffff;">Bien comunicada</span><br />
<span style="color: #ffffff;">Junto al Corte inglés</span></p>

		</div>
	</div>
<div class="divider-wrap"><div style="height: 15px;" class="divider"></div></div>
			</div>
		</div>
	</div>
</div></div>
		</div><!--/row-->

	</div><!--/container-->

</div><!--/container-wrap-->

<?php include('resources_2/includes/footer.php') ?>


</div><!--blurred-wrap-->
	<div id="slide-out-widget-area-bg" class="fullscreen dark"></div>
	<div id="slide-out-widget-area" class="fullscreen" data-back-txt="Back">

		<div class="inner-wrap">
		<div class="inner">

		  <a class="slide_out_area_close" href="#"><span class="icon-salient-x icon-default-style"></span></a>


		   		 	 <div class="off-canvas-menu-container">
		  		<ul class="menu">
					    <li id="menu-item-7082" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-7082"><a href="index.php">HOME</a></li>
<li id="menu-item-7560" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-7471 current_page_item current-menu-ancestor current_page_ancestor menu-item-has-children menu-item-7560"><a href="current-promotion-sales.php">PROMOCIONES EN CURSO</a>
<ul class="sub-menu">
	<li id="menu-item-8176" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children menu-item-8176"><a href="#">VENTA</a>
	<ul class="sub-menu">
		<li id="menu-item-8181" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-7471 current_page_item menu-item-8181"><a href="current-promotion-sales.php">AVD. FRANCIA</a></li>
		<li id="menu-item-8180" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8180"><a href="current-promotion-sales.php">PICANYA</a></li>
	</ul>
</li>
	<li id="menu-item-8177" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-8177"><a href="#">ALQUILER</a>
	<ul class="sub-menu">
		<li id="menu-item-8185" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8185"><a href="current-promotion-rental.php">AVD. FRANCIA</a></li>
		<li id="menu-item-8184" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8184"><a href="current-promotion-rental.php">PICANYA</a></li>
	</ul>
</li>
</ul>
</li>
<li id="menu-item-7797" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7797"><a href="next-promotions.php">PRÓXIMAS PROMOCIONES</a></li>
<li id="menu-item-7799" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-7799"><a href="about-us.php">CARECO, CASAS COMO TÚ</a>
<ul class="sub-menu">
	<li id="menu-item-8246" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8246"><a href="about-us.php">ASI SOMOS</a></li>
	<li id="menu-item-7800" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7800"><a href="our-milestones.php">NUESTROS HITOS</a></li>
	<li id="menu-item-9375" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9375"><a href="our-works.php">NUESTRAS OBRAS</a></li>
</ul>
</li>
<li id="menu-item-8216" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8216"><a href="contact.php">CONTACTO</a></li>

				</ul>
		    </div>


		</div>

		<div class="bottom-meta-wrap"><p class="bottom-text" data-has-desktop-social="true">Derechos reservados CARECO. 2016</p></div><!--/bottom-meta-wrap--></div> <!--/inner-wrap-->
	</div>


</div> <!--/ajax-content-wrap-->


	<a id="to-top" class=""><i class="fas fa-angle-up"></i></a>
			<script type="text/javascript">function showhide_toggle(a,b,d,f){var e=jQuery("#"+a+"-link-"+b),c=jQuery("a",e),g=jQuery("#"+a+"-content-"+b);a=jQuery("#"+a+"-toggle-"+b);e.toggleClass("sh-show sh-hide");g.toggleClass("sh-show sh-hide").toggle();"true"===c.attr("aria-expanded")?c.attr("aria-expanded","false"):c.attr("aria-expanded","true");a.text()===d?a.text(f):a.text(d)};</script>
	<link rel='stylesheet' id='sbvcbgslider-style-css'  href='resources_2/css/style.4.7.css' type='text/css' media='all' />
<script type='text/javascript' src='resources_2/js/core.min.1.11.4.js'></script>
<script type='text/javascript' src='resources_2/js/widget.min.1.11.4.js'></script>
<script type='text/javascript' src='resources_2/js/position.min.1.11.4.js'></script>
<script type='text/javascript' src='resources_2/js/menu.min.1.11.4.js'></script>
<script type='text/javascript' src='resources_2/js/wp-a11y.min.4.7.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var uiAutocompleteL10n = {"noResults":"Sin resultados.","oneResult":"1 resultado encontrado. Utiliza las teclas de flecha arriba y abajo para navegar.","manyResults":"%d resultados encontrados. Utiliza las teclas arriba y abajo para navegar.","itemSelected":"Elemento seleccionado."};
/* ]]> */
</script>
<script type='text/javascript' src='resources_2/js/autocomplete.min.1.11.4.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var MyAcSearch = {"url":"http:\/\/www.careco.es\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='resources_2/js/wpss-search-suggest.js'></script>
<script type='text/javascript' src='resources_2/js/jquery.form.min.3.51.0-2014.06.20.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpcf7 = {"recaptcha":{"messages":{"empty":"Por favor, prueba que no eres un robot."}}};
/* ]]> */
</script>
<script type='text/javascript' src='resources_2/js/scripts.4.6.1.js'></script>
<script type='text/javascript' src='resources_2/js/nicescroll.3.5.4.js'></script>
<script type='text/javascript' src='resources_2/js/prettyPhoto.7.0.1.js'></script>
<script type='text/javascript' src='resources_2/js/midnight.1.0.js'></script>
<script type='text/javascript' src='resources_2/js/superfish.1.4.8.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var nectarLove = {"ajaxurl":"http:\/\/www.careco.es\/wp-admin\/admin-ajax.php","postID":"7471","rooturl":"http:\/\/www.careco.es","pluginPages":[],"disqusComments":"false","loveNonce":"5725bf779a","mapApiKey":"AIzaSyA44aW6ukgUoBzZd5Dn5bsJloe5v0u-5rw"};
/* ]]> */
</script>
<script type='text/javascript' src='resources_2/js/init.7.6.js'></script>
<script type='text/javascript' src='resources_2/js/infinitescroll.1.1.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var mejsL10n = {"language":"es-ES","strings":{"Close":"Cerrar","Fullscreen":"Pantalla completa","Turn off Fullscreen":"Salir de pantalla completa","Go Fullscreen":"Ver en pantalla completa","Download File":"Descargar archivo","Download Video":"Descargar v\u00eddeo","Play":"Reproducir","Pause":"Pausa","Captions\/Subtitles":"Pies de foto \/ Subt\u00edtulos","None":"None","Time Slider":"Control de tiempo","Skip back %1 seconds":"Retroceder %1 segundos","Video Player":"Reproductor de v\u00eddeo","Audio Player":"Reproductor de audio","Volume Slider":"Control de volumen","Mute Toggle":"Desactivar sonido","Unmute":"Activar sonido","Mute":"Silenciar","Use Up\/Down Arrow keys to increase or decrease volume.":"Utiliza las teclas de flecha arriba\/abajo para aumentar o disminuir el volumen.","Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.":"Usa las teclas de direcci\u00f3n izquierda\/derecha para avanzar un segundo, y las flechas arriba\/abajo para avanzar diez segundos."}};
var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/"};
/* ]]> */
</script>
<script type='text/javascript' src='resources_2/js/mediaelement-and-player.min.2.22.0.js'></script>
<script type='text/javascript' src='resources_2/js/wp-mediaelement.min.4.7.js'></script>
<script type='text/javascript' src='resources_2/js/flickity.min.1.1.1.js'></script>
<script type='text/javascript' src='resources_2/js/nectar-slider.7.6.js'></script>
<script type='text/javascript' src='resources_2/js/select2.min.3.5.2.js'></script>
<script type='text/javascript' src='resources_2/js/wp-embed.min.4.7.js'></script>
<script type='text/javascript' src='resources_2/js/js_composer_front.min.4.12.1.js'></script>
<script type='text/javascript' src='resources_2/js/jquery.backstretch.min.1.1.js'></script>
<script type='text/javascript' src='resources_2/js/jquery.flexverticalcenter.1.1.js'></script>
<script type='text/javascript' src='resources_2/js/script.1.1.js'></script>
<script type='text/javascript' src='resources_2/js/jquery.flexslider-min.4.12.1.js'></script>
<script type='text/javascript' src='resources_2/js/map.7.0.7.js'></script>
<script type="text/javascript" src="plugins/bootstrap/js/bootstrap.js"></script>
<div class="essb_mailform">
   <div class="essb_mailform_content"><p>Send this to friend</p><label class="essb_mailform_content_label">Your email</label><input type="text" id="essb_mailform_from" class="essb_mailform_content_input" placeholder="Your email"/><label class="essb_mailform_content_label">Recipient email</label><input type="text" id="essb_mailform_to" class="essb_mailform_content_input" placeholder="Recipient email"/><div class="essb_mailform_content_buttons"><button id="essb_mailform_btn_submit" class="essb_mailform_content_button" onclick="essb_mailform_send();">Send</button><button id="essb_mailform_btn_cancel" class="essb_mailform_content_button" onclick="essb_close_mailform(); return false;">Cancel</button></div><input type="hidden" id="essb_mail_salt" value="1362929168"/><input type="hidden" id="essb_mail_instance" value=""/><input type="hidden" id="essb_mail_post" value=""/></div></div><div class="essb_mailform_shadow"></div><link rel="stylesheet" id="essb-cct-style"  href="http://www.careco.es/wp-content/plugins/easy-social-share-buttons3/lib/modules/click-to-tweet/assets/css/styles.css" type="text/css" media="all" /><script type="text/javascript">var essb_window = function(oUrl, oService, oInstance) { var element = jQuery('.essb_'+oInstance); var instance_post_id = jQuery(element).attr("data-essb-postid") || ""; var instance_position = jQuery(element).attr("data-essb-position") || ""; var wnd; var w = 800 ; var h = 500; if (oService == "twitter") { w = 500; h= 300; } var left = (screen.width/2)-(w/2); var top = (screen.height/2)-(h/2); if (oService == "twitter") { wnd = window.open( oUrl, "essb_share_window", "height=300,width=500,resizable=1,scrollbars=yes,top="+top+",left="+left ); } else { wnd = window.open( oUrl, "essb_share_window", "height=500,width=800,resizable=1,scrollbars=yes,top="+top+",left="+left ); } if (typeof(essb_settings) != "undefined") { if (essb_settings.essb3_stats) { if (typeof(essb_handle_stats) != "undefined") { essb_handle_stats(oService, instance_post_id, oInstance); } } if (essb_settings.essb3_ga) { essb_ga_tracking(oService, oUrl, instance_position); } } essb_self_postcount(oService, instance_post_id); if (typeof(essb_abtesting_logger) != "undefined") { essb_abtesting_logger(oService, instance_post_id, oInstance); } var pollTimer = window.setInterval(function() { if (wnd.closed !== false) { window.clearInterval(pollTimer); essb_smart_onclose_events(oService, instance_post_id); } }, 200); }; var essb_self_postcount = function(oService, oCountID) { if (typeof(essb_settings) != "undefined") { oCountID = String(oCountID); jQuery.post(essb_settings.ajax_url, { 'action': 'essb_self_postcount', 'post_id': oCountID, 'service': oService, 'nonce': essb_settings.essb3_nonce }, function (data) { if (data) { }},'json'); } }; var essb_smart_onclose_events = function(oService, oPostID) { if (oService == "subscribe" || oService == "comments") return; if (typeof (essbasc_popup_show) == 'function') { essbasc_popup_show(); } if (typeof essb_acs_code == 'function') { essb_acs_code(oService, oPostID); } if (typeof(after_share_easyoptin) != "undefined") { essb_toggle_subscribe(after_share_easyoptin); } }; var essb_tracking_only = function(oUrl, oService, oInstance, oAfterShare) { var element = jQuery('.essb_'+oInstance); if (oUrl == "") { oUrl = document.URL; } var instance_post_id = jQuery(element).attr("data-essb-postid") || ""; var instance_position = jQuery(element).attr("data-essb-position") || ""; if (typeof(essb_settings) != "undefined") { if (essb_settings.essb3_stats) { if (typeof(essb_handle_stats) != "undefined") { essb_handle_stats(oService, instance_post_id, oInstance); } } if (essb_settings.essb3_ga) { essb_ga_tracking(oService, oUrl, instance_position); } } essb_self_postcount(oService, instance_post_id); if (oAfterShare) { essb_smart_onclose_events(oService, instance_post_id); } }; var essb_pinterest_picker = function(oInstance) { essb_tracking_only('', 'pinterest', oInstance); var e=document.createElement('script'); e.setAttribute('type','text/javascript'); e.setAttribute('charset','UTF-8'); e.setAttribute('src','//assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e); };var essb_mailform_opened = false; function essb_open_mailform(unique_id) { jQuery.fn.extend({ center: function () { return this.each(function() { var top = (jQuery(window).height() - jQuery(this).outerHeight()) / 2; var left = (jQuery(window).width() - jQuery(this).outerWidth()) / 2; jQuery(this).css({position:'fixed', margin:0, top: (top > 0 ? top : 0)+'px', left: (left > 0 ? left : 0)+'px'}); }); } }); if (essb_mailform_opened) { essb_close_mailform(unique_id); return; } var sender_element = jQuery(".essb_"+unique_id); if (!sender_element.length) return; var sender_post_id = jQuery(sender_element).attr("data-essb-postid") || ""; jQuery("#essb_mail_instance").val(unique_id); jQuery("#essb_mail_post").val(sender_post_id); var win_width = jQuery( window ).width(); var win_height = jQuery(window).height(); var doc_height = jQuery('document').height(); var base_width = 300; if (win_width < base_width) { base_width = win_width - 30; } var height_correction = 20; var element_class = ".essb_mailform"; var element_class_shadow = ".essb_mailform_shadow"; jQuery(element_class).css( { width: base_width+'px'}); var popup_height = jQuery(element_class).outerHeight(); if (popup_height > (win_height - 30)) { jQuery(element_class).css( { height: (win_height - height_correction)+'px'}); } jQuery("#essb_mailform_from").val(""); jQuery("#essb_mailform_to").val(""); if (jQuery("#essb_mailform_c").length) jQuery("#essb_mailform_c").val(""); jQuery(element_class_shadow).css( { height: (win_height)+'px'}); jQuery(element_class).center(); jQuery(element_class).slideDown(200); jQuery(element_class_shadow).fadeIn(200); essb_mailform_opened = true; essb_tracking_only("", "mail", unique_id); }; function essb_close_mailform() { var element_class = ".essb_mailform"; var element_class_shadow = ".essb_mailform_shadow"; jQuery(element_class).fadeOut(200); jQuery(element_class_shadow).fadeOut(200); essb_mailform_opened = false; }; function essb_mailform_send() { var sender_email = jQuery("#essb_mailform_from").val(); var recepient_email = jQuery("#essb_mailform_to").val(); var captcha_validate = jQuery("#essb_mailform_c").length ? true : false; var captcha = captcha_validate ? jQuery("#essb_mailform_c").val() : ""; var custom_message = jQuery("#essb_mailform_custom").length ? jQuery("#essb_mailform_custom").val() : ""; if (sender_email == "" || recepient_email == "" || (captcha == "" && captcha_validate)) { alert("Please fill all fields in form!"); return; } var mail_salt = jQuery("#essb_mail_salt").val(); var instance_post_id = jQuery("#essb_mail_post").val(); console.log("mail salt = " + mail_salt); if (typeof(essb_settings) != "undefined") { jQuery.post(essb_settings.ajax_url, { "action": "essb_mail_action", "post_id": instance_post_id, "from": sender_email, "to": recepient_email, "c": captcha, "cu": custom_message, "salt": mail_salt, "nonce": essb_settings.essb3_nonce }, function (data) { if (data) { console.log(data); alert(data["message"]); if (data["code"] == "1") essb_close_mailform(); }},'json'); } };</script>

</body>
</html>
