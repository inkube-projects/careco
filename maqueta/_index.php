<!doctype html>
<html lang="es-ES" prefix="og: http://ogp.me/ns# og: http://ogp.me/ns#" >
   <head>
      <!-- Meta Tags -->
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
      <!--Shortcut icon-->
      <title>CARECO - CASAS COMO TÚ</title>
      <meta property="og:url" content="http://www.careco.es" />
      <meta property="og:type" content="article" />
      <meta property="og:site_name" content="Careco" />
      <!-- This site is optimized with the Yoast SEO plugin v4.4 - https://yoast.com/wordpress/plugins/seo/ -->
      <meta name="description" content="Sea como sea tu vida, en CARECO tenemos tu casa"/>
      <meta name="robots" content="noodp"/>
      <link rel="canonical" href="index.php" />
      <meta property="og:locale" content="es_ES" />
      <meta property="og:type" content="website" />
      <meta property="og:title" content="CARECO - CASAS COMO TÚ" />
      <meta property="og:description" content="Sea como sea tu vida, en CARECO tenemos tu casa" />
      <meta property="og:url" content="http://www.careco.es/" />
      <meta property="og:site_name" content="Careco" />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:description" content="Sea como sea tu vida, en CARECO tenemos tu casa" />
      <meta name="twitter:title" content="CARECO - CASAS COMO TÚ" />
      <script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"http:\/\/www.careco.es\/","name":"Careco","potentialAction":{"@type":"SearchAction","target":"http:\/\/www.careco.es\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
      <!-- / Yoast SEO plugin. -->
      <link rel='dns-prefetch' href='//fonts.googleapis.com' />
      <link rel='dns-prefetch' href='//s.w.org' />
      <link rel="alternate" type="application/rss+xml" title="Careco &raquo; Feed" href="http://www.careco.es/?feed=rss2" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/www.careco.es\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.7"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
         img.wp-smiley,
         img.emoji {
         	display: inline !important;
         	border: none !important;
         	box-shadow: none !important;
         	height: 1em !important;
         	width: 1em !important;
         	margin: 0 .07em !important;
         	vertical-align: -0.1em !important;
         	background: none !important;
         	padding: 0 !important;
         }
      </style>
      <link rel='stylesheet' id='contact-form-7-css'  href='css/styles.4.6.1.css' type='text/css' media='all' />
      <link rel='stylesheet' id='mediaelement-css'  href='css/mediaelementplayer.min.css' type='text/css' media='all' />
      <link rel='stylesheet' id='wp-mediaelement-css'  href='css/wp-mediaelement.min.css' type='text/css' media='all' />
      <link rel='stylesheet' id='rgs-css' href='css/rgs.css' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-css'  href='plugins/font-awesome/css/font-awesome.css' type='text/css' media='all' />
      <link rel='stylesheet' id='main-styles-css'  href='css/style7.6.css' type='text/css' media='all' />
      <link rel='stylesheet' id='pretty_photo-css'  href='css/prettyPhoto.css' type='text/css' media='all' />
      <!--[if lt IE 9]>
      <link rel='stylesheet' id='nectar-ie8-css'  href='http://www.careco.es/wp-content/themes/salient/css/ie8.css?ver=4.7' type='text/css' media='all' />
      <![endif]-->
      <link rel='stylesheet' id='responsive-css'  href='css/responsive7.6.css' type='text/css' media='all' />
      <link rel='stylesheet' id='nectarslider-css'  href='css/nectar-slider4.7.css' type='text/css' media='all' />
      <link rel='stylesheet' id='select2-css'  href='css/select2-6.2.css' type='text/css' media='all' />
      <link rel='stylesheet' id='skin-ascend-css'  href='css/ascend7.6.css' type='text/css' media='all' />
      <link rel='stylesheet' id='easy-social-share-buttons-css'  href='css\easy-social-share-buttons.min.4.1.css' type='text/css' media='all' />
      <link rel='stylesheet' id='js_composer_front-css'  href='css/js_composer.min.4.12.1.css' type='text/css' media='all' />
      <link rel='stylesheet' id='redux-google-fonts-salient_redux-css'  href='http://fonts.googleapis.com/css?family=Montserrat%3A400%2C700%7CLato%3A900%2C400&#038;subset=latin&#038;ver=1509443950' type='text/css' media='all' />
      <script type='text/javascript' src='js/jquery.1.12.4.js'></script>
      <script type='text/javascript' src='js/jquery-migrate.min.1.4.1.js'></script>
      <script type='text/javascript' src='js/modernizr.2.6.2.js'></script>
      <link rel='shortlink' href='http://www.careco.es/' />
      <link rel="stylesheet" href="css/main.css">
      <script type="text/javascript">
         var essb_settings = {
            "ajax_url":"http:\/\/www.careco.es\/wp-admin\/admin-ajax.php",
            "essb3_nonce":"87605a7367",
            "essb3_plugin_url":"http:\/\/www.careco.es\/wp-content\/plugins\/easy-social-share-buttons3",
            "essb3_facebook_total":true,
            "essb3_admin_ajax":false,
            "essb3_internal_counter":false,
            "essb3_stats":false,
            "essb3_ga":false,
            "essb3_ga_mode":"simple",
            "essb3_counter_button_min":0,
            "essb3_counter_total_min":0,
            "blog_url":"http:\/\/www.careco.es\/",
            "ajax_type":"wp",
            "essb3_postfloat_stay":false,
            "essb3_no_counter_mailprint":false,
            "essb3_single_ajax":false,
            "twitter_counter":"self",
            "post_id":6690
         };
      </script>
      <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
      <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="http://www.careco.es/wp-content/plugins/js_composer_salient/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><!--[if IE  8]><link rel="stylesheet" type="text/css" href="http://www.careco.es/wp-content/plugins/js_composer_salient/assets/css/vc-ie8.min.css" media="screen"><![endif]-->
      <noscript>
         <style type="text/css">
            .wpb_animate_when_almost_visible {
               opacity: 1;
            }
         </style>
      </noscript>
   </head>

   <body
      class="home page-template-default page page-id-6690 ascend nectar-auto-lightbox wpb-js-composer js-comp-ver-4.12.1 vc_responsive"
      data-footer-reveal="false"
      data-header-format="default"
      data-footer-reveal-shadow="none"
      data-dropdown-style="classic"
      data-cae="linear" data-cad="500"
      data-aie="none"
      data-ls="pretty_photo"
      data-apte="standard"
      data-hhun="0"
      data-fancy-form-rcs="1"
      data-form-style="minimal"
      data-form-submit="default"
      data-is="minimal"
      data-button-style="default"
      data-header-inherit-rc="false"
      data-header-search="false"
      data-animated-anchors="true"
      data-ajax-transitions="true"
      data-full-width-header="true"
      data-slide-out-widget-area="true"
      data-slide-out-widget-area-style="fullscreen"
      data-user-set-ocm="1"
      data-loading-animation="none"
      data-bg-header="false"
      data-ext-responsive="false"
      data-header-resize="0"
      data-header-color="custom"
      data-transparent-header="false"
      data-smooth-scrolling="1"
      data-permanent-transparent="false"
      data-responsive="1">

      <div id="header-secondary-outer" data-full-width="true" data-permanent-transparent="false" >
   		<div class="container">
   			<nav></nav>
   		</div>
      </div>

      <div id="header-space" data-header-mobile-fixed='1'></div>

      <div id="header-outer" data-has-menu="false"  data-mobile-fixed="1" data-ptnm="1" data-lhe="animated_underline" data-user-set-bg="#e8e8e8" data-format="default" data-permanent-transparent="false" data-cart="false" data-transparency-option="0" data-shrink-num="6" data-full-width="true" data-using-secondary="1" data-using-logo="1" data-logo-height="20" data-m-logo-height="24" data-padding="30" data-header-resize="0">
         <header id="top">
            <div class="container">
               <div class="row">
                  <div class="col span_3">
                     <a id="logo" href="index.php" >
                        <img class="stnd default-logo" alt="Careco" src="images/logo-high-n-01.png" />
                        <img class="retina-logo " alt="Careco" src="images/logo-high-n-01.png" />
                     </a>
                  </div><!--/span_3-->

                  <div class="col span_9 col_last">
                     <div class="slide-out-widget-area-toggle mobile-icon fullscreen" data-icon-animation="simple-transform">
                        <div> <a href="#sidewidgetarea" class="closed"> <span> <i class="lines-button x2"> <i class="lines"></i> </i> </span> </a> </div>
                     </div>

                     <nav>
                        <ul class="buttons" data-user-set-ocm="1">
                           <li id="search-btn"><div><a href="#searchbox"><span class="icon-salient-search" aria-hidden="true"></span></a></div> </li>
                           <li class="slide-out-widget-area-toggle" data-icon-animation="simple-transform">
                              <div> <a href="#sidewidgetarea" class="closed"> <span> <i class="lines-button x2"> <i class="lines"></i> </i> </span> </a> </div>
	       						</li>
                        </ul>

                        <ul class="sf-menu">
                           <li class="no-menu-assigned"><a href="#">No menu assigned</a></li>
                        </ul>
                     </nav>
                  </div><!--/span_9-->
               </div><!--/row-->
            </div><!--/container-->
         </header>

         <div class="ns-loading-cover"></div>
      </div><!--/header-outer-->



      <div id="search-outer" class="nectar">
         <div id="search">
            <div class="container">
               <div id="search-box">
                  <div class="col span_12">
                     <form action="http://www.careco.es" method="GET">
                        <input type="text" name="s" id="s" value="Start Typing..." data-placeholder="Start Typing..." />
                     </form>
                  </div><!--/span_12-->
               </div><!--/search-box-->

               <div id="close"><a href="#"><span class="icon-salient-x" aria-hidden="true"></span></a></div>
            </div><!--/container-->
         </div><!--/search-->
      </div><!--/search-outer-->

      <div id="mobile-menu" data-mobile-fixed="1">
         <div class="container">
            <ul>
               <li><a href="">No menu assigned!</a></li>
            </ul>
         </div>
      </div>


      <div id="ajax-loading-screen" data-disable-fade-on-click="0" data-effect="standard" data-method="standard">
         <div class="loading-icon none">
            <span class="default-loading-icon spin"></span>
         </div>
      </div>

      <div id="ajax-content-wrap">
         <div class="blurred-wrap">
            <div class="container-wrap">
               <div class="container main-content">
                  <div class="row">
                     <div id="fws_5a61303732b98"  data-midnight="custom" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-content  vc_row-o-equal-height vc_row-flex standard_section  bloquehorizontal "  style="padding-top: 0px; padding-bottom: 0px; ">
                        <div class="row-bg-wrap">
                           <div class="row-bg" style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.5" data-enable_gradient="false"></div>
                        </div>

                        <div class="col span_12 custom left">
                           <div  class="vc_col-sm-12 textobajo wpb_column column_container vc_column_container col has-animation"  data-bg-cover="" data-padding-pos="left" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="reveal-from-right" data-delay="0"><a class="column-link" href="about-us.php"></a>
                              <div class="column-inner-wrap">
                                 <div  data-bg-cover="" class="column-inner no-extra-padding">
                                    <div class="wpb_wrapper">
                                       <div id="fws_5a61303733260" data-midnight="" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row  vc_row-o-equal-height vc_row-flex standard_section" style="padding-top: 0px; padding-bottom: 0px;">
                                          <div class="row-bg-wrap">
                                             <div class="row-bg"></div>
                                          </div>
                                          <div class="col span_12 left">
                                             <div style="" class="vc_col-sm-4 wpb_column column_container vc_column_container col no-extra-padding" data-using-bg="true" data-bg-cover="" data-padding-pos="bottom-left" data-has-bg-color="true" data-bg-color="#ffffff" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0"><a class="column-link" href="about-us.php"></a>
                                                <div class="vc_column-inner">
                                                   <div class="wpb_wrapper">
                                                      <div class="wpb_text_column wpb_content_element " >
                                                         <div class="wpb_wrapper">
                                                            <img width="479" height="350" src="images/home_banner/01.jpg" class="" alt="home_01" srcset="images/home_banner/01.jpg 479w, images/home_banner/01-282x206.jpg 282w" sizes="(max-width: 479px) 100vw, 479px" />
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>

                                             <div style="" class="vc_col-sm-8 wpb_column column_container vc_column_container col no-extra-padding" data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-color="#ffffff" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                                                <div class="vc_column-inner">
                                                   <div class="wpb_wrapper">
                                                      <div class="divider-wrap">
                                                         <div style="height: 110px;" class="divider"></div>
                                                      </div>

                                                      <div class="img-with-aniamtion-wrap" data-max-width="100%">
                                                         <div class="inner">
                                                            <a href="index.php" target="_self" class="">
                                                               <img data-shadow="none" data-shadow-direction="middle" class="img-with-animation bloque-inicio" data-delay="0" height="22" width="127" data-animation="grow-in" src="images/logo-medium-n-01.png" alt="" />
                                                            </a>
                                                         </div>
                                                      </div>

                                                      <div class="wpb_text_column wpb_content_element  vc_custom_1488297130004 bloque-inicio" >
                                                         <div class="wpb_wrapper">
                                                            <h1><span style="font-size: 30px; background-color: #7fa9ae; padding: 5px 10px 5px 10px; color: #fff;">CASAS COMO TÚ</span></h1>
                                                         </div>
                                                      </div>

                                                      <div class="wpb_text_column wpb_content_element  bloque-inicio" >
                                                         <div class="wpb_wrapper">
                                                            <p style="font-size: 16px;">Sea como sea tu vida, en CARECO tenemos tu casa</p>
                                                         </div>
                                                      </div>
                                                      <div class="divider-wrap"><div style="height: 50px;" class="divider"></div></div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div id="fws_5a6130373ae73"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-content  vc_row-o-equal-height vc_row-flex standard_section promo" style="padding-top: 0px; padding-bottom: 0px; ">
                        <div class="row-bg-wrap">
                           <div class="row-bg" style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
                        </div>

                        <div class="col span_12 dark left">
                           <div style="" class="vc_col-sm-4 textobajo wpb_column column_container vc_column_container col no-extra-padding" data-using-bg="true" data-bg-cover="" data-padding-pos="right" data-has-bg-color="true" data-bg-color="#eeeeee" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                              <div class="vc_column-inner">
                                 <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1485800280511 pad" >
                                       <div class="wpb_wrapper">
                                          <h2 style="text-align: left;"><a href="current-promotion-sales.php"><span style="color: #6b6c6b;">PROMOCIONES</span></a></h2>
                                          <h2 style="text-align: left;"><a href="current-promotion-sales.php"><span style="color: #6b6c6b;">EN CURSO</span></a></h2>
                                       </div>
                                    </div>

                                    <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                                       <div class="wpb_wrapper">
                                          <div class="arrow"></div>
                                       </div>
                                    </div>

                                    <div class="divider-wrap">
                                       <div style="height: 25px;" class="divider"></div>
                                    </div>

                                    <div class="wpb_text_column wpb_content_element  vc_custom_1481537267854 pad" >
                                       <div class="wpb_wrapper">
                                          <p>
                                             <strong>
                                                <div id="links-link-6690" class="sh-link links-link sh-hide">
                                                   <a href="#" onclick="showhide_toggle('links', 6690, 'AVD. FRANCIA (Valencia)', 'AVD. FRANCIA (Valencia)'); return false;" aria-expanded="false">
                                                      <span id="links-toggle-6690">AVD. FRANCIA (Valencia)</span>
                                                   </a>
                                                </div>

                                                <div id="links-content-6690" class="sh-content links-content sh-hide" style="display: none;">
                                             </strong>
                                          </p>

                                          <p style="text-align: left;">
                                             <a href="current-promotion-sales.php">VENTA</a> |
                                             <a href="current-promotion-rental.php">ALQUILER</a>
                                          </p>

                                          <p>
                                             <strong>
                                                </div>
                                                <div id="links2-link-6690" class="sh-link links2-link sh-hide">
                                                   <a href="#" onclick="showhide_toggle('links2', 6690, 'PICANYA', 'PICANYA'); return false;" aria-expanded="false">
                                                      <span id="links2-toggle-6690">PICANYA</span>
                                                   </a>
                                                </div>

                                                <div id="links2-content-6690" class="sh-content links2-content sh-hide" style="display: none;">
                                                   </strong>
                                          </p>

                                          <p style="text-align: left;">
                                             <a href="current-promotion-sales.php">VENTA</a> | <a href="current-promotion-rental.php">ALQUILER</a>
                                          </p>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="wpb_text_column wpb_content_element  vc_custom_1481291575800 pad" >
                                    <div class="wpb_wrapper">
                                       <h2 style="text-align: left;">
                                          <a href="next-promotions.php">
                                             <span style="color: #6b6c6b;">PRÓXIMAS</span>
                                          </a>
                                       </h2>

                                       <h2 style="text-align: left;"><a href="next-promotions.php"><span style="color: #6b6c6b;">PROMOCIONES</span></a></h2>
                                    </div>
                                 </div>

                                 <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                                    <div class="wpb_wrapper">
                                       <div class="arrow"></div>
                                    </div>
                                 </div>

                                 <div class="divider-wrap">
                                    <div style="height: 25px;" class="divider"></div>
                                 </div>

                                 <div class="wpb_text_column wpb_content_element  pad" >
                                    <div class="wpb_wrapper">
                                       <h5><a href="next-promotions.php"><span style="color: #6b6c6b;"><strong>AVD. FRANCIA</strong> (Alameda)</span></a></h5>
                                       <h5><a href="next-promotions.php"><span style="color: #6b6c6b;"><strong>AVD. FRANCIA</strong> (Bolinches)</span></a></h5>
                                       <h5><a href="next-promotions.php"><span style="color: #6b6c6b;"><strong>ROCAFORT</strong>, NÁQUERA, PICANYA&#8230;</span></a></h5>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="vc_col-sm-8 sliderhome wpb_column column_container vc_column_container col centered-text no-extra-padding" data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                           <div class="vc_column-inner">
                              <div class="wpb_wrapper">
                                 <div style="height: 580px"  data-transition="fade" data-overall_style="classic" data-flexible-height="true" data-animate-in-effect="none" data-fullscreen="false" data-button-sizing="regular" data-button-styling="btn_with_count" data-autorotate="10000" data-parallax="false" data-full-width="false" class="nectar-slider-wrap " id="ns-id-5a6130373bbfe">
                                    <div style="height: 580px" class="swiper-container"  data-tho="auto" data-tco="auto" data-pho="auto" data-pco="auto" data-loop="true" data-height="580" data-min-height="300" data-arrows="false" data-bullets="false" data-bullet_style="see_through" data-desktop-swipe="false" data-settings="">
                                       <div class="swiper-wrapper">
                                          <div class="swiper-slide" data-bg-alignment="center" data-color-scheme="light" data-x-pos="left" data-y-pos="middle">
                                             <div class="image-bg" style="background-image: url(images/home_slide/Salon_Cam01_Publi-prueba.jpg);"> &nbsp; </div>
                                             <div class="container">
                                                <div class="content">
                                                   <div class="buttons">
                                                      <div class="button solid_color">
                                                         <a class="primary-color" href="#">VENTA</a>
                                                      </div>

                                                      <div class="button solid_color">
                                                         <a class="primary-color" href="#">ALQUILER</a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div><!--/container-->

                                             <div class="video-texture active_texture">
                                                <span class="ie-fix"></span>
                                             </div>
                                          </div><!--/swiper-slide-->

                                          <div class="swiper-slide" data-bg-alignment="center" data-color-scheme="light" data-x-pos="left" data-y-pos="middle">
                                             <div class="image-bg" style="background-image: url(images/home_slide/0002_Buhardilla_Cam01_Publi.jpg);"> &nbsp; </div>
                                             <div class="video-texture active_texture">
                                                <span class="ie-fix"></span>
                                             </div>
                                          </div><!--/swiper-slide-->

                                          <div class="swiper-slide" data-bg-alignment="center" data-color-scheme="light" data-x-pos="left" data-y-pos="middle">
                                             <div class="image-bg" style="background-image: url(images/home_slide/Panorama_8_V4.jpg);"> &nbsp; </div>
                                             <div class="video-texture">
                                                <span class="ie-fix"></span>
                                             </div>
                                          </div><!--/swiper-slide-->

                                          <div class="swiper-slide" data-bg-alignment="center" data-color-scheme="light" data-x-pos="left" data-y-pos="middle">
                                             <div class="image-bg" style="background-image: url(images/home_slide/004-1.jpg);"> &nbsp; </div>
                                             <div class="video-texture active_texture">
                                                <span class="ie-fix"></span>
                                             </div>
                                          </div><!--/swiper-slide-->

                                          <div class="swiper-slide" data-bg-alignment="center" data-color-scheme="light" data-x-pos="centered" data-y-pos="middle">
                                             <div class="image-bg" style="background-image: url(images/home_slide/Buhardilla_Office_Cam02_FINAL_V2.jpg);"> &nbsp; </div>
                                             <div class="container">
                                                <div class="content">
                                                   <div class="buttons">
                                                      <div class="button solid_color">
                                                         <a class="primary-color" href="#">VENTA</a>
                                                      </div>

                                                      <div class="button solid_color">
                                                         <a class="primary-color" href="#">ALQUILER</a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div><!--/container-->

                                             <div class="video-texture active_texture">
                                                <span class="ie-fix"></span>
                                             </div>
                                          </div><!--/swiper-slide-->
                                       </div>

                                       <div class="nectar-slider-loading ">
                                          <span class="loading-icon none"></span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div id="fws_5a6130373ea11"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-content  vc_row-o-equal-height vc_row-flex standard_section"  style="padding-top: 5%; padding-bottom: 7%; ">
                     <div class="row-bg-wrap">
                        <div class="row-bg  using-bg-color" style="background-color: #ffffff;" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
                     </div>

                     <div class="col span_12 dark left">
                        <div  class="vc_col-sm-2 vc_hidden-sm vc_hidden-xs wpb_column column_container vc_column_container col no-extra-padding" data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                           <div class="vc_column-inner">
                              <div class="wpb_wrapper"></div>
                           </div>
                        </div>

                        <div class="vc_col-sm-8 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                           <div class="vc_column-inner">
                              <div class="wpb_wrapper">
                                 <div id="fws_5a6130373f034" data-midnight="" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row standard_section" style="padding-top: 0px; padding-bottom: 0px;">
                                    <div class="row-bg-wrap">
                                       <div class="row-bg" style=""></div>
                                    </div>

                                    <div class="col span_12  left">
                                       <div style="" class="vc_col-sm-4 caracteristicas wpb_column column_container vc_column_container col padding-2-percent" data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-color="#ffffff" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                                          <a class="column-link" href="about-us.php#personalidad"></a>
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper">
                                                <div class="img-with-aniamtion-wrap center" data-max-width="100%">
                                                   <div class="inner">
                                                      <img data-shadow="none" data-shadow-direction="middle" class="img-with-animation " data-delay="0" height="120" width="120" data-animation="fade-in" src="images/values/personalidad.jpg" alt="" />
                                                   </div>
                                                </div>

                                                <div class="wpb_text_column wpb_content_element  vc_custom_1481290692456" >
                                                   <div class="wpb_wrapper">
                                                      <h5 style="text-align: center;"><strong>PERSONALIDAD</strong></h5>
                                                      <p style="font-size: 14px; text-align: center; padding-top: 15px;">
                                                         <a href="https://www.youtube.com/watch?v=M1Uu0YIdJ7A"><span style="color: #779495;">VIVIENDAS QUE SE REVALORIZAN</span></a>
                                                      </p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>

                                       <div  class="vc_col-sm-4 caracteristicas wpb_column column_container vc_column_container col padding-2-percent"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                                          <a class="column-link" href="about-us.php#personalidad"></a>

                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper">
                                                <div class="img-with-aniamtion-wrap center" data-max-width="100%">
                                                   <div class="inner">
                                                      <img data-shadow="none" data-shadow-direction="middle" class="img-with-animation " data-delay="0" height="120" width="120" data-animation="fade-in" src="images/values/eficiencia.jpg" alt="" />
                                                   </div>
                                                </div>

                                                <div class="wpb_text_column wpb_content_element  vc_custom_1481285814443" >
                                                   <div class="wpb_wrapper">
                                                      <h5 style="text-align: center;"><strong>EFICIENCIA</strong></h5>
                                                      <p style="font-size: 14px; text-align: center; padding-top: 15px;">
                                                         <a href="https://www.youtube.com/watch?v=M1Uu0YIdJ7A">
                                                            <span style="color: #779495;">EQUIPO TÉCNICO</span>
                                                         </a>
                                                      </p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>

                                       <div  class="vc_col-sm-4 caracteristicas wpb_column column_container vc_column_container col padding-2-percent"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                                          <a class="column-link" href="about-us.php#personalidad"></a>
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper">
                                                <div class="img-with-aniamtion-wrap center" data-max-width="100%">
                                                   <div class="inner">
                                                      <img data-shadow="none" data-shadow-direction="middle" class="img-with-animation " data-delay="0" height="120" width="120" data-animation="fade-in" src="images/values/calidad.jpg" alt="" />
                                                   </div>
                                                </div>

                                                <div class="wpb_text_column wpb_content_element  vc_custom_1481285839745" >
                                                   <div class="wpb_wrapper">
                                                      <h5 style="text-align: center;"><strong>CALIDAD</strong></h5>
                                                      <p style="font-size: 14px; text-align: center; padding-top: 15px;"><a href="https://www.youtube.com/watch?v=M1Uu0YIdJ7A"><span style="color: #779495;">CASAS COMO TÚ</span></a></p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div  class="vc_col-sm-2 vc_hidden-sm vc_hidden-xs wpb_column column_container vc_column_container col no-extra-padding" data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                           <div class="vc_column-inner">
                              <div class="wpb_wrapper"></div>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div id="fws_5a613037413b7" data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-content  vc_row-o-equal-height vc_row-flex standard_section   "  style="padding-top: 0px; padding-bottom: 0px; ">
                     <div class="row-bg-wrap">
                        <div class="row-bg" style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
                     </div>

                     <div class="col span_12 dark left">
                        <div  class="vc_col-sm-8 cuadro wpb_column column_container vc_column_container col centered-text no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                           <a class="column-link" href="next-promotions.php"></a>
                           <div class="vc_column-inner">
                              <div class="wpb_wrapper">
                                 <div class="sb-bg-slider-params sb-slider-data-source-shortcode" data-source="shortcode" data-slider_attachments="images/promotion/sofa.jpg" data-animation_speed="750" data-slide_duration="6000" data-centered_x="no" data-centered_y="no" style="height:550px;">
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1488459299221 tab-arrow" >
                                       <div class="wpb_wrapper">
                                          <div class="promocionlayer">
                                             <h2 style="text-align: left; font-size: 18px; font-weight: 400; padding-left: 30px; line-height: 22px;"><span style="color: #6b6c6b;">PRÓXIMA PROMOCIÓN DE 71 VIVIENDAS EN RÉGIMEN DE ALQUILER</span></h2>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="wpb_raw_code wpb_content_element wpb_raw_html vc_custom_1481622345739 tab-arrow">
                                       <div class="wpb_wrapper">
                                          <div class="arrow4"></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div style="" class="vc_col-sm-4 wpb_column column_container vc_column_container col no-extra-padding" data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-color="#ffffff" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                           <div class="vc_column-inner">
                              <div class="wpb_wrapper">

                                 <div id="fws_5a6130374231f" data-midnight="" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row standard_section" style="padding-top: 0px; padding-bottom: 0px;">
                                    <div class="row-bg-wrap">
                                       <div class="row-bg" style=""></div>
                                    </div>

                                    <div class="col span_12  left">
                                       <div style=" background-image: url('images/news/shutterstock_127000511.jpg'); " class="vc_col-sm-12 rectangulo wpb_column column_container vc_column_container col no-extra-padding" data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-color="#ffffff" data-bg-opacity="0.1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                                          <a class="column-link" href="http://www.careco.es/?page_id=9979"></a>

                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper">

                                                <div class="wpb_text_column wpb_content_element  vc_custom_1505467026154 pad3" >
                                                   <div class="wpb_wrapper">
                                                      <h2><strong><span style="color: #6b6c6b;">ÚLTIMOS UNIFAMILIARES EN VENTA</span></strong></h2>
                                                      <h2 style="text-align: left;"><span style="color: #6b6c6b;">PERSONALIZA TU VIVIENDA </span><strong>AQUÍ</strong></h2>
                                                   </div>
                                                </div>

                                                <div class="wpb_raw_code wpb_content_element wpb_raw_html vc_custom_1481544683539">
                                                   <div class="wpb_wrapper">
                                                      <div class="arrow3"></div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>

                                 <div id="fws_5a61303742f18" data-midnight="" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row  vc_row-o-content-middle standard_section   "  style="padding-top: 0px; padding-bottom: 0px; ">
                                    <div class="row-bg-wrap">
                                       <div class="row-bg" style=""></div>
                                    </div>

                                    <div class="col span_12  left">
                                       <div style="" class="vc_col-sm-12 rectangulo wpb_column column_container vc_column_container col has-animation no-extra-padding" data-using-bg="true" data-bg-cover="" data-padding-pos="left-right" data-has-bg-color="true" data-bg-color="#cecece" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="fade-in-from-right" data-delay="0">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element  vc_custom_1490960847432 pad2" >
                                                   <div class="wpb_wrapper">
                                                      <h2 style="text-align: left;"><a href="index.php"><span style="color: #6b6c6b;">NOTICIAS</span></a></h2>
                                                   </div>
                                                </div>

                                                <div class="wpb_raw_code wpb_content_element wpb_raw_html vc_custom_1481544333661">
                                                   <div class="wpb_wrapper">
                                                      <div class="arrow2"></div>
                                                   </div>
                                                </div>

                                                <div class="divider-wrap">
                                                   <div style="height: 25px;" class="divider"></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div id="fws_5a613037435fc"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-content standard_section vertically-align-columns  " data-using-ctc="true" style="padding-top: 0px; padding-bottom: 0px; color: #ffffff;">
                     <div class="row-bg-wrap">
                        <div class="row-bg  using-bg-color" style="background-color: #75a0b2; " data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
                     </div>

                     <div class="col span_12 custom left">
                        <div style="" class="vc_col-sm-12 piedepagina wpb_column column_container vc_column_container col padding-10-percent" data-using-bg="true" data-bg-cover="" data-padding-pos="right" data-has-bg-color="true" data-bg-color="#779c9c" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                           <div class="vc_column-inner">
                              <div class="wpb_wrapper">
                                 <div class="wpb_text_column wpb_content_element  vc_custom_1481627786178" >
                                    <div class="wpb_wrapper">
                                       <h6 style="text-align: right; font-size: 10px;">
                                          <span style="color: #fff;">
                                             <div class="nectar-social  hide-share-count items_4">
                                                <a class='facebook-share nectar-sharing' href='#' title='Share this'>
                                                   <i class="fab fa-facebook-f"></i>
                                                   <span class='count'></span>
                                                </a>

                                                <a class='twitter-share nectar-sharing' href='#' title='Tweet this'>
                                                   <i class="fab fa-twitter"></i>
                                                   <span class='count'></span>
                                                </a>

                                                <a class='linkedin-share nectar-sharing' href='#' title='Share this'>
                                                   <i class="fab fa-linkedin-in"></i>
                                                   <span class='count'></span>
                                                </a>

                                                <a class='pinterest-share nectar-sharing' href='#' title='Pin this'>
                                                   <i class="fas fa-angle-up"></i>
                                                   <span class='count'></span>
                                                </a>
                                             </div>
                                          </span>
                                       </h6>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div><!--/row-->
            </div><!--/container-->
         </div><!--/container-wrap-->

         <div id="footer-outer" data-midnight="light" data-full-width="1" data-using-widget-area="true">
            <div id="footer-widgets" data-cols="3">
               <div class="container">
                  <div class="row">
                     <div class="col span_4">
                        <!-- Footer widget area 1 -->
                        <div id="text-8" class="widget widget_text">
                           <div class="textwidget"></div>
                        </div>
                     </div><!--/span_3-->

                     <div class="col span_4">
                        <!-- Footer widget area 2 -->
                        <div id="text-3" class="widget widget_text">
                           <div class="textwidget">
                              <div style="margin-top:0px">
                                 <a href="index.php">
                                    <img class="alignnone size-full wp-image-6525 aligncenter" src="images/logo-medium-n-01.png" alt="logo medium n-01" width="127" height="22" />
                                 </a>
                              </div>

                              <h5 style="text-align: center;">
                                 <span style="color: #999999;">TEXTO LEGAL / <a href="contact.php">CONTACTO</a></span>
                              </h5>

                              <p>
                                 <img class="alignnone wp-image-7253 aligncenter" src="images/sello-2.jpg" alt="sello" width="117" height="74" />
                              </p>
                           </div>
                        </div>
                     </div><!--/span_3-->

                     <div class="col span_4">
                        <!-- Footer widget area 3 -->
                        <div id="text-9" class="widget widget_text">
                           <div class="textwidget">
                              <span style="color: #ffffff;">.</span>
                           </div>
                        </div>
                     </div><!--/span_3-->
                  </div><!--/row-->
               </div><!--/container-->
            </div><!--/footer-widgets-->

            <div class="row" id="copyright">
               <div class="container">
                  <div class="col span_5">
                     <p>&copy; 2018 Careco.  </p>
                  </div><!--/span_5-->

                  <div class="col span_7 col_last">
                     <ul id="social"></ul>
                  </div><!--/span_7-->
               </div><!--/container-->
            </div><!--/row-->
         </div><!--/footer-outer-->
      </div><!--blurred-wrap-->

      <div id="slide-out-widget-area-bg" class="fullscreen dark"></div>
      <div id="slide-out-widget-area" class="fullscreen" data-back-txt="Back">
         <div class="inner-wrap">
            <div class="inner">
               <a class="slide_out_area_close" href="#"><span class="icon-salient-x icon-default-style"></span></a>
               <div class="off-canvas-menu-container">
                  <ul class="menu">
                     <li id="menu-item-7082" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-6690 current_page_item menu-item-7082"><a href="index.php">HOME</a></li>
                     <li id="menu-item-7560" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-7560"><a href="current-promotion-sales.php">PROMOCIONES EN CURSO</a>
                        <ul class="sub-menu">
                           <li id="menu-item-8176" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-8176"><a href="#">VENTA</a>
                              <ul class="sub-menu">
                                 <li id="menu-item-8181" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8181"><a href="current-promotion-sales.php">AVD. FRANCIA</a></li>
                                 <li id="menu-item-8180" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8180"><a href="current-promotion-sales.php">PICANYA</a></li>
                              </ul>
                           </li>
                           <li id="menu-item-8177" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-8177"><a href="#">ALQUILER</a>
                              <ul class="sub-menu">
                                 <li id="menu-item-8185" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8185"><a href="current-promotion-rental.php">AVD. FRANCIA</a></li>
                                 <li id="menu-item-8184" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8184"><a href="current-promotion-rental.php">PICANYA</a></li>
                              </ul>
                           </li>
                        </ul>
                     </li>
                     <li id="menu-item-7797" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7797"><a href="next-promotions.php">PRÓXIMAS PROMOCIONES</a></li>
                     <li id="menu-item-7799" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-7799"><a href="about-us.php">CARECO, CASAS COMO TÚ</a>
                        <ul class="sub-menu">
                           <li id="menu-item-8246" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8246"><a href="about-us.php">ASI SOMOS</a></li>
                           <li id="menu-item-7800" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7800"><a href="our-milestones.php">NUESTROS HITOS</a></li>
                           <li id="menu-item-9375" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9375"><a href="our-works.php">NUESTRAS OBRAS</a></li>
                        </ul>
                     </li>
                     <li id="menu-item-8216" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8216"><a href="contact.php">CONTACTO</a></li>
                  </ul>
               </div>
            </div>

            <div class="bottom-meta-wrap">
               <p class="bottom-text" data-has-desktop-social="true">Derechos reservados CARECO. 2016</p>
            </div><!--/bottom-meta-wrap-->
         </div> <!--/inner-wrap-->
      </div>
   </div> <!--/ajax-content-wrap-->

      <a id="to-top" class=""><i class="fas fa-angle-up"></i></a>
      <script type="text/javascript">
         function showhide_toggle(a,b,d,f){var e=jQuery("#"+a+"-link-"+b),c=jQuery("a",e),g=jQuery("#"+a+"-content-"+b);a=jQuery("#"+a+"-toggle-"+b);e.toggleClass("sh-show sh-hide");g.toggleClass("sh-show sh-hide").toggle();"true"===c.attr("aria-expanded")?c.attr("aria-expanded","false"):c.attr("aria-expanded","true");a.text()===d?a.text(f):a.text(d)};
      </script>
   	<link rel='stylesheet' id='sbvcbgslider-style-css'  href='css/style.4.7.css' type='text/css' media='all' />
      <script type='text/javascript' src='js/core.min.1.11.4.js'></script>
      <script type='text/javascript' src='js/widget.min.1.11.4.js'></script>
      <script type='text/javascript' src='js/position.min.1.11.4.js'></script>
      <script type='text/javascript' src='js/menu.min.1.11.4.js'></script>
      <script type='text/javascript' src='js/wp-a11y.min.4.7.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
            var uiAutocompleteL10n = {"noResults":"Sin resultados.","oneResult":"1 resultado encontrado. Utiliza las teclas de flecha arriba y abajo para navegar.","manyResults":"%d resultados encontrados. Utiliza las teclas arriba y abajo para navegar.","itemSelected":"Elemento seleccionado."};
         /* ]]> */
      </script>
      <script type='text/javascript' src='js/autocomplete.min.1.11.4.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
            var MyAcSearch = {"url":"http:\/\/www.careco.es\/wp-admin\/admin-ajax.php"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='js/wpss-search-suggest.js'></script>
      <script type='text/javascript' src='js/jquery.form.min.3.51.0-2014.06.20.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
            var _wpcf7 = {"recaptcha":{"messages":{"empty":"Por favor, prueba que no eres un robot."}}};
         /* ]]> */
      </script>
      <script type='text/javascript' src='js/scripts.4.6.1.js'></script>
      <script type='text/javascript' src='js/nicescroll.3.5.4.js'></script>
      <script type='text/javascript' src='js/prettyPhoto.7.0.1.js'></script>
      <script type='text/javascript' src='js/midnight.1.0.js'></script>
      <script type='text/javascript' src='js/superfish.1.4.8.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
            var nectarLove = {
               "ajaxurl":"http:\/\/www.careco.es\/wp-admin\/admin-ajax.php",
               "postID":"6690",
               "rooturl":"http:\/\/www.careco.es",
               "pluginPages":[],
               "disqusComments":"false",
               "loveNonce":"7f66a7224e",
               "mapApiKey":"AIzaSyA44aW6ukgUoBzZd5Dn5bsJloe5v0u-5rw"
            };
         /* ]]> */
      </script>
      <script type='text/javascript' src='js/init.7.6.js'></script>
      <script type='text/javascript' src='js/infinitescroll.1.1.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
            var mejsL10n = {
               "language":"es-ES",
               "strings":{
                  "Close":"Cerrar",
                  "Fullscreen":"Pantalla completa",
                  "Turn off Fullscreen":"Salir de pantalla completa",
                  "Go Fullscreen":"Ver en pantalla completa",
                  "Download File":"Descargar archivo",
                  "Download Video":"Descargar v\u00eddeo",
                  "Play":"Reproducir",
                  "Pause":"Pausa",
                  "Captions\/Subtitles":"Pies de foto \/ Subt\u00edtulos",
                  "None":"None",
                  "Time Slider":"Control de tiempo",
                  "Skip back %1 seconds":"Retroceder %1 segundos",
                  "Video Player":"Reproductor de v\u00eddeo",
                  "Audio Player":"Reproductor de audio",
                  "Volume Slider":"Control de volumen",
                  "Mute Toggle":"Desactivar sonido",
                  "Unmute":"Activar sonido",
                  "Mute":"Silenciar",
                  "Use Up\/Down Arrow keys to increase or decrease volume.":"Utiliza las teclas de flecha arriba\/abajo para aumentar o disminuir el volumen.",
                  "Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.":"Usa las teclas de direcci\u00f3n izquierda\/derecha para avanzar un segundo, y las flechas arriba\/abajo para avanzar diez segundos."
               }
            };
            var _wpmejsSettings = {
               "pluginPath":"\/wp-includes\/js\/mediaelement\/"
            };
         /* ]]> */
      </script>
      <script type='text/javascript' src='js/mediaelement-and-player.min.2.22.0.js'></script>
      <script type='text/javascript' src='js/wp-mediaelement.min.4.7.js'></script>
      <script type='text/javascript' src='js/flickity.min.1.1.1.js'></script>
      <script type='text/javascript' src='js/nectar-slider.7.6.js'></script>
      <script type='text/javascript' src='js/select2.min.3.5.2.js'></script>
      <script type='text/javascript' src='js/wp-embed.min.4.7.js'></script>
      <script type='text/javascript' src='js/js_composer_front.min.4.12.1.js'></script>
      <script type='text/javascript' src='js/jquery.backstretch.min.1.1.js'></script>
      <script type='text/javascript' src='js/jquery.flexverticalcenter.1.1.js'></script>
      <script type='text/javascript' src='js/script.1.1.js'></script>

      <div class="essb_mailform">
         <div class="essb_mailform_content">
            <p>Send this to friend</p>
            <label class="essb_mailform_content_label">Your email</label>
            <input type="text" id="essb_mailform_from" class="essb_mailform_content_input" placeholder="Your email"/>
            <label class="essb_mailform_content_label">Recipient email</label>
            <input type="text" id="essb_mailform_to" class="essb_mailform_content_input" placeholder="Recipient email"/>
            <div class="essb_mailform_content_buttons">
               <button id="essb_mailform_btn_submit" class="essb_mailform_content_button" onclick="essb_mailform_send();">Send</button>
               <button id="essb_mailform_btn_cancel" class="essb_mailform_content_button" onclick="essb_close_mailform(); return false;">Cancel</button>
            </div>
            <input type="hidden" id="essb_mail_salt" value="1362929168"/>
            <input type="hidden" id="essb_mail_instance" value=""/>
            <input type="hidden" id="essb_mail_post" value=""/>
         </div>
      </div>
      <div class="essb_mailform_shadow"></div>

      <link rel="stylesheet" id="essb-cct-style"  href="css/styles.css" type="text/css" media="all" />

      <script type="text/javascript">
         var essb_window = function(oUrl, oService, oInstance) { var element = jQuery('.essb_'+oInstance); var instance_post_id = jQuery(element).attr("data-essb-postid") || ""; var instance_position = jQuery(element).attr("data-essb-position") || ""; var wnd; var w = 800 ; var h = 500; if (oService == "twitter") { w = 500; h= 300; } var left = (screen.width/2)-(w/2); var top = (screen.height/2)-(h/2); if (oService == "twitter") { wnd = window.open( oUrl, "essb_share_window", "height=300,width=500,resizable=1,scrollbars=yes,top="+top+",left="+left ); } else { wnd = window.open( oUrl, "essb_share_window", "height=500,width=800,resizable=1,scrollbars=yes,top="+top+",left="+left ); } if (typeof(essb_settings) != "undefined") { if (essb_settings.essb3_stats) { if (typeof(essb_handle_stats) != "undefined") { essb_handle_stats(oService, instance_post_id, oInstance); } } if (essb_settings.essb3_ga) { essb_ga_tracking(oService, oUrl, instance_position); } } essb_self_postcount(oService, instance_post_id); if (typeof(essb_abtesting_logger) != "undefined") { essb_abtesting_logger(oService, instance_post_id, oInstance); } var pollTimer = window.setInterval(function() { if (wnd.closed !== false) { window.clearInterval(pollTimer); essb_smart_onclose_events(oService, instance_post_id); } }, 200); }; var essb_self_postcount = function(oService, oCountID) { if (typeof(essb_settings) != "undefined") { oCountID = String(oCountID); jQuery.post(essb_settings.ajax_url, { 'action': 'essb_self_postcount', 'post_id': oCountID, 'service': oService, 'nonce': essb_settings.essb3_nonce }, function (data) { if (data) { }},'json'); } }; var essb_smart_onclose_events = function(oService, oPostID) { if (oService == "subscribe" || oService == "comments") return; if (typeof (essbasc_popup_show) == 'function') { essbasc_popup_show(); } if (typeof essb_acs_code == 'function') { essb_acs_code(oService, oPostID); } if (typeof(after_share_easyoptin) != "undefined") { essb_toggle_subscribe(after_share_easyoptin); } }; var essb_tracking_only = function(oUrl, oService, oInstance, oAfterShare) { var element = jQuery('.essb_'+oInstance); if (oUrl == "") { oUrl = document.URL; } var instance_post_id = jQuery(element).attr("data-essb-postid") || ""; var instance_position = jQuery(element).attr("data-essb-position") || ""; if (typeof(essb_settings) != "undefined") { if (essb_settings.essb3_stats) { if (typeof(essb_handle_stats) != "undefined") { essb_handle_stats(oService, instance_post_id, oInstance); } } if (essb_settings.essb3_ga) { essb_ga_tracking(oService, oUrl, instance_position); } } essb_self_postcount(oService, instance_post_id); if (oAfterShare) { essb_smart_onclose_events(oService, instance_post_id); } }; var essb_pinterest_picker = function(oInstance) { essb_tracking_only('', 'pinterest', oInstance); var e=document.createElement('script'); e.setAttribute('type','text/javascript'); e.setAttribute('charset','UTF-8'); e.setAttribute('src','//assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e); };var essb_mailform_opened = false; function essb_open_mailform(unique_id) { jQuery.fn.extend({ center: function () { return this.each(function() { var top = (jQuery(window).height() - jQuery(this).outerHeight()) / 2; var left = (jQuery(window).width() - jQuery(this).outerWidth()) / 2; jQuery(this).css({position:'fixed', margin:0, top: (top > 0 ? top : 0)+'px', left: (left > 0 ? left : 0)+'px'}); }); } }); if (essb_mailform_opened) { essb_close_mailform(unique_id); return; } var sender_element = jQuery(".essb_"+unique_id); if (!sender_element.length) return; var sender_post_id = jQuery(sender_element).attr("data-essb-postid") || ""; jQuery("#essb_mail_instance").val(unique_id); jQuery("#essb_mail_post").val(sender_post_id); var win_width = jQuery( window ).width(); var win_height = jQuery(window).height(); var doc_height = jQuery('document').height(); var base_width = 300; if (win_width < base_width) { base_width = win_width - 30; } var height_correction = 20; var element_class = ".essb_mailform"; var element_class_shadow = ".essb_mailform_shadow"; jQuery(element_class).css( { width: base_width+'px'}); var popup_height = jQuery(element_class).outerHeight(); if (popup_height > (win_height - 30)) { jQuery(element_class).css( { height: (win_height - height_correction)+'px'}); } jQuery("#essb_mailform_from").val(""); jQuery("#essb_mailform_to").val(""); if (jQuery("#essb_mailform_c").length) jQuery("#essb_mailform_c").val(""); jQuery(element_class_shadow).css( { height: (win_height)+'px'}); jQuery(element_class).center(); jQuery(element_class).slideDown(200); jQuery(element_class_shadow).fadeIn(200); essb_mailform_opened = true; essb_tracking_only("", "mail", unique_id); }; function essb_close_mailform() { var element_class = ".essb_mailform"; var element_class_shadow = ".essb_mailform_shadow"; jQuery(element_class).fadeOut(200); jQuery(element_class_shadow).fadeOut(200); essb_mailform_opened = false; }; function essb_mailform_send() { var sender_email = jQuery("#essb_mailform_from").val(); var recepient_email = jQuery("#essb_mailform_to").val(); var captcha_validate = jQuery("#essb_mailform_c").length ? true : false; var captcha = captcha_validate ? jQuery("#essb_mailform_c").val() : ""; var custom_message = jQuery("#essb_mailform_custom").length ? jQuery("#essb_mailform_custom").val() : ""; if (sender_email == "" || recepient_email == "" || (captcha == "" && captcha_validate)) { alert("Please fill all fields in form!"); return; } var mail_salt = jQuery("#essb_mail_salt").val(); var instance_post_id = jQuery("#essb_mail_post").val(); console.log("mail salt = " + mail_salt); if (typeof(essb_settings) != "undefined") { jQuery.post(essb_settings.ajax_url, { "action": "essb_mail_action", "post_id": instance_post_id, "from": sender_email, "to": recepient_email, "c": captcha, "cu": custom_message, "salt": mail_salt, "nonce": essb_settings.essb3_nonce }, function (data) { if (data) { console.log(data); alert(data["message"]); if (data["code"] == "1") essb_close_mailform(); }},'json'); } };
      </script>
   </body>
</html>
