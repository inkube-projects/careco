<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>CARECO | CASA COMO TÚ</title>
      <link href="resources_1/css/bootstrap.min.css" rel="stylesheet">
      <link href="resources_1/css/animate.min.css" rel="stylesheet">
      <link href="plugins/font-awesome/web-fonts-with-css/css/fontawesome-all.min.css" rel="stylesheet">
      <link href="resources_1/css/lightbox.css" rel="stylesheet">
      <link href="resources_1/css/main.css" rel="stylesheet">
      <link href="resources_1/css/presets/preset1.css" id="css-preset" rel="stylesheet">
      <link href="resources_1/css/responsive.css" rel="stylesheet">
      <!--[if lt IE 9]>
         <script src="js/html5shiv.js"></script>
         <script src="js/respond.min.js"></script>
      <![endif]-->
      <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
      <link rel="shortcut icon" href="images/favicon.ico">
      <link href="resources_1/css/css-style.css" rel="stylesheet">
   </head>
   <!--/head-->

   <body>
      <!--.preloader-->
      <div class="preloader"> <i class="fas fa-circle-notch fa-spin"></i></div>
      <!--/.preloader-->

      <header id="home">
         <nav class="css-nav-top">
            <div class="container">
               <div class="col-md-6">
                  <i class="fas fa-phone"></i> +34 687 715 531
                  <i class="fas fa-envelope css-marginL30"></i> careco@careco.es
               </div>

               <div class="col-md-6 text-right">
                  <a href="#"><i class="fab fa-facebook-square"></i></a>
                  <a href="#"><i class="fab fa-youtube"></i></a>
                  <a href="#"><i class="fab fa-twitter-square"></i></a>
                  <a href="#"><i class="fab fa-pinterest"></i></a>
                  <a href="#"><i class="fab fa-linkedin"></i></a>
               </div>
            </div>
         </nav>

         <nav class="navbar navbar-default">
            <div class="container">
               <div class="container-fluid">
                  <div class="navbar-header">
                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                     </button>
                     <a class="navbar-brand" href="#">
                        <img src="images/logo-high-n-01.png" alt="CARECO" width="120">
                     </a>
                  </div>

                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav navbar-right">
                        <li><a href="current-promotion-sales.php">Promociones en curso <div class="css-nav-separator"></div></a></li>
                        <li class="dropdown">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Próximas promociones <div class="css-nav-separator"></div></a>
                           <ul class="dropdown-menu">
                              <li><a href="next-promotions.php"><i class="fa fa-chevron-right"></i> Avd. Francia (Alameda)</a></li>
                              <li><a href="next-promotions.php"><i class="fa fa-chevron-right"></i> Avd. Francia (Bolinches)</a></li>
                              <li><a href="next-promotions.php"><i class="fa fa-chevron-right"></i> Rocafort</a></li>
                              <li><a href="next-promotions.php"><i class="fa fa-chevron-right"></i> Náquera</a></li>
                              <li><a href="next-promotions.php"><i class="fa fa-chevron-right"></i> Picanya</a></li>
                           </ul>
                        </li>
                        <li><a href="about-us.php">Careco <div class="css-nav-separator"></div></a></li>
                        <li><a href="#">Blog <div class="css-nav-separator"></div></a></li>
                        <li><a href="contact.php">Atención al cliente <div class="css-nav-separator"></div></a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </nav>
         <!--/#main-nav-->

         <div id="home-slider" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
               <div class="item active" style="background-image: url(images/slider/1.jpg)">
                  <div class="caption">
                     <div class="container">
                        <div class="col-md-6 text-left">
                           <a data-scroll class="btn btn-start btn-green animated fadeInUpBig btn-slide" href="#">CASAS CÓMO TÚ</a>
                           <p class="animated fadeInRightBig css-text-black css-medium css-marginT20">Sea como sea tu vida, en CARECO tenemos tu casa</p>
                        </div>

                        <div class="col-md-6">
                           <!-- <h1 class="animated fadeInLeftBig">CAR<span>ECO</span></h1> -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- <a class="left-control" href="#home-slider" data-slide="prev"><i class="fa fa-angle-left"></i></a>
            <a class="right-control" href="#home-slider" data-slide="next"><i class="fa fa-angle-right"></i></a> -->
            <a id="tohash" href="#portfolio"><i class="fa fa-angle-down css-text-black"></i></a>
         </div>
      </header>


      <section id="portfolio">
         <div class="container">
            <div class="col-md-6 css-borde-right">
               <div class="text-right">
                  <h3 class="css-promotion-title"><span class="css-text-black">PROMOCIONES</span> EN CURSO<i class="fas fa-angle-down css-text-green css-marginL10"></i></h3>
                  <p>Lorem ipsum dolor sit amet, consectetur <br>adipiscing elit. Nam dapibus sapien nec leo laoreet,<br> eu interdum est varius.</p>
                  <a href="#" class="btn btn-start">Ver todas</a>
               </div>

               <div class="row css-marginT30">
                  <div class="col-md-6">
                     <div class="folio-item wow fadeInRightBig" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="folio-image css-promotion-panel" style="background-image: url('images/promotion/1.jpg')">
                           <div class="css-tag">
                              Venta
                           </div>
                        </div>

                        <div class="overlay">
                           <div class="overlay-content">
                              <div class="overlay-text">
                                 <div class="folio-info">
                                    <h3 class="css-marginB0 css-semibold css-marginT20">Promoción de viviendas</h3>
                                    <p class="css-marginT0 css-semibold">Avd de Francia (Valencia)</p>
                                    <p class="pull-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                    <i class="fas fa-angle-right pull-right css-fontSize20"></i>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>


                  <div class="col-md-6">
                     <div class="folio-item wow fadeInRightBig" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="folio-image css-promotion-panel" style="background-image: url('images/promotion/2.jpg')">
                           <div class="css-tag">
                              Alquiler
                           </div>
                        </div>
                        <div class="overlay">
                           <div class="overlay-content">
                              <div class="overlay-text">
                                 <div class="folio-info">
                                    <h3 class="css-marginB0 css-semibold css-marginT20">Promoción de viviendas</h3>
                                    <p class="css-marginT0 css-semibold">Avd de Francia (Valencia)</p>
                                    <p class="pull-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                    <i class="fas fa-angle-right pull-right css-fontSize20"></i>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="row fadeInRight" data-wow-duration="1000ms" data-wow-delay="300ms">
                  <div class="col-md-6">
                     <p class="css-text-black"><span class="css-bold">Picanya</span> <span class="css-text-green">(Valencia)</span></p>
                  </div>

                  <div class="col-md-6">
                     <p class="css-text-black"><span class="css-bold">Avd de Francia</span> <span class="css-text-green">(Valencia)</span></p>
                  </div>
               </div>
            </div>

            <div class="col-md-6">
               <div class="text-left">
                  <h3 class="css-promotion-title"><i class="fas fa-angle-down css-text-green css-marginR10"></i> PRÓXIMAS <span class="css-text-black">PROMOCIONES</span></h3>
                  <p>Lorem ipsum dolor sit amet, consectetur <br>adipiscing elit. Nam dapibus sapien nec leo laoreet,<br> eu interdum est varius.</p>
                  <a href="#" class="btn btn-start">Ver todas</a>
               </div>

               <div class="row css-marginT30">
                  <div class="col-md-6">
                     <div class="folio-item wow fadeInRightBig" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="folio-image css-promotion-panel" style="background-image: url('images/promotion/1.jpg')">
                           <div class="css-tag">
                              Venta
                           </div>
                        </div>

                        <div class="overlay">
                           <div class="overlay-content">
                              <div class="overlay-text">
                                 <div class="folio-info">
                                    <h3 class="css-marginB0 css-semibold css-marginT20">Promoción de viviendas</h3>
                                    <p class="css-marginT0 css-semibold">Avd de Francia (Valencia)</p>
                                    <p class="pull-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                    <i class="fas fa-angle-right pull-right css-fontSize20"></i>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>


                  <div class="col-md-6">
                     <div class="folio-item wow fadeInRightBig" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="folio-image css-promotion-panel" style="background-image: url('images/promotion/2.jpg')">
                           <div class="css-tag">
                              Alquiler
                           </div>
                        </div>
                        <div class="overlay">
                           <div class="overlay-content">
                              <div class="overlay-text">
                                 <div class="folio-info">
                                    <h3 class="css-marginB0 css-semibold css-marginT20">Promoción de viviendas</h3>
                                    <p class="css-marginT0 css-semibold">Avd de Francia (Valencia)</p>
                                    <p class="pull-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                    <i class="fas fa-angle-right pull-right css-fontSize20"></i>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="row fadeInRight" data-wow-duration="1000ms" data-wow-delay="300ms">
                  <div class="col-md-6">
                     <p class="css-text-black"><span class="css-bold">Picanya</span> <span class="css-text-green">(Valencia)</span></p>
                  </div>

                  <div class="col-md-6">
                     <p class="css-text-black"><span class="css-bold">Avd de Francia</span> <span class="css-text-green">(Valencia)</span></p>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section id="features">
         <div class="">
            <div class="container">
               <h2 class="css-semibold">CARECO</h2>
               <p>
                  <i class="fas fa-quote-left"></i>
                  Llevamos trabajando casi 70 años, <br> para ser un referente de calidad en todos nuestros proyectos
                  <i class="fas fa-quote-right"></i>
               </p>
            </div>
         </div>
      </section>

      <section id="services">
         <div class="container">
            <div class="text-center our-services">
               <div class="row">
                  <div class="col-sm-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                     <div class="service-icon">
                        <i class="fas fa-home"></i>
                     </div>
                     <div class="service-info">
                        <h3 class="css-text-black css-semibold">Careco</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                        <a href="#" class="btn btn-start">+ Info</a>
                     </div>
                  </div>

                  <div class="col-sm-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                     <div class="service-icon">
                        <i class="fas fa-sitemap"></i>
                     </div>
                     <div class="service-info">
                        <h3 class="css-text-black css-semibold">Nuestros hitos</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                        <a href="#" class="btn btn-start">+ Info</a>
                     </div>
                  </div>

                  <div class="col-sm-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                     <div class="service-icon">
                        <i class="fas fa-building"></i>
                     </div>
                     <div class="service-info">
                        <h3 class="css-text-black css-semibold">Nuestras obras</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                        <a href="#" class="btn btn-start">+ Info</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section><!--/#services-->

      <section id="client">
         <div>
            <a class="twitter-left-control" href="#client-carousel" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
            <a class="twitter-right-control" href="#client-carousel" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
            <div class="container">
               <div class="row">
                  <div class="col-md-11 css-noFloat center-block">
                     <div class="twitter-icon text-center">
                        <h4 class="css-marginB0 css-semibold">Que opinan nuestros clientes</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                     </div>

                     <div id="client-carousel" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                           <div class="item active wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
                              <div class="col-md-4">
                                 <img src="images/clients/user2-160x160.jpg" class="img-circle img-thumbnail center-block" width="80" alt="">
                                 <div class="text-center">
                                    <p class="css-text-green css-marginB0 css-bold">Nombre</p>
                                    <p>Empresa o ciudad</p>
                                 </div>
                                 <div class="css-client-panel">
                                    <p>
                                       <i class="fas fa-quote-left css-text-green"></i>
                                       Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum bibendum augue sit amet arcu fermentum varius.
                                       <i class="fas fa-quote-right css-text-green"></i>
                                    </p>
                                 </div>
                              </div>

                              <div class="col-md-4">
                                 <img src="images/clients/user2-160x160.jpg" class="img-circle img-thumbnail center-block" width="80" alt="">
                                 <div class="text-center">
                                    <p class="css-text-green css-marginB0 css-bold">Nombre</p>
                                    <p>Empresa o ciudad</p>
                                 </div>
                                 <div class="css-client-panel">
                                    <p>
                                       <i class="fas fa-quote-left css-text-green"></i>
                                       Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum bibendum augue sit amet arcu fermentum varius.
                                       <i class="fas fa-quote-right css-text-green"></i>
                                    </p>
                                 </div>
                              </div>

                              <div class="col-md-4">
                                 <img src="images/clients/user2-160x160.jpg" class="img-circle img-thumbnail center-block" width="80" alt="">
                                 <div class="text-center">
                                    <p class="css-text-green css-marginB0 css-bold">Nombre</p>
                                    <p>Empresa o ciudad</p>
                                 </div>
                                 <div class="css-client-panel">
                                    <p>
                                       <i class="fas fa-quote-left css-text-green"></i>
                                       Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum bibendum augue sit amet arcu fermentum varius.
                                       <i class="fas fa-quote-right css-text-green"></i>
                                    </p>
                                 </div>
                              </div>
                           </div>
                           <div class="item">
                              <div class="col-md-4">
                                 <img src="images/clients/user2-160x160.jpg" class="img-circle img-thumbnail center-block" width="80" alt="">
                                 <div class="text-center">
                                    <p class="css-text-green css-marginB0 css-bold">Nombre</p>
                                    <p>Empresa o ciudad</p>
                                 </div>
                                 <div class="css-client-panel">
                                    <p>
                                       <i class="fas fa-quote-left css-text-green"></i>
                                       Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum bibendum augue sit amet arcu fermentum varius.
                                       <i class="fas fa-quote-right css-text-green"></i>
                                    </p>
                                 </div>
                              </div>

                              <div class="col-md-4">
                                 <img src="images/clients/user2-160x160.jpg" class="img-circle img-thumbnail center-block" width="80" alt="">
                                 <div class="text-center">
                                    <p class="css-text-green css-marginB0 css-bold">Nombre</p>
                                    <p>Empresa o ciudad</p>
                                 </div>
                                 <div class="css-client-panel">
                                    <p>
                                       <i class="fas fa-quote-left css-text-green"></i>
                                       Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum bibendum augue sit amet arcu fermentum varius.
                                       <i class="fas fa-quote-right css-text-green"></i>
                                    </p>
                                 </div>
                              </div>

                              <div class="col-md-4">
                                 <img src="images/clients/user2-160x160.jpg" class="img-circle img-thumbnail center-block" width="80" alt="">
                                 <div class="text-center">
                                    <p class="css-text-green css-marginB0 css-bold">Nombre</p>
                                    <p>Empresa o ciudad</p>
                                 </div>
                                 <div class="css-client-panel">
                                    <p>
                                       <i class="fas fa-quote-left css-text-green"></i>
                                       Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum bibendum augue sit amet arcu fermentum varius.
                                       <i class="fas fa-quote-right css-text-green"></i>
                                    </p>
                                 </div>
                              </div>
                           </div>
                           <div class="item">
                              <div class="col-md-4">
                                 <img src="images/clients/user2-160x160.jpg" class="img-circle img-thumbnail center-block" width="80" alt="">
                                 <div class="text-center">
                                    <p class="css-text-green css-marginB0 css-bold">Nombre</p>
                                    <p>Empresa o ciudad</p>
                                 </div>
                                 <div class="css-client-panel">
                                    <p>
                                       <i class="fas fa-quote-left css-text-green"></i>
                                       Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum bibendum augue sit amet arcu fermentum varius.
                                       <i class="fas fa-quote-right css-text-green"></i>
                                    </p>
                                 </div>
                              </div>

                              <div class="col-md-4">
                                 <img src="images/clients/user2-160x160.jpg" class="img-circle img-thumbnail center-block" width="80" alt="">
                                 <div class="text-center">
                                    <p class="css-text-green css-marginB0 css-bold">Nombre</p>
                                    <p>Empresa o ciudad</p>
                                 </div>
                                 <div class="css-client-panel">
                                    <p>
                                       <i class="fas fa-quote-left css-text-green"></i>
                                       Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum bibendum augue sit amet arcu fermentum varius.
                                       <i class="fas fa-quote-right css-text-green"></i>
                                    </p>
                                 </div>
                              </div>

                              <div class="col-md-4">
                                 <img src="images/clients/user2-160x160.jpg" class="img-circle img-thumbnail center-block" width="80" alt="">
                                 <div class="text-center">
                                    <p class="css-text-green css-marginB0 css-bold">Nombre</p>
                                    <p>Empresa o ciudad</p>
                                 </div>
                                 <div class="css-client-panel">
                                    <p>
                                       <i class="fas fa-quote-left css-text-green"></i>
                                       Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum bibendum augue sit amet arcu fermentum varius.
                                       <i class="fas fa-quote-right css-text-green"></i>
                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <ol class="carousel-indicators">
                           <li data-target="#client-carousel" data-slide-to="0" class="active"></li>
                           <li data-target="#client-carousel" data-slide-to="1"></li>
                           <li data-target="#client-carousel" data-slide-to="2"></li>
                        </ol>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!--/#twitter-->

      <section id="blog">
         <div class="container">
            <div class="row">
               <div class="heading text-center col-sm-8 col-sm-offset-2 wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="300ms">
                  <h2 class="css-semibold css-fontSize20 css-text-black">Blog de Careco</h2>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam</p>
               </div>
            </div>
            <div class="blog-posts">
               <div class="row">
                  <div class="col-sm-3 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="400ms">
                     <div class="post-thumb">
                        <a href="#"><img class="img-responsive" src="images/blog/1.jpg" alt=""></a>
                     </div>
                     <div class="entry-header">
                        <span class="date">02/02/2018</span>
                        <h3 class="css-marginT0 css-fontSize16"><a href="#">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h3>
                     </div>
                     <div class="entry-content">
                        <p class="css-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                        <a href="#" class="btn btn-start css-paddingL40 css-paddingR40">+ Info</a>
                     </div>
                  </div>

                  <div class="col-sm-3 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="600ms">
                     <div class="post-thumb">
                        <a href="#"><img class="img-responsive" src="images/blog/2.jpg" alt=""></a>
                     </div>
                     <div class="entry-header">
                        <span class="date">02/02/2018</span>
                        <h3 class="css-marginT0 css-fontSize16"><a href="#">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h3>
                     </div>
                     <div class="entry-content">
                        <p class="css-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                        <a href="#" class="btn btn-start css-paddingL40 css-paddingR40">+ Info</a>
                     </div>
                  </div>

                  <div class="col-sm-3 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="800ms">
                     <div class="post-thumb">
                        <a href="#"><img class="img-responsive" src="images/blog/3.jpg" alt=""></a>
                     </div>
                     <div class="entry-header">
                        <span class="date">02/02/2018</span>
                        <h3 class="css-marginT0 css-fontSize16"><a href="#">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h3>
                     </div>
                     <div class="entry-content">
                        <p class="css-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                        <a href="#" class="btn btn-start css-paddingL40 css-paddingR40">+ Info</a>
                     </div>
                  </div>

                  <div class="col-sm-3 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="1000ms">
                     <div class="post-thumb">
                        <a href="#"><img class="img-responsive" src="images/blog/4.jpg" alt=""></a>
                     </div>
                     <div class="entry-header">
                        <span class="date">02/02/2018</span>
                        <h3 class="css-marginT0 css-fontSize16"><a href="#">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h3>
                     </div>
                     <div class="entry-content">
                        <p class="css-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                        <a href="#" class="btn btn-start css-paddingL40 css-paddingR40">+ Info</a>
                     </div>
                  </div>
               </div>

               <div class="load-more wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="500ms">
                  <a href="#" class="btn-loadmore"><i class="fa fa-repeat"></i> Visitar el Blog</a>
               </div>
            </div>
         </div>
      </section>
      <!--/#blog-->

      <section id="contact">
         <div class="col-md-6 css-paddingL90 css-paddingR70 css-background-green css-paddingT50 css-paddingB30 css-min-height950">
            <h2 class="css-bold css-text-green css-fontSize22">ATENCIÓN AL CLIENTE</h2>
            <p class="css-text-black css-justify">En Careco llevamos desde 1947 anticipándonos al cambio. Creemos que sólo así se puede alcanzar la calidad de vida, por eso, diseñamos, construimos y promovemos viviendas y construcciones tecnológicamente avanzadas que han sido pensadas para disfrutar del mañana. <span class="css-bolditalic">Hoy por hoy, vamos por delante.</span></p>

            <h4 class="css-bold css-text-green css-marginT40 css-fontSize16">SEDE CENTRAL</h4>
            <p class="css-text-black">
               Carrer del Mar, 44 <br>
               46003 Valéncia, Valencia
            </p>

            <p class="css-text-black">
               T. +34 96 315 43 80 <br>
               M. +34 687 715 531 <br>
               <b>Email: careco@careco.es</b>
            </p>

            <div class="col-md-10 css-noPadding css-marginT50">
               <div id="google-map" class="wow fadeIn" data-latitude="52.365629" data-longitude="4.871331" data-wow-duration="1000ms" data-wow-delay="400ms"></div>
            </div>
         </div>

         <div class="col-md-6 css-background-gray css-paddingT50 css-min-height950 css-paddingL70 css-paddingR90">
            <form id="main-contact-form" name="contact-form" method="post" action="#">
               <i class="fas fa-comments css-text-green css-fontSize35 css-marginB30"></i>
               <div class="row  wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label>¿Qué te interesa?</label>
                        <select class="form-control" name="regime" id="regime">
                           <option value="">Régimen</option>
                           <option value="1">Opción 1</option>
                           <option value="2">Opción 2</option>
                           <option value="3">Opción 3</option>
                           <option value="4">Opción 4</option>
                           <option value="5">Opción 5</option>
                        </select>
                     </div>
                  </div>

                  <div class="col-sm-6">
                     <div class="form-group">
                        <div style="height:29px"></div>
                        <select class="form-control" name="zone" id="zone">
                           <option value="">Zona</option>
                           <option value="1">Opción 1</option>
                           <option value="2">Opción 2</option>
                           <option value="3">Opción 3</option>
                           <option value="4">Opción 4</option>
                           <option value="5">Opción 5</option>
                        </select>
                     </div>
                  </div>

                  <div class="col-sm-6">
                     <div class="form-group">
                        <label>Nombre(s)*</label>
                        <input type="text" class="form-control" name="name" id="name">
                     </div>
                  </div>

                  <div class="col-sm-6">
                     <div class="form-group">
                        <label>Apellido(s)*</label>
                        <input type="text" class="form-control" name="last_name" id="last_name">
                     </div>
                  </div>

                  <div class="col-sm-6">
                     <div class="form-group">
                        <label>Teléfono*</label>
                        <input type="text" class="form-control" name="phone" id="phone">
                     </div>
                  </div>

                  <div class="col-sm-6">
                     <div class="form-group">
                        <label>E-mail*</label>
                        <input type="text" class="form-control" name="email" id="email">
                     </div>
                  </div>
               </div>

               <div class="form-group">
                  <label>Cuéntanos</label>
                  <textarea name="message" id="message" class="form-control" rows="4"></textarea>
               </div>

               <div class="form-group">
                  <button type="submit" class="btn-submit">Envíar</button>
               </div>

               <div class="checkbox">
                  <label class="css-regular">
                     <input type="checkbox"> Acepto los términos expuestos:
                  </label>
               </div>

               <p class="css-text-terms">Los datos de carácter personal que se faciliten mediante este formulario quedarán registrados en un fichero de titularidad privada, debidamente inscrito en el RGPD, cuyo responsable es CARECO S.A. y serán utilizados para contestar a las consultas planteadas. Ud. puede ejercitar los derechos de acceso, rectificación, cancelación  y oposición según lo establecido en el Título III del Reglamento de desarrollo de la LOPD (RD 1720/2007 de 21 de diciembre) <br> visite nuestro <a href="#"> Aviso Legal</a> y <a href="#">Política de Privacidad</a></p>
            </form>
         </div>

         <div class="clearfix"></div>
      </section>

      <section class="css-newsletter">
         <div class="container">
            <h2 class="css-marginB0 css-medium">
               <i class="fa fa-envelope"></i><br>
               Suscíbete a nuestra Newsletter
            </h2>
            <p class="css-fontSize14">Siempre estarás informado de todo lo que ocurre en Careco <br> (Noticias, Novedades, Consejos, Promociones, etc...)</p>

            <div class="input-group css-newsletter-container">
               <input type="text" class="form-control" placeholder="Introduce tu e-mail">

               <div class="input-group-btn">
                  <button type="button" class="btn btn-default">Envíar</button>
               </div>
            </div>
         </div>
      </section>

      <section class="css-info">
         <div class="container wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
            <h2 class="text-center css-medium css-fontSize20">Certificaciones y premios</h2>
            <img src="images/certifications.jpg" class="img-responsive css-noFloat center-block" alt="">
         </div>
      </section>

      <footer id="footer">
         <div class="footer-top wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
            <div class="container text-center">
               <h2 class="css-text-black css-semibold fontSize30">CARECO</h2>
               <p class="css-text-black">Copyright&copy; 2018 CARECO S.A. Carrer del Mar, 44  <br> 46003 Vléncia, Valencia</p>

               <div class="col-md-6 text-center css-noFloat center-block">
                  <a href="#"><i class="fab fa-facebook-square"></i></a>
                  <a href="#"><i class="fab fa-youtube"></i></a>
                  <a href="#"><i class="fab fa-twitter-square"></i></a>
                  <a href="#"><i class="fab fa-pinterest"></i></a>
                  <a href="#"><i class="fab fa-linkedin"></i></a>
               </div>

               <div class="css-links-footer">
                  <a href="current-promotion-sales.php">Promociones en curso</a> |
                  <a href="next-promotions.php">Próximas promociones</a> |
                  <a href="#">Careco</a> |
                  <a href="#">Blog</a> |
                  <a href="contact.php">Atención al cliente</a> |
                  <a href="#">Legal</a>
               </div>
            </div>
         </div>
      </footer>

      <script type="text/javascript" src="resources_1/js/jquery.js"></script>
      <script type="text/javascript" src="resources_1/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyAfoFMSNU6Vr-JaVW4BN86B_VGkCwowO_w&sensor=true"></script>
      <script type="text/javascript" src="resources_1/js/jquery.inview.min.js"></script>
      <script type="text/javascript" src="resources_1/js/wow.min.js"></script>
      <script type="text/javascript" src="resources_1/js/mousescroll.js"></script>
      <script type="text/javascript" src="resources_1/js/smoothscroll.js"></script>
      <script type="text/javascript" src="resources_1/js/jquery.countTo.js"></script>
      <script type="text/javascript" src="resources_1/js/lightbox.min.js"></script>
      <script type="text/javascript" src="resources_1/js/main.js"></script>
   </body>
</html>
