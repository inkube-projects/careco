$(function(e){

   var form_name = $('#key').data('form');
   var acc = $('#key').data('acc');

   $.validator.addMethod("noSpecialCharacters", function(value, element, arg){
      var response = true;
      if (value.match(arg)) {
         response = false;
      }
      return response;
   }, "No special characters allowed");

   $("#" + form_name).validate({
      rules:{
         description:{
            required: true,
            noSpecialCharacters: /([*+^${}><|\[\]\/\\])/g
         },
      },
      messages: {
         description:{
            required: "Debes agregar el nombre de la categoría",
            noSpecialCharacters: "No se permiten caracteres especiales"
         },
      },
      errorPlacement: function (error, element) {
         var name = $(element).attr("name");
         error.appendTo($("#" + name + "_validate"));
      },
      submitHandler: function(form) {
         $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');

         persistIcon(form_name, acc);
         return false;
      }
   });
});

function persistIcon(form_name, acc){
   var formData = new FormData(document.getElementById(form_name));
   var ext_url;

   if (acc == 2) {
      ext_url = "&t=" + $('#key').data('id');
   }

   $.ajax({
		type: 'POST',
		url: 'ajax.php?m=tag&a=' + acc + ext_url,
		dataType: 'json',
		data: formData,
		cache: false,
		contentType: false,
		processData: false,
		success: function(r){
			if (r.status == "OK") {
				$(location).attr('href', 'index.php?view=tagList')
			} else {
            $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
            $(".btn-submit").attr("disabled", false).html('Guardar');
			}

		}
	});
}
