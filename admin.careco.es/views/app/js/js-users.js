$(function(e){
    var form_name = $("#key").data('form');
    var acc = $("#key").data('acc');

    $(".select2").select2();

    /**
    * Se verifica que la imagen de portada sea una imagen válida
    * @param  {event} e Evento
    * @return {string}
    */
    $("#profile_image").on("change", function(e){
        var img = $(this).val();

        if (!(/\.(jpg|png|gif)$/i).test(img)) {
            $(".btn-submit-profile-image").addClass('btn-warning').html("Debes ingresar una imagen válida (jpg, jpeg, png, gif)").attr('disabled', true);
        } else {
            $(".btn-submit-profile-image").removeClass('btn-warning').addClass('btn-success').html("Guardar imagen de portada").attr('disabled', false);
        }
    });

    /**
    * Se envía la imagen de portada
    * @param  {event} e Evento por defecto
    * @return {boolean}
    */
    $(".btn-submit-profile-image").on("click", function(e){
        $("#mod-img-cover").modal('show');
        $("#ajx-img-cover").html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        $("#ajx-img").html("");
        saveImgAjax(form_name, 'ajax.php?m=user&acc=1', 'ajx-img-cover');
        return false;
    });

    $.validator.addMethod("noSpecialCharacters", function(value, element, arg){
        var response = true;
        if (value.match(arg)) {
            response = false;
        }
        return response;
    }, "No special characters allowed");

    $("#frm-add").validate({
        rules:{
            name:{
                required: true,
                noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
            },
            last_name: {
                required: true,
                noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
            },
            username: {
                required: true,
                noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
            },
            pass_1: {
                required: true,
            },
            pass_2: {
                required: true,
                equalTo: "#pass_1"
            },
            email: {
                required: true,
                email: true
            },
            role: {
                required: true,
            },
            status: {
                required: true,
            },
            phone_1: {
                number: true,
            },
            phone_2: {
                number: true,
            }
        },
        messages: {
            name:{
                required: "Debes ingresar el nombre del usuario",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            last_name: {
                required: "Debes ingresar el apellido del usuario",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            username: {
                required: "Debes ingresar un nombre de usuario",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            pass_1: {
                required: "Debes ingresar una contraseña",
            },
            pass_2: {
                required: "Debes repetir la contraseña",
                equalTo: "Las contraseñas deben ser iguales"
            },
            email: {
                required: "Debes ingresar un email",
                email: "Debes ingresar un email válido"
            },
            role: {
                required: "Debes seleccionar un rol",
            },
            status: {
                required: "Debes seleccionar un estado de la cuenta",
            },
            phone_1: {
                number: "Solo se permiten números",
            },
            phone_2: {
                number: "Solo se permiten números",
            }
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function(form) {
            persist(acc, form_name);
            return false;
        }
    });

    $("#frm-edit").validate({
        rules:{
            name:{
                required: true,
                noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
            },
            last_name: {
                required: true,
                noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
            },
            username: {
                required: true,
                noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
            },
            pass_2: {
                equalTo: "#pass_1"
            },
            email: {
                required: true,
                email: true
            },
            role: {
                required: true,
            },
            status: {
                required: true,
            },
        },
        messages: {
            name:{
                required: "Debes ingresar el nombre del usuario",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            last_name: {
                required: "Debes ingresar el apellido del usuario",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            username: {
                required: "Debes ingresar un nombre de usuario",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            pass_2: {
                equalTo: "Las contraseñas deben ser iguales"
            },
            email: {
                required: "Debes ingresar un email",
                email: "Debes ingresar un email válido"
            },
            role: {
                required: "Debes seleccionar un rol",
            },
            status: {
                required: "Debes seleccionar un estado de la cuenta",
            },
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function(form) {
            persist(acc, form_name);
            return false;
        }
    });
});

function persist(acc, form_name) {
    var i = $("#" + form_name).serialize();
    var ext_url = "";

    if (acc == 2) {
        ext_url = "&id=" + $('#key').data('id');
    }

    $.ajax({
        type: 'POST',
        url: 'ajax.php?m=user&acc=' + acc + ext_url,
        data: i,
        dataType: 'json',
        beforeSend: function(e){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", "index.php?view=userEdit&id=" + r.id + "&m=OK" + acc);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}
