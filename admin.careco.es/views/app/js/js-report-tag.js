$(function(e){
   var form_name = $("#key").data('form');
   var acc = $("#key").data('acc');

   // Seleccionador de color
   $(".js-colorpicker").colorpicker();

   $.validator.addMethod("noSpecialCharacters", function(value, element, arg){
      var response = true;
      if (value.match(arg)) {
         response = false;
      }
      return response;
   }, "No special characters allowed");

   $("#" + form_name).validate({
      rules:{
         name:{
            required: true,
            noSpecialCharacters: /([*+^${}><|\[\]\\])/g
         },
         color:{
            required: true,
            noSpecialCharacters: /([*+^${}><|\[\]\\])/g
         },
      },
      messages: {
         name:{
            required: "Debes ingresar el nombre del color",
            noSpecialCharacters: "No se permiten caracteres especiales"
         },
         color: {
            required: "Debes ingresar el código del color",
            noSpecialCharacters: "No se permiten caracteres especiales"
         }
      },
      errorPlacement: function (error, element) {
         var name = $(element).attr("name");

         error.appendTo($("#" + name + "_validate"));
      },
      submitHandler: function(form) {
         persistAction(acc, form_name);
         return false;
      }
   });
});

function persistAction(acc, form_name) {
   var i = $("#" + form_name).serialize();
   var ext_url = "";

   if (acc == 2) {
      ext_url = "&id=" + $('#key').data('id');
   }

   $.ajax({
      type: 'POST',
      url: 'ajax.php?m=report-tag&acc=' + acc + ext_url,
      data: i,
      dataType: 'json',
      beforeSend: function(e){
          $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
      },
      success: function(r){
         if (r.status == 'OK') {
            $(location).attr("href", "index.php?view=reportTagEdit&id=" + r.id + "&m=OK" + acc);
         } else {
            $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
            $(".btn-submit").attr("disabled", false).html('Guardar');
            $(document).scrollTop(0);
         }
      }

   });
}
