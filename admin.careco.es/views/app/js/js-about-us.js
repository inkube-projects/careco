$(function(e){
   // Se inicia el editor de texto CKeditor
   CKEDITOR.replace('new_description', {
      customConfig : '../../js/js-ckeditor-config.js'
   });

   // Inicia el ck editor para los demas textarea
   $("input[type=hidden]").each(function(){
      var val_id = $(this).val();

      CKEDITOR.replace('description_' + val_id, {
         customConfig : '../../js/js-ckeditor-config.js'
      });
   });

   $(".image").on('change', function(e){
      $(this).parent().addClass('btn-success').removeClass('btn-default');
   });

   // Método adicional de caracteres especiales del jQueryValidation
   $.validator.addMethod("noSpecialCharacters", function(value, element, arg){
      var response = true;
      if (value.match(arg)) {
         response = false;
      }
      return response;
   }, "No special characters allowed");

   $("#frm-about-us-add").validate({
      rules:{
         new_title:{
            required: true,
            noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
         },
         new_image: {
            extension: "png|jpg|jpeg|gif",
         },
      },
      messages: {
         new_title:{
            required: "Debes ingresar un título",
            noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
         },
         new_image: {
            extension: "Debes ingresar una imagen válida (png,jpg,jpeg,gif)",
         },
      },
      errorPlacement: function (error, element) {
         var name = $(element).attr("name");
         error.appendTo($("#" + name + "_validate"));
      },
      submitHandler: function(form) {
         if (CKEDITOR.instances.new_description.getData() == "") {
            $('#new_description_validate').html('<label id="new_title-error" class="error" for="new_title">Debes ingresar una descripción</label>');
            return false;
         }

         $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
         $('#new_description').val(CKEDITOR.instances.new_description.getData());
         persistSection();

         return false;
      }
   });

   $("#frm-about-us-edit").validate({
      rules:{
         "title[]":{
            required: true,
            noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
         },
         "image[]": {
            extension: "png|jpg|jpeg|gif",
         }
      },
      messages: {
         "title[]":{
            required: "Debes ingresar un título",
            noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
         },
         "image[]": {
            extension: "Debes ingresar una imagen válida (png,jpg,jpeg,gif)",
         }
      },
      errorPlacement: function (error, element) {
         var name = $(element).attr("id");
         error.appendTo($("#" + name + "_validate"));
      },
      submitHandler: function(form) {
         for(var i in CKEDITOR.instances) {
            if (CKEDITOR.instances[i].name != "new_description") {
               if (CKEDITOR.instances[i].getData() == "") {
                  $("#" + CKEDITOR.instances[i].name + "_validate").html('<label id="new_title-error" class="error" for="new_title">Debes ingresar una descripción</label>');
                  return false;
               } else {
                  $("#" + CKEDITOR.instances[i].name).val(CKEDITOR.instances[i].getData());
               }
            }
         }

         $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
         editSection();
         return false;
      }
   });
});

/**
 * Guarda una sección nueva
 * @return {void}
 */
function persistSection() {
   var i = new FormData(document.getElementById('frm-about-us-add'));

   $.ajax({
      type: 'POST',
      url: 'ajax.php?m=aboutUs&acc=1',
      data: i,
      cache: false,
      contentType: false,
      processData: false,
      dataType: 'json',
      success: function(r){
         if (r.status == 'OK') {
            $(location).attr("href", "index.php?view=aboutUs&m=OK1");
         } else {
            $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
            $(".btn-submit").attr("disabled", false).html('Guardar');
            $(document).scrollTop(0);
         }
      }

   });
}

function editSection() {
   var i = new FormData(document.getElementById('frm-about-us-edit'));

   $.ajax({
      type: 'POST',
      url: 'ajax.php?m=aboutUs&acc=2',
      data: i,
      cache: false,
      contentType: false,
      processData: false,
      dataType: 'json',
      success: function(r){
         if (r.status == 'OK') {
            $(location).attr("href", "index.php?view=aboutUs&m=OK2");
         } else {
            $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
            $(".btn-submit").attr("disabled", false).html('Guardar');
            $(document).scrollTop(0);
         }
      }

   });
}
