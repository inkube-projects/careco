$(function(e){
   var form_name = $("#key").data('form');
   var acc = $("#key").data('acc');

   // select2
   $(".select2").select2();


   // Mostrar imagenes temporales en modal
   $('#ajx-gallery').on('click', '.btn-show', function(e){
      var image = $(this).data('image');
      $('#mod-image').modal('show');
      $('#js-image').html('<img src="' + image + '" class="img-responsive css-noFloat center-block">');
      e.preventDefault();
   });

   // Mostrar imagenes de galería
   $('#js-gallery').on('click', '.btn-show', function(e){
      var image = $(this).data('image');
      $('#mod-image').modal('show');
      $('#js-image').html('<img src="' + image + '" class="img-responsive css-noFloat center-block">');
      e.preventDefault();
   });

   // Subir imagenes
   $('#gallery_images').on('change', function(e){
      if ($(this).val() != "") {
         $('.btn-file-gallery').addClass('btn-success').removeClass('btn-default');
         $('.btn-file-gallery span').html('Subiendo imágenes, por favor espere... <i class="fa fa-spinner fa-spin"></i>');
         $('.btn-submit').attr('disabled', true);
         uploadImages(form_name);
      }
   });

   // Cambia la clase de los botones de adjuntar archivos
   $('.btn-files-adj').on('change', function(e){
      if ($('#' + this.id + ' input').val() != "") {
         $(this).addClass('btn-success').removeClass('btn-default');
      }
   });


   // checkbox
   $('.button-checkbox').each(function () {
      // Settings
      var $widget = $(this),
      $button = $widget.find('button'),
      $checkbox = $widget.find('input:checkbox'),
      color = $button.data('color'),
      settings = {
         on: {
            icon: 'glyphicon glyphicon-check'
         },
         off: {
            icon: 'glyphicon glyphicon-unchecked'
         }
      };

      // Event Handlers
      $button.on('click', function () {
         $checkbox.prop('checked', !$checkbox.is(':checked'));
         $checkbox.triggerHandler('change');
         updateDisplay();
      });
      $checkbox.on('change', function () {
         updateDisplay();
      });

      // Actions
      function updateDisplay() {
         var isChecked = $checkbox.is(':checked');
         var inp_id;

         // Set the button's state
         $button.data('state', (isChecked) ? "on" : "off");

         // Set the button's icon
         $button.find('.state-icon')
         .removeClass()
         .addClass('state-icon ' + settings[$button.data('state')].icon);

         // Update the button's color
         if (isChecked) {
            $button
            .removeClass('btn-default')
            .addClass('btn-' + color + ' active');

            inp_id = $button.data('input');
            $('#' + inp_id).attr('checked', true);
         } else {
            $button
            .removeClass('btn-' + color + ' active')
            .addClass('btn-default');

            inp_id = $button.data('input');
            $('#' + inp_id).attr('checked', false);
         }
      }

      // Initialization
      function init() {
         updateDisplay();

         // Inject the icon if applicable
         if ($button.find('.state-icon').length == 0) {
            $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
         }
      }
      init();
   });

   $.validator.addMethod("noSpecialCharacters", function(value, element, arg){
      var response = true;
      if (value.match(arg)) {
         response = false;
      }
      return response;
   }, "No special characters allowed");

   $.validator.addMethod("numberValidation", function(value, element, arg){
      var response = true;

      if (value != "") {
         if(!arg.test(value)) {
            response = false;
         }
      }
      return response;
   }, "No special characters allowed");

   $("#" + form_name).validate({
      rules:{
         title:{
            required: true,
            noSpecialCharacters: /([*+^${}><|\[\]\\])/g
         },
         cover_title:{
            required: true,
            noSpecialCharacters: /([*+^${}><|\[\]\\])/g
         },
         code:{
            noSpecialCharacters: /([*+^${}><|\[\]\\])/g
         },
         promotion:{
            required: true,
         },
         description: {
            required: true,
            /*noSpecialCharacters: /([*+^${}><|\[\]\\])/g*/
         },
         location: {
            required: true,
            noSpecialCharacters: /([*+^${}><|\[\]\\])/g
         },
         cost: {
            numberValidation: /^[0-9.,]+$/
         },
         monthly_cost: {
            numberValidation: /^[0-9.,]+$/
         },
         "desc[]": {
            noSpecialCharacters: /([*+^${}><|\[\]\\])/g
         },
         blueprints_image: {
            extension: "png|jpg|jpeg|gif"
         },
         quality_memories_doc: {
            extension: "pdf"
         },
         blueprints_doc: {
            extension: "pdf"
         },
         cee_doc: {
            extension: "pdf"
         },
         file_pdf_doc: {
            extension: "pdf"
         }
      },
      messages: {
         title:{
            required: "Debes ingresar el título de la publicación",
            noSpecialCharacters: "No se permiten caracteres especiales"
         },
         cover_title:{
            required: "Debes ingresar el título de la publicación",
            noSpecialCharacters: "No se permiten caracteres especiales"
         },
         code:{
            noSpecialCharacters: "No se permiten caracteres especiales",
         },
         promotion:{
            required: "Debes seleccionar una promoción para que la propiedad se visible",
         },
         description: {
            required: "Debes ingresar la descripción de la propiedad",
            /*noSpecialCharacters: "No se permiten caracteres especiales"*/
         },
         location: {
            required: "Debes ingresar la ubicación de la propiedad",
            noSpecialCharacters: "No se permiten caracteres especiales"
         },
         cost: {
            numberValidation: "Ingrese un monto válido"
         },
         monthly_cost: {
            numberValidation: "Ingrese un monto válido"
         },
         "desc[]": {
            noSpecialCharacters: "No se permiten caracteres especiales"
         },
         blueprints_image: {
            extension: "Debes ingresar una imagen válida (png,jpg,jpeg,gif)"
         },
         quality_memories_doc: {
            extension: "Debes ingresar un archivo PDF"
         },
         blueprints_doc: {
            extension: "Debes ingresar un archivo PDF"
         },
         cee_doc: {
            extension: "Debes ingresar un archivo PDF"
         },
         file_pdf_doc: {
            extension: "Debes ingresar un archivo PDF"
         }
      },
      errorPlacement: function (error, element) {
         var name = $(element).attr("name");

         if (name == "desc[]") {
            name = $(element).attr('id');
         }

         error.appendTo($("#" + name + "_validate"));
      },
      submitHandler: function(form) {
         $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
         persistPropertie(acc, form_name);
         return false;
      }
   });
});

function persistPropertie(acc, form_name) {
   // var i = $("#" + form_name).serialize();
   var i = new FormData(document.getElementById(form_name));
   var ext_url = "";

   if (acc == 4) {
      ext_url = "&p=" + $('#key').data('id');
   }

   $.ajax({
      type: 'POST',
      url: 'ajax.php?m=properties&acc=' + acc + ext_url,
      data: i,
      cache: false,
      contentType: false,
      processData: false,
      dataType: 'json',
      success: function(r){
         if (r.status == 'OK') {
            $(location).attr("href", "index.php?view=propertiesEdit&id=" + r.id + "&m=OK" + acc);
         } else {
            $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
            $(".btn-submit").attr("disabled", false).html('Guardar');
            $(document).scrollTop(0);
         }
      }

   });
}

/**
 * Se encarga de subir las imágenes de galería
 * @param  {string} form_name Nombre del formulario
 * @return {boolean}
 */
function uploadImages(form_name) {
   var formData = new FormData(document.getElementById(form_name));

   $.ajax({
      type: 'POST',
      url: 'ajax.php?m=properties&acc=1',
      dataType: 'json',
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      success: function(r){
         if (r.status == "OK") {
            $('#ajx-gallery').html(r.data);
         } else {
            $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
            $(document).scrollTop(0);
         }

         $('#gallery_images').val('');
         $('.btn-file-gallery').removeClass('btn-success').addClass('btn-default');
         $('.btn-file-gallery span').html('Agregar imágenes');
         $('.btn-submit').attr('disabled', false);
      }
   });
   return false;
}

/**
 * Elimina imágenes temporales de galería
 * @param  {integer} acc   Acción a realizar
 * @param  {string} image Nombre de la imagen
 * @return {void}
 */
function removeImage(acc, image) {
   $.ajax({
      type: 'GET',
      url: 'ajax.php?m=properties&acc=' + acc + '&img=' + image,
      dataType: 'json',
      success: function(r) {
         if (r.status == "OK") {
            $('#ajx-gallery').html(r.data);
            $('.btn-file-gallery').removeClass('btn-success').addClass('btn-default');
            $('.btn-file-gallery span').html('Agregar imágenes');
            $('.btn-submit').attr('disabled', false);
         } else {
            $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
         }
      }
   });
}

/**
 * Muestra una imagen en un modal
 * @param  {string} image Ruta y nombre de la imagen
 * @param  {event} e     Evento del click
 * @return {void}
 */
function showImageModal(image, e) {
   $("#mod-image").modal('show');
   $('#js-image').html('<img src="' + image + '" class="img-responsive css-noFloat center-block">');
   e.preventDefault();
}

function removeGalleryImage(id, e){
   if (confirm('¿Seguro de que deseas eliminar la imagen de galería?')) {
      $.ajax({
         type: 'GET',
         url: 'ajax.php?m=properties&acc=5&id=' + id,
         dataType: 'json',
         success: function(r) {
            if (r.status == "OK") {
               $('#js-gallery').html(r.data);
            } else {
               $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
            }
         }
      });
   }

   e.preventDefault();
}

function duplicate(id) {
    if (confirm('¿Seguro de que deseas duplicar esta propiedad?')) {
        $.ajax({
            type: 'GET',
            url: 'ajax.php?m=properties&acc=6&id=' + id,
            dataType: 'json',
            beforeSend: function(e){
                $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
                $("#mod-duplicate").modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $("#ajx-duplicate").html('<i class="fa fa-spinner fa-spin"></i> Duplicando, no cierres esta ventana...');
            },
            success: function(r) {
                if (r.status == "OK") {
                    $(location).attr("href", "index.php?view=propertiesEdit&id=" + r.id + "&m=OK3");
                } else {
                    $("#ajx-duplicate").html(r.message);
                }
            }
        });
    }
}
