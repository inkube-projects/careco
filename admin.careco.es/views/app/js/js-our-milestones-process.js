$(function(e){
   var form_name = $("#key").data('form');
   var acc = $("#key").data('acc');

   $.validator.addMethod("noSpecialCharacters", function(value, element, arg){
      var response = true;
      if (value.match(arg)) {
         response = false;
      }
      return response;
   }, "No special characters allowed");

   $("#" + form_name).validate({
      rules:{
         title: {
            required: true,
            noSpecialCharacters: /([*+^${}><|\[\]\/\\])/g
         },
         description: {
            required: true,
            noSpecialCharacters: /([*+^${}><|\[\]\/\\])/g
         },
      },
      messages: {
         title: {
            required: "Debes ingresar un título",
            noSpecialCharacters: "No se permiten caracteres especiales"
         },
         description: {
            required: "Debes ingresar la descripción",
            noSpecialCharacters: "No se permiten caracteres especiales"
         },
      },
      errorPlacement: function (error, element) {
         var name = $(element).attr("name");
         error.appendTo($("#" + name + "_validate"));
      },
      submitHandler: function(form) {
         $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
         persisEntrie(form_name, acc);
         return false;
      }
   });
});

function persisEntrie(form_name, acc){
   var i = $('#' + form_name).serialize();
   var ext;

   if (acc == 3) {
      ext = "&h=" + $('#key').data('id');
   }


   $.ajax({
      type: 'POST',
      url: 'ajax.php?m=ourMilestones&acc=' + acc + ext,
      dataType: 'json',
      data: i,
      success: function(r) {
         if (r.status == "OK") {
				$(location).attr("href", "index.php?view=ourMilestonesEdit&id=" + r.id + "&m=OK" + acc);
			} else {
            $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
            $(".btn-submit").attr("disabled", false).html('Guardar');
			}
      }
   });
}
