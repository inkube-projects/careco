$(function(e){
   var form_name = $("#key").data('form');
   var acc = $("#key").data('acc');

   /**
    * Se verifica que la imagen de portada sea una imagen válida
    * @param  {event} e Evento
    * @return {string}
    */
   $("#profile_image").on("change", function(e){
      var img = $(this).val();

      if (!(/\.(jpg|png|gif)$/i).test(img)) {
         $(".btn-submit-profile-image").addClass('btn-warning').html("Debes ingresar una imagen válida (jpg, jpeg, png, gif)").attr('disabled', true);
      } else {
         $(".btn-submit-profile-image").removeClass('btn-warning').addClass('btn-success').html("Guardar imagen de portada").attr('disabled', false);
      }
   });

   /**
    * Se envía la imagen de portada
    * @param  {event} e Evento por defecto
    * @return {boolean}
    */
   $(".btn-submit-profile-image").on("click", function(e){
      $("#mod-img-cover").modal('show');
      $("#ajx-img-cover").html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
      $("#ajx-img").html("");
      saveImgAjax(form_name, 'ajax.php?m=opinion&acc=1', 'ajx-img-cover');
      return false;
   });

   $.validator.addMethod("noSpecialCharacters", function(value, element, arg){
      var response = true;
      if (value.match(arg)) {
         response = false;
      }
      return response;
   }, "No special characters allowed");

   $("#" + form_name).validate({
      rules:{
         name:{
            required: true,
            noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
         },
         last_name: {
            required: true,
            noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
         },
         company: {
            noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
         },
         description: {
            required: true,
            noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
         }
      },
      messages: {
         name:{
            required: "Debes ingresar un nombre",
            noSpecialCharacters: "No se permiten caracteres especiales"
         },
         last_name: {
            required: "Debes ingresar un apellido",
            noSpecialCharacters: "No se permiten caracteres especiales"
         },
         company: {
            noSpecialCharacters: "No se permiten caracteres especiales"
         },
         description: {
            required: "Debes ingresar la opinión del cliente",
            noSpecialCharacters: "No se permiten caracteres especiales"
         }
      },
      errorPlacement: function (error, element) {
         var name = $(element).attr("name");
         error.appendTo($("#" + name + "_validate"));
      },
      submitHandler: function(form) {
         $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
         persistOpinion(acc, form_name);
         return false;
      }
   });
});

function persistOpinion(acc, form_name) {
   var i = $("#" + form_name).serialize();
   var ext_url;

   if (acc == 4) {
      ext_url = "&o=" + $('#key').data('id');
   }

   $.ajax({
      type: 'POST',
      url: 'ajax.php?m=opinion&acc=' + acc + ext_url,
      data: i,
      dataType: 'json',
      success: function(r){
         if (r.status == 'OK') {
            $(location).attr("href", "index.php?view=opinionsEdit&id=" + r.id + "&m=OK" + acc);
         } else {
            $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
            $(".btn-submit").attr("disabled", false).html('Guardar');
            $(document).scrollTop(0);
         }
      }

   });
}
