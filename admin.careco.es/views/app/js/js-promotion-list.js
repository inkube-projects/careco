$(function(e){
   // select2
   $(".select2").select2();

   //Limpiar el formulario de búsqueda
   $('.btn-reset').on('click', function(e){
      $('#frm-search select').val('').trigger('change');
      $('#frm-search input[type=text]').val('');
   });

   // Configuración de la paginación con DataTable
   $('.table').DataTable({
      "language": {
         "lengthMenu": "Mostrar _MENU_ entradas por página",
         "zeroRecords": "No se han encontrado resultados",
         "info": "Mostrando página _PAGE_ de _PAGES_",
         "infoEmpty": "No se han encontrado resultados",
         "infoFiltered": "(Se ha filtrado un total de _MAX_ entradas)",
         "search": "Buscar: ",
         "paginate": {
            "previous": "Anterior",
            "next": "Siguiente"
         }
      }
   });

   $('.button-checkbox').each(function () {

      // Settings
      var $widget = $(this),
      $button = $widget.find('button'),
      $checkbox = $widget.find('input:checkbox'),
      color = $button.data('color'),
      settings = {
         on: {
            icon: 'glyphicon glyphicon-check'
         },
         off: {
            icon: 'glyphicon glyphicon-unchecked'
         }
      };

      // Event Handlers
      $button.on('click', function () {
         $checkbox.prop('checked', !$checkbox.is(':checked'));
         $checkbox.triggerHandler('change');
         updateDisplay();
      });
      $checkbox.on('change', function () {
         updateDisplay();
      });

      // Actions
      function updateDisplay() {
         var isChecked = $checkbox.is(':checked');

         // Set the button's state
         $button.data('state', (isChecked) ? "on" : "off");

         // Set the button's icon
         $button.find('.state-icon')
         .removeClass()
         .addClass('state-icon ' + settings[$button.data('state')].icon);

         // Update the button's color
         if (isChecked) {
            $button
            .removeClass('btn-default')
            .addClass('btn-' + color + ' active');
         } else {
            $button
            .removeClass('btn-' + color + ' active')
            .addClass('btn-default');
         }
      }

      // Initialization
      function init() {
         updateDisplay();

         // Inject the icon if applicable
         if ($button.find('.state-icon').length == 0) {
            $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
         }
      }
      init();
   });
});
