$(function(e){

   var form_name = $('#key').data('form');
   var acc = $('#key').data('acc');

   $('#icon_image').on('change', function(e){
      var value = $(this).val();
      if (value != "") {
         $('#img-container').addClass('btn-success').removeClass('btn-default');
         $('#img-container span').html('Imagen agregada');
      } else {
         $('#img-container').removeClass('btn-success').addClass('btn-default');
         $('#img-container span').html('Agregar imagen de icono');
      }
   });

   $.validator.addMethod("noSpecialCharacters", function(value, element, arg){
      var response = true;
      if (value.match(arg)) {
         response = false;
      }
      return response;
   }, "No special characters allowed");

   $("#" + form_name).validate({
      rules:{
         description:{
            required: true,
            noSpecialCharacters: /([*+^${}><|\[\]\/\\])/g
         },
         icon_image: {
            extension: "jpg|jpeg|png|gif"
         }
      },
      messages: {
         description:{
            required: "Debes agregar la descripción del icono",
            noSpecialCharacters: "No se permiten caracteres especiales"
         },
         icon_image: {
            extension: "Debes ingresar una imagen valida (jpg, jpeg, png, gif)"
         }
      },
      errorPlacement: function (error, element) {
         var name = $(element).attr("name");
         error.appendTo($("#" + name + "_validate"));
      },
      submitHandler: function(form) {
         $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');

         persistIcon(form_name, acc);
         return false;
      }
   });
});

function persistIcon(form_name, acc){
   var formData = new FormData(document.getElementById(form_name));
   var ext_url;

   if (acc == 2) {
      ext_url = "&i=" + $('#key').data('id');
   }

   $.ajax({
		type: 'POST',
		url: 'ajax.php?m=icon&a=' + acc + ext_url,
		dataType: 'json',
		data: formData,
		cache: false,
		contentType: false,
		processData: false,
		success: function(r){
			if (r.status == "OK") {
				$(location).attr('href', 'index.php?view=icon')
			} else {
            $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
            $(".btn-submit").attr("disabled", false).html('Guardar');
			}

		}
	});
}
