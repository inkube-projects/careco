$(function(e){
   var form_name = $("#key").data('form');
   var acc = $("#key").data('acc');

   CKEDITOR.replace('location', {
      customConfig : '../../js/js-ckeditor-config.js'
   });

   // select2
   $(".select2").select2();

   /**
    * Se verifica que la imagen de portada sea una imagen válida
    * @param  {event} e Evento
    * @return {string}
    */
   $("#cover_image").on("change", function(e){
      var img = $(this).val();

      if (!(/\.(jpg|png|gif)$/i).test(img)) {
         $(".btn-submit-cover-image").addClass('btn-warning').html("Debes ingresar una imagen válida (jpg, jpeg, png, gif)").attr('disabled', true);
      } else {
         $(".btn-submit-cover-image").removeClass('btn-warning').addClass('btn-success').html("Guardar imagen de portada").attr('disabled', false);
      }
   });

   // Subir imagenes de galería
   $('#gallery_images').on('change', function(e){
      if ($(this).val() != "") {
         $('.btn-file-gallery').addClass('btn-success').removeClass('btn-default');
         $('.btn-file-gallery span').html('Subiendo imágenes, por favor espere... <i class="fa fa-spinner fa-spin"></i>');
         $('.btn-submit').attr('disabled', true);
         uploadImages(form_name);
      }
   });

   // Mostrar imagenes temporales en modal
   $('#ajx-gallery').on('click', '.btn-show', function(e){
      var image = $(this).data('image');
      $('#mod-image').modal('show');
      $('#js-image').html('<img src="' + image + '" class="img-responsive css-noFloat center-block">');
      e.preventDefault();
   });

   // Mostrar imagenes de galería
   $('#js-gallery').on('click', '.btn-show', function(e){
      var image = $(this).data('image');
      $('#mod-image').modal('show');
      $('#js-image').html('<img src="' + image + '" class="img-responsive css-noFloat center-block">');
      e.preventDefault();
   });

   /**
    * Se envía la imagen de portada
    * @param  {event} e Evento por defecto
    * @return {boolean}
    */
   $(".btn-submit-cover-image").on("click", function(e){
      $("#mod-img-cover").modal('show');
      $("#ajx-img-cover").html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
      $("#ajx-img").html("");
      saveImgAjax(form_name, 'ajax.php?m=promotion&acc=1', 'ajx-img-cover');
      return false;
   });

   $.validator.addMethod("noSpecialCharacters", function(value, element, arg){
      var response = true;
      if (value.match(arg)) {
         response = false;
      }
      return response;
   }, "No special characters allowed");

   $("#" + form_name).validate({
      rules:{
         title:{
            required: true,
            noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
         },
         category: {
            required: true,
         },
         description: {
            noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
         },
         location: {
            noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
         },
         latitude: {
            noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
         },
         longitude: {
            noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
         }
      },
      messages: {
         title:{
            required: "Debes ingresar el título de la promoción",
            noSpecialCharacters: "No se permiten caracteres especiales"
         },
         category: {
            required: "Debes seleccionar una categoría",
         },
         description: {
            noSpecialCharacters: "No se permiten caracteres especiales"
         },
         location: {
            noSpecialCharacters: "No se permiten caracteres especiales"
         },
         latitude: {
            noSpecialCharacters: "No se permiten caracteres especiales"
         },
         longitude: {
            noSpecialCharacters: "No se permiten caracteres especiales"
         }
      },
      errorPlacement: function (error, element) {
         var name = $(element).attr("name");
         error.appendTo($("#" + name + "_validate"));
      },
      submitHandler: function(form) {
         $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');

         // Se le añade el valor del ckeditor al textarea
         $('#location').val(CKEDITOR.instances.location.getData());

         persistPromo(acc, form_name);
         return false;
      }
   });
});

function persistPromo(acc, form_name) {
   var i = $("#" + form_name).serialize();
   var ext_url = "";

   if (acc == 4) {
      ext_url = "&p=" + $('#key').data('id');
   }

   $.ajax({
      type: 'POST',
      url: 'ajax.php?m=promotion&acc=' + acc + ext_url,
      data: i,
      dataType: 'json',
      success: function(r){
         if (r.status == 'OK') {
            $(location).attr("href", "index.php?view=promotionEdit&id=" + r.id + "&m=OK" + acc);
         } else {
            $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
            $(".btn-submit").attr("disabled", false).html('Guardar');
         }
      }

   });
}

/**
 * Se encarga de subir las imágenes de galería
 * @param  {string} form_name Nombre del formulario
 * @return {boolean}
 */
function uploadImages(form_name) {
   var formData = new FormData(document.getElementById(form_name));

   $.ajax({
      type: 'POST',
      url: 'ajax.php?m=promotion&acc=5',
      dataType: 'json',
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      success: function(r){
         if (r.status == "OK") {
            $('#ajx-gallery').html(r.data);

         } else {
            $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
         }

         $('#gallery_images').val('');
         $('.btn-file-gallery').removeClass('btn-success').addClass('btn-default');
         $('.btn-file-gallery span').html('Agregar imágenes');
         $('.btn-submit').attr('disabled', false);
      }
   });
   return false;
}

/**
 * Elimina imágenes temporales de galería
 * @param  {integer} acc   Acción a realizar
 * @param  {string} image Nombre de la imagen
 * @return {void}
 */
function removeImage(acc, image) {
   $.ajax({
      type: 'GET',
      url: 'ajax.php?m=promotion&acc=' + acc + '&img=' + image,
      dataType: 'json',
      success: function(r) {
         if (r.status == "OK") {
            $('#ajx-gallery').html(r.data);
            $('.btn-file-gallery').removeClass('btn-success').addClass('btn-default');
            $('.btn-file-gallery span').html('Agregar imágenes');
            $('.btn-submit').attr('disabled', false);
         } else {
            $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
         }
      }
   });
}

function removeGalleryImage(id, e){
   if (confirm('¿Seguro de que deseas eliminar la imagen de galería?')) {
      $.ajax({
         type: 'GET',
         url: 'ajax.php?m=promotion&acc=7&id=' + id,
         dataType: 'json',
         success: function(r) {
            if (r.status == "OK") {
               $('#js-gallery').html(r.data);
            } else {
               $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
            }
         }
      });
   }

   e.preventDefault();
}
