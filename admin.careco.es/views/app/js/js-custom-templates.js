/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

var header = '<tr>'+
	'<td>'+
		'<img src="https://careco.inkube.net/newsletter/images/header.jpg" alt="alt text" height="100" width="600" border="0" style="display: block;" class="fluid">'+
	'</td>'+
'</tr>'+
'<tr>'+
	'<td style="border-bottom: 1px solid #e5e5e5; border-top: 1px solid #e5e5e5;">'+
		'<table border="0" bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="600" class="table600con" style="border-collapse: collapse;">'+
			'<tbody>'+
				'<tr>'+
					'<td width="100%" style="padding-top: 10px;padding-bottom: 10px;border-collapse: collapse;  " align="center" valign="middle">'+
						'<p style="color: #000000; font-weight: normal; font-size: 9pt; font-family:sans-serif; margin: 0; padding: 0;">'+
							'<a href="https://careco.inkube.net/careco.es/index.php?view=currentPromotion" style="color: #000000;font-family: sans-serif;margin-left:10px">PROMOCIONES </a>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>|&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>'+
							'<a href="https://careco.inkube.net/careco.es/index.php?view=nextPromotion" style="color: #000000;">PRÓXIMAS PROMOCIONES</a>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>|&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>'+
							'<a href="https://careco.inkube.net/careco.es/index.php?view=aboutUs" style="color: #000000;">CARECO</a>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>|&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>'+
							'<a href="https://careco.inkube.net/careco.es/index.php?view=blog" style="color: #000000;">BLOG</a>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>|&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>'+
							'<a href="#" style="color: #000000;">CONTACTO</a>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>|&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>'+
						'</p>'+
					'</td>'+
				'</tr>'+
			'</tbody>'+
		'</table>'+
	'</td>'+
'</tr>';

var sub_footer = '<tr>'+
	'<td>'+
		'<a href="#"><img src="https://careco.inkube.net/newsletter/images/subfooter.jpg" alt="alt text" height="100" width="600" border="0" style="display: block;" class="fluid"></a>'+
	'</td>'+
'</tr>';

var footer = '<tr>'+
	'<td style="border-bottom: 1px solid #e5e5e5;">'+
		'<table border="0" width="100%" height="190px" cellspacing="0" align="center" style="background:#7fa9ae !important;text-align:center">'+
			'<tbody>'+
				'<tr>'+
					'<td style="padding: 30px;">'+
							'<a href="http://www.andisspain.com/" style="text-decoration:none;">'+
								'<img src="https://careco.inkube.net/newsletter/images/logo-high-n-01.png" width="177" style="outline : none;max-width:100% display: block; text-align:center;" class="fluid">'+
							'</a><br>'+
							'<a href="https://careco.inkube.net/careco.es/index.php" style="text-decoration:none;font-family: sans-serif; font-size: 14px; color:#000;">'+
								'www.careco.es'+
							'</a><br>'+
							'<p style="text-decoration:none;font-family: sans-serif; font-size: 14px; color:#000;">'+
							'Copyright© 2018 CARECO S.A. <br>Carrer del Mar, 44  46003, Valencia<a href="mailto:careco@careco.es" style="color:#000;text-decoration: none;outline: none;"> careco@careco.es</a>'+
							'</p><br>'+
					'</td>'+
				'</tr>'+
			'</tbody>'+
		'</table>'+
	'</td>'+
'</tr>';

var social_network ='<table cellpadding="10" cellspacing="0" border="0" class="socialCntrBtn" align="center"> '+
	'<tbody>'+
		'<tr>'+
			'<td><a href="https://twitter.com/Andis_Spain?ref_src=twsrc%5Etfw" target="_blank" style="text-decoration: none;"><img src="https://careco.inkube.net/newsletter/images/socialLinkedin.png" alt="IMG" border="0" style="text-decoration: none;border: none;" data-crop="false"></a></td> '+
			'<td><a href="https://www.instagram.com/andisspain/" target="_blank" style="text-decoration: none;"><img src="https://careco.inkube.net/newsletter/images/socialInstram.png" alt="IMG" border="0" style="text-decoration: none;border: none;" data-crop="false"></a></td>'+
		'</tr>'+
	'</tbody>'+
'</table>';
// Register a templates definition set named "default".
CKEDITOR.addTemplates( 'custom', {
	// The name of sub folder which hold the shortcut preview images of the
	// templates.
    imagesPath: CKEDITOR.getUrl( CKEDITOR.plugins.getPath( 'templates' ) + 'templates/images/' ),

	// The templates definitions.
	templates: [ {
		title: '- Plantilla 1 -',
		image: 'template1.gif',
		description: 'careco.inkube.net - Plantilla 1',
		html:'<table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" bgcolor="#C9DDDC" id="bodyTable" style="border-collapse: collapse;table-layout: fixed;margin:0 auto;">'+
			'<tbody>'+
				'<tr>'+
					'<td>'+
						'<table border="0" width="600" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" style="border: 1px solid #C9DDDC;margin: auto;" class="email-container">'+
							'<tbody>'
								+header+
								'<tr>'+
									'<td style="border-bottom: 1px solid #e5e5e5;">'+
										'<table border="0" width="100%" height="106px" cellpadding="0" cellspacing="0" align="center">'+
											'<tbody>'+
												'<tr>'+
													'<td style=" color:#7FA9A3; padding-top: 30px; font-family: sans-serif; font-size: 22px; line-height: 26px; color: #939393; text-align:center; font-weight:300;" align="center">'+
														'<a href="" style="color:#2e5d79; text-decoration:none;"><span style=" color:#7FA9A3; color:#7FA9A3; padding: 30px; font-family: sans-serif; font-size: 22px; line-height: 22px; text-align:center;">Le presentamos nuestra nueva web</strong></span></a>'+
													'</td>'+
												'</tr>'+
											'</tbody>'+
										'</table>'+
									'</td>'+
								'</tr>'+
								'<tr>'+
									'<td style="padding-bottom:30px;">'+
										'<a href="https://careco.inkube.net/careco.es/index.php"><img src="https://careco.inkube.net/newsletter/images/imagen-1.jpg" alt="alt text" height="514" width="600" border="0" style="display: block;" class="fluid"></a>'+
									'</td>'+
								'</tr>'+
								'<tr>'+
									'<td style="border-bottom: 1px solid #e5e5e5;">'+
										'<table border="0" width="100%" height="150px" cellpadding="30" cellspacing="0" align="center">'+
											'<tbody>'+
												'<tr>'+
													'<td style="padding: 30px; font-family: sans-serif; font-size: 16px; line-height: 24px; color: #939393;">'+
															'<span style="padding: 30px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #939393;">'+
																'Como culminación al proceso de renovación de la nueva imagen corporativa de la empresa, y la necesidad de ofrecer nuevos canales de comunicación con sus clientes. Careco presenta su nuevo espacio web.'+
																'En el diseño se ha buscado la máxima simplicidad en la presentación de los contenidos, proporcionando una navegación intuitiva y clara, ofreciendo a los visitantes la imagen de Careco con la mayor transparencia.<br>'+
																'Aquí ofrecemos toda la información sobre nuestra compañía: conocer su historia, la actividad en el sector inmobiliario, junto a todos nuestro parque de inmubles. Todo ello,  junto con un nuevo blog, que le informará de todas aquellas novedades del sector inmobiliario, de ayuda en la búsqueda de vivienda y la actividad de la compañía.'+
															'<span>'+
														'<br><br>'+
													'</td>'+
												'</tr>'+
											'</tbody>'+
										'</table>'+
									'</td>'+
								'</tr>'
								+sub_footer
								+footer+
							'</tbody>'+
						'</table>'
						+social_network+
					'</td>'+
				'</tr>'+
			'</tbody>'+
		'</table>'
	},
	{
		title: '- Plantilla 2 -',
		image: 'template2.gif',
		description: 'careco.inkube.net - Plantilla 2',
		html: '<table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" bgcolor="#C9DDDC" id="bodyTable" style="border-collapse: collapse;table-layout: fixed;margin:0 auto;">'+
			'<tbody>'+
				'<tr>'+
					'<td>'+
						'<table border="0" width="600" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" style="border: 1px solid #C9DDDC;margin: auto;" class="email-container">'+
							'<tbody>'
								+header+
								'<tr>'+
									'<td style="border-bottom: 1px solid #e5e5e5;">'+
										'<table border="0" width="100%" height="106px" cellpadding="0" cellspacing="0" align="center">'+
											'<tbody>'+
												'<tr>'+
													'<td style=" color:#7FA9A3; padding-top: 30px; font-family: sans-serif; font-size: 22px; line-height: 26px; color: #939393; text-align:center; font-weight:300;" align="center">'+
														'<a href="" style="color:#7FA9A3; text-decoration:none;"><span style="padding: 30px; font-family: sans-serif; font-size: 22px; line-height: 22px; color: #7FA9A3; text-align:center; font-weight:300;">RESIDENCIAL CARECO PICANYA</span></a>'+
													'</td>'+
												'</tr>'+
											'</tbody>'+
										'</table>'+
									'</td>'+
								'</tr>'+
								'<tr>'+
									'<td style="padding-bottom:30px;">'+
										'<a href="http://www.careco.es/?page_id=7576#francia_alameda"><img src="https://careco.inkube.net/newsletter/images/imagen-1B.jpg" alt="alt text" height="514" width="600" border="0" style="display: block;" class="fluid"></a>'+
									'</td>'+
								'</tr>'+
								'<tr>'+
									'<td style="border-bottom: 1px solid #e5e5e5;">'+
										'<table border="0" width="100%" height="150px" cellpadding="30" cellspacing="0" align="center">'+
											'<tbody>'+
												'<tr>'+
													'<td style="padding: 30px; font-family: sans-serif; font-size: 16px; line-height: 24px; color: #939393;">'+
															'<span style="padding: 30px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #939393;">'+
																'TITULAR PARA EL TEXTO<br>'+
																'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especiment.'+
															'<span>'+
														'<br><br>'+
													'</td>'+
												'</tr>'+
											'</tbody>'+
										'</table>'+
									'</td>'+
								'</tr>'+
								'<tr>'+
									'<td style="border-bottom: 1px solid #e5e5e5;">'+
										'<table border="0" width="100%" height="150px" cellpadding="30" cellspacing="0" align="center">'+
											'<tbody>'+
												'<tr>'+
													'<td style="padding: 30px; font-family: sans-serif; font-size: 16px; line-height: 24px; color: #939393;">'+
															'<span style="padding: 30px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #939393;">'+
																'Incluir Listado de Items'+
															'<span>'+
														'<br><br>'+
													'</td>'+
												'</tr>'+
											'</tbody>'+
										'</table>'+
									'</td>'+
								'</tr>'
								+sub_footer
								+footer+
							'</tbody>'+
						'</table>'
						+social_network+
					'</td>'+
				'</tr>'+
			'</tbody>'+
		'</table>'
	},
	{
		title: '- Plantilla 3 -',
		image: 'template3.gif',
		description: 'careco.inkube.net - Plantilla 3',
		html:'<table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" bgcolor="#C9DDDC" id="bodyTable" style="border-collapse: collapse;table-layout: fixed;margin:0 auto;">'+
			'<tbody>'+
				'<tr>'+
					'<td>'+
						'<table border="0" width="600" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" style="border: 1px solid #C9DDDC;margin: auto;" class="email-container">'+
							'<tbody>'
								+header+
								'<tr>'+
									'<td style="border-bottom: 1px solid #e5e5e5;">'+
										'<table border="0" width="100%" height="106px" cellpadding="0" cellspacing="0" align="center">'+
											'<tbody>'+
												'<tr>'+
													'<td style=" color:#7FA9A3; padding-top: 30px; font-family: sans-serif; font-size: 22px; line-height: 26px; color: #939393; text-align:center; font-weight:300;" align="center">'+
														'<a href="" style="color:#2e5d79; text-decoration:none;"><span style=" color:#7FA9A3; padding: 30px; font-family: sans-serif; font-size: 22px; line-height: 22px; color: #2e5d79; text-align:center; font-weight:300;">AVENIDA DE FRANCIA - <strong>CARECO ALAMEDA</strong></span></a>'+
													'</td>'+
												'</tr>'+
											'</tbody>'+
										'</table>'+
									'</td>'+
								'</tr>'+
								'<tr>'+
									'<td style="padding-bottom:30px;">'+
										'<a href="http://www.careco.es/?page_id=7576#francia_alameda"><img src="https://careco.inkube.net/newsletter/images/imagen-1C.jpg" alt="alt text" height="514" width="600" border="0" style="display: block;" class="fluid"></a>'+
									'</td>'+
								'</tr>'+
								'<tr>'+
									'<td style="border-bottom: 1px solid #e5e5e5;">'+
										'<table border="0" width="100%" height="150px" cellpadding="30" cellspacing="0" align="center">'+
											'<tbody>'+
												'<tr>'+
													'<td style="padding: 30px; font-family: sans-serif; font-size: 16px; line-height: 24px; color: #939393;">'+
															'<span style="padding: 30px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #939393;">'+
																'PRÓXIMA CONSTRUCCIÓN DE CARECO<br>'+
																'Construcción de edificio de 71 viviendas, aparcamiento y locales comerciales <strong>en una de las parcelas más privilegiadas actualmente en Valencia</strong>. En frente de la Ciudad de las Artes y de las Ciencias.'+
															'<span>'+
														'<br><br>'+
													'</td>'+
												'</tr>'+
											'</tbody>'+
										'</table>'+
									'</td>'+
								'</tr>'
								+sub_footer
								+footer+
							'</tbody>'+
						'</table>'
						+social_network+
					'</td>'+
				'</tr>'+
			'</tbody>'+
		'</table>'
	},
	{
		title: '- Plantilla 4 -',
		image: 'template3.gif',
		description: 'careco.inkube.net - Plantilla 4',
		html: '<table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" bgcolor="#C9DDDC" id="bodyTable" style="border-collapse: collapse;table-layout: fixed;margin:0 auto;">'+
			'<tbody>'+
				'<tr>'+
					'<td>'+
						'<table border="0" width="600" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" style="border: 1px solid #C9DDDC;margin: auto;" class="email-container">'+
							'<tbody>'
								+header+
								'<tr>'+
									'<td style="border-bottom: 1px solid #e5e5e5;">'+
										'<table border="0" width="100%" height="106px" cellpadding="0" cellspacing="0" align="center">'+
											'<tbody>'+
												'<tr>'+
													'<td style=" color:#7FA9A3; padding-top: 30px; font-family: sans-serif; font-size: 22px; line-height: 26px; color: #939393; text-align:center; font-weight:300;" align="center">'+
														'<a href="" style="color:#2e5d79; text-decoration:none; font-size: 28px; line-height: 22px; ">CARECO <strong>BLOG</strong></a><br><span style="color: #7FA9A3;font-size: 22px;font-family: sans-serif;"> UN BLOG COMO TÚ</span>'+
													'</td>'+
												'</tr>'+
											'</tbody>'+
										'</table>'+
									'</td>'+
								'</tr>'+
								'<tr>'+
									'<td style="padding-bottom:30px;">'+
										'<a href="http://www.careco.es/?page_id=7576#francia_alameda">'+
											'<img src="https://careco.inkube.net/newsletter/images/imagen-1D.jpg" alt="alt text" height="514" width="600" border="0" style="display: block;" class="fluid">'+
										'</a>'+
									'</td>'+
								'</tr>'+
								'<tr>'+
									'<td style="border-bottom: 1px solid #e5e5e5;">'+
										'<table border="0" width="100%" height="150px" cellpadding="30" cellspacing="0" align="center">'+
											'<tbody>'+
												'<tr>'+
													'<td style="padding: 30px; font-family: sans-serif; font-size: 16px; line-height: 24px; color: #939393;">'+
														'<strong>'+
															'<span style="padding: 30px; font-family: sans-serif; font-size: 16px; line-height: 24px; color: #939393;">'+
																' UN BLOG COMO TÚ'+
															'</span>'+
														'</strong>'+
														'<br>'+
														'<span style="padding: 30px; font-family: sans-serif; font-size: 16px; line-height: 24px; color: #939393;">'+
															'Te proponemos una forma sencilla de estar informado y que está ocurriendo en el sector inmobiliario de forma gemeral pero espceiclamente en Valencia. '+
														'</span>'+
														'<br><br>'+
													'</td>'+
												'</tr>'+
											'</tbody>'+
										'</table>'+
									'</td>'+
								'</tr>'
								+sub_footer
								+footer+
							'</tbody>'+
						'</table>'
						+social_network+
					'</td>'+
				'</tr>'+
			'</tbody>'+
		'</table>'
	} ]
} );
