$(function(e){
    var form_name = $("#key").data('form');
    var acc = $("#key").data('acc');

    $(".select2").select2();

    // checkbox
    $('.button-checkbox').each(function () {
        // Settings
        var $widget = $(this),
        $button = $widget.find('button'),
        $checkbox = $widget.find('input:checkbox'),
        color = $button.data('color'),
        settings = {
            on: {
                icon: 'glyphicon glyphicon-check'
            },
            off: {
                icon: 'glyphicon glyphicon-unchecked'
            }
        };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');
            var inp_id;

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
            .removeClass()
            .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                .removeClass('btn-default')
                .addClass('btn-' + color + ' active');

                inp_id = $button.data('input');
                $('#' + inp_id).attr('checked', true);
            } else {
                $button
                .removeClass('btn-' + color + ' active')
                .addClass('btn-default');

                inp_id = $button.data('input');
                $('#' + inp_id).attr('checked', false);
            }
        }

        // Initialization
        function init() {
            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });

    $.validator.addMethod("noSpecialCharacters", function(value, element, arg){
       var response = true;
       if (value.match(arg)) {
          response = false;
       }
       return response;
    }, "No special characters allowed");

    $("#" + form_name).validate({
        rules:{
            user:{
                required: true,
            },
            message: {
                required: true,
                noSpecialCharacters: /([*+^${}><|\[\]\\])/g
            },
            comment: {
                noSpecialCharacters: /([*+^${}><|\[\]\\])/g
            },
        },
        messages: {
            user:{
                required: "Debes seleccionar un usuario",
            },
            message: {
                required: "Debes ingresar el mensaje del cliente",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            comment: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");

            if (name == "desc[]") {
                name = $(element).attr('id');
            }

            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function(form) {
            persist(acc, form_name);
            return false;
        }
    });
});

function persist(acc, form_name) {
   var i = $("#" + form_name).serialize();
   var ext_url = "";

   if (acc == 2) {
      ext_url = "&r=" + $('#key').data('id');
   }

   $.ajax({
      type: 'POST',
      url: 'ajax.php?m=report&acc=' + acc + ext_url,
      data: i,
      dataType: 'json',
      beforeSend: function(e){
        $(".btn-submit").attr("disabled", true).html('<i class="fas fa-spinner fa-spin"></i> Cargando...');
      },
      success: function(r){
         if (r.status == 'OK') {
            $(location).attr("href", "index.php?view=reportEdit&id=" + r.id + "&m=OK" + acc);
         } else {
            $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
            $(".btn-submit").attr("disabled", false).html('Guardar');
            $(document).scrollTop(0);
         }
      }

   });
}
