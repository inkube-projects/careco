$(function(e){
   // Se inicia el editor de texto CKeditor
   CKEDITOR.replace('description', {
      customConfig : '../../js/js-ckeditor-config.js'
   });

   $('#banner').on('change', function(e){
      $(this).parent().addClass('btn-success').removeClass('btn-default');
   });

   $.validator.addMethod("noSpecialCharacters", function(value, element, arg){
      var response = true;
      if (value.match(arg)) {
         response = false;
      }
      return response;
   }, "No special characters allowed");

   $("#frm-milestones").validate({
      rules:{
         banner: {
            extension: "png|jpg|jpeg|gif",
         },
      },
      messages: {
         banner: {
            extension: "Debes ingresar una imagen válida (png, jpg, jpeg, gif)",
         },
      },
      errorPlacement: function (error, element) {
         var name = $(element).attr("name");
         error.appendTo($("#" + name + "_validate"));
      },
      submitHandler: function(form) {
         if (CKEDITOR.instances.description.getData() == "") {
            $('#description_validate').html('<label id="description-error" class="error" for="description">Debes ingresar una descripción</label>');
            return false;
         } else {
            $('#description').val(CKEDITOR.instances.description.getData());
         }

         $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');

         persistDescription();

         return false;
      }
   });
});


/**
 * Guarda una sección nueva
 * @return {void}
 */
function persistDescription() {
   var i = new FormData(document.getElementById('frm-milestones'));

   $.ajax({
      type: 'POST',
      url: 'ajax.php?m=ourMilestones&acc=1',
      data: i,
      cache: false,
      contentType: false,
      processData: false,
      dataType: 'json',
      success: function(r){
         if (r.status == 'OK') {
            $(location).attr("href", "index.php?view=ourMilestones&m=OK2");
         } else {
            $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
            $(".btn-submit").attr("disabled", false).html('Guardar');
            $(document).scrollTop(0);
         }
      }

   });
}
