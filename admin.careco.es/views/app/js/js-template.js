$(function(e){
    CKEDITOR.replace('email_template', {
        toolbar: [
            { "name": 'document', "items": [ 'Source', '-', 'Preview', 'Print', '-', 'Templates' ] },
    		{ "name": 'clipboard', "items": [ 'Cut', 'Copy', 'Paste', 'Undo', 'Redo' ] },
    		{ "name": 'basicstyles', "items": [ 'Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat' ] },
    		{ "name": 'paragraph', "items": [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-' ] },
    		{ "name": 'links', "items": [ 'Link', 'Unlink' ] },
    		{ "name": 'insert', "items": [ 'Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak' ] },
    		{ "name": 'styles', "items": [ 'Styles', 'Format', 'Font', 'FontSize' ] },
    		{ "name": 'colors', "items": [ 'TextColor', 'BGColor' ] },
    		{ "name": 'tools', "items": [ 'Maximize', 'ShowBlocks' ] },
        ],
    });
    CKEDITOR.config.templates_files = [ 'views/app/js/js-custom-templates.js' ];
    CKEDITOR.config.templates = 'custom';
    CKEDITOR.config.height = 500; 
});
