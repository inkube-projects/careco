<?php
/**
 * Handler que controla las sesiones y las autenticaciones
*/

/**
 * Clase de conexion
 * @var Connection
 */
$db = new Connection();
$user_view = (isset($_GET['view']) && (!empty($_GET['view']))) ? $_GET['view'] : "index" ;
$redirect = (isset($_GET['r']) && (!empty($_GET['r']))) ? $_GET['r'] : "" ;
$role = "ROLE_ANONYMOUS";

$arr_pages = array(
   'notFound' => array('ROLE_ANONYMOUS', 'ROLE_ADMIN', 'ROLE_MODERATOR'),
   'logOut' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'index' => array('ROLE_ANONYMOUS', 'ROLE_ADMIN', 'ROLE_MODERATOR'),
   'home' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'promotionList' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'promotionAdd' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'promotionEdit' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'icon' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'iconEdit' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'propertiesList' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'propertiesAdd' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'propertiesEdit' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'aboutUs' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'ourMilestones' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'ourMilestonesAdd' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'opinionsList' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'opinionsAdd' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'opinionsEdit' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'portfolioList' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'portfolioAdd' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'portfolioEdit' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'tagList' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'tagEdit' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'postList' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'postAdd' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'postEdit' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'newsletter' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'reportTagList' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'reportTagAdd' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'reportTagEdit' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'userList' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'userAdd' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'userEdit' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'reportList' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'reportAdd' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'reportEdit' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'newsletterList' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'newsletterAdd' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'newsletterEdit' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'templateList' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
);

/**
 * Se verifica si existe una sesión, sino se le asigna un Rol anónimo
*/
if (isset($_SESSION['ADMIN_SESSION_CARECO']) && $_SESSION['ADMIN_SESSION_CARECO'] != "") {
   $role = $db->getValue("role", "role", "id='".$_SESSION['ADMIN_SESSION_CARECO']['role']."'");
   $status_account = $_SESSION['ADMIN_SESSION_CARECO']['status'];
   $admin_pp_id = $_SESSION['ADMIN_SESSION_CARECO']['id'];
   $admin_username = $_SESSION['ADMIN_SESSION_CARECO']['username'];
}
foreach ($arr_pages as $key => $val) {
   if ($key == $user_view) {
      if (!in_array($role, $val)) {
         header('location: index.php?view=index&r=1'); exit;
      }
   }
}

if (($role != "ROLE_ANONYMOUS") && $user_view == "index") {
   header('location: index.php?view=home'); exit;
}
?>
