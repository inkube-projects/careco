<?php
/**
 * Handler que controla el acceso a los archivos ajax
*/
$db = new Connection();
$current_ajax = $_GET['m'];

$role = "ROLE_ANONYMOUS";

$arr_ajax = array(
   'login' => array('ROLE_ANONYMOUS'),
   'promotion' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'remove' => array('ROLE_ADMIN'),
   'icon' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'properties' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'aboutUs' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'ourMilestones' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'opinion' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'portfolio' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'tag' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'post' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
);

/**
 * Se verifica si existe una sesión, sino se le asigna un Rol anónimo
*/
if (isset($_SESSION['ADMIN_SESSION_CARECO']) && $_SESSION['ADMIN_SESSION_CARECO'] != "") {
   $role = $db->getValue("role", "role", "id='".$_SESSION['ADMIN_SESSION_CARECO']['role']."'");
   $admin_pp_id = $_SESSION['ADMIN_SESSION_CARECO']['id'];
}

try {
    foreach ($arr_ajax as $key => $val) {
       if ($key == $current_ajax) {
          if (!in_array($role, $val)) {
            throw new \Exception("No tienes acceso a esta acción", 1);
          }
       }
    }
} catch (Exception $e) {
    $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    header('Content-Type: application/json');
    echo json_encode($arr_response);
    exit;
}
?>
