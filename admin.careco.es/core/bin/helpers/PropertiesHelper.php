<?php

/**
 * Clase que se encarga de procesar la información de las propiedades
 */
class PropertiesHelper extends GeneralMethods
{

    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
        $this->admin_id = $_SESSION['ADMIN_SESSION_CARECO']['id'];
    }

    /**
     * Guardando imagenes temporales
     * @return void
     */
    public function uploadImages()
    {
        $arr_lenght = count($_FILES['gallery_images']['tmp_name']);

        // se elimina las imagenes temporales que esten en la carpeta
        $path = APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/properties/";

        for ($i = 0; $i < $arr_lenght; $i++) {
            // se sube la nueva imagen temporal
            $extens = str_replace("image/",".",$_FILES['gallery_images']['type'][$i]);
            $img_name = "property_".rand(00000, 99999).uniqid().$extens;
            list($width, $height) = getimagesize($_FILES["gallery_images"]['tmp_name'][$i]);

            if($width > 1500){
                UpPictureArray('gallery_images', 1500, $img_name, $path, $i);
            }else{
                UpPictureArray('gallery_images', $width, $img_name, $path, $i);
            }
        }

        return $this->getTempImages();
    }

    /**
     * Retorna las miniaturas de todas las imágenes temporales en html
     * @return string
     */
    public function getTempImages()
    {
        $path = APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/properties/";
        $handle = opendir($path);
        $result = "";

        while ($file = readdir($handle)){
            if (is_file($path.$file)) {
                if (substr($file, 0,4) != "min_") {
                    $result .= '
                    <div class="col-md-3 css-marginB20 container-thumb">
                        <div class="css-background-image" style="background-image: url(\''.$path.'min_'.$file.'\')"></div>
                        <a href="#" class="btn btn-info btn-flat center-block btn-show" data-image="'.$path.$file.'"><i class="fas fa-search"></i> Ver imagen</a>
                        <a href="javascript: removeImage(2, \''.$file.'\')" class="btn btn-danger btn-flat center-block"><i class="fas fa-trash-alt"></i> Eliminar</a>
                    </div>
                    ';
                }
            }
        }

        return $result;
    }

    /**
     * Registra una propiedad nueva
     * @return array
     */
    public function persistProperty()
    {
        $arr_fields = array(
            'title',
            'cover_title',
            'description',
            'promotion_id',
            'location',
            'cost',
            'monthly_cost',
            'code',
            'outstanding',
            'status',
            'sold',
            'create_at',
            'update_at'
        );

        $arr_values = array(
            mb_convert_case($_POST['title'], MB_CASE_LOWER, "UTF-8"),
            mb_convert_case($_POST['cover_title'], MB_CASE_LOWER, "UTF-8"),
            addslashes($_POST['description']),
            $_POST['promotion'],
            $_POST['location'],
            $_POST['cost'],
            $_POST['monthly_cost'],
            $_POST['code'],
            $_POST['outstanding'],
            $_POST['status'],
            $_POST['sold'],
            date('Y-m-d H:i:s'),
            date('Y-m-d H:i:s')
        );

        $property = $this->db->insertAction("properties", $arr_fields, $arr_values);

        mkdir(APP_IMG_PROPERTIES."property_".$property[0]['id']."/");
        mkdir(APP_IMG_PROPERTIES."property_".$property[0]['id']."/PDF/");
        mkdir(APP_IMG_PROPERTIES."property_".$property[0]['id']."/images/");
        mkdir(APP_IMG_PROPERTIES."property_".$property[0]['id']."/images/gallery/");

        $this->addLogs(sprintf("Agregando propiedad: %s - id: %d", $property[0]['title'], $property[0]['id']));

        return $property[0];
    }

    public function editProperty($id)
    {
        $arr_fields = array(
            'title',
            'cover_title',
            'description',
            'promotion_id',
            'location',
            'cost',
            'monthly_cost',
            'outstanding',
            'status',
            'sold',
            'update_at'
        );

        $arr_values = array(
            mb_convert_case($_POST['title'], MB_CASE_LOWER, "UTF-8"),
            mb_convert_case($_POST['cover_title'], MB_CASE_LOWER, "UTF-8"),
            $_POST['description'],
            $_POST['promotion'],
            $_POST['location'],
            $_POST['cost'],
            $_POST['monthly_cost'],
            $_POST['outstanding'],
            $_POST['status'],
            $_POST['sold'],
            date('Y-m-d H:i:s')
        );

        $property = $this->db->updateAction("properties", $arr_fields, $arr_values, "id='".$id."'");

        $this->addLogs(sprintf("Editando propiedad: %s - id: %d", $property[0]['title'], $property[0]['id']));

        return $property[0];
    }

    /**
     * Guarda las descripciones/iconos del la propiedad
     * @param  array  $arr_add      Arreglo de los iconos
     * @param  integer $property_id ID de la propiedad
     * @return void
     */
    public function persistIcons(array $arr_add, $property_id)
    {
        $this->db->deleteAction("properties_descriptions", "propertie_id='".$property_id."'");

        foreach ($arr_add as $key => $val) {
            $this->db->insertAction("properties_descriptions", array('propertie_id', 'icon_id', 'description'), array($property_id, $val, $_POST['desc'][$key]));
        }
    }

    /**
     * Guarda la imagen de los planos
     * @param  array $property Arreglo de la propiedad
     * @return void
     */
    public function persistBluePrintsImage($property)
    {
        if ($_FILES["blueprints_image"]['tmp_name'] != "") {
            $path = APP_IMG_PROPERTIES."property_".$property['id']."/images/";
            $extens = str_replace("image/",".",$_FILES['blueprints_image']['type']);
            $img_name = "blueprints_".rand(00000, 99999).uniqid().$extens;

            list($width, $height) = getimagesize($_FILES["blueprints_image"]['tmp_name']);

            if (UpPicture('blueprints_image', 200, $img_name, $path)) {
                if ($property['blueprints']) {
                    @unlink($path.$property['blueprints']);
                    @unlink($path."min_".$property['blueprints']);
                }

                $this->db->updateAction("properties", array('blueprints'), array($img_name), "id='".$property['id']."'");
            }
        }
    }

    /**
     * Se encarga de guardar los archivos pdf
     * @param  array  $property Arreglo de las propiedas
     * @param  integer $acc       Accion a ejecutar
     * @return void
     */
    public function persistPDF(array $property, $acc)
    {
        switch ($acc) {
            case 1: // Guarda la memoria de calidad
                if ($_FILES['quality_memories_doc']['tmp_name'] != "") {
                    $path = APP_IMG_PROPERTIES."property_".$property['id']."/PDF/";

                    $file_name = "quality_".rand(00000, 99999).uniqid().".pdf";

                    if(move_uploaded_file($_FILES['quality_memories_doc']['tmp_name'], $path.$file_name)){
                        if ($property['quality_memories']) {
                            @unlink($path.$property['quality_memories']);
                        }

                        $this->db->updateAction("properties", array('quality_memories'), array($file_name), "id='".$property['id']."'");
                    }
                }
            break;

            case 2: // sube los planos pdf
                if ($_FILES['blueprints_doc']['tmp_name'] != "") {
                    $path = APP_IMG_PROPERTIES."property_".$property['id']."/PDF/";

                    $file_name = "blueprint_".rand(00000, 99999).uniqid().".pdf";

                    if(move_uploaded_file($_FILES['blueprints_doc']['tmp_name'], $path.$file_name)){
                        if ($property['blueprints_doc']) {
                            @unlink($path.$property['blueprints_doc']);
                        }

                        $this->db->updateAction("properties", array('blueprints_doc'), array($file_name), "id='".$property['id']."'");
                    }
                }
            break;

            case 3: // Guarda el C.E.E.
                if ($_FILES['cee_doc']['tmp_name'] != "") {
                    $path = APP_IMG_PROPERTIES."property_".$property['id']."/PDF/";

                    $file_name = "cee_".rand(00000, 99999).uniqid().".pdf";

                    if(move_uploaded_file($_FILES['cee_doc']['tmp_name'], $path.$file_name)){
                        if ($property['cee']) {
                            @unlink($path.$property['cee']);
                        }

                        $this->db->updateAction("properties", array('cee'), array($file_name), "id='".$property['id']."'");
                    }
                }
            break;

            case 4: // Guarda la ficha  PDF
                if ($_FILES['file_pdf_doc']['tmp_name'] != "") {
                    $path = APP_IMG_PROPERTIES."property_".$property['id']."/PDF/";

                    $file_name = "file_pdf_".rand(00000, 99999).uniqid().".pdf";

                    if(move_uploaded_file($_FILES['file_pdf_doc']['tmp_name'], $path.$file_name)){
                        if ($property['pdf_sheet']) {
                            @unlink($path.$property['pdf_sheet']);
                        }

                        $this->db->updateAction("properties", array('pdf_sheet'), array($file_name), "id='".$property['id']."'");
                    }
                }
            break;
        }
    }

    /**
     * Se encarga de mover las imágenes de galería a su ubicación final
     * @param  array  $arr_propertie Arreglo de las propiedad
     * @return void
     */
    public function moveImages(array $arr_propertie)
    {
        $temp_path = APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/properties/";
        $property_path = APP_IMG_PROPERTIES."property_".$arr_propertie['id']."/images/gallery/";
        $handle = opendir($temp_path);

        while ($file = readdir($handle)){
            if (is_file($temp_path.$file)) {
                if (rename($temp_path.$file, $property_path.$file)) {
                    $arr_fields = array(
                        'propertie_id',
                        'image'
                    );
                    $arr_values = array(
                        $arr_propertie['id'],
                        $file
                    );

                    if (substr($file, 0,4) != "min_") {
                        $this->db->insertAction('properties_images_gallery', $arr_fields, $arr_values);
                    }
                }
            }
        }
    }

    /**
     * Elimina una imagen de galería
     * @param  integer $image_id ID imagen de galería
     * @return string
     */
    public function removeImageGallery($image_id)
    {
        $s = "SELECT * FROM properties_images_gallery WHERE id='".$image_id."'";
        $arr_gallery = $this->db->fetchSQL($s);
        $property_path = APP_IMG_PROPERTIES."property_".$arr_gallery[0]['propertie_id']."/images/gallery/";

        $property_name = $this->db->getValue("title", "properties", "id='".$arr_gallery[0]['propertie_id']."'");

        $delete = $this->db->deleteAction("properties_images_gallery", "id='".$image_id."'");

        if ($delete) {
            @unlink($property_path.$arr_gallery[0]['image']);
            @unlink($property_path."min_".$arr_gallery[0]['image']);
        }

        $this->addLogs(sprintf("Eliminando imagen de galería de propiedad: %s - ID: %d", $property_name, $arr_gallery[0]['propertie_id']));

        return $this->getGalleryImages($arr_gallery[0]['propertie_id']);
    }

    /**
     * Retorna las miniaturas de las imágenes de galería de una propiedad
     * @return string
     */
    public function getGalleryImages($property_id)
    {
        $path = APP_IMG_PROPERTIES."property_".$property_id."/images/gallery/";
        $result = "";

        $s = "SELECT * FROM properties_images_gallery WHERE propertie_id='".$property_id."'";
        $arr_gallery = $this->db->fetchSQL($s);

        foreach ($arr_gallery as $val) {
            $result .= '
            <div class="col-md-3 css-marginB20 container-thumb">
                <div class="css-background-image" style="background-image: url(\''.$path.'min_'.$val['image'].'\')"></div>
                <a href="#" class="btn btn-info btn-flat center-block btn-show" data-image="'.$path."min_".$val['image'].'"><i class="fas fa-search"></i> Ver imagen</a>
                <a href="javascript: removeGalleryImage(\''.$val['id'].'\', this.event)" class="btn btn-danger btn-flat center-block btn-remove-gallery" data-id="'.$val['id'].'"><i class="fas fa-trash-alt"></i> Eliminar</a>
            </div>
            ';
        }

        return $result;
    }

    public function duplicate($id)
    {
        $property = $this->duplicateProperty($id);
        $propertyIcons = $this->duplicateIcons($property['id'], $id);
        $gallery = $this->duplicateGallery($property['id'], $id, $property);

        $this->addLogs(sprintf("Duplicando propiedad: %s - id: %d", $property['title'], $property['id']));

        return $property;
    }


    /**
     * Duplica una propiedad a base de otra
     * @param  integer $id ID del inmueble base
     * @return array
     */
    public function duplicateProperty($id)
    {
        $db = $this->db;

        $s = "SELECT * FROM properties WHERE id='".$id."'";
        $arr_property = $db->fetchSQL($s);

        $arr_fields = array(
            'title',
            'cover_title',
            'description',
            'blueprints',
            'quality_memories',
            'blueprints_doc',
            'cee',
            'pdf_sheet',
            'location',
            'latitude',
            'longitude',
            'promotion_id',
            'cost',
            'monthly_cost',
            'code',
            'outstanding',
            'status',
            'sold',
            'create_at',
            'update_at',
        );
        $arr_value = array(
            $arr_property[0]['title'],
            $arr_property[0]['cover_title'],
            $arr_property[0]['description'],
            $arr_property[0]['blueprints'],
            $arr_property[0]['quality_memories'],
            $arr_property[0]['blueprints_doc'],
            $arr_property[0]['cee'],
            $arr_property[0]['pdf_sheet'],
            $arr_property[0]['location'],
            $arr_property[0]['latitude'],
            $arr_property[0]['longitude'],
            $arr_property[0]['promotion_id'],
            $arr_property[0]['cost'],
            $arr_property[0]['monthly_cost'],
            $arr_property[0]['code'],
            $arr_property[0]['outstanding'],
            $arr_property[0]['status'],
            $arr_property[0]['sold'],
            date('Y-m-d H:i:s'),
            date('Y-m-d H:i:s')
        );

        $property = $db->insertAction("properties", $arr_fields, $arr_value);

        mkdir(APP_IMG_PROPERTIES."property_".$property[0]['id']."/");
        mkdir(APP_IMG_PROPERTIES."property_".$property[0]['id']."/PDF/");
        mkdir(APP_IMG_PROPERTIES."property_".$property[0]['id']."/images/");
        mkdir(APP_IMG_PROPERTIES."property_".$property[0]['id']."/images/gallery/");

        return $property[0];
    }

    /**
     * Duplica la descripción de los iconos
     * @param  integer $id      ID de la nueva propiedad
     * @param  integer $base_id ID de la propiedad base
     * @return array
     */
    public function duplicateIcons($id, $base_id)
    {
        $db = $this->db;
        $s = "SELECT * FROM properties_descriptions WHERE propertie_id='".$base_id."'";
        $arr_icon = $db->fetchSQL($s);

        $arr_fields = array(
            'propertie_id',
            'icon_id',
            'description'
        );
        $arr_values = array(
            $id,
            $arr_icon[0]['icon_id'],
            $arr_icon[0]['description']
        );

        $icons = $db->insertAction("properties_descriptions", $arr_fields, $arr_values);

        return $icons[0];
    }

    /**
     * Dupica las imagenes de galería
     * @param  integer $id      ID de la nueva propiedad
     * @param  integer $base_id ID de la propiedad base
     * @return void
     */
    public function duplicateGallery($id, $base_id, $property)
    {
        $db = $this->db;

        $s = "SELECT * FROM properties_images_gallery WHERE propertie_id='".$base_id."'";
        $arr_gallery = $db->fetchSQL($s);

        foreach ($arr_gallery as $val) {
            $arr_fields = array(
                'propertie_id',
                'image'
            );
            $arr_values = array(
                $id,
                $val['image']
            );

            $gallery = $db->insertAction("properties_images_gallery", $arr_fields, $arr_values);
        }

        $this->copyImages($base_id, $id, $property);
        $this->copyFiles($base_id, $id);
    }

    /**
     * Copia las imagenes de la propiedad original en la carpeta de la nueva propiedad
     * @param  integer $base_id ID del inmueble base
     * @param  integer $new_id  ID del nuevo inmueble
     * @return void
     */
    public function copyImages($base_id, $new_id, $property)
    {
        $base_path = APP_IMG_PROPERTIES."property_".$base_id."/images/gallery/";
        $property_path = APP_IMG_PROPERTIES."property_".$new_id."/images/gallery/";
        $handle = opendir($base_path);

        while ($file = readdir($handle)){
            if (is_file($base_path.$file)) {
                copy($base_path.$file, $property_path.$file);
            }
        }

        if ($property['blueprints']) {
            $base_path = APP_IMG_PROPERTIES."property_".$base_id."/images/";
            $property_path = APP_IMG_PROPERTIES."property_".$new_id."/images/";
            copy($base_path.$property['blueprints'], $property_path.$property['blueprints']);
        }
    }

    /**
     * Duplica los archivos PDF
     * @param  integer $base_id ID del inmueble base
     * @param  integer $new_id  ID del nuevo inmueble
     * @return void
     */
    public function copyFiles($base_id, $new_id)
    {
        $base_path = APP_IMG_PROPERTIES."property_".$base_id."/PDF/";
        $property_path = APP_IMG_PROPERTIES."property_".$new_id."/PDF/";
        $handle = opendir($base_path);

        while ($file = readdir($handle)){
            if (is_file($base_path.$file)) {
                copy($base_path.$file, $property_path.$file);
            }
        }
    }
}


?>
