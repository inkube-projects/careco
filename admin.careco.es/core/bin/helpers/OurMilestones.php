<?php

/**
 * Se encarga de guardar la información de nuestros hitos
 */
class OurMilestones extends GeneralMethods
{

   public $db;

   function __construct($db)
   {
      parent::__construct($db);
      $this->db = $db;
      $this->admin_id = $_SESSION['ADMIN_SESSION_CARECO']['id'];
   }

   /**
    * Gurda la información de introducción
    * @return array
    */
   public function persistIntro()
   {
      $verify = $this->db->getCount("our_milestones", "");

      if ($verify == 0) {
         $result = $this->db->insertAction("our_milestones", array('description'), array(addslashes($_POST['description'])));
      } else {
         $result = $this->db->updateAction("our_milestones", array('description'), array(addslashes($_POST['description'])));
      }

      if ($_FILES['banner']['tmp_name'] != "") {
         $this->persistImage($result[0]);
      }

      $this->addLogs(sprintf("Agregando información de introducción para los hitos"));

      return $result[0];
   }

   /**
    * Se encarga de guardar la imagen de banner
    * @param  array  $arr [description]
    * @return [type]      [description]
    */
   public function persistImage(array $arr)
   {
      $path = APP_IMG_MILESTONES;
      $extens = str_replace("image/",".",$_FILES['banner']['type']);
      $img_name = "milestones_".rand(00000, 99999).uniqid().$extens;

      list($width, $height) = getimagesize($_FILES["banner"]['tmp_name']);

      if ($width > 1500) {
         $upload = ImagenTemp('banner', 1500, $img_name, $path);
      } else {
         $upload = ImagenTemp('banner', $width, $img_name, $path);
      }

      if ($upload) {
         $this->db->updateAction("our_milestones", array('cover_image'), array($img_name));

         if ($arr['cover_image']) {
            @unlink($path.$arr['cover_image']);
            @unlink($path."min_".$arr['cover_image']);
         }
      }
   }

   /**
    * Registra una entrada nueva de hitos
    * @return array
    */
   public function persistEntrie()
   {
      $arr_fields = array(
         'title',
         'description',
      );
      $arr_values = array(
         $_POST['title'],
         $_POST['description'],
      );

      $result = $this->db->insertAction("our_milestones_entries", $arr_fields, $arr_values);

      $this->addLogs(sprintf("Agregando entrada de hito: %s - ID: %d", $result[0]['title'], $result[0]['id']));

      return $result[0];
   }

   /**
    * Edita una entrada de nuestros hitos
    * @param  integer $id ID de la entrada
    * @return array
    */
   public function editEntry($id)
   {
      $arr_fields = array(
         'title',
         'description',
      );
      $arr_values = array(
         $_POST['title'],
         $_POST['description'],
      );

      $result = $this->db->updateAction("our_milestones_entries", $arr_fields, $arr_values, "id='".$id."'");

      $this->addLogs(sprintf("Editada entrada de hito: %s - ID: %d", $result[0]['title'], $result[0]['id']));

      return $result[0];
   }
}


?>
