<?php

/**
 * Clase que se encarga de eliminar entradas en la base de datos
 */
class RemoveHelper extends GeneralMethods
{
   public $db;

   function __construct($db)
   {
      parent::__construct($db);
      $this->db = $db;
   }

   /**
    * Elimina una promoción
    * @param  integer $id ID de la promoción
    * @return void
    */
   public function removePromotion($id)
   {
      // $image = $this->db->getValue('image', 'promotions', "id='".$id."'");
      $s = "SELECT * FROM promotions WHERE id='".$id."'";
      $arr_promo = $this->db->fetchSQL($s);
      $image = $arr_promo[0]['image'];

      $s = "SELECT * FROM promotions_images_gallery WHERE promotion_id='".$id."'";
      $arr_gallery = $this->db->fetchSQL($s);

      $remove = $this->db->deleteAction("promotions", "id='".$id."'");

      if ((!is_null($image)) && ($image != "")) {
         @unlink(APP_IMG_PROMOTIONS.'cover/'.$image);
         @unlink(APP_IMG_PROMOTIONS."cover/min_".$image);
      }

      foreach ($arr_gallery as $val) {
         @unlink(APP_IMG_PROMOTIONS.'gallery/'.$val['image']);
         @unlink(APP_IMG_PROMOTIONS."gallery/min_".$val['image']);
      }

      $this->addLogs(sprintf("Eliminando promoción: %s - ID: %d", $arr_promo[0]['title'], $id));
   }

   /**
    * Elimina un icono
    * @param  integer $id ID del icono
    * @return void
    */
   public function removeIcon($id)
   {
      $s = "SELECT * FROM icons WHERE id='".$id."'";
      $arr_icon = $this->db->fetchSQL($s);
      $image = $arr_icon[0]['image'];

      $remove = $this->db->deleteAction("icons", "id='".$id."'");

      if ((!is_null($image)) && ($image != "")) {
         @unlink(APP_IMG_ICON.$image);
      }

      $this->addLogs(sprintf("Eliminando icono: %s - ID: %d", $arr_icon[0]['description'], $id));
   }

   /**
    * Elimina una propuiedad y su directorio
    * @param  integer $id ID de la propiedad
    * @return void
    */
   public function removeProperty($id)
   {
      $s = "SELECT * FROM properties WHERE id='".$id."'";
      $arr_property = $this->db->fetchSQL($s);

      $delete = $this->db->deleteAction("properties", "id='".$id."'");

      if ($delete) {
         removeDir(APP_IMG_PROPERTIES."property_".$id."/images/gallery/");
         removeDir(APP_IMG_PROPERTIES."property_".$id."/PDF/");
         removeDir(APP_IMG_PROPERTIES."property_".$id."/images/");
         removeDir(APP_IMG_PROPERTIES."property_".$id."/");
      }

      $this->addLogs(sprintf("Eliminando propiedad: %s - ID: %d", $arr_property[0]['title'], $id));
   }

   /**
    * Elimina las secciones de about us
    * @param  integer $id ID de la sección
    * @return void
    */
   public function removeSection($id)
   {
      $s = "SELECT * FROM about_us WHERE id='".$id."'";
      $arr_section = $this->db->fetchSQL($s);

      $delete = $this->db->deleteAction("about_us", "id='".$id."'");

      if ($delete) {
         @unlink(APP_IMG_ABOUT_US.$arr_section[0]['image']);
         @unlink(APP_IMG_ABOUT_US."min_".$arr_section[0]['image']);
      }

      $this->addLogs(sprintf("Eliminando sección: %s - ID: %d", $arr_section[0]['title'], $id));
   }

   /**
    * Elimina una entrada de los hitos
    * @param  integer $id ID de las entradas
    * @return void
    */
   public function removeMilestone($id)
   {
      $s = "SELECT * FROM our_milestones_entries WHERE id='".$id."'";
      $arr_entry = $this->db->fetchSQL($s);

      $delete = $this->db->deleteAction('our_milestones_entries', "id='".$id."'");

      if ($delete) {
         $this->addLogs(sprintf("Eliminando entrada de nuestros hitos: %s - ID: %d", $arr_entry[0]['title'], $id));
      }
   }

   /**
    * Elimina un opinión
    * @param  integer $id ID de la opinión
    * @return void
    */
   public function removeOpinion($id)
   {
      $s = "SELECT * FROM opinions WHERE id='".$id."'";
      $arr_opinion = $this->db->fetchSQL($s);

      $delete = $this->db->deleteAction('opinions', "id='".$id."'");

      if ($delete) {
         @unlink(APP_IMG_OPINION."profile/".$arr_opinion[0]['image']);
         @unlink(APP_IMG_OPINION."profile/min_".$arr_opinion[0]['image']);

         $this->addLogs(sprintf("Eliminando opinion: %s - ID: %d", $arr_opinion[0]['name'], $id));
      }
   }

   /**
    * Elimina una categoría del blog
    * @return void
    */
   public function removeBlogCategory($id)
   {
      $s = "SELECT * FROM blog_tags WHERE id='".$id."'";
      $arr_tag = $this->db->fetchSQL($s);

      $delete = $this->db->deleteAction('blog_tags', "id='".$id."'");

      if ($delete) {
         $this->addLogs(sprintf("Eliminando la categoría del blog: %s - ID: %d", $arr_tag[0]['name'], $id));
      }
   }

   /**
    * Elimina un email del newsletter
    * @return void
    */
   public function removeNewsletter($id)
   {
       $s = "SELECT * FROM newsletter WHERE id='".$id."'";
       $arr_newsletter = $this->db->fetchSQL($s);

       $delete = $this->db->deleteAction('newsletter', "id='".$id."'");

       if ($delete) {
          $this->addLogs(sprintf("Eliminando la email de newsletter: %s - ID: %d", $arr_newsletter[0]['email'], $id));
       }
   }

   /**
    * Elimina una entrada del blog
    * @param  integer $id ID del post
    * @return void
    */
   public function removePost($id)
   {
      $s = "SELECT * FROM post WHERE id='".$id."'";
      $arr_post = $this->db->fetchSQL($s);
      $image = $arr_post[0]['image'];

      $remove = $this->db->deleteAction("post", "id='".$id."'");

      if ((!is_null($image)) && ($image != "")) {
         @unlink(APP_IMG_POST.$image);
         @unlink(APP_IMG_POST."min_".$image);
      }

      $this->addLogs(sprintf("Eliminando post: %s - ID: %d", $arr_post[0]['title'], $id));
   }

   public function removeMessage($id)
   {
      $s = "SELECT * FROM contact WHERE id='".$id."'";
      $arr_contact = $this->db->fetchSQL($s);

      $remove = $this->db->deleteAction("contact", "id='".$id."'");

      $this->addLogs(sprintf("Eliminando mensaje: %s - ID: %d", $arr_contact[0]['email'], $id));
   }

   public function removeUser($id)
   {
      $s = "SELECT * FROM user WHERE id='".$id."'";
      $arr_user = $this->db->fetchSQL($s);

      $remove = $this->db->deleteAction("user", "id='".$id."'");

      $this->addLogs(sprintf("Eliminando usuario: %s - ID: %d", $arr_user[0]['username'], $id));
   }

   public function removeReportTag($id)
   {
      $s = "SELECT * FROM report_tag WHERE id='".$id."'";
      $arr_tag = $this->db->fetchSQL($s);

      $remove = $this->db->deleteAction("report_tag", "id='".$id."'");

      $this->addLogs(sprintf("Eliminando tag de reporte: %s - ID: %d", $arr_tag[0]['name'], $id));
   }

   public function removeReport($id)
   {
       $s = "SELECT * FROM report WHERE id='".$id."'";
       $arr_report = $this->db->fetchSQL($s);

       $remove = $this->db->deleteAction("report", "id='".$id."'");

       $this->addLogs(sprintf("Eliminando reporte, ID: %d", $id));
   }
}

?>
