<?php

/**
 * Clase que se encarga de guardar las promociones
 */
class PostHelper extends GeneralMethods
{
   public $db;

   function __construct($db)
   {
      parent::__construct($db);
      $this->db = $db;
      $this->admin_id = $_SESSION['ADMIN_SESSION_CARECO']['id'];
   }

   /**
    * Guarda una promoción nueva
    * @return array
    */
   public function persistPost()
   {
      $arr_fields = array(
         'title',
         'description',
         'create_at',
         'update_at'
      );

      $arr_values = array(
         $_POST['title'],
         addslashes($_POST['description']),
         date('Y-m-d H:i:s'),
         date('Y-m-d H:i:s'),
      );

      $post = $this->db->insertAction("post", $arr_fields, $arr_values);

      foreach ($_POST['tag'] as $val) {
         $this->db->insertAction("post_tags", array('post_id', 'tag_id'), array($post[0]['id'], $val));
      }

      if (($post) && ($_POST['hid-name'] != "")) {
         $this->saveImage($_POST['hid-name'], $post[0]['id']);
      }

      $this->addLogs(sprintf("Agregando entrada del blog: %s - id: %d", $post[0]['title'], $post[0]['id']));

      return $post[0];
   }

   /**
    * Edita la promoción
    * @param  integer $id ID de la promoción
    * @return void
    */
   public function editPortfolio($id)
   {
      $arr_fields = array(
         'title',
         'description',
         'update_at'
      );

      $arr_values = array(
         $_POST['title'],
         addslashes($_POST['description']),
         date('Y-m-d H:i:s'),
      );

      $post = $this->db->updateAction("post", $arr_fields, $arr_values, "id='".$id."'");

      $this->db->deleteAction("post_tags", "post_id='".$id."'");

      foreach ($_POST['tag'] as $val) {
         $this->db->insertAction("post_tags", array('post_id', 'tag_id'), array($id, $val));
      }

      if (($post) && ($_POST['hid-name'] != "")) {
         $this->saveImage($_POST['hid-name'], $id, $post[0]['image']);
      }

      $this->addLogs(sprintf("Editando entrada del blog: %s - id: %d", $post[0]['title'], $id));

      return $post[0];
   }

   /**
    * Se encarga de guardar las imagenes
    * @param  string $name Nombre de la imagen
    * @param  integer $id   ID de la promoción
    * @param  string $old  Nombre de la imagen vieja
    * @return void
    */
   public function saveImage($name, $id, $old = "")
   {
      $filename = APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/post/".$name;

      if (fileExist($filename)) {
         @rename($filename, APP_IMG_POST.$name);
         @rename(APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/post/min_".$name, APP_IMG_POST."/min_".$name);

         if ((!is_null($old)) && ($old != "")) {
            @unlink(APP_IMG_POST.$old);
            @unlink(APP_IMG_POST."min_".$old);
         }

         $update = $this->db->updateAction("post", array('image'), array($name), "id = '".$id."'");

         @unlink($filename);
         @unlink(APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/post/min_".$name);
      }
   }

   /**
    * Prepara imagen de portada de la promoción
    * @param  integer $acc Acción a ejecutar [1: Guarda la imagen base, 2: Recorta la imagen]
    * @return object
    */
   public function prepareCoverImage($acc)
   {
      if ($acc == 1) {
         $tempIMG = $this->prepareCoverTemp();
         $form = $this->showFormCoverIMG($tempIMG);
         return $form;
      } elseif ($acc == 2) {
         return $this->createIMG();
      }
   }

   /**
    * Guarda la imagen base
    * @return string
    */
   public function prepareCoverTemp()
   {
      // se elimina las imagenes temporales que esten en la carpeta
      $dir = APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/post/";
      $handle = opendir($dir);
      while ($file = readdir($handle)){
         if (is_file($dir.$file)) {
            @unlink($dir.$file);
         }
      }

      // se sube la nueva imagen temporal
      $extens = str_replace("image/",".",$_FILES['cover_image']['type']);
		$nb_img = "promo_".rand(00000, 99999).uniqid().$extens;
      list($width, $height) = getimagesize($_FILES["cover_image"]['tmp_name']);

      if($width > 1000){
			ImagenTemp('cover_image', 1000, $nb_img, $dir);
      }else{
			ImagenTemp('cover_image', $width, $nb_img, $dir);
      }

      return $nb_img;
   }

   /**
    * Retorna el fomulario de la imagen de perfil a recortar
    * @param  string $img_name Nombre asignado a la imagen
    * @return string
    */
   public function showFormCoverIMG($img_name)
   {
      $directory = APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/post/";
      $directory_post = APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/post/";

      list($width, $height) = getimagesize($directory.$img_name);

      if($height > 600) {
         $altura = 600;
      } else {
         $altura = $height;
      }
      if($width > 600) {
         $ancho = 600;
      } else {
         $ancho = $width;
      }

      $form = '
         <link rel="stylesheet" href="views/app/plugins/jcrop/jquery.Jcrop.css" type="text/css" />
         <script src="views/app/plugins/jcrop/jquery.Jcrop.js"></script>
         <script type="text/javascript">
            $(function(){
            	$(\'#img-recortar\').Jcrop({
            		setSelect: [ 0, 0, 0, 0],
            		maxSize: ['.$width.', '.$height.'],
            		minSize: [100, 100],
            		onSelect: updateCoords
            	});
            });
            function updateCoords(c){
         		$(\'#x\').val(c.x);
         		$(\'#y\').val(c.y);
         		if($(\'#w\').val()==0){ $(\'#w\').val('.$width.');}else{ $(\'#w\').val(c.w);}
         		if($(\'#h\').val()==0){ $(\'#h\').val('.$height.');}else{ $(\'#h\').val(c.h);}
         	}
            function recortarImagen(){
            	var i = $("#frm-img").serialize();
            		$.ajax({
            			type: \'POST\',
            			url: \'ajax.php?m=post&acc=2\',
                     dataType: \'json\',
            			data: i,
            			success: function(r){
            				$(\'#mod-img-cover\').modal(\'hide\');
                        if (r.status === "OK") {
                           $("#hid-name").val("'.$this->admin_id."_".$img_name.'");
                           $("#img-cover").attr("src", "'.$directory_post.'"+r.data);
                        }
                     }
            		});
            	return false;
            }
         </script>
         <img src="'.$directory.$img_name.'" id="img-recortar" class="img-responsive">
         <form method="post" name="frm-img" id="frm-img" onSubmit="return recortarImagen();">
             <input type="hidden" id="x" name="x" />
             <input type="hidden" id="y" name="y" />
             <input type="hidden" id="w" name="w" />
             <input type="hidden" id="h" name="h" />
             <div class="clearfix css-espacio10"></div>
             <button type="submit" class="btn btn-default pull-right">Cortar Imagen <i class="fa fa-scissors"></i></button>
         </form>
      ';

      return $form;
   }

   /**
    * Crea la imagen de Portada
    * @return string
    */
   public function createIMG()
   {
      $db = new Connection();
      $directory_temp = APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/post/";
      $directory_profile = APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/post/";

      $dir = $directory_temp;  $handle = opendir($dir);
      $file = readdir($handle);
      while ($file = readdir($handle)){
         if (is_file($dir.$file)) {
            $image = $file;
         }
      }
      $ext = explode(".", $image)[1];

      $x = sanitize($_POST['x']);
   	$y = sanitize($_POST['y']);
   	$w = sanitize($_POST['w']);
   	$h = sanitize($_POST['h']);
      $targ_w = $w;
   	$targ_h = $h;
   	$jpeg_quality = 90;
   	$src = $directory_temp.$image;
   	$src_min = $directory_profile."min_".$image;

      if(($ext == "jpeg")||($ext == "jpg")){
   		$img_r = imagecreatefromjpeg($src);
   	}if($ext == "png"){
   		$img_r = imagecreatefrompng($src);
   	} if($ext == "gif"){
   		$img_r = imagecreatefromgif($src);
   	}

      $src = $directory_temp.$this->admin_id."_".$image;

      $dst_r = imagecreatetruecolor($targ_w, $targ_h);
      imagecopyresampled($dst_r, $img_r, 0, 0, $x, $y, $targ_w, $targ_h, $w, $h);
      if (($ext == "jpeg") || ($ext== "jpg ")){
   		imagejpeg($dst_r,$src,90);
      } elseif ($ext == "png") {
   		imagepng($dst_r, $src);
      } elseif ($ext=="gif") {
   		imagegif($dst_r,$src);
      }

      // se crea la miniatura
      $src_min = $directory_profile."min_".$this->admin_id."_".$image;
      list($width, $height) = getimagesize($src);
   	$new_width = 200;
   	$new_height = floor($height * ($new_width / $width));
   	$targ_w = $new_width;
   	$targ_h = $new_height;
   	$dst_r = imagecreatetruecolor($targ_w, $targ_h);

      imagecopyresampled($dst_r,$img_r,0,0,$x,$y, $targ_w,$targ_h,$w,$h);
   	if(($ext=="jpeg")||($ext=="jpg")){
   		imagejpeg($dst_r,$src_min,90);
   	} if($ext=="png"){
   		imagepng($dst_r,$src_min);
   	} if($ext=="gif"){
   		imagegif($dst_r,$src_min);
   	}

      imagedestroy($dst_r);

      // rename($src, $directory_profile.$image);
      return $this->admin_id."_".$image;
   }
}


?>
