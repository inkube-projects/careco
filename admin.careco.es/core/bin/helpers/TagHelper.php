<?php

/**
 * Clase que se encarga de guardar los iconos
 */
class TagHelper extends GeneralMethods
{
   public $db;

   function __construct($db)
   {
      parent::__construct($db);
      $this->db = $db;
      $this->admin_id = $_SESSION['ADMIN_SESSION_CARECO']['id'];
   }

   /**
    * Guarda en la bd los iconos para las propiedades
    * @return array
    */
   public function persistTag()
   {
      $arr_fields = array('name');
      $arr_values = array($_POST['description']);

      $tag = $this->db->insertAction("blog_tags", $arr_fields, $arr_values);

      $this->addLogs(sprintf("Agregando categoría del blog: %s - id: %d", $tag[0]['name'], $tag[0]['id']));

      return $tag[0];
   }

   public function editTag($id)
   {
      $arr_fields = array('name');
      $arr_values = array($_POST['description']);

      $tag = $this->db->updateAction("blog_tags", $arr_fields, $arr_values, "id='".$id."'");

      $this->addLogs(sprintf("Editando categoría del blog: %s - id: %d", $tag[0]['name'], $tag[0]['id']));

      return $tag[0];
   }

}
?>
