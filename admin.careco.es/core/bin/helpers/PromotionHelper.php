<?php

/**
 * Clase que se encarga de guardar las promociones
 */
class PromotionHelper extends GeneralMethods
{
   public $db;

   function __construct($db)
   {
      parent::__construct($db);
      $this->db = $db;
      $this->admin_id = $_SESSION['ADMIN_SESSION_CARECO']['id'];
   }

   /**
    * Guarda una promoción nueva
    * @return array
    */
   public function persistPromo()
   {
      $arr_fields = array(
         'title',
         'category',
         'description',
         'location',
         'latitude',
         'longitude',
         'create_at',
         'update_at'
      );

      $arr_values = array(
         $_POST['title'],
         $_POST['category'],
         $_POST['description'],
         addslashes($_POST['location']),
         $_POST['latitude'],
         $_POST['longitude'],
         date('Y-m-d H:i:s'),
         date('Y-m-d H:i:s'),
      );
      $promo = $this->db->insertAction("promotions", $arr_fields, $arr_values);

      if (($promo) && ($_POST['hid-name'] != "")) {
         $this->saveImage($_POST['hid-name'], $promo[0]['id']);
      }

      $this->addLogs(sprintf("Agregando promoción: %s - id: %d", $promo[0]['title'], $promo[0]['id']));

      return $promo[0];
   }

   /**
    * Edita la promoción
    * @param  integer $id ID de la promoción
    * @return void
    */
   public function editPromo($id)
   {
      $arr_fields = array(
         'title',
         'category',
         'description',
         'location',
         'latitude',
         'longitude',
         'update_at'
      );

      $arr_values = array(
         $_POST['title'],
         $_POST['category'],
         $_POST['description'],
         addslashes($_POST['location']),
         $_POST['latitude'],
         $_POST['longitude'],
         date('Y-m-d H:i:s'),
      );

      $promo = $this->db->updateAction("promotions", $arr_fields, $arr_values, "id = '".$id."'");

      if (($promo) && ($_POST['hid-name'] != "")) {
         $this->saveImage($_POST['hid-name'], $id, $promo[0]['image']);
      }

      $this->addLogs(sprintf("Editando promoción: %s - id: %d", $promo[0]['title'], $id));

      return $promo[0];
   }

   /**
    * Se encarga de guardar las imagenes
    * @param  string $name Nombre de la imagen
    * @param  integer $id   ID de la promoción
    * @param  string $old  Nombre de la imagen vieja
    * @return void
    */
   public function saveImage($name, $id, $old = "")
   {
      $filename = APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/promotions/cover/".$name;

      if (fileExist($filename)) {
         @rename($filename, APP_IMG_PROMOTIONS."/cover/".$name);
         @rename(APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/promotions/cover/min_".$name, APP_IMG_PROMOTIONS."/cover/min_".$name);

         if ((!is_null($old)) && ($old != "")) {
            @unlink(APP_IMG_PROMOTIONS.'cover/'.$old);
            @unlink(APP_IMG_PROMOTIONS."cover/min_".$old);
         }

         $update = $this->db->updateAction("promotions", array('image'), array($name), "id = '".$id."'");


         @unlink($filename);
         @unlink(APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/promotions/cover/min_".$name);
      }
   }

   /**
    * Prepara imagen de portada de la promoción
    * @param  integer $acc Acción a ejecutar [1: Guarda la imagen base, 2: Recorta la imagen]
    * @return object
    */
   public function prepareCoverImage($acc)
   {
      if ($acc == 1) {
         $tempIMG = $this->prepareCoverTemp();
         $form = $this->showFormCoverIMG($tempIMG);
         return $form;
      } elseif ($acc == 2) {
         return $this->createIMG();
      }
   }

   /**
    * Guarda la imagen base
    * @return string
    */
   public function prepareCoverTemp()
   {
      // se elimina las imagenes temporales que esten en la carpeta
      $dir = APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/promotions/cover/";
      $handle = opendir($dir);
      while ($file = readdir($handle)){
         if (is_file($dir.$file)) {
            @unlink($dir.$file);
         }
      }

      // se sube la nueva imagen temporal
      $extens = str_replace("image/",".",$_FILES['cover_image']['type']);
		$nb_img = "promo_".rand(00000, 99999).uniqid().$extens;
      list($width, $height) = getimagesize($_FILES["cover_image"]['tmp_name']);

      if($width > 1000){
			ImagenTemp('cover_image', 1000, $nb_img, $dir);
      }else{
			ImagenTemp('cover_image', $width, $nb_img, $dir);
      }

      return $nb_img;
   }

   /**
    * Retorna el fomulario de la imagen de perfil a recortar
    * @param  string $img_name Nombre asignado a la imagen
    * @return string
    */
   public function showFormCoverIMG($img_name)
   {
      $directory = APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/promotions/cover/";
      $directory_promotion = APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/promotions/cover/";

      list($width, $height) = getimagesize($directory.$img_name);

      if($height > 305) {
         $altura = 305;
      } else {
         $altura = $height;
      }
      if($width > 725) {
         $ancho = 725;
      } else {
         $ancho = $width;
      }

      $form = '
         <link rel="stylesheet" href="views/app/plugins/jcrop/jquery.Jcrop.css" type="text/css" />
         <script src="views/app/plugins/jcrop/jquery.Jcrop.js"></script>
         <script type="text/javascript">
            $(function(){
            	$(\'#img-recortar\').Jcrop({
            		setSelect: [ 0, 0, 0, 0],
            		maxSize: ['.$ancho.', '.$altura.'],
            		minSize: ['.$ancho.', '.$altura.'],
            		onSelect: updateCoords
            	});
            });
            function updateCoords(c){
         		$(\'#x\').val(c.x);
         		$(\'#y\').val(c.y);
         		if($(\'#w\').val()==0){ $(\'#w\').val('.$ancho.');}else{ $(\'#w\').val(c.w);}
         		if($(\'#h\').val()==0){ $(\'#h\').val('.$altura.');}else{ $(\'#h\').val(c.h);}
         	}
            function recortarImagen(){
            	var i = $("#frm-img").serialize();
            		$.ajax({
            			type: \'POST\',
            			url: \'ajax.php?m=promotion&acc=2\',
                     dataType: \'json\',
            			data: i,
            			success: function(r){
            				$(\'#mod-img-cover\').modal(\'hide\');
                        if (r.status === "OK") {
                           $("#hid-name").val("'.$this->admin_id."_".$img_name.'");
                           $("#img-cover").attr("src", "'.$directory_promotion.'"+r.data);
                        }
                     }
            		});
            	return false;
            }
         </script>
         <img src="'.$directory.$img_name.'" id="img-recortar" class="img-responsive">
         <form method="post" name="frm-img" id="frm-img" onSubmit="return recortarImagen();">
             <input type="hidden" id="x" name="x" />
             <input type="hidden" id="y" name="y" />
             <input type="hidden" id="w" name="w" />
             <input type="hidden" id="h" name="h" />
             <div class="clearfix css-espacio10"></div>
             <button type="submit" class="btn btn-default pull-right">Cortar Imagen <i class="fa fa-scissors"></i></button>
         </form>
      ';

      return $form;
   }

   /**
    * Crea la imagen de Portada
    * @return string
    */
   public function createIMG()
   {
      $db = new Connection();
      $directory_temp = APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/promotions/cover/";
      $directory_profile = APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/promotions/cover/";

      $dir = $directory_temp;  $handle = opendir($dir);
      $file = readdir($handle);
      while ($file = readdir($handle)){
         if (is_file($dir.$file)) {
            $image = $file;
         }
      }
      $ext = explode(".", $image)[1];

      $x = sanitize($_POST['x']);
   	$y = sanitize($_POST['y']);
   	$w = sanitize($_POST['w']);
   	$h = sanitize($_POST['h']);
      $targ_w = $w;
   	$targ_h = $h;
   	$jpeg_quality = 90;
   	$src = $directory_temp.$image;
   	$src_min = $directory_profile."min_".$image;

      if(($ext == "jpeg")||($ext == "jpg")){
   		$img_r = imagecreatefromjpeg($src);
   	}if($ext == "png"){
   		$img_r = imagecreatefrompng($src);
   	} if($ext == "gif"){
   		$img_r = imagecreatefromgif($src);
   	}

      $src = $directory_temp.$this->admin_id."_".$image;

      $dst_r = imagecreatetruecolor($targ_w, $targ_h);
      imagecopyresampled($dst_r, $img_r, 0, 0, $x, $y, $targ_w, $targ_h, $w, $h);
      if (($ext == "jpeg") || ($ext== "jpg ")){
   		imagejpeg($dst_r,$src,90);
      } elseif ($ext == "png") {
   		imagepng($dst_r, $src);
      } elseif ($ext=="gif") {
   		imagegif($dst_r,$src);
      }

      // se crea la miniatura
      $src_min = $directory_profile."min_".$this->admin_id."_".$image;
      list($width, $height) = getimagesize($src);
   	$new_width = 500;
   	$new_height = floor($height * (500 / $width));
   	$targ_w = $new_width;
   	$targ_h = $new_height;
   	$dst_r = imagecreatetruecolor($targ_w, $targ_h);

      imagecopyresampled($dst_r,$img_r,0,0,$x,$y, $targ_w,$targ_h,$w,$h);
   	if(($ext=="jpeg")||($ext=="jpg")){
   		imagejpeg($dst_r,$src_min,90);
   	} if($ext=="png"){
   		imagepng($dst_r,$src_min);
   	} if($ext=="gif"){
   		imagegif($dst_r,$src_min);
   	}

      imagedestroy($dst_r);

      // rename($src, $directory_profile.$image);
      return $this->admin_id."_".$image;
   }

   /**
    * Guardando imagenes temporales
    * @return void
    */
   public function uploadImages()
   {
      $arr_lenght = count($_FILES['gallery_images']['tmp_name']);

      // se elimina las imagenes temporales que esten en la carpeta
      $path = APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/promotions/gallery/";

      for ($i = 0; $i < $arr_lenght; $i++) {
         // se sube la nueva imagen temporal
        $extens = str_replace("image/",".",$_FILES['gallery_images']['type'][$i]);
   		$img_name = "promo_".rand(00000, 99999).uniqid().$extens;
         list($width, $height) = getimagesize($_FILES["gallery_images"]['tmp_name'][$i]);

         if($width > 1500){
   			UpPictureArray('gallery_images', 1500, $img_name, $path, $i);
         }else{
   			UpPictureArray('gallery_images', $width, $img_name, $path, $i);
         }
      }

      return $this->getTempImages();
   }

   /**
    * Retorna las miniaturas de todas las imágenes temporales en html
    * @return string
    */
   public function getTempImages()
   {
      $path = APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/promotions/gallery/";
      $handle = opendir($path);
      $result = "";

      while ($file = readdir($handle)){
         if (is_file($path.$file)) {
            if (substr($file, 0,4) != "min_") {
               $result .= '
               <div class="col-md-3 css-marginB20 container-thumb">
                  <div class="css-background-image" style="background-image: url(\''.$path.'min_'.$file.'\')"></div>
                  <a href="#" class="btn btn-info btn-flat center-block btn-show" data-image="'.$path.$file.'"><i class="fas fa-search"></i> Ver imagen</a>
                  <a href="javascript: removeImage(6, \''.$file.'\')" class="btn btn-danger btn-flat center-block"><i class="fas fa-trash-alt"></i> Eliminar</a>
               </div>
               ';
            }
         }
      }

      return $result;
   }

   /**
    * Se encarga de mover las imágenes de galería a su ubicación final
    * @param  array  $arr_promotion Arreglo de las propiedad
    * @return void
    */
   public function moveImages(array $arr_promotion)
   {
      $temp_path = APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/promotions/gallery/";
      $promo_path = APP_IMG_PROMOTIONS."gallery/";
      $handle = opendir($temp_path);

      while ($file = readdir($handle)){
         if (is_file($temp_path.$file)) {
            if (rename($temp_path.$file, $promo_path.$file)) {
               $arr_fields = array(
                  'promotion_id',
                  'image'
               );
               $arr_values = array(
                  $arr_promotion['id'],
                  $file
               );

               if (substr($file, 0,4) != "min_") {
                  $this->db->insertAction('promotions_images_gallery', $arr_fields, $arr_values);
               }
            }
         }
      }
   }

   /**
    * Elimina una imagen de galería
    * @param  integer $image_id ID imagen de galería
    * @return string
    */
   public function removeImageGallery($image_id)
   {
      $s = "SELECT * FROM promotions_images_gallery WHERE id='".$image_id."'";
      $arr_gallery = $this->db->fetchSQL($s);
      $path = APP_IMG_PROMOTIONS."gallery/";

      $promo_name = $this->db->getValue("title", "promotions", "id='".$arr_gallery[0]['promotion_id']."'");

      $delete = $this->db->deleteAction("promotions_images_gallery", "id='".$image_id."'");

      if ($delete) {
         @unlink($path.$arr_gallery[0]['image']);
         @unlink($path."min_".$arr_gallery[0]['image']);
      }

      $this->addLogs(sprintf("Eliminando imagen de galería de la promoción: %s - ID: %d", $promo_name, $arr_gallery[0]['promotion_id']));

      return $this->getGalleryImages($arr_gallery[0]['promotion_id']);
   }

   /**
    * Retorna las miniaturas de las imágenes de galería de una propiedad
    * @return string
    */
   public function getGalleryImages($id)
   {
      $path = APP_IMG_PROMOTIONS."gallery/";
      $result = "";

      $s = "SELECT * FROM promotions_images_gallery WHERE promotion_id='".$id."'";
      $arr_gallery = $this->db->fetchSQL($s);

      foreach ($arr_gallery as $val) {
         $result .= '
         <div class="col-md-3 css-marginB20 container-thumb">
            <div class="css-background-image" style="background-image: url(\''.$path.'min_'.$val['image'].'\')"></div>
            <a href="#" class="btn btn-info btn-flat center-block btn-show" data-image="'.$path."min_".$val['image'].'"><i class="fas fa-search"></i> Ver imagen</a>
            <a href="javascript: removeGalleryImage(\''.$val['id'].'\', this.event)" class="btn btn-danger btn-flat center-block btn-remove-gallery" data-id="'.$val['id'].'"><i class="fas fa-trash-alt"></i> Eliminar</a>
         </div>
         ';
      }

      return $result;
   }
}


?>
