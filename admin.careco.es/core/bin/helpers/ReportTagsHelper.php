<?php

/**
 * Helper para los tags de los reportes
 */
class ReportTagsHelper extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
       parent::__construct($db);
       $this->db = $db;
       $this->admin_id = $_SESSION['ADMIN_SESSION_CARECO']['id'];
    }

    /**
     * Agrega un nuevo tag
     * @return array
     */
    public function persist()
    {
        $db = $this->db;

        $arr_fields = array(
            'name',
            'color'
        );
        $arr_values = array(
            $_POST['name'],
            $_POST['color']
        );

        $tag = $db->insertAction("report_tag", $arr_fields, $arr_values);

        $this->addLogs(sprintf("Agregando tag para los reportes: %s - id: %d", $tag[0]['name'], $tag[0]['id']));

        return $tag[0];
    }

    /**
     * Edita un tag
     * @param  integer $id ID del tag
     * @return array
     */
    public function update($id)
    {
        $db = $this->db;
        $arr_fields = array(
            'name',
            'color'
        );
        $arr_values = array(
            $_POST['name'],
            $_POST['color']
        );

        $tag = $db->updateAction("report_tag", $arr_fields, $arr_values, "id='".$id."'");
        $this->addLogs(sprintf("editando tag para los reportes: %s - id: %d", $tag[0]['name'], $tag[0]['id']));

        return $tag[0];
    }
}


?>
