<?php

/**
 * Se encarga de guardar las secciones para la seccion de nosotros
 */
class AboutUsHelper extends GeneralMethods
{

   public $db;

   function __construct($db)
   {
      parent::__construct($db);
      $this->db = $db;
      $this->admin_id = $_SESSION['ADMIN_SESSION_CARECO']['id'];
   }

   /**
    * Se encarga de guardar la información de la sección
    * @return array
    */
   public function persistSection()
   {
      $arr_fields = array(
         'title',
         'description',
         'create_at',
         'update_at'
      );

      $arr_values = array(
         $_POST['new_title'],
         addslashes($_POST['new_description']),
         date('Y-m-d H:i:s'),
         date('Y-m-d H:i:s')
      );

      $section = $this->db->insertAction("about_us", $arr_fields, $arr_values);

      if (!empty($_FILES['new_image']['tmp_name'])) {
         $this->persistImage($section[0]);
      }

      $this->addLogs(sprintf("Agregando sección de about us: %s - id: %d", $section[0]['title'], $section[0]['id']));

      return $section[0];
   }

   /**
    * Guarda la imagen de la sección
    * @param  array $section Arreglo de ;a sección
    * @return void
    */
   public function persistImage($section)
   {
      $path = APP_IMG_ABOUT_US;
      $extens = str_replace("image/",".",$_FILES['new_image']['type']);
      $img_name = "about_us_".rand(00000, 99999).uniqid().$extens;

      list($width, $height) = getimagesize($_FILES["new_image"]['tmp_name']);

      if (UpPicture("new_image", 200, $img_name, $path)) {
         if ($section['image']) {
            @unlink($path.$section['image']);
            @unlink($path."min_".$section['image']);
         }

         $this->db->updateAction("about_us", array('image'), array($img_name), "id='".$section['id']."'");
      }
   }

   /**
    * Se encarga de editar la seccion de about us
    * @param  integer $i Posición del arreglo
    * @return void
    */
   public function editSection($i)
   {
      $arr_fields = array(
         'title',
         'description',
         'update_at'
      );
      $arr_values = array(
         $_POST['title'][$i],
         addslashes($_POST['description'][$i]),
         date('Y-m-d H:i:s')
      );

      $section = $this->db->updateAction("about_us", $arr_fields, $arr_values, "id='".$_POST['section_id'][$i]."'");

      if (!empty($_FILES['image']['tmp_name'][$i])) {
         $this->persistImageArray($section[0], $i);
      }

      $this->addLogs(sprintf("Editando sección de about us: %s - id: %d", $section[0]['title'], $section[0]['id']));
   }

   /**
    * Guarda las imágenes de las secciones a editar
    * @param  array  $section Arreglo de la sección
    * @param  integer $i      Posición del arreglo
    * @return void
    */
   public function persistImageArray(array $section, $i)
   {
      $path = APP_IMG_ABOUT_US;
      $extens = str_replace("image/",".",$_FILES['image']['type'][$i]);
      $img_name = "about_us_".rand(00000, 99999).uniqid().$extens;

      list($width, $height) = getimagesize($_FILES["image"]['tmp_name'][$i]);

      if (UpPictureArray('image', 200, $img_name, $path, $i)) {
         if ($section['image']) {
            @unlink($path.$section['image']);
            @unlink($path."min_".$section['image']);
         }

         $this->db->updateAction("about_us", array('image'), array($img_name), "id='".$section['id']."'");
      }
   }
}


?>
