<?php

/**
 * Clase que se encarga de guardar los iconos
 */
class IconHelper extends GeneralMethods
{
   public $db;

   function __construct($db)
   {
      parent::__construct($db);
      $this->db = $db;
      $this->admin_id = $_SESSION['ADMIN_SESSION_CARECO']['id'];
   }

   /**
    * Guarda en la bd los iconos para las propiedades
    * @return array
    */
   public function persistIcon()
   {
      $arr_fields = array(
         'description',
         'create_at'
      );
      $arr_values = array(
         $_POST['description'],
         date('Y-m-d H:i:s')
      );

      $icon = $this->db->insertAction("icons", $arr_fields, $arr_values);

      if (!empty($_FILES['icon_image']['tmp_name'])) {
         $extens = str_replace("image/",".",$_FILES['icon_image']['type']);
         $nb_img = "icon_".rand(00000, 99999).uniqid().$extens;

         if (ImagenTemp('icon_image', 100, $nb_img, APP_IMG_ICON)) {
            $this->db->updateAction("icons", array('image'), array($nb_img), "id='".$icon[0]['id']."'");
         } else {
            throw new \Exception("Error al guardar la imagen", 1);
         }
      }

      $this->addLogs(sprintf("Agregando icono: %s - id: %d", $icon[0]['description'], $icon[0]['id']));

      return $icon[0];
   }

   public function editIcon($id)
   {
      $arr_fields = array(
         'description',
         'update_at'
      );
      $arr_values = array(
         $_POST['description'],
         date('Y-m-d H:i:s')
      );

      $icon = $this->db->updateAction("icons", $arr_fields, $arr_values, "id='".$id."'");

      if (!empty($_FILES['icon_image']['tmp_name'])) {
         $extens = str_replace("image/",".",$_FILES['icon_image']['type']);
         $nb_img = "icon_".rand(00000, 99999).uniqid().$extens;

         if (ImagenTemp('icon_image', 100, $nb_img, APP_IMG_ICON)) {
            $this->db->updateAction("icons", array('image'), array($nb_img), "id='".$icon[0]['id']."'");
            @unlink(APP_IMG_ICON.$icon[0]['image']);
         } else {
            throw new \Exception("Error al guardar la imagen", 1);
         }
      }

      $this->addLogs(sprintf("Editando icono: %s - id: %d", $icon[0]['description'], $icon[0]['id']));

      return $icon[0];
   }

}
?>
