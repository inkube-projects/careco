<?php

/**
 * Helper para los reportes
 */
class ReportHelper extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
       parent::__construct($db);
       $this->db = $db;
       $this->admin_id = $_SESSION['ADMIN_SESSION_CARECO']['id'];
    }

    /**
     * Agrega un nuevo reporte
     * @return array
     */
    public function persist()
    {
        $db = $this->db;

        $arr_fields = array(
            'user_id',
            'user_description',
            'comment',
            'departament_id',
            'user_commercial_id',
            'create_at',
            'update_at',
        );
        $arr_values = array(
            $_POST['user'],
            $_POST['message'],
            $_POST['comment'],
            $_POST['departament'],
            $_POST['commercial'],
            date('Y-m-d H:i:s'),
            date('Y-m-d H:i:s')
        );

        $report = $db->insertAction("report", $arr_fields, $arr_values);

        if (isset($_POST['tag'])) {
            foreach ($_POST['tag'] as $val) {
                $db->insertAction("reports_tags", array('report_id', 'tag_id'), array($report[0]['id'], $val));
            }
        }

        $this->addLogs(sprintf("Agregando reporte id: %d", $report[0]['id']));

        return $report[0];
    }

    /**
     * Edita un reporte
     * @param  integer $id ID del tag
     * @return array
     */
    public function update($id)
    {
        $db = $this->db;

        $arr_fields = array(
            'user_id',
            'user_description',
            'comment',
            'departament_id',
            'user_commercial_id',
            'update_at',
        );
        $arr_values = array(
            $_POST['user'],
            $_POST['message'],
            $_POST['comment'],
            $_POST['departament'],
            $_POST['commercial'],
            date('Y-m-d H:i:s')
        );

        $report = $db->updateAction("report", $arr_fields, $arr_values, "id='".$id."'");

        $db->deleteAction("reports_tags", "report_id='".$id."'");
        if (isset($_POST['tag'])) {
            foreach ($_POST['tag'] as $val) {
                $db->insertAction("reports_tags", array('report_id', 'tag_id'), array($report[0]['id'], $val));
            }
        }

        $this->addLogs(sprintf("editando reporte id: %d", $report[0]['id']));

        return $report[0];
    }

    /**
     * Se crea un reportes desde un mensaje
     * @param  integer $contact_id ID del mensaje de contacto
     * @return void
     */
    public function createFromMessage($contact_id)
    {
        $db = $this->db;

        $s = "SELECT * FROM contact WHERE id='".$contact_id."'";
        $arr_contact = $db->fetchSQL($s);

        $user_id = $db->getValue("id", "user", "email='".$arr_contact[0]['email']."'");

        $arr_fields = array(
            'user_id',
            'user_description',
            'message_id',
            'create_at',
            'update_at',
        );
        $arr_values = array(
            $user_id,
            $arr_contact[0]['message'],
            $contact_id,
            date('Y-m-d H:i:s'),
            date('Y-m-d H:i:s'),
        );

        $report = $db->insertAction("report", $arr_fields, $arr_values);
        $this->addLogs(sprintf("Agregando reporte desde un mensaje, id: %d", $report[0]['id']));

        return $report[0];
    }
}


?>
