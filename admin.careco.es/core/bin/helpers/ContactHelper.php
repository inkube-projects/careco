<?php

/**
 * Se encarga de guardar las secciones para la seccion de nosotros
 */
class ContactHelper extends GeneralMethods
{

   public $db;

   function __construct($db)
   {
      parent::__construct($db);
      $this->db = $db;
      $this->admin_id = $_SESSION['ADMIN_SESSION_CARECO']['id'];
   }

   /**
    * Se crea un reportes desde un mensaje
    * @param  integer $contact_id ID del mensaje de contacto
    * @return boolean
    */
   public function readMessage($contact_id,$status)
   {

       $db = $this->db;
       $report=$db->updateAction("contact", array('status'), array($status), "id='".$contact_id."'");
       return  $report;

   }
}

?>
