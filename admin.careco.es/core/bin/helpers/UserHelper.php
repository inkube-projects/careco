<?php

/**
 * Helper para los usuarios
 */
class UserHelper extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
       parent::__construct($db);
       $this->db = $db;
       $this->admin_id = $_SESSION['ADMIN_SESSION_CARECO']['id'];
    }

    /**
     * Agrega un usuario nuevo
     * @return array
     */
    public function persist()
    {
        $db = $this->db;
        $email = mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8");

        $arr_fields = array(
            'username',
            'email',
            'password',
            'role',
            'status',
            'username_canonical',
            'email_canonical',
            'name',
            'last_name',
            'phone_1',
            'phone_2',
            'create_at',
            'update_at',
        );

        $arr_values = array(
            mb_convert_case($_POST['username'], MB_CASE_UPPER, "UTF-8"),
            mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            hashPass($_POST['pass_1'], 12),
            $_POST['role'],
            $_POST['status'],
            mb_convert_case($_POST['username'], MB_CASE_UPPER, "UTF-8"),
            mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            mb_convert_case($_POST['name'], MB_CASE_UPPER, "UTF-8"),
            mb_convert_case($_POST['last_name'], MB_CASE_UPPER, "UTF-8"),
            $_POST['phone_1'],
            $_POST['phone_2'],
            date('Y-m-d H:i:s'),
            date('Y-m-d H:i:s'),
        );

        $user = $db->insertAction("user", $arr_fields, $arr_values);

        $this->addLogs(sprintf("Agregando usuario: %s - id: %d", $user[0]['username'], $user[0]['id']));

        return $user[0];
    }

    /**
     * Edita un usuario
     * @param  integer $id ID del ususario
     * @return void
     */
    public function update($id)
    {
        $db = $this->db;
        $email = mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8");

        $arr_fields = array(
            'username',
            'email',
            'role',
            'status',
            'username_canonical',
            'email_canonical',
            'name',
            'last_name',
            'phone_1',
            'phone_2',
            'update_at',
        );

        $arr_values = array(
            mb_convert_case($_POST['username'], MB_CASE_UPPER, "UTF-8"),
            mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            $_POST['role'],
            $_POST['status'],
            mb_convert_case($_POST['username'], MB_CASE_UPPER, "UTF-8"),
            mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            mb_convert_case($_POST['name'], MB_CASE_UPPER, "UTF-8"),
            mb_convert_case($_POST['last_name'], MB_CASE_UPPER, "UTF-8"),
            $_POST['phone_1'],
            $_POST['phone_2'],
            date('Y-m-d H:i:s'),
        );

        $user = $db->updateAction("user", $arr_fields, $arr_values, "id='".$id."'");

        if (isset($_POST['pass_1']) && $_POST['pass_1'] != "") {
            $user = $db->updateAction("user", array('password'), array(hashPass($_POST['pass_1'], 12)), "id='".$id."'");
        }

        $this->addLogs(sprintf("Editando usuario: %s - id: %d", $user[0]['username'], $user[0]['id']));

        return $user[0];
    }
}


?>
