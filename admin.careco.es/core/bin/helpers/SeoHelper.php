<?php

/**
 * Clase que se encarga del guardado y editado del SEO
 */
class SeoHelper
{
   function __construct()
   {
      $this->db =  new Connection();
   }

   public function persist()
   {
      $delete = $this->db->deleteAction("seo", "");;

      if ($delete) {
         $this->db->insertAction("seo", array('keywords', 'description'), array($_POST['keywords'], $_POST['description']));
      }

      return true;
   }
}


?>
