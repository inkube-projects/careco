<?php

include('core/model/GeneralMethods.php');
include('core/bin/helpers/ReportHelper.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$acc = @number_format($_GET['acc'],0,"","");

if ($_GET) {
    $reportHelper = new ReportHelper($db);

    switch ($acc) {
        case 1: // Se guarda una entrada
            $db->beginTransaction();

            try {
                isRequiredValuesPost($_POST, array('user', 'message'));
                $db->existRecord("id='".@number_format($_POST['user'],0,"","")."'", "user", "El usuario no es válido");
                if ($_POST['departament'] != "") {
                    $db->existRecord("id='".@number_format($_POST['departament'],0,"","")."'", "contact_department", "El departamento no es válido");
                }
                if ($_POST['commercial'] != "") {
                    $db->existRecord("id='".@number_format($_POST['commercial'],0,"","")."'", "user", "El usuario comercial no es válido");
                }
                isValidString($_POST['message']);
                isValidString($_POST['comment']);

                if (isset($_POST['tag'])) {
                    foreach ($_POST['tag'] as $val) {
                        $db->existRecord("id='".@number_format($val,0,"","")."'", "report_tag", "El tag no es válido");
                    }
                }

                $result = $reportHelper->persist();

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente el reporte", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Se edita una entrada
            $db->beginTransaction();
            $id = @number_format($_GET['r'],0,"","");

            try {
                $db->existRecord("id='".$id."'", "report", "El reporte no es válido");
                isRequiredValuesPost($_POST, array('user', 'message'));
                $db->existRecord("id='".@number_format($_POST['user'],0,"","")."'", "user", "El usuario no es válido");
                if ($_POST['departament'] != "") {
                    $db->existRecord("id='".@number_format($_POST['departament'],0,"","")."'", "contact_department", "El departamento no es válido");
                }
                if ($_POST['commercial'] != "") {
                    $db->existRecord("id='".@number_format($_POST['commercial'],0,"","")."'", "user", "El usuario comercial no es válido");
                }
                isValidString($_POST['message']);
                isValidString($_POST['comment']);

                if (isset($_POST['tag'])) {
                    foreach ($_POST['tag'] as $val) {
                        $db->existRecord("id='".@number_format($val,0,"","")."'", "report_tag", "El tag no es válido");
                    }
                }

                $result = $reportHelper->update($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente el reporte", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 3:
            $db->beginTransaction();
            $contact_id = @number_format($_GET['c'],0,"","");

            try {
                $db->existRecord("id='".$contact_id."'", "contact", "Mensaje de contacto no es válido");

                $result = $reportHelper->createFromMessage($contact_id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha creado correctamente el reporte", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
