<?php

include('core/model/GeneralMethods.php');
include('core/bin/helpers/ReportTagsHelper.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$acc = @number_format($_GET['acc'],0,"","");

if ($_GET) {
    $reportTagsHelper = new ReportTagsHelper($db);

    switch ($acc) {
        case 1: // Se guarda una entrada
            $db->beginTransaction();

            try {
                isRequiredValuesPost($_POST, array('name', 'color'));
                isValidString($_POST['name']);

                $result = $reportTagsHelper->persist();

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente el tag", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Se guarda una entrada
            $db->beginTransaction();
            $id = @number_format($_GET['id'],0,"","");

            try {
                $db->existRecord("id='".$id."'", "report_tag", "El tag del reporte no existe");
                isRequiredValuesPost($_POST, array('name', 'color'));
                isValidString($_POST['name']);

                $result = $reportTagsHelper->update($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente el tag", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
