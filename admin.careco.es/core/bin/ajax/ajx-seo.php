<?php

include('core/bin/helpers/SeoHelper.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');

if ($_POST) {
   $seoHelper = new SeoHelper;

   try {
      isRequiredValuesPost($_POST, array('keywords', 'description'));
      isValidString($_POST['keywords']);
      isValidString($_POST['description']);

      $result = $seoHelper->persist();

      $arr_response = array('status' => 'OK', 'message' => 'Se ha guardado correctamente');
   } catch (Exception $e) {
      $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
   }

}
header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
