<?php

include('core/model/GeneralMethods.php');
include('core/bin/helpers/ContactHelper.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$acc = @number_format($_GET['acc'],0,"","");

if ($_GET) {
    $contactHelper = new ContactHelper($db);

    switch ($acc) {
        case 1:
        try {
            $contact_id = @number_format($_POST['id'],0,"","");
            $result= $contactHelper->readMessage($_POST['id'],$_POST['read']);
            $arr_response = array('status' => 'OK', 'data' => $result);
        } catch (Exception $e) {
           $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
        }
     break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
