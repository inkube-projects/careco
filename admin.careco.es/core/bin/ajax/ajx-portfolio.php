<?php

include('core/model/GeneralMethods.php');
include('core/bin/helpers/PortfolioHelper.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$acc = @number_format($_GET['acc'],0,"","");

if ($_GET) {
   $portfolioHelper = new PortfolioHelper($db);

   switch ($acc) {
      case 1: // Se guarda la imagen de promocion en la carpeta temporal
         try {
            isValidPortfolioImage(1);
            $result = $portfolioHelper->prepareCoverImage(1);

            $arr_response = array('status' => 'OK', 'data' => $result);
         } catch (Exception $e) {
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;

      case 2: // se recorta la imagen
      try {
         isRequiredValuesPost($_POST, array('x','y','w','h'));
         $result = $portfolioHelper->prepareCoverImage(2);

         $arr_response = array('status' => 'OK', 'data' => $result);
      } catch (Exception $e) {
         $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
      }
      break;

      case 3: // Se guarda una Obra
         $db->beginTransaction();

         try {
            isRequiredValuesPost($_POST, array('title', 'category'));

            $title = isValidString($_POST['title']);
            $category = isValidCategory($_POST['category']);
            isValidString($_POST['description']);

            $result = $portfolioHelper->persistPortfolio();

            $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente el portafolio", 'id' => $result['id']);
            $db->commit();
         } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;

      case 4: // Se edita una obra
         $db->beginTransaction();

         $id = @number_format($_GET['p'],0,"","");

         try {
            isValidPortfolio($id);
            isRequiredValuesPost($_POST, array('title', 'category'));

            $title = isValidString($_POST['title']);
            $category = isValidCategory($_POST['category']);
            isValidString($_POST['description']);

            $result = $portfolioHelper->editPortfolio($id);

            $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente la Promoción", 'id' => $id);
            $db->commit();
         } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;
   }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

/**
 * Verifica si la promocion existe
 * @param  integer  $id ID de la promoción
 * @return boolean
 */
function isValidPortfolio($id)
{
   $db = new Connection();
   $cnt_val = $db->getCount("portfolio", "id = '".$id."'");

   if ($cnt_val == 0) {
      throw new \Exception("La Obra no existe", 1);
   }

   return true;
}

/**
 * Verifica la imagen de portada de la promoción
 * @param  integer  $acc Acción a ejecutar [1: validar imagen nueva de protada]
 * @return boolean
 */
function isValidPortfolioImage($acc)
{
   if ($acc == 1) { // Imagen de portada
      if (!empty($_FILES['cover_image']['tmp_name'])) {
         $img_type = $_FILES['cover_image']['type'];

         if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type,"jpg"))||(strpos($img_type,"png"))){
            list($width, $height) = getimagesize($_FILES['cover_image']['tmp_name']);

            if (($width < 400) || ($height < 400)) {
               throw new Exception("La imagen debe tener por lo menos 400 x 400px", 1);
            }
         } else {
            throw new Exception("Debes ingresar una imagen válida (jpg, jpeg, png, gif)", 1);
         }
      } else {
         throw new Exception("La imagen de portada no puede estar vacia", 1);
      }
   }
   return true;
}

/**
 * Verifica la categoría del portafolio
 * @param  [type]  $cat [description]
 * @return boolean      [description]
 */
function isValidCategory($cat)
{
   $db = new Connection();
   $cat = @number_format($cat,0,"","");
   $cnt_val = $db->getCount('portfolio_category', "id='".$cat."'");

   if ($cnt_val == 0) {
      throw new \Exception("La categoría no es válida", 1);
   }

   return true;
}

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
