<?php

include('core/model/GeneralMethods.php');
include('core/bin/helpers/TagHelper.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');

if ($_POST) {
   $acc = @number_format($_GET['a'],0,"","");
   $tagHelper = new TagHelper($db);

   switch ($acc) {
      case 1:
         $db->beginTransaction();

         try {
            isRequiredValuesPost($_POST, array('description'));
            isValidString($_POST['description']);

            $result = $tagHelper->persistTag();

            $arr_response = array('status' => 'OK', 'message' => 'Categoría agregado');
            $db->commit();
         } catch (Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;

      case 2:
         $id = @number_format($_GET['t'],0,"","");

         $db->beginTransaction();

         try {
            isValidTag($id);

            $result = $tagHelper->editTag($id);

            $arr_response = array('status' => 'OK', 'message' => 'Categoría editado');
            $db->commit();
         } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }

      break;
   }
}

//------------------------------------------------------------------------------------------------------------

function isValidTag($id)
{
   $db = new Connection;
   $cnt_val = $db->getCount("blog_tags", "id='".$id."'");

   if ($cnt_val == 0) {
      throw new \Exception("La categoría no existe", 1);
   }

   return true;
}

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
