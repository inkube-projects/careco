<?php

include('core/model/GeneralMethods.php');
include('core/bin/helpers/UserHelper.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$acc = @number_format($_GET['acc'],0,"","");

if ($_GET) {
    $userHelper = new UserHelper($db);

    switch ($acc) {
        case 1: // Se guarda una entrada
            $db->beginTransaction();

            try {
                isRequiredValuesPost($_POST, array('name', 'last_name', 'username', 'pass_1', 'pass_2', 'email', 'role', 'status'));
                isValidString($_POST['name']);
                isValidString($_POST['last_name']);
                isValidString($_POST['username']);
                isValidEmail($_POST['email']);
                isValidPhoneNumber($_POST['phone_1']);
                isValidPhoneNumber($_POST['phone_2']);

                $db->existRecord("id='".$_POST['status']."'", "user_status", "El estado de la cuenta no es válido");
                $db->existRecord("id='".$_POST['role']."'", "role", "El rol no es válido");
                $db->duplicatedRecord("email='".$_POST['email']."'", "user", "El email ya se encuentra en uso");
                $db->duplicatedRecord("username='".$_POST['username']."'", "user", "El Nombre de usuario ya se encuentra en uso");

                $result = $userHelper->persist();

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente el post", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Se guarda una entrada
            $db->beginTransaction();
            $id = @number_format($_GET['id'],0,"","");

            try {
                $db->existRecord("id='".$id."'", "user", "El usuario no existe");
                isRequiredValuesPost($_POST, array('name', 'last_name', 'username', 'email', 'role', 'status'));
                isValidString($_POST['name']);
                isValidString($_POST['last_name']);
                isValidString($_POST['username']);
                isValidEmail($_POST['email']);
                isValidPhoneNumber($_POST['phone_1']);
                isValidPhoneNumber($_POST['phone_2']);

                $db->existRecord("id='".$_POST['status']."'", "user_status", "El estado de la cuenta no es válido");
                $db->existRecord("id='".$_POST['role']."'", "role", "El rol no es válido");
                $db->duplicatedRecord("email='".$_POST['email']."' AND NOT id='".$id."'", "user", "El email ya se encuentra en uso");
                $db->duplicatedRecord("username='".$_POST['username']."' AND NOT id='".$id."'", "user", "El Nombre de usuario ya se encuentra en uso");

                $result = $userHelper->update($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente el post", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
