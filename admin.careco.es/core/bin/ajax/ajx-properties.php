<?php

include('core/model/GeneralMethods.php');
include('core/bin/helpers/PropertiesHelper.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$acc = @number_format($_GET['acc'],0,"","");

if ($_GET) {
    $propertiesHelper = new PropertiesHelper($db);

    switch ($acc) {
        case 1: //Subir imagenes
            try {
                isValidGalleryImage(1);

                $result = $propertiesHelper->uploadImages();

                $arr_response = array('status' => 'OK', 'message' => 'Se han subido las imagenes temporales', 'data' => $result);
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: //Eliminar imagen temporal
            $path = APP_IMG_ADMIN."user_".$admin_pp_id."/temp_files/properties/";
            $image = $_GET['img'];

            try {
                if (!fileExist($path.$image)) {
                    throw new \Exception("La imagen no existe", 1);
                }

                @unlink($path.$image);
                @unlink($path."min_".$image);

                $result = $propertiesHelper->getTempImages();

                $arr_response = array('status' => 'OK', 'data' => 'Imagen temporal eliminada', 'data' => $result);
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 3:
            $db->beginTransaction();

            try {
                isRequiredValuesPost($_POST, array('title', 'cover_title', 'description', 'location', 'promotion'));

                isValidString($_POST['title']);
                isValidString($_POST['cover_title']);
                isValidString($_POST['code']);
                isValidString($_POST['location']);
                isValidString($_POST['cost']);
                isValidString($_POST['monthly_cost']);
                isValidPromo($_POST['promotion']);

                if (isset($_POST['outstanding'])) {
                    isValidOutstanding($_POST['outstanding']);
                } else {
                    $_POST['outstanding'] = "0";
                }
                if (isset($_POST['status'])) {
                    isValidStatus($_POST['status']);
                } else {
                    $_POST['status'] = "0";
                }
                if (isset($_POST['sold'])) {
                    isValidStatus($_POST['sold']);
                } else {
                    $_POST['sold'] = "0";
                }

                isValidPDF('quality_memories_doc');
                isValidPDF('blueprints_doc');
                isValidPDF('cee_doc');
                isValidPDF('file_pdf_doc');

                $result = $propertiesHelper->persistProperty();

                if (isset($_POST['desc'])) {
                    foreach ($_POST['desc'] as $val) {
                        isValidString($val);
                    }
                }

                if (isset($_POST['add'])) {
                    foreach ($_POST['add'] as $val) {
                        isValidIcon($val);
                    }

                    $propertiesHelper->persistIcons($_POST['add'], $result['id']);
                }

                if (isset($_FILES['blueprints_image'])) {
                    isValidImage('blueprints_image');
                    $propertiesHelper->persistBluePrintsImage($result);
                }

                if (isset($_FILES['quality_memories_doc'])) {
                    $propertiesHelper->persistPDF($result, 1);
                }

                if (isset($_FILES['blueprints_doc'])) {
                    $propertiesHelper->persistPDF($result, 2);
                }

                if (isset($_FILES['cee_doc'])) {
                    $propertiesHelper->persistPDF($result, 3);
                }

                if (isset($_FILES['file_pdf_doc'])) {
                    $propertiesHelper->persistPDF($result, 4);
                }

                if (verifyTempFiles($admin_pp_id)) {
                    $propertiesHelper->moveImages($result);
                }

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente la propiedad", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 4: // Edita la propiedad
            $id = @number_format($_GET['p'],0,"","");

            isValidProperty($id);

            $db->beginTransaction();

            try {
                isRequiredValuesPost($_POST, array('title', 'cover_title', 'description', 'location', 'promotion'));

                isValidString($_POST['title']);
                isValidString($_POST['cover_title']);
                isValidString($_POST['code']);
                isValidString($_POST['location']);
                isValidString($_POST['cost']);
                isValidString($_POST['monthly_cost']);
                isValidPromo($_POST['promotion']);

                if (isset($_POST['outstanding'])) {
                    isValidOutstanding($_POST['outstanding']);
                } else {
                    $_POST['outstanding'] = "0";
                }
                if (isset($_POST['status'])) {
                    isValidStatus($_POST['status']);
                } else {
                    $_POST['status'] = "0";
                }
                if (isset($_POST['sold'])) {
                    isValidStatus($_POST['sold']);
                } else {
                    $_POST['sold'] = "0";
                }

                isValidPDF('quality_memories_doc');
                isValidPDF('blueprints_doc');
                isValidPDF('cee_doc');
                isValidPDF('file_pdf_doc');

                $result = $propertiesHelper->editProperty($id);

                if (isset($_POST['desc'])) {
                    foreach ($_POST['desc'] as $val) {
                        isValidString($val);
                    }
                }

                if (isset($_POST['add'])) {
                    foreach ($_POST['add'] as $val) {
                        isValidIcon($val);
                    }

                    $propertiesHelper->persistIcons($_POST['add'], $result['id']);
                }

                if (isset($_FILES['blueprints_image'])) {
                    isValidImage('blueprints_image');
                    $propertiesHelper->persistBluePrintsImage($result);
                }

                if (isset($_FILES['quality_memories_doc'])) {
                    $propertiesHelper->persistPDF($result, 1);
                }

                if (isset($_FILES['blueprints_doc'])) {
                    $propertiesHelper->persistPDF($result, 2);
                }

                if (isset($_FILES['cee_doc'])) {
                    $propertiesHelper->persistPDF($result, 3);
                }

                if (isset($_FILES['file_pdf_doc'])) {
                    $propertiesHelper->persistPDF($result, 4);
                }

                if (verifyTempFiles($admin_pp_id)) {
                    $propertiesHelper->moveImages($result);
                }

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente la propiedad", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 5: // Elimina una imagen de galería
            $image_id = number_format($_GET['id'],0,"","");

            try {
                isValidImageID($image_id);

                $data = $propertiesHelper->removeImageGallery($image_id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha eliminado correctamente la imagen", 'data' => $data);
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 6:
            $id = @number_format($_GET['id'],0,'','');
            try {
                $db->existRecord("id='".$id."'", "properties", "La propiedad base no es válida");

                $result = $propertiesHelper->duplicate($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha duplicado correctamente la propiedad", "id" => $result['id']);
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

function isValidStatus($value)
{
    $val = @number_format($value,0,"","");

    if ($val > 1 && $val < 0) {
        throw new \Exception("El estado es inválido", 1);
    }

    return true;
}

/**
 * Valida que el valor destacado sea válido
 * @param  integer  $value Valor del destacado
 * @return boolean
 */
function isValidOutstanding($value)
{
    $val = @number_format($value,0,"","");

    if ($val > 1 && $val < 0) {
        throw new \Exception("El valor destacado es inválido", 1);
    }

    return true;
}

/**
 * Verifica la imagen de portada de la promoción
 * @param  integer  $acc Acción a ejecutar [1: validar imagen nueva de protada]
 * @return boolean
 */
function isValidGalleryImage($acc)
{
    if ($acc == 1) { // Sube imágenes temporales
        $arr_lenght = count($_FILES['gallery_images']['tmp_name']);

        if ($arr_lenght <= 0) {
            throw new Exception("Debes ingresar al menos una imagen", 1);
        }

        for ($i = 0; $i < $arr_lenght; $i++) {
            if (!empty($_FILES['gallery_images']['tmp_name'][$i])) {
                $img_type = $_FILES['gallery_images']['type'][$i];

                if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type,"jpg"))||(strpos($img_type,"png"))){
                    list($width, $height) = getimagesize($_FILES['gallery_images']['tmp_name'][$i]);

                    if (($width < 500) || ($height < 250)) {
                        throw new Exception("La imagen debe tener por lo menos 500 x 250px", 1);
                    }
                } else {
                    throw new Exception("Debes ingresar una imagen válida (jpg, jpeg, png, gif)", 1);
                }
            } else {
                throw new Exception("La imagen de portada no puede estar vacia", 1);
            }
        }
    }
    return true;
}

/**
 * Verifica la categoría
 * @param  integer  $cat Categoría de la propiedad
 * @return boolean
 */
function isValidCategory($cat)
{
    if (($cat != 1) && ($cat != 2)) {
        throw new \Exception("Debes ingresar una categoría válida", 1);
    }

    return true;
}

/**
 * Valida que el icono/descripción sea válida
 * @param  integer  $id ID del icono
 * @return boolean
 */
function isValidIcon($id)
{
    $id = @number_format($id,0,"","");
    $db = new Connection();
    $cnt_val = $db->getCount('icons', "id='".$id."'");

    if ($cnt_val == 0) {
        throw new \Exception("La descripción/icono no es válida", 1);
    }

    return true;
}

/**
 * Valida si el archivo es PDF
 * @param  string  $input Nombre del input
 * @return boolean
 */
function isValidPDF($input)
{
    if (!empty($_FILES[$input]['tmp_name'])) {
        $file_type = $_FILES[$input]['type'];

        if (strpos($file_type, "pdf")){
            return true;
        } else {
            throw new Exception("Debes ingresar un archivo PDF", 1);
        }
    }
}

/**
 * Verifica si la carpeta temporal esta vacía
 * @return boolean
 */
function verifyTempFiles($admin_pp_id)
{
    $path = APP_IMG_ADMIN."user_".$admin_pp_id."/temp_files/properties/";
    $handle = opendir($path);

    while ($file = readdir($handle)){
        if (is_file($path.$file)) {
            return true;
        }
    }

    return false;
}


/**
 * Verifica que la promoción sea válida
 * @param  integer  $promo ID de la promoción
 * @return boolean|exception
 */
function isValidPromo($promo)
{
    $id = @number_format($promo,0,"","");
    $db = new Connection();
    $cnt_val = $db->getCount("promotions", "id='".$id."'");

    if ($cnt_val == 0) {
        throw new \Exception("La promoción no es válida", 1);
    }

    return true;
}

/**
 * Verifica si la imagen de galería exite
 * @param  integer  $id ID de la imagen de galería
 * @return boolean
 */
function isValidImageID($id)
{
    $db = new Connection();
    $cnt_val = $db->getCount("properties_images_gallery", "id='".$id."'");

    if ($cnt_val == 0) {
        throw new \Exception("La imagen de galería no existe", 1);
    }

    return true;
}

/**
 * Verifica si la propiedad existe
 * @param  integer  $id ID de la propiedad
 * @return boolean
 */
function isValidProperty($id)
{
    $db = new Connection;
    $cnt_val = $db->getCount("properties", "id='".$id."'");

    if ($cnt_val == 0) {
        throw new \Exception("La propiedad no existe", 1);
    }

    return true;
}

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
