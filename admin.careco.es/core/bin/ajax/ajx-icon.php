<?php

include('core/model/GeneralMethods.php');
include('core/bin/helpers/IconHelper.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');

if ($_POST) {
   $acc = @number_format($_GET['a'],0,"","");
   $iconHelper = new IconHelper($db);

   switch ($acc) {
      case 1:
         $db->beginTransaction();

         try {
            isRequiredValuesPost($_POST, array('description'));
            isValidString($_POST['description']);
            isValidIconImage();

            $result = $iconHelper->persistIcon();

            $arr_response = array('status' => 'OK', 'message' => 'Icono agregado');
            $db->commit();
         } catch (Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;

      case 2:
         $id = @number_format($_GET['i'],0,"","");

         $db->beginTransaction();

         try {
            isValidIcon($id);
            isValidIconImage();

            $result = $iconHelper->editIcon($id);

            $arr_response = array('status' => 'OK', 'message' => 'Icono editado');
            $db->commit();
         } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }

      break;
   }
}

//------------------------------------------------------------------------------------------------------------

function isValidIcon($id)
{
   $db = new Connection;
   $cnt_val = $db->getCount("icons", "id='".$id."'");

   if ($cnt_val == 0) {
      throw new \Exception("El icono no existe", 1);
   }

   return true;
}

/**
 * Valida la imagen de Icono
 * @return boolean
 */
function isValidIconImage()
{
   if (!empty($_FILES['icon_image']['tmp_name'])) {
      $img_type = $_FILES['icon_image']['type'];

      if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type,"jpg"))||(strpos($img_type,"png"))){
         list($width, $height) = getimagesize($_FILES['icon_image']['tmp_name']);

         if (($width != 100) || ($height != 100)) {
            throw new Exception("La imagen debe ser de 100 x 100px", 1);
         }
      } else {
         throw new Exception("Debes ingresar una imagen válida (jpg, jpeg, png, gif)", 1);
      }
   }

   return true;
}

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
