<?php

include('core/model/GeneralMethods.php');
include('core/bin/helpers/OurMilestones.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');

if ($_POST) {
   $acc = @number_format($_GET['acc'],0,"","");
   $ourMilestones = new OurMilestones($db);

   switch ($acc) {
      case 1: // Agregar la información de introducción
         $db->beginTransaction();

         try {
            isRequiredValuesPost($_POST, array('description'));
            isValidImage();

            $ourMilestones->persistIntro();

            $arr_response = array('status' => 'OK', 'message' => 'Se ha guardado correctamente');
            $db->commit();
         } catch (\Exception $e) {
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            $db->rollBack();
         }
      break;

      case 2: // Agregar nueva entrada de los hitos
         $db->beginTransaction();

         try {
            isRequiredValuesPost($_POST, array('title', 'description'));
            isValidString($_POST['title']);
            isValidString($_POST['description']);

            $result = $ourMilestones->persistEntrie();

            $arr_response = array('status' => 'OK', 'message' => 'Se ha guardado correctamente la entrada', 'id' => $result['id']);
            $db->commit();
         } catch (\Exception $e) {
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            $db->rollBack();
         }
      break;

      case 3: // Edita una entrada de nuestros Hitos
         $id = @number_format($_GET['h'],0,"","");

         $db->beginTransaction();

         try {
            isValidEntry($id);
            isRequiredValuesPost($_POST, array('title', 'description'));
            isValidString($_POST['title']);
            isValidString($_POST['description']);

            $result = $ourMilestones->editEntry($id);

            $arr_response = array('status' => 'OK', 'message' => 'Se ha editada correctamente la entrada', 'id' => $result['id']);
            $db->commit();
         } catch (\Exception $e) {
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            $db->rollBack();
         }
      break;
   }
}

//------------------------------------------------------------------------------------------------------------

/**
 * Valida si la entrada es valida
 * @param  integer  $id ID de la entrada
 * @return boolean
 */
function isValidEntry($id)
{
   $db = new Connection;
   $cnt_val = $db->getCount("our_milestones_entries", "id='".$id."'");

   if ($cnt_val == 0) {
      throw new \Exception("La entrada no existe", 1);
   }

   return true;
}

/**
 * Valida si la imagen es válida
 * @return boolean
 */
function isValidImage()
{
   if (!empty($_FILES['banner']['tmp_name'])) {
      $img_type = $_FILES['banner']['type'];

      if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type,"jpg"))||(strpos($img_type,"png"))){
         list($width, $height) = getimagesize($_FILES['banner']['tmp_name']);

         if (($width < 1000) || ($height < 300)) {
            throw new Exception("La imagen debe tener por lo menos 1000 x 300px", 1);
         }
      } else {
         throw new Exception("Debes ingresar una imagen válida (jpg, jpeg, png, gif)", 1);
      }
   }

   return true;
}

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
