<?php

$db = new Connection();

$arr_response = array("Ha ocurrido un error por favor vuelve a intentarlo");
if (isset($_GET)) {
   $acc = number_format($_GET['method'],0,"","");

   switch ($acc) {
      case 1: // verifica el username
         $arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');

         try {
            isValidPromotionImage(1);

            $arr_response = array('status' => 'OK', 'message' => 'Imagen verificada');
         } catch (\Exception $e) {
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;
   }
}

/**
 * Verifica la imagen de portada de las categorias
 * @return boolean
 */
function isValidPromotionImage($acc)
{
   if ($acc == 1) { // Imagen de portada
      if (!empty($_FILES['cover_image']['tmp_name'])) {
         $img_type = $_FILES['cover_image']['type'];

         if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type,"jpg"))||(strpos($img_type,"png"))){
            list($height, $width) = getimagesize($_FILES['cover_image']['tmp_name']);

            if (($width >= 725) && ($height >= 305)) {
               throw new Exception("La imagen debe tener por lo menos 725 x 305px", 1);
            }
         } else {
            throw new Exception("Debes ingresar una imagen válida (jpg, jpeg, png, gif)", 1);
         }
      } else {
         throw new Exception("La imagen de portada no puede estar vacia", 1);
      }
   }
   return true;
}

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
