<?php

include('core/model/GeneralMethods.php');
include('core/bin/helpers/OpinionHelper.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$acc = @number_format($_GET['acc'],0,"","");

if ($_GET) {
   $opinionHelper = new OpinionHelper($db);

   switch ($acc) {
      case 1: // Se gurda la imagen original en la carpeta temporal
         try {
            isValidProfileImage(1);
            $result = $opinionHelper->prepareProfileImage(1);

            $arr_response = array('status' => 'OK', 'data' => $result);
         } catch (Exception $e) {
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;

      case 2: // Se guarda la imagen recortada en la arpeta tempral
      try {
         isRequiredValuesPost($_POST, array('x','y','w','h'));
         $result = $opinionHelper->prepareProfileImage(2);

         $arr_response = array('status' => 'OK', 'data' => $result);
      } catch (Exception $e) {
         $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
      }
      break;

      case 3: // Se guarda una nueva opinion
         $db->beginTransaction();

         try {
            isRequiredValuesPost($_POST, array('name', 'last_name', 'description'));

            isValidString($_POST['name']);
            isValidString($_POST['last_name']);
            isValidString($_POST['company']);
            isValidString($_POST['description']);

            $result = $opinionHelper->persistOpinion();

            $arr_response = array('status' => 'OK', 'message' => 'Se ha guardado la opinión correctamente', 'id' => $result['id']);
            $db->commit();
         } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;

      case 4: // Se edita una opinion
         $id = @number_format($_GET['o'],0,"","");

         $db->beginTransaction();

         try {
            isValidOpinion($id);
            isRequiredValuesPost($_POST, array('name', 'last_name', 'description'));
            isValidString($_POST['name']);
            isValidString($_POST['last_name']);
            isValidString($_POST['company']);
            isValidString($_POST['description']);

            $result = $opinionHelper->editOpinion($id);

            $arr_response = array('status' => 'OK', 'message' => 'Se ha guardado la opinión correctamente', 'id' => $id);
            $db->commit();
         } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;
   }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

/**
 * Se verifica si la opinión existe
 * @param  integer  $id ID de la opinión
 * @return boolean
 */
function isValidOpinion($id)
{
   $db = new Connection;
   $cnt_val = $db->getCount("opinions", "id='".$id."'");

   if ($cnt_val == 0) {
      throw new \Exception("La opinión no existe", 1);
   }

   return true;
}

/**
 * Verifica la imagen de portada de la promoción
 * @param  integer  $acc Acción a ejecutar [1: validar imagen nueva de protada]
 * @return boolean
 */
function isValidProfileImage($acc)
{
   if ($acc == 1) { // Imagen de portada
      if (!empty($_FILES['profile_image']['tmp_name'])) {
         $img_type = $_FILES['profile_image']['type'];

         if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type,"jpg"))||(strpos($img_type,"png"))){
            list($width, $height) = getimagesize($_FILES['profile_image']['tmp_name']);

            if (($width < 300) || ($height < 300)) {
               throw new Exception("La imagen debe tener por lo menos 300 x 300px", 1);
            }
         } else {
            throw new Exception("Debes ingresar una imagen válida (jpg, jpeg, png, gif)", 1);
         }
      } else {
         throw new Exception("La imagen de portada no puede estar vacia", 1);
      }
   }
   return true;
}

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
