<?php

include('core/model/GeneralMethods.php');
include('core/bin/helpers/AboutUsHelper.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');

if ($_POST) {
   $acc = @number_format($_GET['acc'],0,"","");
   $aboutUsHelper = new AboutUsHelper($db);

   switch ($acc) {
      case 1:

         $db->beginTransaction();

         try {
            isRequiredValuesPost($_POST, array('new_title', 'new_description'));
            isValidString($_POST['new_title']);
            isValidImage();

            $result = $aboutUsHelper->persistSection();

            $arr_response = array('status' => 'OK', 'message' => 'Se ha guardado correctamente', 'id' => $result['id']);
            $db->commit();
         } catch (\Exception $e) {
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            $db->rollBack();
         }
      break;

      case 2:
         $db->beginTransaction();

         try {
            foreach ($_POST['title'] as $key => $val) {
               isValidString($val);
            }
            foreach ($_POST['description'] as $key => $val) {
               if (empty($val)) {
                  throw new \Exception("La descripción no puede estar vacía", 1);
               }
            }

            isValidImageArray();

            foreach ($_POST['section_id'] as $key => $val) {
               $id = @number_format($val,0,"","");
               isValidSection($id);
               $aboutUsHelper->editSection($key);
            }


            $arr_response = array('status' => 'OK', 'message' => 'Se ha guardado correctamente');
            $db->commit();
         } catch (\Exception $e) {
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            $db->rollBack();
         }

      break;
   }
}

//------------------------------------------------------------------------------------------------------------

/**
 * Verifica que exista el id de la sección
 * @param  integer  $id ID de la sección
 * @return boolean
 */
function isValidSection($id)
{
   $db = new Connection;
   $cnt_val = $db->getCount("about_us", "id='".$id."'");

   if ($cnt_val == 0) {
      throw new \Exception("La sección no existe", 1);
   }

   return true;
}

/**
 * Valida si la imagen es válida
 * @return boolean
 */
function isValidImage()
{
   if (!empty($_FILES['new_image']['tmp_name'])) {
      $img_type = $_FILES['new_image']['type'];

      if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type,"jpg"))||(strpos($img_type,"png"))){
         list($width, $height) = getimagesize($_FILES['new_image']['tmp_name']);

         if (($width < 200) || ($height < 200)) {
            throw new Exception("La imagen debe tener por lo menos 200 x 200px", 1);
         }
      } else {
         throw new Exception("Debes ingresar una imagen válida (jpg, jpeg, png, gif)", 1);
      }
   }

   return true;
}

/**
 * Valida si la imagen es válida (Arreglo)
 * @return boolean
 */
function isValidImageArray()
{
   for ($i=0; $i < count($_FILES['image']['tmp_name']); $i++) {
      if (!empty($_FILES['image']['tmp_name'][$i])) {
         $img_type = $_FILES['image']['type'][$i];

         if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type,"jpg"))||(strpos($img_type,"png"))){
            list($width, $height) = getimagesize($_FILES['image']['tmp_name'][$i]);

            if (($width < 200) || ($height < 200)) {
               throw new Exception("La imagen debe tener por lo menos 200 x 200px", 1);
            }
         } else {
            throw new Exception("Debes ingresar una imagen válida (jpg, jpeg, png, gif)", 1);
         }
      }
   }

   return true;
}


header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
