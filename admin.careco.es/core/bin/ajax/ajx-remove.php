<?php

$db = new Connection();
$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');

include('core/model/GeneralMethods.php');
include('core/bin/helpers/RemoveHelper.php');

if ($_GET) {
   $acc = @number_format($_GET['acc'],0,"","");
   $removeHelper = new RemoveHelper($db);

   switch ($acc) {
      case 1: // Eliminar promoción
         $id = @number_format($_GET['p'],0,"","");

         $db->beginTransaction();

         try {
            isValidPromotion($id);

            $removeHelper->removePromotion($id);

            $arr_response = array('status' => 'OK', 'message' => 'La promoción ha sido eliminada correctamente');
            $db->commit();
         } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;

      case 2: // Eliminar iconos
         $id = @number_format($_GET['i'],0,"","");

         $db->beginTransaction();

         try {
            isValidIcon($id);

            $removeHelper->removeIcon($id);

            $arr_response = array('status' => 'OK', 'message' => 'El icono ha sido eliminado correctamente');
            $db->commit();
         } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }

      break;

      case 3: // Elimina una propiedad
         $id = @number_format($_GET['p'],0,"","");

         $db->beginTransaction();

         try {
            isValidProperty($id);

            $removeHelper->removeProperty($id);

            $arr_response = array('status' => 'OK', 'message' => 'La propiedad ha sido eliminada correctamente');
            $db->commit();
         } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;

      case 4: //Eliminar secciones de about us
         $id = @number_format($_GET['s'],0,"","");

         $db->beginTransaction();

         try {
            isValidSection($id);

            $removeHelper->removeSection($id);

            $arr_response = array('status' => 'OK', 'message' => 'La sección ha sido eliminada correctamente');
            $db->commit();
         } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;

      case 5:
         $id = @number_format($_GET['h'],0,"","");

         $db->beginTransaction();

         try {
            isValidEntry($id);

            $removeHelper->removeMilestone($id);

            $arr_response = array('status' => 'OK', 'message' => 'La entrada ha sido eliminada correctamente');
            $db->commit();
         } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;

      case 6:
         $id = @number_format($_GET['o'],0,"","");

         $db->beginTransaction();

         try {
            isValidOpinion($id);

            $removeHelper->removeOpinion($id);

            $arr_response = array('status' => 'OK', 'message' => 'La opinión ha sido eliminada correctamente');
            $db->commit();
         } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;

      case 7: // Categorías del blog
         $id = number_format($_GET['t'],0,"","");

         $db->beginTransaction();

         try {
            isValidCategoryBlog($id);

            $removeHelper->removeBlogCategory($id);

            $arr_response = array('status' => 'OK', 'message' => 'La categoría del blog ha sido eliminada correctamente');
            $db->commit();
         } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;

      case 8: // Newsletter
         $id = number_format($_GET['e'],0,"","");

         $db->beginTransaction();

         try {
            isValidNewsletter($id);

            $removeHelper->removeNewsletter($id);

            $arr_response = array('status' => 'OK', 'message' => 'El email ha sido eliminado correctamente');
            $db->commit();
         } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;

      case 9: // Posts
         $id = number_format($_GET['p'],0,"","");

         $db->beginTransaction();

         try {
            $db->existRecord("id='".$id."'", "post", "La entrada no existe");

            $removeHelper->removePost($id);

            $arr_response = array('status' => 'OK', 'message' => 'El post ha sido eliminado correctamente');
            $db->commit();
         } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;

      case 10: // Mensjaes
         $id = number_format($_GET['c'],0,"","");

         $db->beginTransaction();

         try {
            $db->existRecord("id='".$id."'", "contact", "El mensaje no existe");

            $removeHelper->removeMessage($id);

            $arr_response = array('status' => 'OK', 'message' => 'El mensaje ha sido eliminado correctamente');
            $db->commit();
         } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;

      case 11: // Elimina un usuario
          $id = number_format($_GET['u'],0,"","");

          $db->beginTransaction();

          try {
             $db->existRecord("id='".$id."'", "user", "El usuario no existe");

             $removeHelper->removeUser($id);

             $arr_response = array('status' => 'OK', 'message' => 'El usuario ha sido eliminado correctamente');
             $db->commit();
          } catch (\Exception $e) {
             $db->rollBack();
             $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
          }
      break;

      case 12: // Elimina un tag de reporte
          $id = number_format($_GET['t'],0,"","");

          $db->beginTransaction();

          try {
             $db->existRecord("id='".$id."'", "report_tag", "El tag no existe");

             $removeHelper->removeReportTag($id);

             $arr_response = array('status' => 'OK', 'message' => 'El tag ha sido eliminado correctamente');
             $db->commit();
          } catch (\Exception $e) {
             $db->rollBack();
             $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
          }
      break;

      case 13: // Elimina un reporte
          $id = @number_format($_GET['r'],0,"","");

          $db->beginTransaction();

          try {
             $db->existRecord("id='".$id."'", "report", "El reporte no existe");

             $removeHelper->removeReport($id);

             $arr_response = array('status' => 'OK', 'message' => 'El reporte ha sido eliminado correctamente');
             $db->commit();
          } catch (\Exception $e) {
             $db->rollBack();
             $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
          }
      break;
   }
}

//------------------------------------------------------------------------------

function isValidNewsletter($id) {
    $db = new Connection();
    $cnt_val = $db->getCount("newsletter", "id='".$id."'");

    if ($cnt_val == 0) {
       throw new \Exception("El email no es válido", 1);
    }

    return true;
}

/**
 * Valida si la categoría del blog es válida
 * @param  integer  $id ID de la categoría
 * @return boolean
 */
function isValidCategoryBlog($id)
{
   $db = new Connection();
   $cnt_val = $db->getCount("blog_tags", "id='".$id."'");

   if ($cnt_val == 0) {
      throw new \Exception("La categoría no es válida", 1);
   }

   return true;
}

/**
 * Verifica si una opinión es valida
 * @param  integer  $id ID de la opinión
 * @return boolean
 */
function isValidOpinion($id)
{
   $db = new Connection();
   $cnt_val = $db->getCount("opinions", "id='".$id."'");

   if ($cnt_val == 0) {
      throw new \Exception("La opinión no es válida", 1);
   }

   return true;
}

/**
 * Verifica si la promoción existe
 * @param  integer  $id ID de la promoción
 * @return boolean
 */
function isValidPromotion($id)
{
   $db = new Connection();
   $cnt_val = $db->getCount("promotions", "id='".$id."'");

   if ($cnt_val == 0) {
      throw new \Exception("La promoción no es válida", 1);
   }

   return true;
}

/**
 * Verifica si el icono existe
 * @param  integer  $id ID del icono
 * @return boolean
 */
function isValidIcon($id)
{
   $db = new Connection();
   $cnt_val = $db->getCount("icons", "id='".$id."'");

   if ($cnt_val == 0) {
      throw new \Exception("El icono no existe", 1);
   }

   return true;
}

/**
 * Verifica si la propiedad existe
 * @param  integer  $id ID de la propiedad
 * @return boolean
 */
function isValidProperty($id)
{
   $db = new Connection();
   $cnt_val = $db->getCount("properties", "id='".$id."'");

   if ($cnt_val == 0) {
      throw new \Exception("La propiedad no existe", 1);
   }

   return true;
}

/**
 * Verifica que exista el id de la sección
 * @param  integer  $id ID de la sección
 * @return boolean
 */
function isValidSection($id)
{
   $db = new Connection;
   $cnt_val = $db->getCount("about_us", "id='".$id."'");

   if ($cnt_val == 0) {
      throw new \Exception("La sección no existe", 1);
   }

   return true;
}

/**
 * Verifica si es válido la entrada de nuestros hitos
 * @param  integer  $id ID de la entrada de nuestro hitos
 * @return boolean
 */
function isValidEntry($id)
{
   $db = new Connection;
   $cnt_val = $db->getCount("our_milestones_entries", "id='".$id."'");

   if ($cnt_val == 0) {
      throw new \Exception("La entrada no existe", 1);
   }

   return true;
}

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null;
?>
