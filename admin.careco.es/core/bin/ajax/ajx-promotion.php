<?php

include('core/model/GeneralMethods.php');
include('core/bin/helpers/PromotionHelper.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$acc = @number_format($_GET['acc'],0,"","");

if ($_GET) {
   $promotionHelper = new PromotionHelper($db);

   switch ($acc) {
      case 1: // Se guarda la imagen de promocion en la carpeta temporal
         try {
            isValidPromotionImage(1);
            $result = $promotionHelper->prepareCoverImage(1);

            $arr_response = array('status' => 'OK', 'data' => $result);
         } catch (Exception $e) {
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;

      case 2: // se recorta la imagen
      try {
         isRequiredValuesPost($_POST, array('x','y','w','h'));
         $result = $promotionHelper->prepareCoverImage(2);

         $arr_response = array('status' => 'OK', 'data' => $result);
      } catch (Exception $e) {
         $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
      }
      break;

      case 3: // Se guarda una promoción
         $db->beginTransaction();

         try {
            isRequiredValuesPost($_POST, array('title', 'category'));

            $title = isValidString($_POST['title']);
            $category = isValidCategory($_POST['category']);
            isValidString($_POST['description']);
            isValidString($_POST['latitude']);
            isValidString($_POST['longitude']);

            $result = $promotionHelper->persistPromo();

            if (verifyTempFiles($admin_pp_id)) {
               $promotionHelper->moveImages($result);
            }

            $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente la Promoción", 'id' => $result['id']);
            $db->commit();
         } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;

      case 4:
         $db->beginTransaction();

         $id = @number_format($_GET['p'],0,"","");

         try {
            isValidPromotion($id);
            isRequiredValuesPost($_POST, array('title', 'category'));

            $title = isValidString($_POST['title']);
            $category = isValidCategory($_POST['category']);
            isValidString($_POST['description']);
            isValidString($_POST['latitude']);
            isValidString($_POST['longitude']);

            $result = $promotionHelper->editPromo($id);

            if (verifyTempFiles($admin_pp_id)) {
               $promotionHelper->moveImages($result);
            }

            $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente la Promoción", 'id' => $id);
            $db->commit();
         } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;

      case 5: // Subir imágenes temporales de la galería
         try {
            isValidGalleryImage(1);

            $result = $promotionHelper->uploadImages();

            $arr_response = array('status' => 'OK', 'message' => 'Se han subido las imagenes temporales', 'data' => $result);
         } catch (\Exception $e) {
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;

      case 6: //Eliminar imagen temporal
         $path = APP_IMG_ADMIN."user_".$admin_pp_id."/temp_files/promotions/gallery/";
         $image = $_GET['img'];

         try {
            if (!fileExist($path.$image)) {
               throw new \Exception("La imagen no existe", 1);
            }

            @unlink($path.$image);
            @unlink($path."min_".$image);

            $result = $promotionHelper->getTempImages();

            $arr_response = array('status' => 'OK', 'data' => 'Imagen temporal eliminada', 'data' => $result);
         } catch (\Exception $e) {
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;

      case 7: // Elimina una imagen de galería
         $image_id = number_format($_GET['id'],0,"","");

         try {
            isValidImageID($image_id);

            $data = $promotionHelper->removeImageGallery($image_id);

            $arr_response = array('status' => 'OK', 'message' => "Se ha eliminado correctamente la imagen", 'data' => $data);
         } catch (\Exception $e) {
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }
      break;
   }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

/**
 * Verifica si la promocion existe
 * @param  integer  $id ID de la promoción
 * @return boolean
 */
function isValidPromotion($id)
{
   $db = new Connection();
   $cnt_val = $db->getCount("promotions", "id = '".$id."'");

   if ($cnt_val == 0) {
      throw new \Exception("La promoción no existe", 1);
   }

   return true;
}

/**
 * Verifica la imagen de portada de la promoción
 * @param  integer  $acc Acción a ejecutar [1: validar imagen nueva de protada]
 * @return boolean
 */
function isValidPromotionImage($acc)
{
   if ($acc == 1) { // Imagen de portada
      if (!empty($_FILES['cover_image']['tmp_name'])) {
         $img_type = $_FILES['cover_image']['type'];

         if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type,"jpg"))||(strpos($img_type,"png"))){
            list($width, $height) = getimagesize($_FILES['cover_image']['tmp_name']);

            if (($width < 725) || ($height < 305)) {
               throw new Exception("La imagen debe tener por lo menos 725 x 305px", 1);
            }
         } else {
            throw new Exception("Debes ingresar una imagen válida (jpg, jpeg, png, gif)", 1);
         }
      } else {
         throw new Exception("La imagen de portada no puede estar vacia", 1);
      }
   }
   return true;
}


function isValidCategory($cat)
{
   $cat = @number_format($cat,0,"","");

   if (($cat != 1) && $cat != 2) {
      throw new \Exception("Debes ingresar una categoría válida", 1);
   }

   return true;
}

/**
 * Verifica la imagen de portada de la promoción
 * @param  integer  $acc Acción a ejecutar [1: validar imagen nueva de protada]
 * @return boolean
 */
function isValidGalleryImage($acc)
{
   if ($acc == 1) { // Sube imágenes temporales
      $arr_lenght = count($_FILES['gallery_images']['tmp_name']);

      if ($arr_lenght <= 0) {
         throw new Exception("Debes ingresar al menos una imagen", 1);
      }

      for ($i = 0; $i < $arr_lenght; $i++) {
         if (!empty($_FILES['gallery_images']['tmp_name'][$i])) {
            $img_type = $_FILES['gallery_images']['type'][$i];

            if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type,"jpg"))||(strpos($img_type,"png"))){
               list($width, $height) = getimagesize($_FILES['gallery_images']['tmp_name'][$i]);

               if (($width < 200) || ($height < 200)) {
                  throw new Exception("La imagen debe tener por lo menos 200 x 200px", 1);
               }
            } else {
               throw new Exception("Debes ingresar una imagen válida (jpg, jpeg, png, gif)", 1);
            }
         } else {
            throw new Exception("La imagen de portada no puede estar vacia", 1);
         }
      }
   }
   return true;
}

/**
 * Verifica si la carpeta temporal esta vacía
 * @return boolean
 */
function verifyTempFiles($admin_pp_id)
{
   $path = APP_IMG_ADMIN."user_".$admin_pp_id."/temp_files/promotions/gallery/";
   $handle = opendir($path);

   while ($file = readdir($handle)){
      if (is_file($path.$file)) {
         return true;
      }
   }

   return false;
}

/**
 * Verifica si la imagen de galería exite
 * @param  integer  $id ID de la imagen de galería
 * @return boolean
 */
function isValidImageID($id)
{
   $db = new Connection();
   $cnt_val = $db->getCount("promotions_images_gallery", "id='".$id."'");

   if ($cnt_val == 0) {
      throw new \Exception("La imagen de galería no existe", 1);
   }

   return true;
}

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
