<?php
// nucleo del sitio web
@session_start();
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', 'AJF_19466118');
define('DB_DSN', 'mysql:dbname=careco_dev_bd;host=localhost;charset=utf8');

// ---- Preproducción
// define('DB_USER', 'qzg711');
// define('DB_PASS', 'Careco00');
// define('DB_DSN', 'mysql:dbname=qzg711;host=qzg711.careco.es;charset=utf8');

define('HTML_DIR', 'html/');
define('APP_TITLE', 'Administrador de Contenido');
define('APP_COPY', '&copy; CARECO '.date('Y').' All rights reserved.');
define('APP_IMG', 'views/images/');
define('APP_IMG_PUBLIC', '../careco.es/views/images/');
define('APP_IMG_PROMOTIONS', '../careco.es/views/images/promotions/');
define('APP_NO_IMAGES', '../careco.es/views/images/no_images/');
define('APP_IMG_ADMIN', 'views/images/users/');
define('APP_IMG_ICON', '../careco.es/views/images/icons/');
define('APP_IMG_PROPERTIES', '../careco.es/views/images/properties/');
define('APP_IMG_ABOUT_US', '../careco.es/views/images/about_us/');
define('APP_IMG_MILESTONES', '../careco.es/views/images/milestones/');
define('APP_IMG_OPINION', '../careco.es/views/images/opinion/');
define('APP_IMG_PORTFOLIO', '../careco.es/views/images/portfolio/');
define('APP_IMG_POST', '../careco.es/views/images/post/');

// Datos de email
define('EMAIL_HOST', 'smtpout.secureserver.net');
define('EMAIL_SENDER', 'pruebas@prontopublicidad.com');
define('EMAIL_PASSWORD', 'pronto123');

setlocale(LC_TIME, 'es_VE');
date_default_timezone_set('Europe/Madrid');

require('core/model/classConnection.php');
require('core/bin/functions/main-lib.php');
require('vendor/autoload.php');

?>
