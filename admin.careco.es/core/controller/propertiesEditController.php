<?php

require('core/handler/session-handler.php');

$db = new Connection();
$id = @number_format($_GET['id'],0,"","");
$cnt_val = $db->getCount("properties", "id='".$id."'");
$path_propertie_files = APP_IMG_PROPERTIES."property_".$id."/PDF/";
$path_propertie_images = APP_IMG_PROPERTIES."property_".$id."/images/";
$path_propertie_gallery = APP_IMG_PROPERTIES."property_".$id."/images/gallery/";

if ($cnt_val == 0) {
   header('location: index.php?view=propertiesList'); exit;
}

$s = "SELECT * FROM properties WHERE id='".$id."'";
$arr_propierty = $db->fetchSQL($s);

$s = "SELECT * FROM properties_descriptions WHERE propertie_id='".$id."'";
$arr_descriptions = $db->fetchSQL($s);
$arr_desc = array();
$arr_icon_description = array();

$s = "SELECT * FROM properties_images_gallery WHERE propertie_id='".$id."'";
$arr_gallery = $db->fetchSQL($s);

$s = "SELECT * FROM icons ORDER BY description ASC";
$arr_icons = $db->fetchSQL($s);

$s = "SELECT * FROM promotions";
$arr_promotions = $db->fetchSQL($s);

$title = mb_convert_case($arr_propierty[0]['title'], MB_CASE_TITLE, "UTF-8");
$cover_title = mb_convert_case($arr_propierty[0]['cover_title'], MB_CASE_TITLE, "UTF-8");
$code = $arr_propierty[0]['code'];
$description = $arr_propierty[0]['description'];
$location = $arr_propierty[0]['location'];
$latitude = $arr_propierty[0]['latitude'];
$longitude = $arr_propierty[0]['longitude'];


foreach ($arr_descriptions as $key => $val) {
   $arr_desc[] = $val['icon_id'];
   $arr_icon_description[$val['icon_id']] = $val['description'];
}

// showArr($arr_propierty); exit;

$flash_message = "";
if (isset($_GET['m'])) {
   if ($_GET['m'] == "OK3") {
      $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La propiedad ha sido <b>creada</b> correctamente
      </div>';
   } elseif ($_GET['m'] == "OK4") {
      $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La propiedad ha sido <b>editada</b> correctamente
      </div>';
   }
}

// Se eliminan las imágenes temporales
$path = APP_IMG_ADMIN."user_".$admin_pp_id."/temp_files/properties/";
$handle = opendir($path);

while ($file = readdir($handle)){
   if (is_file($path.$file)) {
      @unlink($path.$file);
   }
}

include('html/properties/properties-edit.php');

?>
