<?php

require('core/handler/session-handler.php');

$db = new Connection();

// Se eliminan las imágenes temporales
$path = APP_IMG_ADMIN."user_".$admin_pp_id."/temp_files/properties/";
$handle = opendir($path);

while ($file = readdir($handle)){
   if (is_file($path.$file)) {
      @unlink($path.$file);
   }
}

$s = "SELECT * FROM icons ORDER BY description ASC";
$arr_icons = $db->fetchSQL($s);

$s = "SELECT * FROM promotions";
$arr_promotions = $db->fetchSQL($s);

include('html/properties/properties-add.php');

?>
