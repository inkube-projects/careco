<?php

require('core/handler/session-handler.php');

$db = new Connection();

$id = @number_format($_GET['i'],0,"","");
$cnt_val = $db->getCount("blog_tags", "id='".$id."'");

if ($cnt_val == 0) {
   header("location: index.php?view=tagList"); exit;
}

$s = "SELECT * FROM blog_tags WHERE id='".$id."'";
$arr_icon = $db->fetchSQL($s);

$name = $arr_icon[0]['name'];

$flash_message = "";
if (isset($_GET['m'])) {
   if ($_GET['m'] == "OK4") {
      $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La categoría ha sido <b>editada</b> correctamente
      </div>';
   }
}

include('html/tags/tag-edit.php');

?>
