<?php

require('core/handler/session-handler.php');

$db = new Connection();

$s = "SELECT * FROM view_report_users";
$arr_sql = $db->fetchSQL($s);
$arr_report = array();

foreach ($arr_sql as $val) {
    $create = datetime_format($val['report_create']);

    $q = "SELECT rts.id, rts.report_id, rts.tag_id, t.name, t.color FROM reports_tags rts INNER JOIN report_tag t ON rts.tag_id = t.id WHERE rts.report_id = '".$val['id']."'";
    $arr_tag = $db->fetchSQL($q);

    $arr_report[] = array(
        'id' => $val['id'],
        'ref' => $val['user_id'].$val['id'],
        'user' => $val['name']." ".$val['last_name'],
        'user_id' => $val['user_id'],
        'create' => $create['date'],
        'tags' => $arr_tag
    );
}

// Se carga el mensaje flash
$flash_message = "";
if (isset($_GET['m'])) {
   if ($_GET['m'] == "OK1") {
      $flash_message = '<div class="alert alert-warning alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         El reporte ha sido <b>eliminado</b> correctamente
      </div>';
   }
}

include('html/reports/report-list.php');
?>
