<?php

require('core/handler/session-handler.php');

$db = new Connection();

$src_category = (isset($_GET['src_category']) && (!empty($_GET['src_category']))) ? number_format($_GET['src_category'],0,'','') : "" ;
$src_promotion = (isset($_GET['src_promotion']) && (!empty($_GET['src_promotion']))) ? number_format($_GET['src_promotion'],0,'','') : "" ;
$src_start_date_create = (isset($_GET['src_start_date_create']) && (!empty($_GET['src_start_date_create']))) ? sanitize($_GET['src_start_date_create']) : "" ;
$src_end_date_create = (isset($_GET['src_end_date_create']) && (!empty($_GET['src_end_date_create']))) ? sanitize($_GET['src_end_date_create']) : "" ;
$src_cost_init = (isset($_GET['src_cost_init']) && (!empty($_GET['src_cost_init']))) ? sanitize($_GET['src_cost_init']) : "" ;
$src_cost_end = (isset($_GET['src_cost_end']) && (!empty($_GET['src_cost_end']))) ? sanitize($_GET['src_cost_end']) : "" ;
$src_status = (isset($_GET['src_status']) && (!empty($_GET['src_status']))) ? number_format($_GET['src_status'],0,'','') : "" ;
$src_outstanding = (isset($_GET['src_outstanding']) && (!empty($_GET['src_outstanding']))) ? number_format($_GET['src_outstanding'],0,'','') : "" ;

$s = "SELECT * FROM properties WHERE NOT id = ''";

if (($src_category != "") && ($src_category != 0)) {
    if ($src_category == 1) {
        $s .= " AND cost <> ''";
    } else {
        $s .= " AND monthly_cost <> ''";
    }
}
if (($src_promotion != "") && ($src_promotion != 0)) {
    $s .= " AND promotion_id = '".$src_promotion."'";
}
if ($src_start_date_create != "") {
    $s .= " AND DATE_FORMAT(create_at, '%Y-%m-%d') >= '".to_date($src_start_date_create)."'";
}
if ($src_end_date_create != "") {
    $s .= " AND DATE_FORMAT(create_at, '%Y-%m-%d') <= '".to_date($src_end_date_create)."'";
}
if ($src_cost_init != "") {
    $s .= " AND cost >= '".$src_cost_init."'";
}
if ($src_cost_end != "") {
    $s .= " AND cost <= '".$src_cost_init."'";
}
if (($src_status != "") && ($src_status != 0)) {
    $s .= " AND status = '".$src_status."'";
}
if (($src_status != "") && ($src_status != 0)) {
    $s .= " AND status = '".$src_status."'";
}
if (($src_outstanding != "") && ($src_outstanding != 0)) {
    $s .= " AND outstanding = '1'";
}

$arr_properties = $db->fetchSQL($s);
$arr_property = array();

$s = "SELECT * FROM promotions";
$arr_promotions = $db->fetchSQL($s);

foreach ($arr_properties as $val) {
    $create_at = datetime_format($val['create_at']);
    $update_at = datetime_format($val['update_at']);

    if ($val['cost']) {
        $cat = "Venta";
    }
    if ($val['monthly_cost']) {
        $cat = "Venta";
    }
    if ($val['monthly_cost'] && $val['cost']) {
        $cat = "Venta, Alquiler";
    }

    $arr_property[] = array(
        'id' => $val['id'],
        'title' => mb_convert_case($val['title'], MB_CASE_TITLE, "UTF-8"),
        'promo' => $db->getValue("title", "promotions", "id='".$val['promotion_id']."'"),
        'create_at' => $create_at['date']." ".$create_at['time'],
        'update_at' => $update_at['date']." ".$update_at['time'],
        'cost' => $val['cost'],
        'code' => $val['code'],
        'cat' => $cat,
    );
}

// Mensaje flash cuando se elimina una promoción
$flash_message = "";
if (isset($_GET['m'])) {
    if ($_GET['m'] == "OK1") {
        $flash_message = '<div class="alert alert-warning alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        La propiedad ha sido <b>Eliminada</b> correctamente
        </div>';
    }
}

include('html/properties/properties-list.php');

?>
