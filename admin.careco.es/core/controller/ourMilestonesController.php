<?php

require('core/handler/session-handler.php');

$db = new Connection();

$s = "SELECT * FROM our_milestones LIMIT 0,1";
$arr_milestones = $db->fetchSQL($s);
$path = APP_IMG_MILESTONES;

$image = APP_IMG_PUBLIC."no_images/promo_no_image.jpg";

if (isset($arr_milestones[0]['cover_image'])) {
   if ($arr_milestones[0]['cover_image']) {
      $image = APP_IMG_MILESTONES.$arr_milestones[0]['cover_image'];
   }
}

$s = "SELECT * FROM our_milestones_entries";
$arr_entries = $db->fetchSQL($s);

// Se carga el mensaje flash
$flash_message = "";
if (isset($_GET['m'])) {
   if ($_GET['m'] == "OK2") {
      $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La información ha sido agregada correctamente
      </div>';
   } elseif ($_GET['m'] == "OK1") {
      $flash_message = '<div class="alert alert-warning alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La entrada ha sido <b>Eliminada</b> correctamente
      </div>';
   }
}

include('html/about-us/our-milestones.php');

?>
