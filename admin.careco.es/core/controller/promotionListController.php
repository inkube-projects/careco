<?php

require('core/handler/session-handler.php');

$db = new Connection();

// Variales de búsqueda
$src_cat = (isset($_GET['src_category']) && (!empty($_GET['src_category']))) ? number_format($_GET['src_category'],0,'','') : "" ;

$s = "SELECT * FROM promotions WHERE NOT id = ''";

if ($src_cat) {
   $s .= " AND category = '".$src_cat."'";
}

$arr_promotions = $db->fetchSQL($s);
$arr_promo = array();

foreach ($arr_promotions as $val) {
   if ($val['category'] == 1) {
      $cat = "Promoción Actual";
   } else {
      $cat = "Proxima Promoción";
   }

   $create_at = datetime_format($val['create_at']);
   $update_at = datetime_format($val['update_at']);

   // showArr($create_at); exit;

   $arr_promo[] = array(
      'id' => $val['id'],
      'title' => mb_convert_case($val['title'], MB_CASE_TITLE, "UTF-8"),
      'cat' => $cat,
      'create_at' => $create_at['date']." ".$create_at['time'],
      'update_at' => $update_at['date']." ".$update_at['time'],
   );
}

// Mensaje flash cuando se elimina una promoción
$flash_message = "";
if (isset($_GET['m'])) {
   if ($_GET['m'] == "OK1") {
      $flash_message = '<div class="alert alert-warning alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La promoción ha sido <b>Eliminada</b> correctamente
      </div>';
   }
}

include('html/promotion/promotion-list.php');

?>
