<?php

require('core/handler/session-handler.php');

$db = new Connection();

$s = "SELECT * FROM post";
$arr_sql = $db->fetchSQL($s);
$arr_posts = array();

foreach ($arr_sql as $val) {
   $create_at = datetime_format($val['create_at']);

   $arr_posts[] = array(
      'id' => $val['id'],
      'title' => $val['title'],
      'create' => $create_at['date']." ".$create_at['time']
   );
}

// Mensaje flash cuando se elimina una Obra
$flash_message = "";
if (isset($_GET['m'])) {
   if ($_GET['m'] == "OK1") {
      $flash_message = '<div class="alert alert-warning alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La Entrada del blog ha sido <b>Eliminada</b> correctamente
      </div>';
   }
}

include('html/posts/post-list.php');
?>
