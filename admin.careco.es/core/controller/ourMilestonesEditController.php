<?php

require('core/handler/session-handler.php');

$db = new Connection();
$id = @number_format($_GET['id'],0,"","");
$cnt_val = $db->getCount("our_milestones_entries", "id='".$id."'");

if ($cnt_val == 0) {
   header('location: index.php?view=ourMilestones');
}

$s = "SELECT * FROM our_milestones_entries WHERE id='".$id."'";
$arr_milestones = $db->fetchSQL($s);

$flash_message = "";
if (isset($_GET['m'])) {
   if ($_GET['m'] == "OK2") {
      $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La entrada ha sido <b>creada</b> correctamente
      </div>';
   } elseif ($_GET['m'] == "OK3") {
      $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La entrada ha sido <b>editada</b> correctamente
      </div>';
   }
}

include('html/about-us/our-milestones-edit.php');

?>
