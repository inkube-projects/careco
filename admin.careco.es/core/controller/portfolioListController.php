<?php

require('core/handler/session-handler.php');

$db = new Connection();

$s = "SELECT * FROM portfolio";
$arr_sql = $db->fetchSQL($s);
$arr_portfolio = array();

foreach ($arr_sql as $val) {
   $arr_portfolio[] = array(
      'id' => $val['id'],
      'title' => $val['title'],
      'category' => $db->getValue('name', 'portfolio_category', "id='".$val['category_id']."'")
   );
}

// Mensaje flash cuando se elimina una Obra
$flash_message = "";
if (isset($_GET['m'])) {
   if ($_GET['m'] == "OK1") {
      $flash_message = '<div class="alert alert-warning alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La Obra ha sido <b>Eliminada</b> correctamente
      </div>';
   }
}

include('html/portfolio/portfolio-list.php');
?>
