<?php

require('core/handler/session-handler.php');

$db = new Connection();

$s = "SELECT * FROM icons";
$arr_icons = $db->fetchSQL($s);


// Se carga el mensaje flash
$flash_message = "";
if (isset($_GET['m'])) {
   if ($_GET['m'] == "OK") {
      $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         El icono ha sido <b>creado</b> correctamente
      </div>';
   }
}

include('html/icons/icon-list.php');

?>
