<?php

require('core/handler/session-handler.php');

$db = new Connection();
$id = @number_format($_GET['id'],0,"","");
$cnt_val = $db->getCount("view_contact_info", "id='".$id."'");

if ($cnt_val == 0) {
   header("location: index.php?view=contactList"); exit;
}

$s = "SELECT * FROM view_contact_info WHERE id='".$id."'";
$arr_contact = $db->fetchSQL($s);

$datetime = datetime_format($arr_contact[0]['create_at']);

$ext = "";
if ($arr_contact[0]['promotion_id']) {
   $ext = '<a href="index.php?view=promotionEdit&id='.$arr_contact[0]['promotion_id'].'" target="_blank">Ir a la promoción</a>';
} elseif ($arr_contact[0]['property_id']) {
   $ext = '<a href="index.php?view=propertiesEdit&id='.$arr_contact[0]['property_id'].'" target="_blank">Ir a la propiedad</a>';
}

$db->updateAction("contact", array('status'), array(2), "id='".$id."'");

$cnt_report = $db->getCount("report", "message_id='".$id."'");
$report_id = $db->getValue("id", "report", "message_id='".$id."'");

$user_id = $db->getValue("id", "user", "email='".$arr_contact[0]['email']."'");

include('html/contact/contact-edit.php');

?>
