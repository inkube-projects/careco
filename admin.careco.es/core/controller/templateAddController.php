<?php

require('core/handler/session-handler.php');

$db = new Connection();

$flash_message = "";
if (isset($_GET['m'])) {
   if ($_GET['m'] == "OK3") {
      $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La plantilla ha sido <b>creada</b> correctamente
      </div>';
   } elseif ($_GET['m'] == "OK4") {
      $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La plantilla ha sido <b>editada</b> correctamente
      </div>';
   }
}

include('html/template/template-add.php');

?>
