<?php

require('core/handler/session-handler.php');

$db = new Connection();
$id = @number_format($_GET['id'],0,"","");
$cnt_val = $db->getCount("user", "id='".$id."'");

if ($cnt_val == 0) {
    header("location: index.php?view=userList"); exit;
}

$s = "SELECT * FROM user WHERE id='".$id."'";
$arr_user = $db->fetchSQL($s);

$s = "SELECT * FROM role WHERE NOT role = 'ROLE_ADMIN'";
$arr_role = $db->fetchSQL($s);

$s = "SELECT * FROM user_status";
$arr_status = $db->fetchSQL($s);

$frm = "frm-edit";
$acc = 2;
$username = $arr_user[0]['username'];
$email = $arr_user[0]['email'];
$role = $arr_user[0]['role'];
$status = $arr_user[0]['status'];
$name = $arr_user[0]['name'];
$last_name = $arr_user[0]['last_name'];
$phone_1 = $arr_user[0]['phone_1'];
$phone_2 = $arr_user[0]['phone_2'];

$flash_message = "";
if (isset($_GET['m'])) {
    if ($_GET['m'] == "OK1") {
        $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            El usuario ha sido <b>creado</b> correctamente
        </div>';
    } elseif ($_GET['m'] == "OK2") {
        $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            El usuario ha sido <b>editado</b> correctamente
        </div>';
    }
}

include('html/user/user-form.php');
?>
