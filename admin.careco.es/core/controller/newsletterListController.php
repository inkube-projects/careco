<?php

require('core/handler/session-handler.php');

$db = new Connection();

$s = "SELECT * FROM newsletterList";
$arr_sql = $db->fetchSQL($s);
$arr_newsletterList = array();

foreach ($arr_sql as $val) {
    $datetime = datetime_format($val['fechaAlta']);

    $arr_newsletterList[] = array(
        'id' => $val['id'],
        'titulo' => $val['titulo'],
		'asunto' => $val['asunto'],
		'descripcion' => $val['descripcion'],
        'enviado' => $val['enviado'],
		'fechaAlta' => $datetime['date']
    );
}

// Se carga el mensaje flash
$flash_message = "";
if (isset($_GET['m'])) {
   if ($_GET['m'] == "OK1") {
      $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         El email ha sido <b>borrado</b> correctamente
      </div>';
   }
}

include('html/newsletter/newsletter-campaign-list.php');


?>
