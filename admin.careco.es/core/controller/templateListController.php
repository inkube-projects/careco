<?php

require('core/handler/session-handler.php');

$db = new Connection;

$s = "SELECT * FROM template";
$arr_template = $db->fetchSQL($s);

foreach ($arr_template as $key => $val) {
    $create_at = datetime_format($val['create_at']);
    $arr_template[$key]['create'] = $create_at['date'];
}

// Mensaje flash cuando se elimina una promoción
$flash_message = "";
if (isset($_GET['m'])) {
   if ($_GET['m'] == "OK1") {
      $flash_message = '<div class="alert alert-warning alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La plantilla ha sido <b>Eliminada</b> correctamente
      </div>';
   }
}

include('html/template/template-list.php');

?>
