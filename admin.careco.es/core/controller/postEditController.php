<?php

require('core/handler/session-handler.php');

$db = new Connection();

$id = @number_format($_GET['id'],0,"","");
$cnt_val = $db->getCount('post', "id = '".$id."'");

if ($cnt_val == 0) {
   header('location: index.php?view=postList'); exit;
}

$s = "SELECT * FROM post WHERE id='".$id."'";
$arr_post = $db->fetchSQL($s);

$s = "SELECT * FROM blog_tags";
$arr_tags = $db->fetchSQL($s);

$s = "SELECT * FROM post_tags WHERE post_id='".$id."'";
$arr_post_tags = $db->fetchSQL($s);
$arr_tags_s = array();

foreach ($arr_post_tags as $val) {
   $arr_tags_s[] = $val['tag_id'];
}

// showArr($arr_tags_s); exit;


// se verifica la imagen
$image = APP_NO_IMAGES."promo_no_image.jpg";

if ($arr_post[0]['image']) {
   $image = APP_IMG_POST.$arr_post[0]['image'];
}

// Se eliminan las imágenes temporales
$path = APP_IMG_ADMIN."user_".$admin_pp_id."/temp_files/post/";
$handle = opendir($path);

while ($file = readdir($handle)){
   if (is_file($path.$file)) {
      @unlink($path.$file);
   }
}

$flash_message = "";
if (isset($_GET['m'])) {
   if ($_GET['m'] == "OK3") {
      $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La entrada ha sido <b>creada</b> correctamente
      </div>';
   } elseif ($_GET['m'] == "OK4") {
      $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La entrada ha sido <b>editada</b> correctamente
      </div>';
   }
}

include('html/posts/post-edit.php');

?>
