<?php

require('core/handler/session-handler.php');

$db = new Connection();
$id = number_format($_GET['id'],0,"","");
$cnt_val = $db->getCount('portfolio', "id='".$id."'");

if ($cnt_val == 0) {
   header('location: index.php?view=portfolioList'); exit;
}

$s = "SELECT * FROM portfolio WHERE id='".$id."'";
$arr_portfolio = $db->fetchSQL($s);

$image = APP_NO_IMAGES."promo_no_image.jpg";
if ($arr_portfolio[0]['image'] != "") {
   $image = APP_IMG_PORTFOLIO.$arr_portfolio[0]['image'];
}

$s = "SELECT * FROM portfolio_category";
$arr_category = $db->fetchSQL($s);

// Se eliminan las imágenes temporales
$path = APP_IMG_ADMIN."user_".$admin_pp_id."/temp_files/promotions/";
$handle = opendir($path);

while ($file = readdir($handle)){
   if (is_file($path.$file)) {
      @unlink($path.$file);
   }
}

$flash_message = "";
if (isset($_GET['m'])) {
   if ($_GET['m'] == "OK3") {
      $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La Obra ha sido <b>creada</b> correctamente
      </div>';
   } elseif ($_GET['m'] == "OK4") {
      $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La Obra ha sido <b>editada</b> correctamente
      </div>';
   }
}

include('html/portfolio/portfolio-edit.php');

?>
