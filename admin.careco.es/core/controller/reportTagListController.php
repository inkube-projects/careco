<?php

require('core/handler/session-handler.php');

$db = new Connection();

$s = "SELECT * FROM report_tag";
$arr_tags = $db->fetchSQL($s);

// Se carga el mensaje flash
$flash_message = "";
if (isset($_GET['m'])) {
   if ($_GET['m'] == "OK1") {
      $flash_message = '<div class="alert alert-warning alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         El tag ha sido <b>eliminado</b> correctamente
      </div>';
   }
}

include('html/reports/report-tags-list.php');
?>
