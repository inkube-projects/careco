<?php

require('core/handler/session-handler.php');

$db = new Connection();

$id = @number_format($_GET['i'],0,"","");
$cnt_val = $db->getCount("icons", "id='".$id."'");

if ($cnt_val == 0) {
   header("location: index.php?view=icon"); exit;
}

$s = "SELECT * FROM icons WHERE id='".$id."'";
$arr_icon = $db->fetchSQL($s);

$title = $arr_icon[0]['description'];

$flash_message = "";
if (isset($_GET['m'])) {
   if ($_GET['m'] == "OK4") {
      $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La especificación ha sido <b>editada</b> correctamente
      </div>';
   }
}

include('html/icons/icon-edit.php');

?>
