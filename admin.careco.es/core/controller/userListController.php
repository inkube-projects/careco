<?php

require('core/handler/session-handler.php');

$db = new Connection();

$src_start_date_create = (isset($_GET['src_start_date_create']) && (!empty($_GET['src_start_date_create']))) ? sanitize($_GET['src_start_date_create']) : "" ;
$src_end_date_create = (isset($_GET['src_end_date_create']) && (!empty($_GET['src_end_date_create']))) ? sanitize($_GET['src_end_date_create']) : "" ;

$s = "SELECT * FROM view_users WHERE NOT role_id = '1'";

if ($src_start_date_create != "") {
    $s .= " AND DATE_FORMAT(create_at, '%Y-%m-%d') >= '".to_date($src_start_date_create)."'";
}
if ($src_end_date_create != "") {
    $s .= " AND DATE_FORMAT(create_at, '%Y-%m-%d') <= '".to_date($src_end_date_create)."'";
}

$arr_sql = $db->fetchSQL($s);
$arr_users = array();

foreach ($arr_sql as $val) {
    $date = datetime_format($val['create_at']);

    $arr_users[] = array(
        'id' => $val['id'],
        'names' => $val['name']." ".$val['last_name'],
        'email' => $val['email'],
        'username' => $val['username'],
        'status' => $val['status_name'],
        'role' => $val['role_name'],
        'create' => $date['date']
    );
}


// Se carga el mensaje flash
$flash_message = "";
if (isset($_GET['m'])) {
   if ($_GET['m'] == "OK1") {
      $flash_message = '<div class="alert alert-warning alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         El ususario ha sido <b>eliminado</b> correctamente
      </div>';
   }
}

include('html/user/user-list.php');

?>
