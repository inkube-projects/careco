<?php

require('core/handler/session-handler.php');

$db = new Connection();

$src_status = (isset($_GET['src_status']) && (!empty($_GET['src_status']))) ? number_format($_GET['src_status'],0,'','') : "" ;
$src_init_date = (isset($_GET['src_init_date']) && (!empty($_GET['src_init_date']))) ? sanitize($_GET['src_init_date']) : "" ;
$src_end_date = (isset($_GET['src_end_date']) && (!empty($_GET['src_end_date']))) ? sanitize($_GET['src_end_date']) : "" ;
$src_departament = (isset($_GET['src_departament'])) ? $_GET['src_departament'] : array() ;


$s = "SELECT * FROM contact_department";
$arr_departament = $db->fetchSQL($s);

$s = "SELECT * FROM view_contact_info WHERE NOT id = ''";

if (($src_status != "") && ($src_status != 0)) {
   $s .= " AND status = '".$src_status."'";
}
if ($src_init_date != "") {
   $src_init_date = str_replace('/', '-', $src_init_date);
   $s .= " AND DATE_FORMAT(create_at, '%Y-%m-%d') >= '".to_date($src_init_date)."'";
}
if ($src_end_date != "") {
   $src_end_date = str_replace('/', '-', $src_end_date);
   $s .= " AND DATE_FORMAT(create_at, '%Y-%m-%d') <= '".to_date($src_end_date)."'";
}

if (count($src_departament) > 0) {
   $s .= " AND (";
   foreach ($src_departament as $key => $val) {
      if ($key == 0) {
         $s .= "department_id = '".$val."'";
      } else {
         $s .= " OR department_id = '".$val."'";
      }
   }
   $s .= ")";
}

$s.= " ORDER BY id DESC";

// showArr($s); exit;

$arr_sql = $db->fetchSQL($s);
$arr_contact = array();
$cnt = 1;

foreach ($arr_sql as $val) {
   $datetime = datetime_format($val['create_at']);
   $class = "";

   if ($val['status'] == 1) {
      $class = "css-active";
   }

   $arr_contact[] = array(
      'ref' => $cnt,
      'id' => $val['id'],
      'email' => $val['email'],
      'phone' => $val['phone'],
      'departament_name' => $val['departament_name'],
      'date' => $datetime['date'],
      'status' => ($val['status'] == 1) ? "No leído" : "Leído",
      'class' => $class
   );

   $cnt++;
}

// showArr(); exit;

// Se carga el mensaje flash
$flash_message = "";
if (isset($_GET['m'])) {
   if ($_GET['m'] == "OK") {
      $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La opinion ha sido <b>borrada</b> correctamente
      </div>';
   }
}

include('html/contact/contact-list.php');

?>
