<?php

require('core/handler/session-handler.php');

$db = new Connection();
$id = @number_format($_GET['id'],0,"","");
$cnt_val = $db->getCount('promotions', "id = '".$id."'");
$path_gallery = APP_IMG_PROMOTIONS."gallery/";

if ($cnt_val == 0) {
   header('location: index.php?view=promotionList'); exit;
}

$flash_message = "";
if (isset($_GET['m'])) {
   if ($_GET['m'] == "OK3") {
      $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La promoción ha sido <b>creada</b> correctamente
      </div>';
   } elseif ($_GET['m'] == "OK4") {
      $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La promoción ha sido <b>editada</b> correctamente
      </div>';
   }
}

$s = "SELECT * FROM promotions WHERE id = '".$id."'";
$arr_promotion = $db->fetchSQL($s);

$s = "SELECT * FROM promotions_images_gallery WHERE promotion_id = '".$id."'";
$arr_gallery = $db->fetchSQL($s);

$title = $arr_promotion[0]['title'];
$image = APP_NO_IMAGES."promo_no_image.jpg";
$description = $arr_promotion[0]['description'];
$location = $arr_promotion[0]['location'];
$latitude = $arr_promotion[0]['latitude'];
$longitude = $arr_promotion[0]['longitude'];
$create_at = datetime_format($arr_promotion[0]['create_at']);
$update_at = datetime_format($arr_promotion[0]['update_at']);

if ($arr_promotion[0]['image'] != "") {
   $image = APP_IMG_PROMOTIONS."cover/".$arr_promotion[0]['image'];
}

// Se eliminan las imágenes temporales
$path = APP_IMG_ADMIN."user_".$admin_pp_id."/temp_files/promotions/gallery/";
$handle = opendir($path);

while ($file = readdir($handle)){
   if (is_file($path.$file)) {
      @unlink($path.$file);
   }
}

include('html/promotion/promotion-edit.php');

?>
