<?php

require('core/handler/session-handler.php');

$db = new Connection();

$s = "SELECT * FROM newsletter";
$arr_sql = $db->fetchSQL($s);
$arr_newsletter = array();

foreach ($arr_sql as $val) {
    $datetime = datetime_format($val['create_at']);

    $arr_newsletter[] = array(
        'id' => $val['id'],
        'email' => $val['email'],
        'create_at' => $datetime['date']
    );
}

// Se carga el mensaje flash
$flash_message = "";
if (isset($_GET['m'])) {
   if ($_GET['m'] == "OK1") {
      $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         El email ha sido <b>borrado</b> correctamente
      </div>';
   }
}

include('html/newsletter/newsletter-list.php');

?>
