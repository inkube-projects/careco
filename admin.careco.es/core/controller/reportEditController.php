<?php

require('core/handler/session-handler.php');

$db = new Connection();
$id = @number_format($_GET['id'],0,"","");
$cnt_val = $db->getCount("report", "id='".$id."'");

if ($cnt_val == 0) {
    header('location: index.php?view=reportList'); exit;
}

$s = "SELECT * FROM report WHERE id='".$id."'";
$arr_report = $db->fetchSQL($s);

$s = "SELECT * FROM report_tag";
$arr_tags = $db->fetchSQL($s);

$s = "SELECT * FROM user";
$arr_user = $db->fetchSQL($s);

$s = "SELECT * FROM user WHERE role ='4'";
$arr_commercial = $db->fetchSQL($s);

$s = "SELECT * FROM contact_department";
$arr_departament = $db->fetchSQL($s);

$s = "SELECT * FROM reports_tags WHERE report_id='".$id."'";
$arr_report_tag = $db->fetchSQL($s);
$tag = array();

foreach ($arr_report_tag as $val) {
    $tag[] = $val['tag_id'];
}

$frm = "frm-edit";
$acc = 2;
$user = $arr_report[0]['user_id'];
$message = $arr_report[0]['user_description'];
$comment = $arr_report[0]['comment'];
$message_id = $arr_report[0]['message_id'];
$departament = $arr_report[0]['departament_id'];
$commercial = $arr_report[0]['user_commercial_id'];

if ($message_id) {
    $s = "SELECT * FROM contact WHERE id='".$message_id."'";
    $arr_message = $db->fetchSQL($s);
}

// Se carga el mensaje flash
$flash_message = "";
if (isset($_GET['m'])) {
    if ($_GET['m'] == "OK1") {
        $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        El reporte ha sido <b>creado</b> correctamente
        </div>';
    } elseif ($_GET['m'] == "OK2") {
        $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        El reporte ha sido <b>editado</b> correctamente
        </div>';
    }
}

include('html/reports/report-form.php');
?>
