<?php

require('core/handler/session-handler.php');

$db = new Connection();
$id = @number_format($_GET['id'],0,"","");
$cnt_val = $db->getCount("report_tag", "id='".$id."'");

if ($cnt_val == 0) {
    header("location: index.php?view=reportTagList"); exit;
}

$s = "SELECT * FROM report_tag WHERE id='".$id."'";
$arr_tag = $db->fetchSQL($s);

$frm = "frm-edit"; $acc = 2;
$name = $arr_tag[0]['name'];
$color = $arr_tag[0]['color'];

// Se carga el mensaje flash
$flash_message = "";
if (isset($_GET['m'])) {
    if ($_GET['m'] == "OK1") {
        $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        El tag ha sido <b>creado</b> correctamente
        </div>';
    } elseif ($_GET['m'] == "OK2") {
        $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        El tag ha sido <b>editado</b> correctamente
        </div>';
    }
}

include('html/reports/report-tags-form.php');
?>
