<?php

require('core/handler/session-handler.php');

$db = new Connection();

$s = "SELECT * FROM about_us";
$arr_aboutUs = $db->fetchSQL($s);

// Se carga el mensaje flash
$flash_message = "";
if (isset($_GET['m'])) {
   if ($_GET['m'] == "OK1") {
      $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La sección ha sido <b>creado</b> correctamente
      </div>';
   } elseif ($_GET['m'] == "OK2") {
      $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La sección ha sido <b>Editada</b> correctamente
      </div>';
   }
}

include('html/about-us/about-us.php');

?>
