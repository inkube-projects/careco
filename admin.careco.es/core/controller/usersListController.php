<?php

require('core/handler/session-handler.php');

$db = new Connection();

$s = "SELECT * FROM users";
$arr_users = $db->fetchSQL($s);
$arr_user = array();

foreach ($arr_users as $val) {
   $datetime = datetime_format($val['create_at']);

   $arr_user[] = array(
      'id' => $val['id'],
      'name' => mb_convert_case($val['name']." ".$val['last_name'], MB_CASE_TITLE, "UTF-8"),
      'create_at' => $datetime['date']." ".$datetime['time'],
   );
}


// Se carga el mensaje flash
$flash_message = "";
if (isset($_GET['m'])) {
   if ($_GET['m'] == "OK") {
      $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         La user ha sido <b>borrada</b> correctamente
      </div>';
   }
}

include('html/user/user-list.php');

?>
