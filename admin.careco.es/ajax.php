<?php

require('core/core.php');

if (isset($_GET['m'])) {
    require('core/handler/access-ajax-handler.php');

    if ($_GET['m'] == "login") {  // Inicio de sesión
        require('core/bin/ajax/ajx-session.php');
    } elseif ($_GET['m'] == "promotion") {  // Promociones
        require('core/bin/ajax/ajx-promotion.php');
    } elseif ($_GET['m'] == "remove") {  // Eliminar
        require('core/bin/ajax/ajx-remove.php');
    } elseif ($_GET['m'] == "icon") {  // Iconos
        require('core/bin/ajax/ajx-icon.php');
    } elseif ($_GET['m'] == "properties") {  // Propiedades
        require('core/bin/ajax/ajx-properties.php');
    }  elseif ($_GET['m'] == "aboutUs") {  // Sección de nosotros
        require('core/bin/ajax/ajx-about-us.php');
    } elseif ($_GET['m'] == "ourMilestones") {  // Sección de nosotros
        require('core/bin/ajax/ajx-our-milestones.php');
    } elseif ($_GET['m'] == "opinion") {  // Sección de las opiniones
        require('core/bin/ajax/ajx-opinion.php');
    } elseif ($_GET['m'] == "portfolio") {  // Sección de Portafolio
        require('core/bin/ajax/ajx-portfolio.php');
    } elseif ($_GET['m'] == "tag") {  // Sección de Portafolio
        require('core/bin/ajax/ajx-tag.php');
    } elseif ($_GET['m'] == "post") {  // Sección de Portafolio
        require('core/bin/ajax/ajx-post.php');
    } elseif ($_GET['m'] == "user") {  // Usuarios
        require('core/bin/ajax/ajx-user.php');
    } elseif ($_GET['m'] == "report-tag") {  // tags de los reportes
        require('core/bin/ajax/ajx-report-tag.php');
    } elseif ($_GET['m'] == "report") {  // tags de los reportes
        require('core/bin/ajax/ajx-report.php');
    } elseif ($_GET['m'] == "contact") {  // Sección de Contactos
        require('core/bin/ajax/ajx-contact.php');
    } else {
        header('location: index.php');
    }
} else {
    include('location: index.php');
}

?>
