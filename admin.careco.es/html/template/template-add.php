<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- Date picker -->
        <link rel="stylesheet" href="views/app/plugins/datepicker/datepicker3.css">
        <!-- Select 2 -->
        <link rel="stylesheet" href="views/app/plugins/select2/select2.min.css">
        <!-- Datatables -->
        <link rel="stylesheet" href="views/app/plugins/datatables/dataTables.bootstrap.css">
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Plantillas
                            <small>Agregar plantilla</small>
                        </h3>
                    </div>
                </div> <!-- / .row -->

                <section class="content">
                    <div class="js-alert"></div>

                    <form name="frm-add" id="frm-add" method="post" action="">
                        <input type="hidden" name="hid-name" id="hid-name" value="">
                        <div class="box box-solid">
                            <div class="box-header">
                                <h2 class="box-title">Agregar plantilla</h2>
                            </div>

                            <div class="box-body">
                                <div class="form-group">
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Agregar el nombre de la plantilla">
                                    <div class="text-danger" id="name_validate"></div>
                                </div>
                            </div>
                        </div>

                        <div class="box box-solid">
                            <div class="box-header">
                                <h2 class="box-title">Crear plantilla HTML</h2>
                            </div>

                            <div class="box-body">
                                <textarea name="email_template" id="email_template" rows="8"></textarea>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
                    </form>
                </section>

                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->

        <div id="key" data-form="frm-add" data-acc="1"></div>

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>

        <!-- CKeditor -->
        <script type="text/javascript" src="views/app/plugins/ckeditor/ckeditor.js"></script>
        <!-- Select2 -->
        <script src="views/app/plugins/select2/select2.full.min.js"></script>
        <!-- jQuery validation -->
        <script type="text/javascript" src="views/app/plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="views/app/plugins/jquery-validation/additional-methods.js"></script>
        <!-- Custom JS -->
        <script src="views/app/js/js-template.js" type="text/javascript"></script>
    </body>
</html>
