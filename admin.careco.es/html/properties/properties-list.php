<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="admin">
      <meta name="author" content="Inkube">
      <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
      <title><?=APP_TITLE?></title>
      <!-- Date picker -->
      <link rel="stylesheet" href="views/app/plugins/datepicker/datepicker3.css">
      <!-- Select 2 -->
      <link rel="stylesheet" href="views/app/plugins/select2/select2.min.css">
      <!-- Datatables -->
      <link rel="stylesheet" href="views/app/plugins/datatables/dataTables.bootstrap.css">
      <?php include('html/overall/header.php'); ?>
   </head>

   <body>

      <div class="wrapper">
         <div class="container-fluid">
            <?php include('html/overall/topnav.php'); ?>

            <div class="row">
               <div class="col-xs-12">
                  <h3 class="page-header pull-left">
                     Propiedades
                     <small>Listado de propiedades</small>
                  </h3>

                  <div class="col-md-2 pull-right css-marginT35">
                     <a href="index.php?view=propertiesAdd" class="btn btn-info btn-flat pull-right">Nueva propiedad</a>
                  </div>
               </div>
            </div> <!-- / .row -->

            <section class="content">
               <form name="frm-search" id="frm-search" method="get" action="">
                  <input type="hidden" name="view" id="view" value="propertiesList">
                  <div class="box box-solid">
                     <div class="box-header">
                        <h2 class="box-title">Búsqueda avanzada</h2>
                     </div>

                     <div class="box-body">
                        <div class="row css-marginB20">
                           <div class="col-md-4">
                              <select class="form-control select2" name="src_category" id="src_category">
                                 <option value="">Seleccione una categoría</option>
                                 <option value="1" <?=isSelected(1, $src_category)?>>Venta</option>
                                 <option value="2" <?=isSelected(2, $src_category)?>>Alquiler</option>
                              </select>
                           </div>

                           <div class="col-md-4">
                              <select class="form-control select2" name="src_promotion" id="src_promotion">
                                 <option value="">Seleccione una promoción</option>
                                 <?php foreach ($arr_promotions as $val): ?>
                                    <option value="<?=$val['id']?>" <?=isSelected($val['id'], $src_promotion)?>><?=$val['title']?> - <?=($val['category'] == 1) ? "Actual" : "Proxima"?></option>
                                 <?php endforeach; ?>
                              </select>
                           </div>

                           <div class="col-md-4">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <input type="text" class="form-control js-date" name="src_start_date_create" id="src_start_date_create" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask placeholder="F. creación (I)" value="<?=$src_start_date_create?>">
                                    </div>
                                 </div>

                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <input type="text" class="form-control js-date" name="src_end_date_create" id="src_end_date_create" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask placeholder="F. creación (F)" value="<?=$src_end_date_create?>">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="row css-marginB20">
                           <div class="col-md-4">
                              <select class="form-control select2" name="src_status" id="src_status">
                                 <option value="">Todos</option>
                                 <option value="1" <?=isSelected(1, $src_status)?>>Habilitados</option>
                                 <option value="2" <?=isSelected(2, $src_status)?>>Deshabilitados</option>
                              </select>
                           </div>

                           <div class="col-md-4">
                              <select class="form-control select2" name="src_outstanding" id="src_outstanding">
                                 <option value="">Todos</option>
                                 <option value="1" <?=isSelected(1, $src_outstanding)?>>Destacados</option>
                              </select>
                           </div>
                        </div>

                        <!-- <div class="row">
                           <div class="col-md-4">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <input type="text" class="form-control" name="src_cost_init" id="src_cost_init" placeholder="Costo (I)" value="<?=$src_cost_init?>">
                                    </div>
                                 </div>

                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <input type="text" class="form-control" name="src_cost_end" id="src_cost_end" placeholder="Costo (F)" value="<?=$src_cost_end?>">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div> -->
                     </div>

                     <div class="box-footer">
                        <button type="button" class="btn btn-warning btn-flat btn-reset" onclick="formReset('frm-search')"><i class="fa fa-brush"></i> Limpiar formulario</button>
                        <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fas fa-search"></i> Buscar</button>
                     </div>
                  </div>
               </form>

               <div class="js-alert"><?=$flash_message?></div>

               <div class="box box-solid">
                  <div class="box-header">
                     <h2 class="box-title">Listado de propiedades</h2>
                  </div>
                  <div class="box-body">
                     <table class="table table-hover">
                        <thead>
                           <tr>
                              <th>Ref.</th>
                              <th>Code</th>
                              <th>Título</th>
                              <th>Categoría</th>
                              <th>Promo.</th>
                              <th>Creada</th>
                              <th>Costo</th>
                              <th>Acciones</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach ($arr_property as $val): ?>
                              <tr>
                                 <td><?=$val['id']?></td>
                                 <td><?=$val['code']?></td>
                                 <td><?=$val['title']?></td>
                                 <td><?=$val['cat']?></td>
                                 <td><?=$val['promo']?></td>
                                 <td><?=$val['create_at']?></td>
                                 <td><?=$val['cost']?></td>
                                 <td>
                                    <a href="index.php?view=propertiesEdit&id=<?=$val['id']?>" class="btn btn-info btn-flat">Editar</a>
                                    <?php if ($role === "ROLE_ADMIN"): ?>
                                       <a href="javascript: deleteBoostrapAction('remove&p=<?=$val['id']?>&acc=3', 1)" class="btn btn-danger btn-flat">Eliminar</a>
                                    <?php endif; ?>
                                 </td>
                              </tr>
                           <?php endforeach; ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </section>

            <div class="modal fade modal-danger" tabindex="-1" role="dialog" id="mod-remove">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Eliminar propiedad</h4>
                     </div>
                     <div class="modal-body">
                        <p>Estas seguro de eliminar esta Propiedad</p>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No eliminar</button>
                        <button type="button" class="btn btn-danger btn-flat" id="mod-remove-btn" data-id="" onclick="deleteBoostrapAction('',2)"><i class="fa fa-trash"></i> Eliminar</button>
                     </div>
                  </div>
               </div>
            </div>

            <!-- Footer -->
            <?php include('html/overall/footer.php') ?>
         </div> <!-- / .container-fluid -->
      </div> <!-- / .wrapper -->

      <!-- JavaScript
      ================================================== -->
      <!-- JS Global -->
      <?php include('html/overall/js.php'); ?>

      <script src="views/app/plugins/select2/select2.full.min.js"></script>
      <!-- bootstrap datepicker -->
      <script src="views/app/plugins/datepicker/bootstrap-datepicker.js"></script>
      <!-- DataTables -->
      <script src="views/app/plugins/datatables/jquery.dataTables.js"></script>
      <script src="views/app/plugins/datatables/dataTables.bootstrap.min.js"></script>
      <!-- InputMask -->
      <script src="views/app/plugins/input-mask/jquery.inputmask.js"></script>
      <script src="views/app/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
      <script src="views/app/plugins/input-mask/jquery.inputmask.extensions.js"></script>
      <!-- Custom JS -->
      <script src="views/app/js/js-promotion-list.js" type="text/javascript"></script>
   </body>
</html>
