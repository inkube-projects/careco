<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="admin">
      <meta name="author" content="Inkube">
      <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
      <title><?=APP_TITLE?></title>
      <!-- Select 2 -->
      <link rel="stylesheet" href="views/app/plugins/select2/select2.min.css">
      <!-- Datatables -->
      <link rel="stylesheet" href="views/app/plugins/datatables/dataTables.bootstrap.css">
      <?php include('html/overall/header.php'); ?>
   </head>

   <body>

      <div class="wrapper">
         <div class="container-fluid">
            <?php include('html/overall/topnav.php'); ?>

            <div class="row">
               <div class="col-xs-12">
                  <h3 class="page-header pull-left">
                     Propiedades
                     <small>Agregar propiedad</small>
                  </h3>
               </div>
            </div> <!-- / .row -->

            <section class="content">
               <div class="js-alert"></div>

               <form name="frm-properties-add" id="frm-properties-add" enctype="multipart/form-data" method="post" action="">
                  <div class="box box-solid">
                     <div class="box-header with-border">
                        <h2 class="box-title">Agregar propiedad</h2>

                        <div class="pull-right">
                           <div class="form-group">
                              <span class="button-checkbox">
                                 <button type="button" class="btn btn-flat css-marginR10 btn-primary active css-width100" data-color="primary" data-input="outstanding">
                                    <i class="state-icon glyphicon glyphicon-check"></i> Destacada
                                 </button>
                                 <input class="hidden" type="checkbox" name="outstanding" id="outstanding" value="1">
                              </span>
                           </div>
                        </div>

                        <div class="pull-right">
                           <div class="form-group">
                              <span class="button-checkbox">
                                 <button type="button" class="btn btn-flat css-marginR10 btn-primary active css-width100" data-color="primary" data-input="status">
                                    <i class="state-icon glyphicon glyphicon-check"></i> Habilitada
                                 </button>
                                 <input class="hidden" type="checkbox" name="status" id="status" value="1">
                              </span>
                           </div>
                        </div>

                        <div class="pull-right">
                           <div class="form-group">
                              <span class="button-checkbox">
                                 <button type="button" class="btn btn-flat css-marginR10 btn-primary active css-width100" data-color="primary" data-input="status">
                                    <i class="state-icon glyphicon glyphicon-check"></i> Vendido
                                 </button>
                                 <input class="hidden" type="checkbox" name="sold" id="sold" value="1">
                              </span>
                           </div>
                        </div>
                     </div>

                     <div class="box-body">
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <input type="text" name="title" id="title" class="form-control" placeholder="Agregar el título de la publicación">
                                 <div class="text-danger" id="title_validate"></div>
                              </div>

                              <div class="form-group">
                                 <input type="text" name="cover_title" id="cover_title" class="form-control" placeholder="Agregar el título del listado">
                                 <div class="text-danger" id="cover_title_validate"></div>
                              </div>

                              <div class="form-group">
                                 <textarea name="description" id="description" class="form-control" placeholder="Descripción de la propiedad" rows="8"></textarea>
                                 <div class="text-danger" id="description_validate"></div>
                              </div>
                           </div>

                           <div class="col-md-6">
                              <div class="form-group">
                                 <input type="text" name="code" id="code" class="form-control" placeholder="Código de la propiedad">
                                 <div class="text-danger" id="code_validate"></div>
                              </div>

                              <div class="form-group">
                                 <select class="form-control select2" name="promotion" id="promotion">
                                    <option value="">Seleccione una promoción</option>
                                    <?php foreach ($arr_promotions as $val): ?>
                                       <option value="<?=$val['id']?>"><?=$val['title']?> - <?=($val['category'] == 1) ? "Actual" : "Proxima"?></option>
                                    <?php endforeach; ?>
                                 </select>
                                 <div class="text-danger" id="promotion_validate"></div>
                              </div>

                              <div class="form-group">
                                 <textarea id="location" name="location" class="form-control" rows="8" placeholder="Ingrese la dirección de la propiedad"></textarea>
                                 <div class="text-danger" id="location_validate"></div>
                              </div>

                              <!-- <div class="row">
                                 <div class="css-bold text-center css-marginB10">Coordenadas</div>

                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <input type="text" name="latitude" id="latitude" class="form-control" placeholder="Latitud">
                                       <div class="text-danger" id="latitude_validate"></div>
                                    </div>
                                 </div>

                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <input type="text" name="longitude" id="longitude" class="form-control" placeholder="Longitud">
                                       <div class="text-danger" id="longitude_validate"></div>
                                    </div>
                                 </div>
                              </div> -->
                           </div>
                        </div>
                     </div>
                  </div>

                  <div class="box box-solid">
                     <div class="box-header with-border">
                        <h2 class="box-title">Costos</h2>

                        <div class="box-tools pull-right">
                           <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                           </button>
                        </div>
                     </div>

                     <div class="box-body">
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <input type="text" name="cost" id="cost" class="form-control" placeholder="Costo total">
                                 <div class="text-danger" id="cost_validate"></div>
                              </div>
                           </div>

                           <div class="col-md-6">
                              <div class="form-group">
                                 <input type="text" name="monthly_cost" id="monthly_cost" class="form-control" placeholder="Costo mensual / alquileres">
                                 <div class="text-danger" id="monthly_cost_validate"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div class="box box-solid">
                     <div class="box-header with-border">
                        <h2 class="box-title">Descripciones</h2>

                        <div class="box-tools pull-right">
                           <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                           </button>
                        </div>
                     </div>

                     <div class="box-body">
                        <?php foreach ($arr_icons as $val): ?>
                           <div class="col-md-3">
                              <div class="form-group">
                                 <span class="button-checkbox">
                                    <button type="button" class="btn btn-flat css-marginR10 btn-primary active css-width100" data-color="primary" data-input="act_<?=$val['id']?>">
                                       <i class="state-icon glyphicon glyphicon-check"></i> <?=$val['description']?>
                                    </button>
                                    <input class="hidden" type="checkbox" name="add[<?=$val['id']?>]" id="act_<?=$val['id']?>" value="<?=$val['id']?>">
                                 </span>
                              </div>

                              <div class="form-group">
                                 <input type="text" id="desc_<?=$val['id']?>" name="desc[<?=$val['id']?>]" class="form-control" placeholder="description">
                                 <div class="text-danger" id="desc_<?=$val['id']?>_validate"></div>
                              </div>
                           </div>
                        <?php endforeach; ?>
                     </div>
                  </div>

                  <div class="box box-solid">
                     <div class="box-header with-border">
                        <h2 class="box-title">Archivos</h2>

                        <div class="box-tools pull-right">
                           <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                           </button>
                        </div>
                     </div>

                     <div class="box-body">
                        <div class="row css-marginB20">
                           <div class="col-md-3">
                              <div class="btn btn-default btn-file css-width100 btn-files-adj" id="blueprints">
                                 <i class="fas fa-file-image"></i> <span>Planos</span>
                                 <input type="file" name="blueprints_image" id="blueprints_image">
                              </div>
                              <div class="text-danger" id="blueprints_image_validate"></div>
                           </div>

                           <div class="col-md-3">
                              <div class="btn btn-default btn-file css-width100 btn-files-adj" id="quality_memories">
                                 <i class="far fa-file-pdf"></i> <span>Memoria de calidades</span>
                                 <input type="file" name="quality_memories_doc" id="quality_memories_doc">
                              </div>
                              <div class="text-danger" id="quality_memories_doc_validate"></div>
                           </div>

                           <div class="col-md-3">
                              <div class="btn btn-default btn-file css-width100 btn-files-adj" id="quality_memories">
                                 <i class="far fa-file-pdf"></i> <span>Planos PDF</span>
                                 <input type="file" name="blueprints_doc" id="blueprints_doc">
                              </div>
                              <div class="text-danger" id="blueprints_doc_validate"></div>
                           </div>

                           <div class="col-md-3">
                              <div class="btn btn-default btn-file css-width100 btn-files-adj" id="cee">
                                 <i class="far fa-file-pdf"></i> <span>C.E.E</span>
                                 <input type="file" name="cee_doc" id="cee_doc">
                              </div>
                              <div class="text-danger" id="cee_doc_validate"></div>
                           </div>
                        </div>

                        <div class="row">
                           <div class="col-md-3">
                              <div class="btn btn-default btn-file css-width100 btn-files-adj" id="file_pdf">
                                 <i class="far fa-file-pdf"></i> <span>Ficha PDF</span>
                                 <input type="file" name="file_pdf_doc" id="file_pdf_doc">
                              </div>
                              <div class="text-danger" id="file_pdf_doc_validate"></div>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div class="box box-solid">
                     <div class="box-header with-border">
                        <h2 class="box-title">Galería de imágenes</h2>

                        <div class="box-tools pull-right">
                           <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                           </button>
                        </div>
                     </div>

                     <div class="box-body">
                        <div class="col-md-5 css-noFloat center-block text-center well">
                           <div class="btn btn-default btn-file css-width100 btn-file-gallery">
                              <i class="fa fa-camera"></i> <span>Agregar imágenes</span>
                              <input type="file" name="gallery_images[]" id="gallery_images" class="gallery_images" multiple>
                           </div>
                           <p>Las imagenes deben tener mínimo 500 x 250px</p>
                           <div class="text-danger" id="cover_image_validate"></div>
                        </div>

                        <div class="row css-marginT40" id="ajx-gallery"></div>
                     </div>
                  </div>

                  <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
               </form>
            </section>

            <!-- Footer -->
            <?php include('html/overall/footer.php') ?>
         </div> <!-- / .container-fluid -->
      </div> <!-- / .wrapper -->

      <div id="key" data-form="frm-properties-add" data-acc="3"></div>

      <!-- Modals -->

      <div class="modal fade" tabindex="-1" role="dialog" id="mod-image">
         <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Imagen de galería</h4>
               </div>
               <div class="modal-body" id="js-image"></div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
               </div>
            </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->

      <!-- JavaScript
      ================================================== -->
      <!-- JS Global -->
      <?php include('html/overall/js.php'); ?>

      <script src="views/app/plugins/select2/select2.full.min.js"></script>
      <!-- jQuery validation -->
      <script type="text/javascript" src="views/app/plugins/jquery-validation/jquery.validate.js"></script>
      <script type="text/javascript" src="views/app/plugins/jquery-validation/additional-methods.js"></script>
      <!-- Custom JS -->
      <script src="views/app/js/js-properties.js" type="text/javascript"></script>
   </body>
</html>
