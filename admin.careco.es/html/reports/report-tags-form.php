<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- Date picker -->
        <link rel="stylesheet" href="views/app/plugins/datepicker/datepicker3.css">
        <!-- Select 2 -->
        <link rel="stylesheet" href="views/app/plugins/select2/select2.min.css">
        <!-- Datatables -->
        <link rel="stylesheet" href="views/app/plugins/datatables/dataTables.bootstrap.css">
        <!-- Bootstrap Color Picker -->
        <link rel="stylesheet" href="views/app/plugins/colorpicker/bootstrap-colorpicker.min.css">
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Reportes
                            <?php if ($acc == 1): ?>
                                <small>Agregar tag</small>
                            <?php else: ?>
                                <small>Editar tag</small>
                            <?php endif; ?>
                        </h3>
                    </div>
                </div> <!-- / .row -->

                <section class="content">
                    <div class="js-alert"><?=$flash_message?></div>

                    <form name="<?=$frm?>" id="<?=$frm?>" method="post" action="">

                        <div class="box box-solid">
                            <div class="box-header">
                                <?php if ($acc == 1): ?>
                                    <h2 class="box-title">Agregar tag</h2>
                                <?php else: ?>
                                    <h2 class="box-title">Editar tag</h2>
                                <?php endif; ?>
                            </div>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Nombre</label>
                                            <input type="text" name="name" id="name" class="form-control" value="<?=$name?>">
                                            <div class="text-danger" id="name_validate"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                           <label class="css-bold css-text-black">Seleccione el color:</label>

                                           <div class="input-group js-colorpicker">
                                              <input type="text" name="color" id="color" class="form-control" value="<?=$color?>">
                                              <div class="input-group-addon"><i></i></div>
                                           </div>
                                           <div class="text-danger" id="color_validate"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="box box-solid">
                            <div class="box-header">
                                <h2 class="box-title">Imagen de perfil</h2>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                </div>
                            </div>

                            <div class="box-body">
                                <img id="img-cover" src="<?=APP_NO_IMAGES."promo_no_image.jpg"?>" class="img-responsive css-marginB20 css-noFloat center-block" alt="">

                                <div class="col-md-12 text-center">
                                    <button type="button" class="btn btn-success btn-flat btn-submit-profile-image" disabled>Debes seleccionar una imagen de portada</button>
                                    <div class="clearfix css-espacio10"></div>

                                    <div class="btn btn-default btn-file">
                                        <i class="fa fa-camera"></i> Agregar imagen de perfil
                                        <input type="file" name="profile_image" id="profile_image" class="profile_image">
                                    </div>
                                    <p>La imagen debe tener al menos 300 x 300px</p>
                                    <div class="text-danger" id="profile_image_validate"></div>
                                </div>
                            </div>
                        </div> -->

                        <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
                    </form>
                </section>

                <!-- Footer -->
                <? include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->

        <div id="key" data-form="<?=$frm?>" data-acc="<?=$acc?>" data-id="<?=$id?>"></div>

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- bootstrap color picker -->
        <script src="views/app/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
        <!-- SELECT2 -->
        <script src="views/app/plugins/select2/select2.full.min.js"></script>
        <!-- jQuery validation -->
        <script type="text/javascript" src="views/app/plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="views/app/plugins/jquery-validation/additional-methods.js"></script>
        <!-- Custom JS -->
        <script src="views/app/js/js-report-tag.js" type="text/javascript"></script>
    </body>
</html>
