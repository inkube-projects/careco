<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- Select 2 -->
        <link rel="stylesheet" href="views/app/plugins/select2/select2.min.css">
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Reportes
                            <?php if ($acc == 1): ?>
                                <small>Agregar reporte</small>
                            <?php else: ?>
                                <small>Editar reporte</small>
                            <?php endif; ?>
                        </h3>
                    </div>
                </div> <!-- / .row -->

                <section class="content">
                    <div class="js-alert"><?=$flash_message?></div>

                    <form name="<?=$frm?>" id="<?=$frm?>" method="post" action="">

                        <div class="box box-solid">
                            <div class="box-header">
                                <?php if ($acc == 1): ?>
                                    <h2 class="box-title">Agregar reporte</h2>
                                <?php else: ?>
                                    <h2 class="box-title">Editar reporte</h2>
                                <?php endif; ?>
                            </div>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Usuario</label>
                                            <select class="form-control select2" name="user" id="user">
                                                <option value="">Seleccione un usuario</option>
                                                <?php foreach ($arr_user as $val): ?>
                                                    <option value="<?=$val['id']?>" <?=isSelected($val['id'], $user)?>><?=$val['name']." ".$val['last_name']." - ".$val['username']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <div class="text-danger" id="user_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Departamento</label>
                                            <select class="form-control select2" name="departament" id="departament">
                                                <option value="">Seleccione un departamento</option>
                                                <?php foreach ($arr_departament as $val): ?>
                                                    <option value="<?=$val['id']?>" <?=isSelected($val['id'], $departament)?>><?=$val['name']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <div class="text-danger" id="departament_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Comercial</label>
                                            <select class="form-control select2" name="commercial" id="commercial">
                                                <option value="">Seleccione un comercial</option>
                                                <?php foreach ($arr_commercial as $val): ?>
                                                    <option value="<?=$val['id']?>" <?=isSelected($val['id'], $commercial)?>><?=$val['name']." ".$val['last_name']." - ".$val['username']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <div class="text-danger" id="commercial_validate"></div>
                                        </div>

                                        <?php if ($message_id): ?>
                                            <div class="form-group">
                                                <label class="css-bold css-text-black">Numero de teléfono</label>
                                                <p><?=$arr_message[0]['phone']?></p>
                                            </div>

                                            <div class="form-group">
                                                <label class="css-bold css-text-black">Email</label>
                                                <p><?=$arr_message[0]['email']?></p>
                                            </div>
                                        <?php endif; ?>
                                    </div>

                                    <div class="col-md-6">
                                        <label class="css-bold css-text-black">Etiquetas</label> <br>
                                        <?php foreach ($arr_tags as $val): ?>
                                            <div class="col-md-6 css-marginB10">
                                                <span class="button-checkbox">
                                                    <button type="button" class="btn css-marginR10 btn-primary active css-width100" data-color="primary" data-input="tag_<?=$val['id']?>">
                                                        <i class="state-icon glyphicon glyphicon-check"></i> <?=$val['name']?> <i class="fas fa-circle css-fontSize17" style="color: <?=$val['color']?>"></i>
                                                    </button>
                                                    <input class="hidden" type="checkbox" name="tag[]" id="tag_<?=$val['id']?>" value="<?=$val['id']?>" <?=isChecked($tag, $val['id'])?>>
                                                </span>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 css-marginB20">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Mensaje del cliente</label>
                                            <?php  $string = str_replace("<br>", ' ', $message);
                                            ?>
                                            <textarea class="form-control" name="message" id="message" rows="8"><?=strip_tags($string)?></textarea>
                                            <div class="text-danger" id="message_validate"></div>

                                            <?php if ($message_id): ?>
                                                <a href="index.php?view=contactEdit&id=<?=$message_id?>" class="css-marginT20 css-marginB20" target="_blank">Ver mensaje original</a>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Comentario</label>
                                            <textarea class="form-control" name="comment" id="comment" rows="8"><?=$comment?></textarea>
                                            <div class="text-danger" id="comment_validate"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
                    </form>
                </section>

                <!-- Footer -->
                <? include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->

        <div id="key" data-form="<?=$frm?>" data-acc="<?=$acc?>" data-id="<?=$id?>"></div>

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- bootstrap color picker -->
        <script src="views/app/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
        <!-- SELECT2 -->
        <script src="views/app/plugins/select2/select2.full.min.js"></script>
        <!-- jQuery validation -->
        <script type="text/javascript" src="views/app/plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="views/app/plugins/jquery-validation/additional-methods.js"></script>
        <!-- Custom JS -->
        <script src="views/app/js/js-report.js" type="text/javascript"></script>
    </body>
</html>
