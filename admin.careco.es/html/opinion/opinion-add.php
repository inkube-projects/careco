<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="admin">
      <meta name="author" content="Inkube">
      <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
      <title><?=APP_TITLE?></title>
      <!-- Date picker -->
      <link rel="stylesheet" href="views/app/plugins/datepicker/datepicker3.css">
      <!-- Select 2 -->
      <link rel="stylesheet" href="views/app/plugins/select2/select2.min.css">
      <!-- Datatables -->
      <link rel="stylesheet" href="views/app/plugins/datatables/dataTables.bootstrap.css">
      <?php include('html/overall/header.php'); ?>
   </head>

   <body>

      <div class="wrapper">
         <div class="container-fluid">
            <?php include('html/overall/topnav.php'); ?>

            <div class="row">
               <div class="col-xs-12">
                  <h3 class="page-header pull-left">
                     Opiniones
                     <small>Agregar opinión</small>
                  </h3>
               </div>
            </div> <!-- / .row -->

            <section class="content">
               <div class="js-alert"></div>

               <form name="frm-opinion-add" id="frm-opinion-add" method="post" action="">
                  <input type="hidden" name="hid-name" id="hid-name" value="">
                  <div class="box box-solid">
                     <div class="box-header">
                        <h2 class="box-title">Agregar opinión</h2>
                     </div>

                     <div class="box-body">
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <input type="text" name="name" id="name" class="form-control" placeholder="Nombre del cliente">
                                 <div class="text-danger" id="name_validate"></div>
                              </div>

                              <div class="form-group">
                                 <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Apellido del cliente">
                                 <div class="text-danger" id="last_name_validate"></div>
                              </div>

                              <div class="form-group">
                                 <input type="text" name="company" id="company" class="form-control" placeholder="Nombre de la empresa">
                                 <div class="text-danger" id="company_validate"></div>
                              </div>
                           </div>

                           <div class="col-md-6">
                              <div class="form-group">
                                 <textarea name="description" id="description" class="form-control" rows="8" placeholder="Opinión del usuario"></textarea>
                                 <div class="text-danger" id="description_validate"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div class="box box-solid">
                     <div class="box-header">
                        <h2 class="box-title">Imagen de perfil</h2>

                        <div class="box-tools pull-right">
                           <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                     </div>

                     <div class="box-body">
                        <img id="img-cover" src="<?=APP_NO_IMAGES."promo_no_image.jpg"?>" class="img-responsive css-marginB20 css-noFloat center-block" alt="">

                        <div class="col-md-12 text-center">
                           <button type="button" class="btn btn-success btn-flat btn-submit-profile-image" disabled>Debes seleccionar una imagen de portada</button>
                           <div class="clearfix css-espacio10"></div>

                           <div class="btn btn-default btn-file">
                              <i class="fa fa-camera"></i> Agregar imagen de perfil
                              <input type="file" name="profile_image" id="profile_image" class="profile_image">
                           </div>
                           <p>La imagen debe tener al menos 300 x 300px</p>
                           <div class="text-danger" id="profile_image_validate"></div>
                        </div>
                     </div>
                  </div>

                  <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
               </form>
            </section>

            <!-- Footer -->
            <?php include('html/overall/footer.php') ?>
         </div> <!-- / .container-fluid -->
      </div> <!-- / .wrapper -->

      <!-- Modal para la imagen de Portada -->
      <div class="modal fade" tabindex="-1" role="dialog" id="mod-img-cover">
         <div class="modal-dialog modal-lg css-width1100">
            <div class="modal-content">
               <div class="modal-header">
                  Imagen de perfil

                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               </div>
               <div class="modal-body">
                  <div class="col-md-12" id="ajx-img-cover"></div>

                  <div class="clearfix css-marginB10"></div>
               </div>
            </div>
         </div>
      </div>

      <div id="key" data-form="frm-opinion-add" data-acc="3"></div>

      <!-- JavaScript
      ================================================== -->
      <!-- JS Global -->
      <?php include('html/overall/js.php'); ?>

      <script src="views/app/plugins/select2/select2.full.min.js"></script>
      <!-- jQuery validation -->
      <script type="text/javascript" src="views/app/plugins/jquery-validation/jquery.validate.js"></script>
      <script type="text/javascript" src="views/app/plugins/jquery-validation/additional-methods.js"></script>
      <!-- Custom JS -->
      <script src="views/app/js/js-opinion.js" type="text/javascript"></script>
   </body>
</html>
