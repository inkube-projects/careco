<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="admin">
      <meta name="author" content="Inkube">
      <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
      <title><?=APP_TITLE?></title>
      <?php include('html/overall/header.php'); ?>
   </head>

   <body>

      <div class="wrapper">
         <div class="container-fluid">
            <?php include('html/overall/topnav.php'); ?>

            <div class="row">
               <div class="col-xs-12">
                  <h3 class="page-header pull-left">
                     Nuestros hitos
                     <small>Agregar entrada</small>
                  </h3>
               </div>
            </div> <!-- / .row -->

            <section class="content">
               <div class="js-alert"></div>

               <form name="frm-our-milestones-add" id="frm-our-milestones-add" method="post" enctype="multipart/form-data" action="">
                  <div class="box box-solid">
                     <div class="box-header">
                        <h2 class="box-title">Agregar entrada</h2>
                     </div>

                     <div class="box-body">
                        <div class="form-group">
                           <input type="text" name="title" id="title" class="form-control" placeholder="Título">
                           <div class="text-danger" id="title_validate"></div>
                        </div>

                        <div class="form-group">
                           <textarea name="description" id="description" class="form-control" rows="8" placeholder="Descripción"></textarea>
                           <div class="text-danger" id="description_validate"></div>
                        </div>
                     </div>

                     <div class="box-footer">
                        <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
                     </div>
                  </div>
               </form>
            </section>

            <!-- Footer -->
            <?php include('html/overall/footer.php') ?>
         </div> <!-- / .container-fluid -->
      </div> <!-- / .wrapper -->

      <div id="key" data-form="frm-our-milestones-add" data-acc="2"></div>

      <!-- JavaScript
      ================================================== -->
      <!-- JS Global -->
      <?php include('html/overall/js.php'); ?>
      <!-- jQuery validation -->
      <script type="text/javascript" src="views/app/plugins/jquery-validation/jquery.validate.js"></script>
      <script type="text/javascript" src="views/app/plugins/jquery-validation/additional-methods.js"></script>
      <!-- Custom JS -->
      <script src="views/app/js/js-our-milestones-process.js" type="text/javascript"></script>
   </body>
</html>
