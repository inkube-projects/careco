<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="admin">
      <meta name="author" content="Inkube">
      <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
      <title><?=APP_TITLE?></title>

      <?php include('html/overall/header.php'); ?>
   </head>

   <body>

      <div class="wrapper">
         <div class="container-fluid">
            <?php include('html/overall/topnav.php'); ?>

            <div class="row">
               <div class="col-xs-12">
                  <h3 class="page-header pull-left">
                     Asi somos
                     <small>Editar / Agregar</small>
                  </h3>
               </div>
            </div> <!-- / .row -->

            <section class="content">
               <div class="js-alert"><?=$flash_message?></div>

               <form name="frm-about-us-edit" id="frm-about-us-edit" method="post" enctype="multipart/form-data">
                  <?php foreach ($arr_aboutUs as $val): ?>
                     <input type="hidden" name="section_id[]" id="section_id_<?=$val['id']?>" value="<?=$val['id']?>">
                     <div class="box box-solid">
                        <div class="box-header with-border">
                           <h2 class="box-title">Editar sección</h2>

                           <?php if ($role === "ROLE_ADMIN"): ?>
                              <a href="javascript: deleteBoostrapAction('remove&s=<?=$val['id']?>&acc=4', 1)" class="btn btn-danger btn-flat pull-right">Eliminar</a>
                           <?php endif; ?>
                        </div>

                        <div class="box-body">
                           <div class="row">
                              <div class="col-md-4">
                                 <div class="form-group">
                                    <input type="text" id="title_<?=$val['id']?>" name="title[]" class="form-control" placeholder="Título" value="<?=$val['title']?>">
                                    <div class="text-danger" id="title_<?=$val['id']?>_validate"></div>
                                 </div>

                                 <div class="form-group">
                                    <div class="btn btn-default btn-file css-width100">
                                       <i class="fa fa-camera"></i> Agregar imagen
                                       <input type="file" name="image[]" id="image_<?=$val['id']?>" class="image">
                                    </div>
                                    <p>Se recomienda que la imagen sea 285 x 400 px</p>
                                    <div class="text-danger" id="image_<?=$val['id']?>_validate"></div>
                                    <img src="<?=APP_IMG_ABOUT_US.$val['image']?>" class="img-responsive" alt="">
                                 </div>
                              </div>

                              <div class="col-md-8">
                                 <label for="">Descripción</label>
                                 <textarea id="description_<?=$val['id']?>" name="description[]" rows="8"><?=$val['description']?></textarea>
                                 <div class="text-danger" id="description_<?=$val['id']?>_validate"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  <?php endforeach; ?>

                  <?php if ($arr_aboutUs): ?>
                     <button type="submit" class="btn btn-success btn-flat pull-right btn-submit">Editar secciones</button>
                     <div class="clearfix css-marginB30"></div>
                  <?php endif; ?>
               </form>

               <form name="frm-about-us-add" id="frm-about-us-add" method="post" enctype="multipart/form-data" action="">
                  <div class="box box-solid">
                     <div class="box-header with-border">
                        <h2 class="box-title">Agregar sección</h2>
                     </div>

                     <div class="box-body">
                        <div class="row">
                           <div class="col-md-4">
                              <div class="form-group">
                                 <input type="text" id="new_title" name="new_title" class="form-control" placeholder="Título">
                                 <div class="text-danger" id="new_title_validate"></div>
                              </div>

                              <div class="form-group">
                                 <div class="btn btn-default btn-file css-width100">
                                    <i class="fa fa-camera"></i> Agregar imagen
                                    <input type="file" name="new_image" id="new_image" class="image">
                                 </div>
                                 <p>Se recomienda que la imagen sea 285 x 400 px</p>
                                 <div class="text-danger" id="new_image_validate"></div>
                              </div>
                           </div>

                           <div class="col-md-8">
                              <label for="">Descripción</label>
                              <textarea id="new_description" name="new_description" rows="8"></textarea>
                              <div class="text-danger" id="new_description_validate"></div>
                           </div>
                        </div>
                     </div>

                     <div class="box-footer">
                        <button type="submit" class="btn btn-success btn-flat pull-right btn-submit">Guardar sección</button>
                     </div>
                  </div>
               </form>
            </section>

            <div class="modal fade modal-danger" tabindex="-1" role="dialog" id="mod-remove">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Eliminar Sección</h4>
                     </div>
                     <div class="modal-body">
                        <p>Estas seguro de eliminar esta sección</p>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No eliminar</button>
                        <button type="button" class="btn btn-danger btn-flat" id="mod-remove-btn" data-id="" onclick="deleteBoostrapAction('',2)"><i class="fa fa-trash"></i> Eliminar</button>
                     </div>
                  </div>
               </div>
            </div>

            <!-- Footer -->
            <?php include('html/overall/footer.php') ?>
         </div> <!-- / .container-fluid -->
      </div> <!-- / .wrapper -->

      <div id="key" data-form="frm-about-us" data-acc="1" data-id=""></div>

      <!-- JavaScript
      ================================================== -->
      <!-- JS Global -->
      <?php include('html/overall/js.php'); ?>

      <!-- CKeditor -->
      <script type="text/javascript" src="views/app/plugins/ckeditor/ckeditor.js"></script>
      <!-- jQuery validator -->
      <script type="text/javascript" src="views/app/plugins/jquery-validation/jquery.validate.js"></script>
      <script type="text/javascript" src="views/app/plugins/jquery-validation/additional-methods.js"></script>
      <!-- Custom JS -->
      <script type="text/javascript" src="views/app/js/js-about-us.js"></script>
   </body>
</html>
