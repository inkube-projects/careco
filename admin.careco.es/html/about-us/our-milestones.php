<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="admin">
      <meta name="author" content="Inkube">
      <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
      <title><?=APP_TITLE?></title>

      <?php include('html/overall/header.php'); ?>
   </head>

   <body>

      <div class="wrapper">
         <div class="container-fluid">
            <?php include('html/overall/topnav.php'); ?>

            <div class="row">
               <div class="col-xs-12">
                  <h3 class="page-header pull-left">
                     Nuestros hitos
                  </h3>

                  <div class="col-md-2 pull-right css-marginT35">
                     <a href="index.php?view=ourMilestonesAdd" class="btn btn-info btn-flat pull-right">Nueva Entrada</a>
                  </div>
               </div>
            </div> <!-- / .row -->

            <section class="content">
               <div class="js-alert"><?=$flash_message?></div>

               <form name="frm-milestones" id="frm-milestones" method="post" enctype="multipart/form-data" action="">
                  <div class="box box-solid">
                     <div class="box-header">
                        <h2 class="box-title">Información principal de "Nuestros Hitos"</h2>

                        <div class="box-tools pull-right">
                           <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                           </button>
                        </div>
                     </div>

                     <div class="box-body">
                        <div class="form-group">
                           <textarea name="description" id="description" rows="8" class="form-control"><?=@$arr_milestones[0]['description']?></textarea>
                           <div id="description_validate" class="text-danger"></div>
                        </div>

                        <hr>

                        <div class="form-group">
                           <img src="<?=$image?>" class="center-block css-marginB30" alt="" height="250">

                           <div class="col-md-5 css-noFloat center-block text-center well">
                              <div class="btn btn-default btn-file css-width100 btn-file-gallery">
                                 <i class="fa fa-camera"></i> <span>Agregar imágenes</span>
                                 <input type="file" name="banner" id="banner" class="banner">
                              </div>
                              <p>Se recomienda que la imagen sea 1500 x 400</p>
                              <div class="text-danger" id="banner_validate"></div>
                           </div>
                        </div>
                     </div>

                     <div class="box-footer">
                        <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
                     </div>
                  </div>
               </form>

               <div class="box box-solid">
                  <div class="box-header">
                     <h2 class="box-title">Listado de promociones</h2>
                  </div>
                  <div class="box-body">
                     <table class="table table-hover">
                        <thead>
                           <tr>
                              <th>Ref.</th>
                              <th>Título</th>
                              <th>Acciones</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach ($arr_entries as $val): ?>
                              <tr>
                                 <td><?=$val['id']?></td>
                                 <td><?=$val['title']?></td>
                                 <td>
                                    <a href="index.php?view=ourMilestonesEdit&id=<?=$val['id']?>" class="btn btn-info btn-flat">Editar</a>
                                    <?php if ($role === "ROLE_ADMIN"): ?>
                                       <a href="javascript: deleteBoostrapAction('remove&h=<?=$val['id']?>&acc=5', 1)" class="btn btn-danger btn-flat">Eliminar</a>
                                    <?php endif; ?>
                                 </td>
                              </tr>
                           <?php endforeach; ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </section>

            <div class="modal fade modal-danger" tabindex="-1" role="dialog" id="mod-remove">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Eliminar entrada</h4>
                     </div>
                     <div class="modal-body">
                        <p>Estas seguro de eliminar esta entrada</p>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No eliminar</button>
                        <button type="button" class="btn btn-danger btn-flat" id="mod-remove-btn" data-id="" onclick="deleteBoostrapAction('',2)"><i class="fa fa-trash"></i> Eliminar</button>
                     </div>
                  </div>
               </div>
            </div>

            <!-- Footer -->
            <?php include('html/overall/footer.php') ?>
         </div> <!-- / .container-fluid -->
      </div> <!-- / .wrapper -->

      <div id="key" data-form="frm-about-us" data-acc="1" data-id=""></div>

      <!-- JavaScript
      ================================================== -->
      <!-- JS Global -->
      <?php include('html/overall/js.php'); ?>

      <!-- CKeditor -->
      <script type="text/javascript" src="views/app/plugins/ckeditor/ckeditor.js"></script>
      <!-- Jquery validation -->
      <script type="text/javascript" src="views/app/plugins/jquery-validation/jquery.validate.js"></script>
      <script type="text/javascript" src="views/app/plugins/jquery-validation/additional-methods.js"></script>
      <!-- Custom JS -->
      <script type="text/javascript" src="views/app/js/js-our-milestones.js"></script>
   </body>
</html>
