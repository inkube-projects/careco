<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="admin">
      <meta name="author" content="Inkube">
      <link rel="shortcut icon" type="image/png" href="" />
      <title><?=APP_TITLE?></title>
      <?php include('html/overall/header.php'); ?>
   </head>

   <body>

      <div class="wrapper">
         <!-- SIDEBAR
         ================================================== -->
         <div class="container-fluid">
            <?php include('html/overall/topnav.php'); ?>

            <div class="row">
               <div class="col-xs-12">
                  <h3 class="page-header">
                     Administrador de contenido <small>Escritorio</small>
                  </h3>
               </div>
            </div> <!-- / .row -->

            <h4>Resumen de blog</h4>
            <div class="row">
               <div class="col-xs-12 col-sm-3">
                  <div class="dashboard-stats__item bg-teal">
                     <i class="fa fa-comment"></i>
                     <h3 class="dashboard-stats__title">
                        <span class="count-to">10</span>
                        <small>Comentarios</small>
                     </h3>
                  </div>
               </div>
               <div class="col-xs-12 col-sm-3">
                  <div class="dashboard-stats__item bg-pink">
                     <i class="fa fa-user"></i>
                     <h3 class="dashboard-stats__title">
                        <span class="count-to">30</span>
                        <small>Usuarios</small>
                     </h3>
                  </div>
               </div>
               <div class="col-xs-12 col-sm-3">
                  <div class="dashboard-stats__item bg-accent">
                     <i class="fa fa-file-text"></i>
                     <h3 class="dashboard-stats__title">
                        <span class="count-to">40</span>
                        <small>Post - Blog</small>
                     </h3>
                  </div>
               </div>
               <div class="col-xs-12 col-sm-3">
                  <div class="dashboard-stats__item bg-orange">
                     <i class="fa fa-exclamation-circle"></i>
                     <h3 class="dashboard-stats__title">
                        <span class="count-to" data-from="0" data-to="105">5</span>
                        <small>Alertas</small>
                     </h3>
                  </div>
               </div>
            </div> <!-- / .row -->

            <div class="row">
               <!-- Dashboard: Comments -->
               <div class="col-xs-12 col-sm-6">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           Últimos comentarios
                        </h4>
                     </div>
                     <div class="panel-body">
                        <div class="dashboard__comments">
                           <div class="dashboard-comments__item">
                              <div class="dashboard-comments__controls">
                                 <a href="index.html#"><i class="fa fa-share-square-o"></i></a>
                                 <a href="index.html#"><i class="fa fa-edit"></i></a>
                                 <a href="index.html#"><i class="fa fa-trash-o"></i></a>
                              </div>
                              <div class="dashboard-comments__avatar">
                                 <img src="views/images/user_4.jpg" alt="...">
                              </div>
                              <div class="dashboard-comments__body">
                                 <h5 class="dashboard-comments__sender">
                                    John Doe <small>2 hours ago</small>
                                 </h5>
                                 <div class="dashboard-comments__content">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, corporis. Voluptatibus odio perspiciatis non quisquam provident, quasi eaque officia.
                                 </div>
                              </div>
                           </div>
                           <div class="dashboard-comments__item">
                              <div class="dashboard-comments__controls">
                                 <a href="index.html#"><i class="fa fa-share-square-o"></i></a>
                                 <a href="index.html#"><i class="fa fa-edit"></i></a>
                                 <a href="index.html#"><i class="fa fa-trash-o"></i></a>
                              </div>
                              <div class="dashboard-comments__avatar">
                                 <img src="views/images/user_4.jpg" alt="...">
                              </div>
                              <div class="dashboard-comments__body">
                                 <h5 class="dashboard-comments__sender">
                                    Jane Roe <small>5 hours ago</small>
                                 </h5>
                                 <div class="dashboard-comments__content">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero itaque dolor laboriosam dolores magnam mollitia, voluptatibus inventore accusamus illo.
                                 </div>
                              </div>
                           </div>
                           <div class="dashboard-comments__item">
                              <div class="dashboard-comments__controls">
                                 <a href="index.html#"><i class="fa fa-share-square-o"></i></a>
                                 <a href="index.html#"><i class="fa fa-edit"></i></a>
                                 <a href="index.html#"><i class="fa fa-trash-o"></i></a>
                              </div>
                              <div class="dashboard-comments__avatar">
                                 <img src="views/images/user_4.jpg" alt="...">
                              </div>
                              <div class="dashboard-comments__body">
                                 <h5 class="dashboard-comments__sender">
                                    Richard Roe <small>1 day ago</small>
                                 </h5>
                                 <div class="dashboard-comments__content">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore, esse, magni aliquam quisquam modi delectus veritatis est ut culpa minus repellendus.
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div><!-- .Dashboard: Comments-->

               <!-- Dashboard: Feed -->
               <div class="col-xs-12 col-sm-6">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           Últimas alertas
                        </h4>
                     </div>
                     <div class="panel-body">
                        <div class="dashboard__feed">
                           <div class="dashboard-feed__item">
                              <span class="label label-danger">
                                 <i class="fa fa-user"></i>
                              </span>
                              Nuevo usuario registrado
                              <small>3 min </small>
                           </div>
                           <div class="dashboard-feed__item">
                              <span class="label label-success">
                                 <i class="fa fa-comment"></i>
                              </span>
                              Nuevo comentario
                              <small>1 hora</small>
                           </div>
                           <div class="dashboard-feed__item">
                              <span class="label label-danger">
                                 <i class="fa fa-user"></i>
                              </span>
                              Nuevo usuario registrado
                              <small>5 horas</small>
                           </div>
                        </div>
                     </div>
                  </div>
               </div> <!-- / .Dashboard: Feed-->
            </div> <!-- / .row -->

            <!-- Footer -->
            <?php include('html/overall/footer.php') ?>
         </div> <!-- / .container-fluid -->
      </div> <!-- / .wrapper -->

      <!-- JavaScript
      ================================================== -->
      <!-- JS Global -->
      <?php include('html/overall/js.php'); ?>

      <script type="text/javascript">
         $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip({
               placement : 'top'
            });
         });
      </script>
   </body>
</html>
