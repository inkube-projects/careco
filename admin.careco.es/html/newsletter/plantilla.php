
      		<table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" bgcolor="#C9DDDC" id="bodyTable" style="border-collapse: collapse;table-layout: fixed;margin:0 auto;">
                	<tr>
						<td width="100%"  style="padding-top: 10px;padding-bottom: 10px;border-collapse: collapse;  " align="center" valign="middle">	
						            <p style="color: #778393; font-weight: normal; font-size: 9pt; font-family:sans-serif; margin: 0; padding: 0;">
						              		¿No lo ves correctamente? <a href="https://www.careco.es/newsletter/nueva_web.html" target="_blank" style="color:#778393;text-decoration: none;outline: none;">ver online</a>
						            </p>
						     </td>
						</tr>
      			</table>

      <!-- END.VER ONLINE-->

<table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" bgcolor="#C9DDDC" id="bodyTable" style="border-collapse: collapse;table-layout: fixed;margin:0 auto;"><tr><td>

    <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" style="border: 1px solid #C9DDDC;margin: auto; background-color:#FFF;" class="email-container">
    
     <!-- IMAGEN LOGO-->
      <tr>
        <td>
          <img src="https://www.careco.es/newsletter/images/header.jpg" alt="alt text" height="100" width="600" border="0" style="display: block;" class="fluid">
        </td>
      </tr>
      <!-- END. IMAGEN LOGO-->
      
      
      <!-- MENU-->
      <tr>
        <td style="border-bottom: 1px solid #e5e5e5; border-top: 1px solid #e5e5e5;">
      		<table border="0" bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="600" class="table600con" style="border-collapse: collapse;">
                	<tr>
						<td width="100%"  style="padding-top: 10px;padding-bottom: 10px;border-collapse: collapse;  " align="center" valign="middle">	
						            <p style="color: #000000; font-weight: normal; font-size: 9pt; font-family:sans-serif; margin: 0; padding: 0;">
						              		<a href="https://www.careco.es/promociones-valencia/venta/penya-roja/avenida-francia-alameda" target="_blank" style="color: #000000;">PROMOCIONES </a>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>|&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>
                                            <a href="https://www.careco.es/proximas-promociones-valencia" target="_blank" style="color: #000000;">PRÓXIMAS PROMOCIONES</a>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>|&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>
                                            <a href="https://www.careco.es/asi-somos" target="_blank" style="color: #000000;">CARECO</a>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>|&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>
                                            <a href="hhttps://www.careco.es/blog" target="_blank" style="color: #000000;">BLOG</a>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>|&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>
                                            <a href="https://www.careco.es/atencion-al-cliente" target="_blank" style="color: #000000;">CONTACTO</a>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>|&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>
						            </p>
						     </td>
						</tr>
      			</table>
     	 </td>
      </tr>
      <!-- END.MENU-->

	<!-- TXT 100% -->
      <tr>
        <td style="border-bottom: 1px solid #e5e5e5;">
          <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center">
            <tr>
              <td style=" color:#7FA9A3; padding: 30px; font-family: sans-serif; font-size: 22px; line-height: 22px; color: #444444; text-align:center;" align="center">
              <a href="" style="color:#7FA9A3; text-decoration:none;"> Le presentamos nuestra nueva web</a>
              </td>
            </tr>
          </table>
        </td>
      </tr>
       <!-- END.TXT 100% -->
      
      <!--IMG 01 100% -->
      <tr>
        <td style="padding-bottom:30px;">
        <a href="https://www.careco.es/" target="_blank"><img src="https://www.careco.es/newsletter/images/imagen-1.jpg" alt="alt text" height="514" width="600" border="0" style="display: block;" class="fluid"></a>
        </td>
      </tr>
      <!--END.IMG 01 100% -->
      
      
      
     
      <!-- TXT 100% -->
      <tr>
        <td style="border-bottom: 1px solid #e5e5e5;">
          <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center">
            <tr>
              <td style="padding: 30px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #939393;">
                Como culminación al proceso de renovación de la nueva imagen corporativa de la empresa, y la necesidad de ofrecer nuevos canales de comunicación con sus clientes. Careco presenta su nuevo espacio web.
En el diseño se ha buscado la máxima simplicidad en la presentación de los contenidos, proporcionando una navegación intuitiva y clara, ofreciendo a los visitantes la imagen de Careco con la mayor transparencia.<br><br>
Aquí ofrecemos toda la información sobre nuestra compañía: conocer su historia, la actividad en el sector inmobiliario, junto a todos nuestro parque de inmuebles. Todo ello,  junto con un nuevo blog, que le informará de todas aquellas novedades del sector inmobiliario, de ayuda en la búsqueda de vivienda y la actividad de la compañía.   
                <br><br>
                <strong>GRACIAS POR CONFIAR EN CARECO</strong>
              </td>
            </tr>
          </table>
        </td>
      </tr>
       <!-- END.TXT 100% -->

       
      <!--IMG 01 100% -->
      <tr>
        <td>
        <a href="#"><img src="https://www.careco.es/newsletter/images/subfooter.jpg" alt="alt text" height="100" width="600" border="0" style="display: block;" class="fluid"></a>
        </td>
      </tr>
      <!--END.IMG 01 100% -->
       

       <!-- TXT 100% -->
      <tr style="background:#7fa9ae !important;">
        <td style="color:#000; padding-top: 20px;padding-bottom: 20px;  border-collapse: collapse;" align="center"  bgcolor="#7fa9ae">
          <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center">
            <tr>
              <td style="padding: 30px; font-family: sans-serif; font-size: 14px; line-height: 22px; color: #000;"  align="center">
              	 <a href="https://www.careco.es" target="_blank"><img src="https://www.careco.es/newsletter/images/logo-high-n-01.png" height="30" width="177"  border="0" style=" max-width:100% display: block; text-align:center;" class="fluid"></a><br>
                <a href="https://www.careco.es" target="_blank"style=" text-decoration:none; font-size:14pt;  color:#000;">www.careco.es</a><br><br>
Copyright© 2018 CARECO S.A. <br>Carrer del Mar, 44  46003, Valencia<a href="mailto:careco@careco.es" style="color:#000;text-decoration: none;outline: none;"> careco@careco.es</a>
              </td>
            </tr>
          </table>
        </td>
      </tr>
       <!-- END.TXT 100% -->

  </table>
  <!-- Email Container : END -->
  
  
  <!-- Footer : BEGIN -->
  <table cellpadding="10" cellspacing="0" border="0" class="socialCntrBtn" align="center"> 
 	 <tbody>
     	<tr>
        </tr>
  		<tr>
    		
            
            <td><a href="https://www.linkedin.com/company/grupo-careco/" target="_blank" style="text-decoration: none;"><img src="https://www.careco.es/newsletter/images/socialLinkedin.png" alt="IMG" border="0" style="text-decoration: none;border: none;" data-crop="false"></a></td> 
            
            <td><a href="https://www.instagram.com/carecoweb/" target="_blank" style="text-decoration: none;"><img src="https://www.careco.es/newsletter/images/socialInstram.png" alt="IMG" border="0" style="text-decoration: none;border: none;" data-crop="false"></a></td>       
             
			</tr>
             
             <tr>
             <td height="0" style="font-size:0; line-height:0;" class="LMrg2">&nbsp;</td>
             </tr>
             </tbody>
             </table>
  <!-- Footer : END -->
  
  <!-- Footer : BEGIN -->
  <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" class="email-container">
    <tr>
      <td style="text-align: center;padding: 20px;font-family: sans-serif; font-size: 12px; line-height: 18px;color: #778393; text-align:center;">
       Si no quieres recibir nuestro newsletter te puedes dar de baja en: <unsubscribe style="color: #b1bccc; padding: 0;text-decoration: underline">
       <a href="mailto:careco@careco.es" style="color:#778393;text-decoration: none;outline: none;">Darme de baja</a></unsubscribe><br>
      </td>
    </tr>
  </table>
  <!-- Footer : END -->

</td></tr></table>
