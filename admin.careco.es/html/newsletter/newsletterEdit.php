<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="admin">
      <meta name="author" content="Inkube">
      <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
      <title><?=APP_TITLE?></title>
      <!-- Date picker -->
      <link rel="stylesheet" href="views/app/plugins/datepicker/datepicker3.css">
      <!-- Select 2 -->
      <link rel="stylesheet" href="views/app/plugins/select2/select2.min.css">
      <!-- Datatables -->
      <link rel="stylesheet" href="views/app/plugins/datatables/dataTables.bootstrap.css">
      <link rel="stylesheet" href="views/app/css/newsletter.css">
      <?php include('html/overall/header.php'); ?>
      <style>
	  
.card {
    box-shadow: 0 1px 4px 0 rgba(0,0,0,.14);
}
.card {
    border: 0;
    margin:10px;
    border-radius: 6px;
    color: #333;
    background: #fff;
    width: 100%;
    box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
	float:left;
}
.card {
    position: relative;
    display: flex;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid #eee;
    border-radius: 5px;
}
.card {
    font-size: .875rem;
	width:150px;
}

.card .card-header {
    border-bottom: none;
    background: transparent;
}
.card-header:first-child {
    border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;
}
.card-header {
    padding: .75rem 1.25rem;
    margin-bottom: 0;
    background-color: #fff;
    border-bottom: 1px solid #eee;
}
.card-stats .card-header .card-category:not([class*=text-]) {
    color: #999;
    font-size: 14px;
	 text-align: center;
}
.card-title {
    margin: 0;
	color:#51535c;
	text-align:center;
	font-size:54px;
}
.card-stats .card-header+.card-footer {
    border-top: 1px solid #eee;
    margin-top: 2px;
}
.card .card-body+.card-footer, .card .card-footer {
    padding: 0;
    padding-top: 10px;
    margin: 0 15px 10px;
    border-radius: 0;
    justify-content: space-between;
    align-items: center;
}
.card .card-footer {
    display: flex;
    align-items: center;
    background-color: transparent;
    border: 0;
}
	  </style>
   </head>

   <body>

      <div class="wrapper">
         <div class="container-fluid">
            <?php include('html/overall/topnav.php'); ?>

            <div class="row">
               <div class="col-xs-12">
                  <h3 class="page-header">
                  	<i class="fa fa-newspaper icoG iconoAdmin" style="color:#74a3a9;"></i>
                     Newsletter
                     <small>editar</small>
                     <button type="button" class="btn btn-danger pull-right marginleft50">Eliminar newsletter</button>
                     <button type="button" class="btn btn-primary pull-right" onclick="location.href = '  ' ">Volver</button>                      
                  </h3>
              </div>
            </div> <!-- / .row -->
               

            <section class="content">
               <div class="js-alert"><?=$flash_message?></div>
               
               <div class="row">
        
        	<!--INFORMACIÓN-->
          <div class="col-md-12">
          	 <form class="form-horizontal">
                   
                   <div class="panel panel-default">
                <div class="panel-heading">
                <h4 class="panel-title">Editar NEWSLETTER</h4>
                </div>
                
                <div class="panel-body">
                
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10 pull-right">
                                <span class="button-checkbox">
                                <button type="button" class="btn btn-default" data-color="primary"><i class="state-icon glyphicon glyphicon-unchecked"></i>&nbsp;Activo</button>
                                <input type="checkbox" class="hidden">
                                </span>
    
                     </div>
                 </div>
                 
                 <div class="form-group">
                    <label class="col-sm-2 control-label">Id*</label>
                    <div class="col-sm-1">
                      <input required="" name="name" type="text" value="  " class="form-control" id="name">
                    </div>
                     <label class="col-sm-2 control-label">Fecha de alta*</label>
                    <div class="col-sm-3">
                      <input required="" name="surname" type="text" value="28-10-2018" class="form-control" id="surname">
                    </div>
                     <label class="col-sm-2 control-label">Estado*</label>
                    <div class="col-sm-2">
                      <input required="" name="name" type="text" value=" " class="form-control" id="name">
                    </div>                  
                  </div>
                
                <div class="form-group">                   
                     <label class="col-sm-2 control-label">Asunto*</label>
                    <div class="col-sm-10">
                      <input required="" name="surname" type="text" value=" " class="form-control" id="surname">
                    </div>
                  </div>
                     
      
                    <div class="form-group">
                    <label class="col-sm-2 control-label">Comentarios</label>
                    <div class="col-sm-10">
                      <textarea rows="4" class="form-control" value="Enviar a los grupos, etc... "></textarea>
                    </div>
                  </div>
                  
                  
                  <button type="button" class="btn btn-success  pull-right marginleft10">Guardar Cambios</button>
                  <button type="button" class="btn btn-warning  pull-right">Deshacer Cambios</button>
        	       
                </div>
              </div>
              </form>
          </div>
          <!--END.INFORMACION-->
			</div>
            <!--END.ROW-->
            
            
              <div class="row">
        
        	<!--INFORMACIÓN-->
          <div class="col-md-12">
          	 <form class="form-horizontal">
                   
                   <div class="panel panel-default">
                <div class="panel-heading">
                <h4 class="panel-title"><i class="fas fa-share-square"></i> Enviar NEWSLETTER</h4>
                </div>
                
                <div class="panel-body">       
                
                <div class="col-md-4">               

                	<div class="form-group">
                    <label class="col-sm-3 control-label" for="selectbasic">Plantilla</label>    
                    <div class="col-sm-9">              
                   <select id="selectbasic" name="selectbasic" class="form-control">
                   <option value="1">Selecciona un grupo</option>
                  <option value="1">Grupo Careco test</option>
                  <option value="2">Careco</option>
                  <option value="3">Grupo user importados</option>
                   </select>
                   </div>
					</div>
                    
                    </div>
                    
                    <div class="col-md-3">
                     <button type="button" class="btn btn-danger pull-right marginleft50" onclick="location.href = 'https://careco.es/newsletter/EnvioNewsletter01.php' "><i class="fa fa-eye" aria-hidden="true"></i> Enviar newsletter</button></div>
                     
                       <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10 pull-right">
                                <span class="button-checkbox">
                                <button type="button" class="btn btn-default" data-color="primary"><i class="state-icon glyphicon glyphicon-unchecked"></i>&nbsp;A toda la base de datos.</button>
                                <input type="checkbox" class="hidden">
                                </span>
    
                     </div>
                 </div>
                  

        	       
                </div>
              </div>
              </form>
          </div>
          <!--END.INFORMACION-->
			</div>
            <!--END.ROW-->
            
            
            <div class="row">
        
        	<!--INFORMACIÓN-->
          <div class="col-md-12">
          	 <form class="form-horizontal">
                   
                   <div class="panel panel-default">
                <div class="panel-heading">
                <h4 class="panel-title">Maquetar NEWSLETTER</h4>
                </div>
                
                <div class="panel-body">
                
                <div class="col-md-12">
                
                <div class="col-md-4">               

                	<div class="form-group">
                    <label class="col-sm-3 control-label" for="selectbasic">Plantilla</label>    
                    <div class="col-sm-9">              
                   <select id="selectbasic" name="selectbasic" class="form-control">
                  <option value="1">Plantilla 01</option>
                   </select>
                   </div>
					</div>
                    
                    </div>
                    
                    <div class="col-md-3">
                     <button type="button" class="btn btn-info pull-right marginleft50" onclick="location.href = 'https://careco.es/newsletter/nueva_web.html' "><i class="fa fa-eye" aria-hidden="true"></i> Previsualizar newsletter</button></div>
                     

                </div>
                
                <!--PLANTILLA DEL NEWSLETTER-->
                 <div class="col-md-8" style="background-color:#C9DDDC; min-height:1000px;">
                 	<div class="panel-heading">
                 	 <h4 class="panel-title"> <i class="fas fa-edit"></i> Maquetador Newsletter</h4>
                     
                     <?php include('html/newsletter/plantilla.php') ?>
                     </div>
                 </div>
                 <!--END.PLANTILLA DEL NEWSLETTER-->
                 
                 <!--MÓDULOS PARA MAQUETAR EL NEWSLETTER-->
                 <div class="col-md-4" style="background-color:#ededed; min-height:1000px;">
                 	<div class="panel-heading">
                 	<h4 class="panel-title"> <i class="fas fa-tasks"></i> Bloques de contenidos <small> Fotos y textos</small> <br><br></h4>
                   
                        <!--bloque titular-->    
                       <div class="card card-stats">
                        <div class="card-header card-header-warning card-header-icon">          
                          <p class="card-category">Titular</p>
                          <h3 class="card-title">
                            <i class="fas fa-text-width"></i>
                          </h3>
                        </div>
                        <div class="card-footer">
                          <div class="stats">
                            <i class="material-icons text-info">Tipo: Texo</i>
                          </div>
                        </div>
                      </div>
                      <!--End.bloque titular-->  
                      
                      <!--bloque párrafo-->    
                       <div class="card card-stats">
                        <div class="card-header card-header-warning card-header-icon">          
                          <p class="card-category">Párrafo</p>
                          <h3 class="card-title">
                            <i class="fas fa-align-justify"></i>
                          </h3>
                        </div>
                        <div class="card-footer">
                          <div class="stats">
                            <i class="material-icons text-info">Tipo: Texo</i>
                          </div>
                        </div>
                      </div>
                      <!--End.bloque párrafo--> 
                      
                      <!--bloque imagen-->    
                       <div class="card card-stats">
                        <div class="card-header card-header-warning card-header-icon">          
                          <p class="card-category">1 imágen</p>
                          <h3 class="card-title">
                            <i class="far fa-image"></i>
                          </h3>
                        </div>
                        <div class="card-footer">
                          <div class="stats">
                            <i class="material-icons text-info">Tipo: Imágen</i>
                          </div>
                        </div>
                      </div>
                      <!--End.bloque imagen-->  
                      
                      <!--bloque doble imagen-->    
                       <div class="card card-stats">
                        <div class="card-header card-header-warning card-header-icon">          
                          <p class="card-category">Doble imágen</p>
                          <h3 class="card-title">
                            <i class="far fa-image"></i>
                            <i class="far fa-image"></i>
                          </h3>
                        </div>
                        <div class="card-footer">
                          <div class="stats">
                            <i class="material-icons text-info">Tipo: Imágen</i>
                          </div>
                        </div>
                      </div>
                      <!--End.bloque doble imagen-->  
                      
                      
                      <!--bloque texto + imagen-->    
                       <div class="card card-stats">
                        <div class="card-header card-header-warning card-header-icon">          
                          <p class="card-category">Párrafo + imágen</p>
                          <h3 class="card-title">                       
                             <i class="fas fa-align-justify"></i>
                             <i class="far fa-image"></i>
                          </h3>
                        </div>
                        <div class="card-footer">
                          <div class="stats">
                            <i class="material-icons text-info">Tipo: Texto +  Imágen</i>
                          </div>
                        </div>
                      </div>
                      <!--End.bloque texto + imagen-->    
              
              
                    </div>
                 </div>
                 <!--END.MÓDULOS PARA MAQUETAR EL NEWSLETTER-->
                  
                  <button type="button" class="btn btn-success  pull-right marginleft10">Guardar Cambios</button>
                  <button type="button" class="btn btn-warning  pull-right">Deshacer Cambios</button>
        	       
                </div>
              </div>
              </form>
          </div>
          <!--END.INFORMACION-->
			</div>
            <!--END.ROW-->
            
            
            <div class="box box-solid">
                            <div class="box-header with-border">
                                <h2 class="box-title">Subir imágenes para el newsletter</h2>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>

                            <div class="box-body">
                                <div class="col-md-5 css-noFloat center-block text-center well">
                                    <div class="btn btn-default btn-file css-width100 btn-file-gallery">
                                        <i class="fa fa-camera"></i> <span>Agregar imágenes</span>
                                        <input type="file" name="gallery_images[]" id="gallery_images" class="gallery_images" multiple>
                                    </div>
                                    <p>Las imagenes deben tener 600px de ancho</p>
                                    <div class="text-danger" id="cover_image_validate"></div>
                                </div>

                                <div class="row css-marginT40" id="ajx-gallery"></div>
                            </div>
                        </div>
                        
               <div class="box box-solid">
                            <div class="box-header">
                                <h2 class="box-title">Imágenes para el newsletter</h2>
                            </div>

                            <div class="box-body" id="js-gallery">
                                        <div class="col-md-3 css-marginB20 container-thumb">
                                        <div class="css-background-image" style="background-image: url('https://www.careco.es/newsletter/images/imagen-1.jpg')"></div>
                                        <a href="#" class="btn btn-info btn-flat center-block btn-show" data-image="https://www.careco.es/newsletter/images/imagen-1.jpg"><i class="fas fa-search"></i> Ver imagen</a>
                                        <a href="javascript: removeGalleryImage('76', this.event)" class="btn btn-danger btn-flat center-block btn-remove-gallery" data-id="76"><i class="fas fa-trash-alt"></i> Eliminar</a>
                              </div>
                                                                    
                      		 </div>
                        </div>
            
              
            </section>



            <!-- Footer -->
            <?php include('html/overall/footer.php') ?>
         </div> <!-- / .container-fluid -->
      </div> <!-- / .wrapper -->

      <!-- JavaScript
      ================================================== -->
      <!-- JS Global -->
      <?php include('html/overall/js.php'); ?>

      <script src="views/app/plugins/select2/select2.full.min.js"></script>
      <!-- bootstrap datepicker -->
      <script src="views/app/plugins/datepicker/bootstrap-datepicker.js"></script>
      <!-- DataTables -->
      <script src="views/app/plugins/datatables/jquery.dataTables.js"></script>
      <script src="views/app/plugins/datatables/dataTables.bootstrap.min.js"></script>
      <!-- InputMask -->
      <script src="views/app/plugins/input-mask/jquery.inputmask.js"></script>
      <script src="views/app/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
      <script src="views/app/plugins/input-mask/jquery.inputmask.extensions.js"></script>
      <!-- Custom JS -->
      <script src="views/app/js/js-promotion-list.js" type="text/javascript"></script>
   </body>
</html>
