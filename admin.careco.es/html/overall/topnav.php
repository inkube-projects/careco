<?php

$header_promotions = array('promotionList', 'promotionAdd', 'promotionEdit');
$header_properties = array('propertiesList', 'propertiesAdd', 'propertiesEdit');
$header_about = array('aboutUs', 'ourMilestones', 'ourMilestonesEdit', 'portfolioList', 'portfolioAdd', 'portfolioEdit');
$header_opinions = array('opinionsList', 'opinionsAdd', 'opinionsEdit');
$header_blog = array('tagList', 'tagEdit', 'postList', 'postAdd', 'postEdit');
$header_contact = array('contactList', 'contactEdit');
$header_users = array('userList', 'userEdit', 'userAdd');
$header_reports = array('reportTagList', 'reportTagAdd', 'reportTagEdit', 'reportList', 'reportAdd', 'reportEdit');
$header_newsletterList = array('newsletterList', 'newsletterAdd', 'newsleteerEdit', 'newsletterPlantillas' ,'newsletterEstadisiticas' );
$header_cnt_messages = $db->getCount("contact", "status='1'");

?>

<div class="sidebar">
   <!-- Close button (mobile devices) -->
   <div class="sidebar__close">
      <img src="views/images/close.svg" alt="Close sidebar">
   </div>

   <!-- Sidebar user -->
   <div class="sidebar__user center-block text-center">
      <div class="sidebar-user__avatar center-block">
         <img src="views/images/logos/logo-whiteAdmin.png" alt="" class="img-responsive">
      </div>
      <a class="sidebar-user__info">
         <h4 class="txtMenu"><?= mb_convert_case($admin_username, MB_CASE_UPPER, "UTF-8") ?></h4>
        <!-- <p>Opciones <i class="fa fa-caret-down"></i></p>-->
      </a>
   </div>

   <!-- Sidebar user nav
   <nav class="sidebar-user__nav">
      <ul class="sidebar__nav">
         <li><a href="#"><i class="fa fa-user icoG"></i><span class="txtMenu">Mi Perfil</span></a></li>
         <li><a href="#"><i class="fa fa-users icoG"></i> <span class="txtMenu">Perfil Usuairos</span></a></li>
         <li><a href="#"><i class="fa fa-briefcase icoG"></i><span class="txtMenu">Perfil Empresas</span></a></li>
      </ul>
   </nav>-->

   <!-- Sidebar nav -->
   <nav>
      <ul class="sidebar__nav">
         <?php if ($user_view == "home") { $class = "active"; } else { $class = ""; } ?>
         <li class="<?=$class?>">
            <a href="index.php"><i class="fa fa-th-large icoG"></i> <span class="txtMenu">INICIO</span></a>
         </li>
         <?php if (in_array($user_view, $header_promotions)) { $class = "active open"; $style = 'style="display:block"'; } else { $class = ""; $style = ''; } ?>
         <li class="sidebar-nav__dropdown <?=$class?>">
            <a href="#"><i class="fab fa-product-hunt icoG"></i><span class="txtMenu">Promociones<i class="fa fa-angle-down"></i></span></a>
            <ul class="sidebar-nav__submenu" <?=$style?>>
               <?php if ($user_view == "promotionList") { $class = "active open"; } else { $class = "";} ?>
               <li class="<?=$class?>"><a href="index.php?view=promotionList">Listado de promociones</a></li>

               <?php if ($user_view == "promotionAdd") { $class = "active open"; } else { $class = "";} ?>
               <li class="<?=$class?>"><a href="index.php?view=promotionAdd">Agregar promoción</a></li>
            </ul>
         </li>

         <?php if (in_array($user_view, $header_properties)) { $class = "active open"; $style = 'style="display:block"'; } else { $class = ""; $style = ''; } ?>
         <li class="sidebar-nav__dropdown <?=$class?>">
            <a href="#"><i class="fa fas fa-building icoG"></i> <span class="txtMenu">Propiedades<i class="fa fa-angle-down"></i></span></a>
            <ul class="sidebar-nav__submenu" <?=$style?>>

               <?php if ($user_view == "propertiesList") { $class = "active open"; } else { $class = "";} ?>
               <li class="<?=$class?>"><a href="index.php?view=propertiesList">Listado de propiedades</a></li>

               <?php if ($user_view == "propertiesAdd") { $class = "active open"; } else { $class = "";} ?>
               <li class="<?=$class?>"><a href="index.php?view=propertiesAdd">Agregar propiedades</a></li>
            </ul>
         </li>

         <?php if ($user_view == "icon") { $class = "active"; } else { $class = ""; } ?>
         <li class="<?=$class?>">
            <a href="index.php?view=icon"><i class="fa fa-bed icoG"></i><span class="txtMenu">Iconos</span></a>
         </li>

         <?php if (in_array($user_view, $header_about)) { $class = "active open"; $style = 'style="display:block"'; } else { $class = ""; $style = ''; } ?>
         <li class="sidebar-nav__dropdown <?=$class?>">
            <a href="#"><i class="fa fas fa-users icoG"></i> <span class="txtMenu">Nosotros<i class="fa fa-angle-down"></i></span></a>
            <ul class="sidebar-nav__submenu" <?=$style?>>

               <?php if ($user_view == "aboutUs") { $class = "active open"; } else { $class = "";} ?>
               <li class="<?=$class?>"><a href="index.php?view=aboutUs">Así somos</a></li>

               <?php if ($user_view == "ourMilestones") { $class = "active open"; } else { $class = "";} ?>
               <li class="<?=$class?>"><a href="index.php?view=ourMilestones">Nuestros hitos</a></li>

               <?php if ($user_view == "portfolioList") { $class = "active open"; } else { $class = "";} ?>
               <li class="<?=$class?>"><a href="index.php?view=portfolioList">Nuestras Obras</a></li>
            </ul>
         </li>

         <?php if (in_array($user_view, $header_opinions)) { $class = "active open"; $style = 'style="display:block"'; } else { $class = ""; $style = ''; } ?>
         <li class="sidebar-nav__dropdown <?=$class?>">
            <a href="#"><i class="fa fas fa-user icoG"></i> <span class="txtMenu">Opiniones<i class="fa fa-angle-down"></i></span></a>
            <ul class="sidebar-nav__submenu" <?=$style?>>

               <?php if ($user_view == "opinionsList") { $class = "active open"; } else { $class = "";} ?>
               <li class="<?=$class?>"><a href="index.php?view=opinionsList">Listado de opiniones</a></li>

               <?php if ($user_view == "opinionsAdd") { $class = "active open"; } else { $class = "";} ?>
               <li class="<?=$class?>"><a href="index.php?view=opinionsAdd">Agregar opinión</a></li>
            </ul>
         </li>

         <?php if (in_array($user_view, $header_blog)) { $class = "active open"; $style = 'style="display:block"'; } else { $class = ""; $style = ''; } ?>
         <li class="sidebar-nav__dropdown <?=$class?>">
            <a href="#"><i class="fa fas fa-user icoG"></i> <span class="txtMenu">BLOG<i class="fa fa-angle-down"></i></span></a>
            <ul class="sidebar-nav__submenu" <?=$style?>>

               <?php if ($user_view == "tagList") { $class = "active open"; } else { $class = "";} ?>
               <li class="<?=$class?>"><a href="index.php?view=tagList">Categoría del blog</a></li>

               <?php if ($user_view == "postList") { $class = "active open"; } else { $class = "";} ?>
               <li class="<?=$class?>"><a href="index.php?view=postList">Listado de publicaciones</a></li>

               <?php if ($user_view == "postAdd") { $class = "active open"; } else { $class = "";} ?>
               <li class="<?=$class?>"><a href="index.php?view=postAdd">Agregar publicación</a></li>
            </ul>
         </li>

         <?php if ($user_view == "newsletter") { $class = "active"; } else { $class = ""; } ?>
         <li class="<?=$class?>">
            <a href="index.php?view=newsletter"><i class="far fa-envelope icoG"></i><span class="txtMenu">Newsletter</span></a>
         </li>

         <?php if (in_array($user_view, $header_contact)) { $class = "active open"; $style = 'style="display:block"'; } else { $class = ""; $style = ''; } ?>
         <li class="<?=$class?>">
            <a href="index.php?view=contactList"><i class="fas fa-envelope icoG"></i><span class="txtMenu">Contacto</span> <div class="pull-right label label-primary"><?=$header_cnt_messages?></div></a>
         </li>

         <?php if (in_array($user_view, $header_users)) { $class = "active open"; $style = 'style="display:block"'; } else { $class = ""; $style = ''; } ?>
         <li class="sidebar-nav__dropdown <?=$class?>">
            <a href="#"><i class="fa fas fa-users icoG"></i> <span class="txtMenu">Usuarios<i class="fa fa-angle-down"></i></span></a>
            <ul class="sidebar-nav__submenu" <?=$style?>>

               <?php if ($user_view == "userList") { $class = "active open"; } else { $class = "";} ?>
               <li class="<?=$class?>"><a href="index.php?view=userList">Listado de usuarios</a></li>

               <?php if ($user_view == "userAdd") { $class = "active open"; } else { $class = "";} ?>
               <li class="<?=$class?>"><a href="index.php?view=userAdd">Agregar usuario</a></li>
            </ul>
         </li>

         <?php if (in_array($user_view, $header_reports)) { $class = "active open"; $style = 'style="display:block"'; } else { $class = ""; $style = ''; } ?>
         <li class="sidebar-nav__dropdown <?=$class?>">
            <a href="#"><i class="fa fas fa-users icoG"></i> <span class="txtMenu">Reportes<i class="fa fa-angle-down"></i></span></a>
            <ul class="sidebar-nav__submenu" <?=$style?>>

               <?php if ($user_view == "reportTagList") { $class = "active open"; } else { $class = "";} ?>
               <li class="<?=$class?>"><a href="index.php?view=reportTagList">Listado de tags</a></li>

               <?php if ($user_view == "reportTagAdd") { $class = "active open"; } else { $class = "";} ?>
               <li class="<?=$class?>"><a href="index.php?view=reportTagAdd">Agregar tag</a></li>

               <!-- listado de reportes -->
               <?php if ($user_view == "reportList") { $class = "active open"; } else { $class = "";} ?>
               <li class="<?=$class?>"><a href="index.php?view=reportList">Listado de reportes</a></li>

               <!-- Agregar Reporte -->
               <?php if ($user_view == "reportAdd") { $class = "active open"; } else { $class = "";} ?>
               <li class="<?=$class?>"><a href="index.php?view=reportAdd">Agregar Reporte</a></li>
            </ul>
         </li>
       
       <!--MÓDULO NEWSLETTER-->  
		<div class="BackNewsletter">         
            <div class="hr2"><hr></div>
            
             <nav>
       		 <ul class="sidebar__nav">
            
                <li class="sidebar-nav__dropdown " style="padding-bottom:10px;">
                    <i class="fa fa-newspaper" style="color:#74a3a9; font-size:44px; padding-left:20px;"></i><br>
                    <span class="txtMenu" style="color:#fff; padding-left:20px; font-size:15px;">MÓDULO NEWSLETTER</span>
                </li>
             
             	<?php if (in_array($user_view, $header_newsletterList)) { $class = "active open"; $style = 'style="display:block"'; } else { $class = ""; $style = ''; } ?>
                 <li class="sidebar-nav__dropdown <?=$class?>">
                    <a href="#"><i class="fa fa-newspaper"></i><span class="txtMenu"> NEWSLETTER<i class="fa fa-angle-down"></i></span></a>
                    <ul class="sidebar-nav__submenu" <?=$style?>>
                    	<?php if ($user_view == "newsletterList") { $class = "active open"; } else { $class = "";} ?>
                        <li id=" " section=" "><a href="index.php?view=newsletterList"> Listado</a></li>
                        
                        <?php if ($user_view == "newsletterPlantillas") { $class = "active open"; } else { $class = "";} ?>
                        <li id="#" section=" "><a href="#"> Plantillas</a></li>
                        
                        <?php if ($user_view == "newsletterEstadisiticas") { $class = "active open"; } else { $class = "";} ?>
                        <li id="#" section=" "><a href="#"> Estadísticas</a></li>
                    </ul>
                 </li>
            
                </ul>
    		</nav>
            
            <div class="hr2"><hr></div>
            
            </div>
            <!--END.MODULO NEWSLETTER-->
         
     </ul>
   </nav>

   <div class="sidebar-footer hidden-small">
      <a href="index.php?view=logOut" data-toggle="tooltip" data-placement="top" title="" data-original-title="Salir">
         <span class="glyphicon glyphicon-off text-red" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Settings">
         <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
      </a>
   </div>
</div>


<!-- MAIN CONTENT
================================================== -->
<!-- Navbar -->
   <div class="row">
      <div class="col-xs-12">
         <nav class="navbar navbar-default">
            <div class="container-fluid">
               <div class="collapse navbar-collapse" id="navbar_main">
                  <a href="index.html#" class="btn btn-default navbar-btn navbar-left" id="sidebar__toggle">
                     <i class="fa fa-bars"></i>
                  </a>
                  <ul class="nav navbar-nav navbar-right">
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                           <span class="hidden-xs">Accesos</span> <i class="fa fa-briefcase fa-2x visible-xs-inline-block"></i>
                        </a>
                        <div class="dropdown-menu navbar-messages">
                           <a href="#" class="navbar-messages__item active">
                              <div class="navbar-messages__avatar">
                                 <img src="views/images/access/careco.jpg" alt="...">
                              </div>
                              <div class="navbar-messages__body">
                                 <h5 class="navbar-messages__sender">
                                    CARECO
                                 </h5>
                              </div>
                           </a>

                           <a href="#" class="navbar-messages__item">
                              <div class="navbar-messages__avatar">
                                 <img src="views/images/user_4.jpg" alt="...">
                              </div>
                              <div class="navbar-messages__body">
                                 <h5 class="navbar-messages__sender">
                                    BLOG
                                 </h5>
                              </div>
                           </a>
                        </div>
                     </li>
                  </ul>
               </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
         </nav>
      </div>
   </div> <!-- / .row -->
