<!-- CSS Plugins -->
<link rel="stylesheet" href="views/app/css/perfect-scrollbar.min.css">
<!-- Bootstrap -->
<link href="views/app/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- Font-awesome 5.0.6 -->
<link rel="stylesheet" href="views/app/plugins/font-awesome/web-fonts-with-css/css/fontawesome-all.min.css">
<!-- Google Fonts -->
<link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300,500' rel='stylesheet' type='text/css'>
<!-- CSS Global -->
<link href="views/app/css/styles.css" rel="stylesheet">
<link href="views/app/css/ico.css" rel="stylesheet">
<!--CSS Custom-->
<link href="views/app/css/BFT.css" rel="stylesheet">
