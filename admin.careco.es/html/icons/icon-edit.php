<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="admin">
      <meta name="author" content="Inkube">
      <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
      <title><?=APP_TITLE?></title>
      <!-- Date picker -->
      <link rel="stylesheet" href="views/app/plugins/datepicker/datepicker3.css">
      <!-- Select 2 -->
      <link rel="stylesheet" href="views/app/plugins/select2/select2.min.css">
      <!-- Datatables -->
      <link rel="stylesheet" href="views/app/plugins/datatables/dataTables.bootstrap.css">
      <?php include('html/overall/header.php'); ?>
   </head>

   <body>

      <div class="wrapper">
         <div class="container-fluid">
            <?php include('html/overall/topnav.php'); ?>

            <div class="row">
               <div class="col-xs-12">
                  <h3 class="page-header pull-left">
                     Iconos
                     <small>Editar icono</small>
                  </h3>
               </div>
            </div> <!-- / .row -->

            <section class="content">
               <div class="js-alert"><?=$flash_message?></div>

               <form name="frm-icon-edit" id="frm-icon-edit" method="post" action="">
                  <div class="box box-solid">
                     <div class="box-header">
                        <h2 class="box-title">Editar promoción</h2>
                        <?php if ($arr_icon[0]['image'] != ""): ?>
                           <div class="pull-right">
                              <img src="<?=APP_IMG_ICON.$arr_icon[0]['image']?>" alt="">
                           </div>
                        <?php endif; ?>
                     </div>

                     <div class="box-body">
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <input type="text" name="description" id="description" class="form-control" placeholder="Agregar descripción" value="<?=$title?>">
                                 <div class="text-danger" id="description_validate"></div>
                              </div>
                           </div>

                           <div class="col-md-6">
                              <div class="btn btn-default btn-file" id="img-container">
                                 <i class="fa fa-camera"></i> <span>Agregar imagen de icono</span>
                                 <input type="file" name="icon_image" id="icon_image">
                              </div>
                              <div class="text-danger" id="icon_image_validate"></div>
                           </div>
                        </div>
                     </div>
                  </div>

                  <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
               </form>
            </section>

            <!-- Footer -->
            <?php include('html/overall/footer.php') ?>
         </div> <!-- / .container-fluid -->
      </div> <!-- / .wrapper -->

      <!-- Modal para la imagen de Portada -->
      <div class="modal fade" tabindex="-1" role="dialog" id="mod-img-cover">
         <div class="modal-dialog modal-lg css-width1100">
            <div class="modal-content">
               <div class="modal-header">
                  Imagen de portada

                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               </div>
               <div class="modal-body">
                  <div class="col-md-12 css-noPadding css-noFloat center-block" id="ajx-img-cover"></div>

                  <div class="clearfix css-marginB10"></div>
               </div>
            </div>
         </div>
      </div>

      <div id="key" data-form="frm-icon-edit" data-acc="2" data-id="<?=$id?>"></div>

      <!-- JavaScript
      ================================================== -->
      <!-- JS Global -->
      <?php include('html/overall/js.php'); ?>

      <script src="views/app/plugins/select2/select2.full.min.js"></script>
      <!-- jQuery validation -->
      <script type="text/javascript" src="views/app/plugins/jquery-validation/jquery.validate.js"></script>
      <script type="text/javascript" src="views/app/plugins/jquery-validation/additional-methods.js"></script>
      <!-- Custom JS -->
      <script src="views/app/js/js-icon.js" type="text/javascript"></script>
   </body>
</html>
