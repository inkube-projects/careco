<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="admin">
      <meta name="author" content="Inkube">
      <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
      <title><?=APP_TITLE?></title>
      <!-- Date picker -->
      <link rel="stylesheet" href="views/app/plugins/datepicker/datepicker3.css">
      <!-- Select 2 -->
      <link rel="stylesheet" href="views/app/plugins/select2/select2.min.css">
      <!-- Datatables -->
      <link rel="stylesheet" href="views/app/plugins/datatables/dataTables.bootstrap.css">
      <?php include('html/overall/header.php'); ?>
   </head>

   <body>

      <div class="wrapper">
         <div class="container-fluid">
            <?php include('html/overall/topnav.php'); ?>

            <div class="row">
               <div class="col-xs-12">
                  <h3 class="page-header pull-left">
                     Iconos
                     <small>Listado de iconos</small>
                  </h3>

                  <div class="col-md-2 pull-right css-marginT35">
                     <a href="index.php?view=promotionAdd" class="btn btn-info btn-flat pull-right">Nueva promoción</a>
                  </div>
               </div>
            </div> <!-- / .row -->

            <section class="content">
               <div class="js-alert"><?=$flash_message?></div>

               <form name="frm-icon-add" id="frm-icon-add" method="post" enctype="multipart/form-data" action="">
                  <div class="box box-solid">
                     <div class="box-header">
                        <h2 class="box-title">Agregar icono</h2>
                     </div>

                     <div class="box-body">
                        <div class="row">
                           <div class="col-md-5">
                              <div class="form-group">
                                 <input type="text" name="description" id="description" class="form-control" placeholder="Descripción del icono">
                                 <div class="text-danger" id="description_validate"></div>
                              </div>
                           </div>

                           <div class="col-md-5">
                              <div class="btn btn-default btn-file" id="img-container">
                                 <i class="fa fa-camera"></i> <span>Agregar imagen de icono</span>
                                 <input type="file" name="icon_image" id="icon_image">
                              </div>
                              <div class="text-danger" id="icon_image_validate"></div>
                           </div>

                           <div class="col-md-2">
                              <button type="submit" class="btn btn-success btn-flat btn-submit">Guardar</button>
                           </div>
                        </div>
                     </div>
                  </div>
               </form>

               <div class="box box-solid">
                  <div class="box-header">
                     <h2 class="box-title">Listado de iconos</h2>
                  </div>
                  <div class="box-body">
                     <table class="table table-hover">
                        <thead>
                           <tr>
                              <th>Ref.</th>
                              <th>Descripción</th>
                              <th>Icono</th>
                              <th>Acciones</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach ($arr_icons as $val): ?>
                              <tr>
                                 <td><?=$val['id']?></td>
                                 <td>
                                    <?=$val['description']?>
                                 </td>
                                 <td>
                                    <?php if ($val['image'] != ""): ?>
                                       <img src="<?=APP_IMG_ICON.$val['image']?>" class="img-responsive" alt="">
                                    <?php endif; ?>
                                 </td>
                                 <td>
                                    <a href="index.php?view=iconEdit&i=<?=$val['id']?>" class="btn btn-info btn-flat">Editar</a>
                                    <?php if ($role === "ROLE_ADMIN"): ?>
                                       <a href="javascript: deleteBoostrapAction('remove&i=<?=$val['id']?>&acc=2', 1)" class="btn btn-danger btn-flat">Eliminar</a>
                                    <?php endif; ?>
                                 </td>
                              </tr>
                           <?php endforeach; ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </section>

            <div class="modal fade modal-danger" tabindex="-1" role="dialog" id="mod-remove">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Eliminar categoría</h4>
                     </div>
                     <div class="modal-body">
                        <p>Estas seguro de eliminar este icono</p>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No eliminar</button>
                        <button type="button" class="btn btn-danger btn-flat" id="mod-remove-btn" data-id="" onclick="deleteBoostrapAction('',2)"><i class="fa fa-trash"></i> Eliminar</button>
                     </div>
                  </div>
               </div>
            </div>

            <!-- Footer -->
            <?php include('html/overall/footer.php') ?>
         </div> <!-- / .container-fluid -->
      </div> <!-- / .wrapper -->

      <div id="key" data-form="frm-icon-add" data-acc="1" data-id=""></div>

      <!-- JavaScript
      ================================================== -->
      <!-- JS Global -->
      <?php include('html/overall/js.php'); ?>

      <script src="views/app/plugins/select2/select2.full.min.js"></script>
      <!-- jQuery validation -->
      <script type="text/javascript" src="views/app/plugins/jquery-validation/jquery.validate.js"></script>
      <script type="text/javascript" src="views/app/plugins/jquery-validation/additional-methods.js"></script>
      <!-- DataTables -->
     <script src="views/app/plugins/datatables/jquery.dataTables.js"></script>
     <script src="views/app/plugins/datatables/dataTables.bootstrap.min.js"></script>
      <!-- Custom JS -->
      <script type="text/javascript" src="views/app/js/js-icon.js"></script>
      <script type="text/javascript">
      // Configuración de la paginación con DataTable
      $('.table').DataTable({
         "language": {
            "lengthMenu": "Mostrar _MENU_ entradas por página",
            "zeroRecords": "No se han encontrado resultados",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No se han encontrado resultados",
            "infoFiltered": "(Se ha filtrado un total de _MAX_ entradas)",
            "search": "Buscar: ",
            "paginate": {
               "previous": "Anterior",
               "next": "Siguiente"
            }
         }
      });
      </script>
   </body>
</html>
