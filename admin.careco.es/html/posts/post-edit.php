<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="admin">
      <meta name="author" content="Inkube">
      <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
      <title><?=APP_TITLE?></title>
      <!-- Date picker -->
      <link rel="stylesheet" href="views/app/plugins/datepicker/datepicker3.css">
      <!-- Select 2 -->
      <link rel="stylesheet" href="views/app/plugins/select2/select2.min.css">
      <!-- Datatables -->
      <link rel="stylesheet" href="views/app/plugins/datatables/dataTables.bootstrap.css">
      <?php include('html/overall/header.php'); ?>
   </head>

   <body>

      <div class="wrapper">
         <div class="container-fluid">
            <?php include('html/overall/topnav.php'); ?>

            <div class="row">
               <div class="col-xs-12">
                  <h3 class="page-header pull-left">
                     Entradas del blog
                     <small>Editar entrada</small>
                  </h3>
               </div>
            </div> <!-- / .row -->

            <section class="content">
               <div class="js-alert"><?=$flash_message?></div>

               <form name="frm-tag-add" id="frm-tag-add" method="post" action="">
                  <input type="hidden" name="hid-name" id="hid-name" value="">
                  <div class="box box-solid">
                     <div class="box-header">
                        <h2 class="box-title">Editar entrada</h2>
                     </div>

                     <div class="box-body">
                        <div class="form-group">
                           <input type="text" name="title" id="title" class="form-control" placeholder="Agregar el título de la entrada" value="<?=$arr_post[0]['title']?>">
                           <div class="text-danger" id="title_validate"></div>
                        </div>

                        <div class="form-group">
                           <textarea id="description" name="description" class="form-control" rows="8"><?=$arr_post[0]['description']?></textarea>
                           <div class="text-danger" id="description_validate"></div>
                        </div>
                     </div>
                  </div>

                  <div class="box box-solid">
                     <div class="box-header">
                        <h2 class="box-title">Imagen de portada</h2>

                        <div class="box-tools pull-right">
                           <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                     </div>

                     <div class="box-body">
                        <img id="img-cover" src="<?=$image?>" class="img-responsive css-marginB20 css-noFloat center-block" alt="">

                        <div class="col-md-12 text-center">
                           <button type="button" class="btn btn-success btn-flat btn-submit-cover-image" disabled>Debes seleccionar una imagen de portada</button>
                           <div class="clearfix css-espacio10"></div>

                           <div class="btn btn-default btn-file">
                              <i class="fa fa-camera"></i> Agregar imagen de portada
                              <input type="file" name="cover_image" id="cover_image" class="cover_image">
                           </div>
                           <p>Se recomienda que la imagen sea 400 x 400 px</p>
                           <div class="text-danger" id="cover_image_validate"></div>
                        </div>
                     </div>
                  </div>

                  <div class="box box-solid">
                     <div class="box-header with-border">
                        <h2 class="box-title">Descripciones</h2>

                        <div class="box-tools pull-right">
                           <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                           </button>
                        </div>
                     </div>

                     <div class="box-body">
                        <?php foreach ($arr_tags as $val): ?>
                           <div class="col-md-3">
                              <div class="form-group">
                                 <span class="button-checkbox">
                                    <button type="button" class="btn btn-flat css-marginR10 btn-primary active css-width100" data-color="primary" data-input="tag_<?=$val['id']?>" style="text-align: left;">
                                       <i class="state-icon glyphicon glyphicon-check"></i> <?=$val['name']?>
                                    </button>
                                    <input class="hidden" type="checkbox" name="tag[]" id="tag_<?=$val['id']?>" value="<?=$val['id']?>" <?=isChecked($arr_tags_s,$val['id'])?>>
                                 </span>
                              </div>
                           </div>
                        <?php endforeach; ?>
                     </div>
                  </div>

                  <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
               </form>
            </section>

            <!-- Footer -->
            <?php include('html/overall/footer.php') ?>
         </div> <!-- / .container-fluid -->
      </div> <!-- / .wrapper -->

      <!-- Modal para la imagen de Portada -->
      <div class="modal fade" tabindex="-1" role="dialog" id="mod-img-cover">
         <div class="modal-dialog modal-lg css-width1100">
            <div class="modal-content">
               <div class="modal-header">
                  Imagen de portada

                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               </div>
               <div class="modal-body">
                  <div class="col-md-12" id="ajx-img-cover"></div>

                  <div class="clearfix css-marginB10"></div>
               </div>
            </div>
         </div>
      </div>

      <div id="key" data-form="frm-tag-add" data-acc="4" data-id="<?=$id?>"></div>

      <!-- JavaScript
      ================================================== -->
      <!-- JS Global -->
      <?php include('html/overall/js.php'); ?>

      <!-- CKeditor -->
      <script type="text/javascript" src="views/app/plugins/ckeditor/ckeditor.js"></script>
      <!-- Select2 -->
      <script src="views/app/plugins/select2/select2.full.min.js"></script>
      <!-- jQuery validation -->
      <script type="text/javascript" src="views/app/plugins/jquery-validation/jquery.validate.js"></script>
      <script type="text/javascript" src="views/app/plugins/jquery-validation/additional-methods.js"></script>
      <!-- Custom JS -->
      <script src="views/app/js/js-blog-category.js" type="text/javascript"></script>
   </body>
</html>
