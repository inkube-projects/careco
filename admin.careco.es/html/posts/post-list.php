<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="admin">
      <meta name="author" content="Inkube">
      <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
      <title><?=APP_TITLE?></title>
      <!-- Date picker -->
      <link rel="stylesheet" href="views/app/plugins/datepicker/datepicker3.css">
      <!-- Select 2 -->
      <link rel="stylesheet" href="views/app/plugins/select2/select2.min.css">
      <!-- Datatables -->
      <link rel="stylesheet" href="views/app/plugins/datatables/dataTables.bootstrap.css">
      <?php include('html/overall/header.php'); ?>
   </head>

   <body>

      <div class="wrapper">
         <div class="container-fluid">
            <?php include('html/overall/topnav.php'); ?>

            <div class="row">
               <div class="col-xs-12">
                  <h3 class="page-header pull-left">
                     Entradas del blog
                     <small>Listado de posts</small>
                  </h3>

                  <div class="col-md-2 pull-right css-marginT35">
                     <a href="index.php?view=postAdd" class="btn btn-info btn-flat pull-right">Nueva entrada</a>
                  </div>
               </div>
            </div> <!-- / .row -->

            <section class="content">
               <div class="js-alert"><?=$flash_message?></div>

               <div class="box box-solid">
                  <div class="box-header">
                     <h2 class="box-title">Listado de entradas</h2>
                  </div>
                  <div class="box-body">
                     <table class="table table-hover">
                        <thead>
                           <tr>
                              <th>Ref.</th>
                              <th>Título</th>
                              <th>Creada</th>
                              <th>Acciones</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach ($arr_posts as $val): ?>
                              <tr>
                                 <td><?=$val['id']?></td>
                                 <td><?=$val['title']?></td>
                                 <td><?=$val['create']?></td>
                                 <td>
                                    <a href="index.php?view=postEdit&id=<?=$val['id']?>" class="btn btn-info btn-flat">Editar</a>
                                    <?php if ($role === "ROLE_ADMIN"): ?>
                                       <a href="javascript: deleteBoostrapAction('remove&p=<?=$val['id']?>&acc=9', 1)" class="btn btn-danger btn-flat">Eliminar</a>
                                    <?php endif; ?>
                                 </td>
                              </tr>
                           <?php endforeach; ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </section>

            <div class="modal fade modal-danger" tabindex="-1" role="dialog" id="mod-remove">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Eliminar Entrada del Blog</h4>
                     </div>
                     <div class="modal-body">
                        <p>Estas seguro de eliminar esta entrada</p>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No eliminar</button>
                        <button type="button" class="btn btn-danger btn-flat" id="mod-remove-btn" data-id="" onclick="deleteBoostrapAction('',2)"><i class="fa fa-trash"></i> Eliminar</button>
                     </div>
                  </div>
               </div>
            </div>

            <!-- Footer -->
            <?php include('html/overall/footer.php') ?>
         </div> <!-- / .container-fluid -->
      </div> <!-- / .wrapper -->

      <!-- JavaScript
      ================================================== -->
      <!-- JS Global -->
      <?php include('html/overall/js.php'); ?>

      <script src="views/app/plugins/select2/select2.full.min.js"></script>
      <!-- bootstrap datepicker -->
      <script src="views/app/plugins/datepicker/bootstrap-datepicker.js"></script>
      <!-- DataTables -->
      <script src="views/app/plugins/datatables/jquery.dataTables.js"></script>
      <script src="views/app/plugins/datatables/dataTables.bootstrap.min.js"></script>
      <!-- InputMask -->
      <script src="views/app/plugins/input-mask/jquery.inputmask.js"></script>
      <script src="views/app/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
      <script src="views/app/plugins/input-mask/jquery.inputmask.extensions.js"></script>
      <!-- Custom JS -->
      <script src="views/app/js/js-promotion-list.js" type="text/javascript"></script>
   </body>
</html>
