<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- Date picker -->
        <link rel="stylesheet" href="views/app/plugins/datepicker/datepicker3.css">
        <!-- Select 2 -->
        <link rel="stylesheet" href="views/app/plugins/select2/select2.min.css">
        <!-- Datatables -->
        <link rel="stylesheet" href="views/app/plugins/datatables/dataTables.bootstrap.css">
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Usuarios
                            <?php if ($acc == 1): ?>
                                <small>Agregar usuarios</small>
                            <?php else: ?>
                                <small>Editar usuarios</small>
                            <?php endif; ?>
                        </h3>
                    </div>
                </div> <!-- / .row -->

                <section class="content">
                    <div class="js-alert"><?=$flash_message?></div>

                    <form name="<?=$frm?>" id="<?=$frm?>" method="post" action="">
                        <input type="hidden" name="hid-name" id="hid-name" value="">
                        <div class="box box-solid">
                            <div class="box-header">
                                <?php if ($acc == 1): ?>
                                    <h2 class="box-title">Agregar usuario</h2>
                                <?php else: ?>
                                    <h2 class="box-title">Editar usuario</h2>
                                <?php endif; ?>
                            </div>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-balck">Nombre</label>
                                            <input type="text" name="name" id="name" class="form-control" value="<?=$name?>">
                                            <div class="text-danger" id="name_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-balck">Username</label>
                                            <input type="text" name="username" id="username" class="form-control" value="<?=$username?>">
                                            <div class="text-danger" id="username_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-balck">Contraseña</label>
                                            <input type="password" name="pass_1" id="pass_1" class="form-control">
                                            <div class="text-danger" id="pass_1_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-balck">Rol</label>
                                            <select class="form-control select2" name="role" id="role">
                                                <option value="">Seleccione un Rol</option>
                                                <?php foreach ($arr_role as $val): ?>
                                                    <option value="<?=$val['id']?>" <?=isSelected($val['id'], $role)?>><?=$val['role_name']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <div class="text-danger" id="role_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-balck">Teléfono 1</label>
                                            <input type="text" name="phone_1" id="phone_1" class="form-control" value="<?=$phone_1?>">
                                            <div class="text-danger" id="phone_1_validate"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-balck">Apellido</label>
                                            <input type="text" name="last_name" id="last_name" class="form-control" value="<?=$last_name?>">
                                            <div class="text-danger" id="last_name_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-balck">E-mail</label>
                                            <input type="text" name="email" id="email" class="form-control" value="<?=$email?>">
                                            <div class="text-danger" id="email_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-balck">Repita la Contraseña</label>
                                            <input type="password" name="pass_2" id="pass_2" class="form-control">
                                            <div class="text-danger" id="pass_2_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-balck">Estado</label>
                                            <select class="form-control select2" name="status" id="status">
                                                <option value="">Seleccione un Estado</option>
                                                <?php foreach ($arr_status as $val): ?>
                                                    <option value="<?=$val['id']?>" <?=isSelected($val['id'], $status)?>><?=$val['description']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <div class="text-danger" id="status_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-balck">Teléfono 2</label>
                                            <input type="text" name="phone_2" id="phone_2" class="form-control" value="<?=$phone_2?>">
                                            <div class="text-danger" id="phone_2_validate"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
                    </form>
                </section>

                <!-- Footer -->
                <? include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->

        <!-- Modal para la imagen de Portada -->
        <div class="modal fade" tabindex="-1" role="dialog" id="mod-img-cover">
            <div class="modal-dialog modal-lg css-width1100">
                <div class="modal-content">
                    <div class="modal-header">
                        Imagen de perfil

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12" id="ajx-img-cover"></div>

                        <div class="clearfix css-marginB10"></div>
                    </div>
                </div>
            </div>
        </div>

        <div id="key" data-form="<?=$frm?>" data-acc="<?=$acc?>" data-id="<?=$id?>"></div>

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>

        <script src="views/app/plugins/select2/select2.full.min.js"></script>
        <!-- jQuery validation -->
        <script type="text/javascript" src="views/app/plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="views/app/plugins/jquery-validation/additional-methods.js"></script>
        <!-- Custom JS -->
        <script src="views/app/js/js-users.js" type="text/javascript"></script>
    </body>
</html>
