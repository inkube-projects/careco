<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="admin">
      <meta name="author" content="Inkube">
      <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
      <title><?=APP_TITLE?></title>
      <!-- Date picker -->
      <link rel="stylesheet" href="views/app/plugins/datepicker/datepicker3.css">
      <!-- Select 2 -->
      <link rel="stylesheet" href="views/app/plugins/select2/select2.min.css">
      <!-- Datatables -->
      <link rel="stylesheet" href="views/app/plugins/datatables/dataTables.bootstrap.css">
      <?php include('html/overall/header.php'); ?>
   </head>

   <body>

      <div class="wrapper">
         <div class="container-fluid">
            <?php include('html/overall/topnav.php'); ?>

            <div class="row">
               <div class="col-xs-12">
                  <h3 class="page-header pull-left">
                     Contacto
                     <small>Listado de mensajes</small>
                  </h3>
               </div>
            </div> <!-- / .row -->

            <section class="content">
               <form name="frm-search" id="frm-search" method="get" action="">
                  <input type="hidden" name="view" id="view" value="contactList">
                  <div class="box box-solid">
                     <div class="box-header">
                        <h2 class="box-title">Búsqueda avanzada</h2>
                     </div>
                     <div class="box-body">
                        <div class="row css-marginB20">
                           <div class="col-md-4">
                              <select class="form-control select2" name="src_status" id="src_status">
                                 <option value="">Todos los mensajes</option>
                                 <option value="1" <?=isSelected(1, $src_status)?>>No leídos</option>
                                 <option value="2" <?=isSelected(2, $src_status)?>>Leídos</option>
                              </select>
                           </div>

                           <div class="col-md-8">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <input type="text" class="form-control js-date" name="src_init_date" id="src_init_date" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask placeholder="Fecha inicial" value="<?=$src_init_date?>">
                                    </div>
                                 </div>

                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <input type="text" class="form-control js-date" name="src_end_date" id="src_end_date" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask placeholder="Fecha final" value="<?=$src_end_date?>">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="row css-marginB20">
                           <div class="col-md-12" id="chk-dep">
                              <?php foreach ($arr_departament as $val): ?>
                                 <span class="button-checkbox">
                                    <button type="button" class="btn btn-flat css-marginR10 css-marginB10 btn-primary active" data-color="primary">
                                       <i class="state-icon glyphicon glyphicon-check"></i> <?=$val['name']?>
                                    </button>
                                    <input class="hidden" type="checkbox" name="src_departament[]" value="<?=$val['id']?>" <?=isChecked($src_departament, $val['id'])?>>
                                 </span>
                              <?php endforeach; ?>
                           </div>
                        </div>
                     </div>

                     <div class="box-footer">
                        <button type="button" class="btn btn-warning btn-flat btn-reset" onclick="formReset('frm-search')"><i class="fa fa-brush"></i> Limpiar formulario</button>
                        <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fas fa-search"></i> Buscar</button>
                     </div>
                  </div>
               </form>

               <div class="js-alert"><?=$flash_message?></div>

               <div class="box box-solid">
                  <div class="box-header">
                     <h2 class="box-title">Listado de mensajes</h2>
                  </div>
                  <div class="box-body">
                     <table class="table table-hover">
                        <thead>
                           <tr>
                              <th>Ref.</th>
                              <th>E-mail</th>
                              <th>Teléfono</th>
                              <th>Departamento</th>
                              <th>Estado</th>
                              <th>Fecha</th>
                              <th>Acciones</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach ($arr_contact as $val): ?>
                              <tr>
                                 <td class="<?=$val['class']?>"><?=$val['ref']?></td>
                                 <td class="<?=$val['class']?>"><?=$val['email']?></td>
                                 <td class="<?=$val['class']?>"><?=$val['phone']?></td>
                                 <td class="<?=$val['class']?>"><?=$val['departament_name']?></td>
                                 <td class="<?=$val['class']?>"><?=$val['status']?></td>
                                 <td class="<?=$val['class']?>"><?=$val['date']?></td>
                                 <td class="<?=$val['class']?>">
                                    <a href="index.php?view=contactEdit&id=<?=$val['id']?>" class="btn btn-info btn-flat">Ver</a>
                                    <?php if ($role === "ROLE_ADMIN"): ?>
                                       <a href="javascript: deleteBoostrapAction('remove&c=<?=$val['id']?>&acc=10', 1)" class="btn btn-danger btn-flat">Eliminar</a>
                                    <?php endif; ?>
                                 </td>
                              </tr>
                           <?php endforeach; ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </section>

            <div class="modal fade modal-danger" tabindex="-1" role="dialog" id="mod-remove">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Eliminar mensaje</h4>
                     </div>
                     <div class="modal-body">
                        <p>Estas seguro de eliminar este <b>Mensaje</b></p>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No eliminar</button>
                        <button type="button" class="btn btn-danger btn-flat" id="mod-remove-btn" data-id="" onclick="deleteBoostrapAction('',2)"><i class="fa fa-trash"></i> Eliminar</button>
                     </div>
                  </div>
               </div>
            </div>

            <!-- Footer -->
            <?php include('html/overall/footer.php') ?>
         </div> <!-- / .container-fluid -->
      </div> <!-- / .wrapper -->

      <!-- JavaScript
      ================================================== -->
      <!-- JS Global -->
      <?php include('html/overall/js.php'); ?>

      <!-- Select2 -->
      <script src="views/app/plugins/select2/select2.full.min.js"></script>
      <!-- InputMask -->
      <script src="views/app/plugins/input-mask/jquery.inputmask.js"></script>
      <script src="views/app/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
      <script src="views/app/plugins/input-mask/jquery.inputmask.extensions.js"></script>
      <!-- DataTables -->
      <script src="views/app/plugins/datatables/jquery.dataTables.js"></script>
      <script src="views/app/plugins/datatables/dataTables.bootstrap.min.js"></script>
      <script src="views/app/js/js-contact-list.js" type="text/javascript"></script>
   </body>
</html>
