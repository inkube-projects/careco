<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Contacto
                            <small>Detalle del mensaje</small>
                        </h3>
                        <div class="col-md-3 pull-right css-marginT35">
                            <?php if ($cnt_report == 0): ?>
                                <?php if ($user_id): ?>
                                    <button type="button" class="btn btn-flat btn-warning" onclick="createReport()">Generar reporte desde este mensaje</button>
                                <?php endif; ?>
                            <?php else: ?>
                                <a href="index.php?view=reportEdit&id=<?=$report_id?>" class="btn btn-info btn-flat" target="_blank" >Ver reporte</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div> <!-- / .row -->
                <input type="hidden" id="msj" value="<?=$arr_contact[0]['message']?>">
                <section class="content">
                    <div class="js-alert"></div>

                    <div class="box box-solid">
                        <div class="box-header">
                            <h2 class="box-title">Detalle del mensaje</h2>

                            <div class="pull-right">
                                <b>Enviado:</b>
                                <?=$datetime['date']." ".$datetime['time']?>
                                <br><input type="checkbox" class="form-check-input" id="read_message" onclick="javascript:readMmessage(<?=$arr_contact[0]['id']?>)">
                                <label class="form-check-label" for="read_message">Marcar como no leído</label>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="row css-marginB30">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="css-text-black css-bold">E-mail:</label>
                                        <p><?=$arr_contact[0]['email']?></p>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="css-text-black css-bold">Teléfono:</label>
                                        <p><?=$arr_contact[0]['phone']?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="row css-marginB30">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="css-text-black css-bold">Nombre:</label>
                                        <p><?=$arr_contact[0]['name']?></p>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="css-text-black css-bold">Apellido:</label>
                                        <p><?=$arr_contact[0]['last_name']?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="row css-marginB30">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="css-text-black css-bold">Departamento:</label>
                                        <p><?=$arr_contact[0]['departament_name']?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box box-solid">
                        <div class="box-header">
                            <h2 class="box-title">Mensaje</h2>
                        </div>

                        <div class="box-body">
                            <p><?=$arr_contact[0]['message']?></p>
                            <p><?=$ext?></p>
                        </div>
                    </div>
                </section>

                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <script type="text/javascript">
            function createReport() {
                $.ajax({
                    type: 'GET',
                    url: 'ajax.php?m=report&acc=3&c=<?=$id?>',
                    dataType: 'json',
                    beforeSend: function(e){
                        $(".btn-submit").attr("disabled", true).html('<i class="fas fa-spinner fa-spin"></i> Cargando...');
                    },
                    success: function(r){
                        if (r.status == 'OK') {
                            $(location).attr("href", "index.php?view=reportEdit&id=" + r.id + "&m=OK1");
                        } else {
                            $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                            $(".btn-submit").attr("disabled", false).html('Generar reporte desde este mensaje');
                            $(document).scrollTop(0);
                        }
                    }
                });
            }
        </script>
    </body>
</html>
