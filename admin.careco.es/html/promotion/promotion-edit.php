<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="admin">
      <meta name="author" content="Inkube">
      <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
      <title><?=APP_TITLE?></title>
      <!-- Date picker -->
      <link rel="stylesheet" href="views/app/plugins/datepicker/datepicker3.css">
      <!-- Select 2 -->
      <link rel="stylesheet" href="views/app/plugins/select2/select2.min.css">
      <!-- Datatables -->
      <link rel="stylesheet" href="views/app/plugins/datatables/dataTables.bootstrap.css">
      <?php include('html/overall/header.php'); ?>
   </head>

   <body>

      <div class="wrapper">
         <div class="container-fluid">
            <?php include('html/overall/topnav.php'); ?>

            <div class="row">
               <div class="col-xs-12">
                  <h3 class="page-header pull-left">
                     Promociones
                     <small>Editar promoción: <?= $title ?></small>
                  </h3>
               </div>
            </div> <!-- / .row -->

            <section class="content">
               <div class="js-alert"><?=$flash_message?></div>

               <form name="frm-promotion-edit" id="frm-promotion-edit" method="post" action="">
                  <input type="hidden" name="hid-name" id="hid-name" value="">
                  <div class="box box-solid">
                     <div class="box-header">
                        <h2 class="box-title">Editar promoción</h2>
                        <div class="pull-right text-right">
                           <b>Creado el:</b> <?=$create_at['date']." ".$create_at['time']?> <br>
                           <b>Actualizado el:</b> <?=$update_at['date']." ".$update_at['time']?>
                        </div>
                     </div>

                     <div class="box-body">
                        <div class="form-group">
                           <input type="text" name="title" id="title" class="form-control" placeholder="Agregar el título de la promoción" value="<?=$title?>">
                           <div class="text-danger" id="title_validate"></div>
                        </div>

                        <div class="form-group">
                           <select class="form-control select2" name="category" id="category">
                              <option value="">Seleccione una categoría</option>
                              <option value="1" <?=isSelected(1,$arr_promotion[0]['category'])?>>Promoción actual</option>
                              <option value="2" <?=isSelected(2,$arr_promotion[0]['category'])?>>Promoción próxima</option>
                           </select>
                           <div class="text-danger" id="category_validate"></div>
                        </div>
                     </div>
                  </div>

                  <div class="box box-solid">
                     <div class="box-header">
                        <h2 class="box-title">Imagen de portada</h2>

                        <div class="box-tools pull-right">
                           <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                     </div>

                     <div class="box-body">
                        <img id="img-cover" src="<?=$image?>" class="img-responsive css-marginB20 css-noFloat center-block" alt="">

                        <div class="col-md-12 text-center">
                           <button type="button" class="btn btn-success btn-flat btn-submit-cover-image" disabled>Debes seleccionar una imagen de portada</button>
                           <div class="clearfix css-espacio10"></div>

                           <div class="btn btn-default btn-file">
                              <i class="fa fa-camera"></i> Agregar imagen de portada
                              <input type="file" name="cover_image" id="cover_image" class="cover_image">
                           </div>
                           <p>Se recomienda que la imagen sea 725 x 305 px</p>
                           <div class="text-danger" id="cover_image_validate"></div>
                        </div>
                     </div>
                  </div>

                  <div class="box box-solid">
                     <div class="box-header with-border">
                        <h2 class="box-title">Información que se mostrará en "próximas promociones"</h2>

                        <div class="box-tools pull-right">
                           <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                     </div>

                     <div class="box-body">
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <textarea name="description" id="description" class="form-control" rows="8" placeholder="Descripción"><?=$description?></textarea>
                                 <div class="text-danger" id="description_validate"></div>
                              </div>
                           </div>

                           <div class="col-md-6">
                              <div class="form-group">
                                 <textarea name="location" id="location" class="form-control" rows="8" placeholder="Descripción"><?=$location?></textarea>
                                 <div class="text-danger" id="location_validate"></div>
                              </div>

                              <div class="row">
                                 <div class="css-bold text-center css-marginB10">Coordenadas</div>

                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <input name="latitude" id="latitude" class="form-control" placeholder="Latitud" type="text" value="<?=$latitude?>">
                                       <div class="text-danger" id="latitude_validate"></div>
                                    </div>
                                 </div>

                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <input name="longitude" id="longitude" class="form-control" placeholder="Longitud" type="text" value="<?=$longitude?>">
                                       <div class="text-danger" id="longitude_validate"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div class="box box-solid">
                     <div class="box-header with-border">
                        <h2 class="box-title">Galería de imágenes</h2>

                        <div class="box-tools pull-right">
                           <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                           </button>
                        </div>
                     </div>

                     <div class="box-body">
                        <div class="col-md-5 css-noFloat center-block text-center well">
                           <div class="btn btn-default btn-file css-width100 btn-file-gallery">
                              <i class="fa fa-camera"></i> <span>Agregar imágenes</span>
                              <input type="file" name="gallery_images[]" id="gallery_images" class="gallery_images" multiple>
                           </div>
                           <p>Las imagenes deben tener mínimo 500 x 250px</p>
                           <div class="text-danger" id="cover_image_validate"></div>
                        </div>

                        <div class="row css-marginT40" id="ajx-gallery"></div>
                     </div>
                  </div>

                  <div class="box box-solid">
                     <div class="box-header">
                        <h2 class="box-title">Imagenes en la galería</h2>
                     </div>

                     <div class="box-body" id="js-gallery">
                        <?php foreach ($arr_gallery as $val): ?>
                           <div class="col-md-3 css-marginB20 container-thumb">
                              <div class="css-background-image" style="background-image: url('<?=$path_gallery."min_".$val['image']?>')"></div>
                              <a href="#" class="btn btn-info btn-flat center-block btn-show" data-image="<?=$path_gallery."min_".$val['image']?>"><i class="fas fa-search"></i> Ver imagen</a>
                              <a href="javascript: removeGalleryImage('<?=$val['id']?>', this.event)" class="btn btn-danger btn-flat center-block btn-remove-gallery" data-id="<?=$val['id']?>"><i class="fas fa-trash-alt"></i> Eliminar</a>
                           </div>
                        <?php endforeach; ?>
                     </div>
                  </div>

                  <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
               </form>
            </section>

            <!-- Footer -->
            <?php include('html/overall/footer.php') ?>
         </div> <!-- / .container-fluid -->
      </div> <!-- / .wrapper -->

      <!-- Modal para la imagen de Portada -->
      <div class="modal fade" tabindex="-1" role="dialog" id="mod-img-cover">
         <div class="modal-dialog modal-lg css-width1100">
            <div class="modal-content">
               <div class="modal-header">
                  Imagen de portada

                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               </div>
               <div class="modal-body">
                  <div class="col-md-12 css-noPadding css-noFloat center-block" id="ajx-img-cover"></div>

                  <div class="clearfix css-marginB10"></div>
               </div>
            </div>
         </div>
      </div>

      <!-- Modal para la imagen de galería -->
      <div class="modal fade" tabindex="-1" role="dialog" id="mod-image">
         <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Imagen de galería</h4>
               </div>
               <div class="modal-body" id="js-image"></div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
               </div>
            </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->

      <div id="key" data-form="frm-promotion-edit" data-acc="4" data-id="<?=$id?>"></div>

      <!-- JavaScript
      ================================================== -->
      <!-- JS Global -->
      <?php include('html/overall/js.php'); ?>
      <!-- CKeditor -->
      <script type="text/javascript" src="views/app/plugins/ckeditor/ckeditor.js"></script>
      <!-- Select2 -->
      <script src="views/app/plugins/select2/select2.full.min.js"></script>
      <!-- jQuery validation -->
      <script type="text/javascript" src="views/app/plugins/jquery-validation/jquery.validate.js"></script>
      <script type="text/javascript" src="views/app/plugins/jquery-validation/additional-methods.js"></script>
      <!-- Custom JS -->
      <script src="views/app/js/js-promotion.js" type="text/javascript"></script>
   </body>
</html>
