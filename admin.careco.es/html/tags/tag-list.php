<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="admin">
      <meta name="author" content="Inkube">
      <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
      <title><?=APP_TITLE?></title>
      <!-- Date picker -->
      <link rel="stylesheet" href="views/app/plugins/datepicker/datepicker3.css">
      <!-- Select 2 -->
      <link rel="stylesheet" href="views/app/plugins/select2/select2.min.css">
      <!-- Datatables -->
      <link rel="stylesheet" href="views/app/plugins/datatables/dataTables.bootstrap.css">
      <?php include('html/overall/header.php'); ?>
   </head>

   <body>

      <div class="wrapper">
         <div class="container-fluid">
            <?php include('html/overall/topnav.php'); ?>

            <div class="row">
               <div class="col-xs-12">
                  <h3 class="page-header pull-left">
                     Categorías del blog
                     <small>Listado de categorías</small>
                  </h3>
               </div>
            </div> <!-- / .row -->

            <section class="content">
               <div class="js-alert"><?=$flash_message?></div>

               <form name="frm-tag-add" id="frm-tag-add" method="post" enctype="multipart/form-data" action="">
                  <div class="box box-solid">
                     <div class="box-header">
                        <h2 class="box-title">Agregar categoría</h2>
                     </div>

                     <div class="box-body">
                        <div class="row">
                           <div class="col-md-5">
                              <div class="form-group">
                                 <input type="text" name="description" id="description" class="form-control" placeholder="Descripción del icono">
                                 <div class="text-danger" id="description_validate"></div>
                              </div>
                           </div>

                           <div class="col-md-2">
                              <button type="submit" class="btn btn-success btn-flat btn-submit">Guardar</button>
                           </div>
                        </div>
                     </div>
                  </div>
               </form>

               <div class="box box-solid">
                  <div class="box-header">
                     <h2 class="box-title">Listado de categorías</h2>
                  </div>
                  <div class="box-body">
                     <table class="table table-hover">
                        <thead>
                           <tr>
                              <th>Ref.</th>
                              <th>Descripción</th>
                              <th>Acciones</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach ($arr_tags as $val): ?>
                              <tr>
                                 <td><?=$val['id']?></td>
                                 <td>
                                    <?=$val['name']?>
                                 </td>
                                 <td>
                                    <a href="index.php?view=tagEdit&i=<?=$val['id']?>" class="btn btn-info btn-flat">Editar</a>
                                    <?php if ($role === "ROLE_ADMIN"): ?>
                                       <a href="javascript: deleteBoostrapAction('remove&t=<?=$val['id']?>&acc=7', 1)" class="btn btn-danger btn-flat">Eliminar</a>
                                    <?php endif; ?>
                                 </td>
                              </tr>
                           <?php endforeach; ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </section>

            <div class="modal fade modal-danger" tabindex="-1" role="dialog" id="mod-remove">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Eliminar categoría</h4>
                     </div>
                     <div class="modal-body">
                        <p>Estas seguro de eliminar esta categoría</p>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No eliminar</button>
                        <button type="button" class="btn btn-danger btn-flat" id="mod-remove-btn" data-id="" onclick="deleteBoostrapAction('',2)"><i class="fa fa-trash"></i> Eliminar</button>
                     </div>
                  </div>
               </div>
            </div>

            <!-- Footer -->
            <?php include('html/overall/footer.php') ?>
         </div> <!-- / .container-fluid -->
      </div> <!-- / .wrapper -->

      <div id="key" data-form="frm-tag-add" data-acc="1" data-id=""></div>

      <!-- JavaScript
      ================================================== -->
      <!-- JS Global -->
      <?php include('html/overall/js.php'); ?>

      <script src="views/app/plugins/select2/select2.full.min.js"></script>
      <!-- jQuery validation -->
      <script type="text/javascript" src="views/app/plugins/jquery-validation/jquery.validate.js"></script>
      <script type="text/javascript" src="views/app/plugins/jquery-validation/additional-methods.js"></script>
      <!-- DataTables -->
     <script src="views/app/plugins/datatables/jquery.dataTables.js"></script>
     <script src="views/app/plugins/datatables/dataTables.bootstrap.min.js"></script>
      <!-- Custom JS -->
      <script type="text/javascript" src="views/app/js/js-tags.js"></script>
      <script type="text/javascript">
      // Configuración de la paginación con DataTable
      $('.table').DataTable({
         "language": {
            "lengthMenu": "Mostrar _MENU_ entradas por página",
            "zeroRecords": "No se han encontrado resultados",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No se han encontrado resultados",
            "infoFiltered": "(Se ha filtrado un total de _MAX_ entradas)",
            "search": "Buscar: ",
            "paginate": {
               "previous": "Anterior",
               "next": "Siguiente"
            }
         }
      });
      </script>
   </body>
</html>
