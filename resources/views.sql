DROP VIEW
IF EXISTS `blog_posts_tags`;

CREATE VIEW `blog_posts_tags` AS SELECT
	`p`.`id` AS `p_id`,
	`p`.`title` AS `p_title`,
	`p`.`description` AS `p_description`,
	`p`.`image` AS `p_image`,
	`p`.`create_at` AS `p_create_at`,
	`p`.`update_at` AS `p_update_at`,
	`pt`.`id` AS `pt_id`,
	`pt`.`tag_id` AS `pt_tag_id`
FROM
	(
		`post` `p`
		JOIN `post_tags` `pt` ON ((`pt`.`post_id` = `p`.`id`))
	)
GROUP BY
	`p`.`id`;

DROP VIEW
IF EXISTS `post_tag_view`;

CREATE VIEW `post_tag_view` AS SELECT
	`pt`.`id` AS `pt_id`,
	`pt`.`post_id` AS `pt_post_id`,
	`pt`.`tag_id` AS `pt_tag_id`,
	`bt`.`id` AS `bt_id`,
	`bt`.`name` AS `bt_name`
FROM
	(
		`post_tags` `pt`
		JOIN `blog_tags` `bt` ON ((`pt`.`tag_id` = `bt`.`id`))
	);

DROP VIEW
IF EXISTS view_contact_info;

CREATE VIEW view_contact_info AS SELECT
	c.id,
	c.email,
	c.department_id,
	c.`name`,
	c.last_name,
	c.message,
	c.phone,
	c.`status`,
	c.terms,
	c.promotion_id,
	c.property_id,
	c.create_at,
	cd.`name` AS departament_name,
	cd.type
FROM
	contact c
INNER JOIN contact_department cd ON c.department_id = cd.id;

DROP VIEW
IF EXISTS view_users;

CREATE VIEW view_users AS SELECT
	u.id,
	u.username,
	u.email,
	u.`password`,
	u.role AS role_id,
	u.`status` AS status_id,
	u.username_canonical,
	u.email_canonical,
	u.last_login,
	u.password_requested_at,
	u.`name`,
	u.last_name,
	u.create_at,
	u.update_at,
	ut.description AS status_name,
	r.role,
	r.role_name
FROM
	USER u
INNER JOIN user_status ut ON u. STATUS = ut.id
INNER JOIN role r ON u.role = r.id;

DROP VIEW
IF EXISTS view_report_users;

CREATE VIEW view_report_users AS SELECT
	r.id,
	r.user_id,
	r.message_id,
	r.user_description,
	r.`comment`,
	r.create_at AS report_create,
	r.update_at AS report_update,
	u.username,
	u.email,
	u.`password`,
	u.role,
	u.`status`,
	u.username_canonical,
	u.email_canonical,
	u.last_login,
	u.password_requested_at,
	u.`name`,
	u.last_name,
	u.create_at AS user_create,
	u.update_at AS user_update
FROM
	report r
INNER JOIN `user` u ON r.user_id = u.id;
