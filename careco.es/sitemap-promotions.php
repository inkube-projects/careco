<?php 

require('core/core.php');



// Etiqueta inicial XML
$xml = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:xhtml="http://www.w3.org/1999/xhtml">';


   
$db = new Connection();
  
$s = "SELECT portfolio.*, pc.ubicacion FROM portfolio LEFT JOIN portfolio_category pc ON pc.id=portfolio.category_id group BY portfolio.category_id" ;

$arr_sql = $db->fetchSQL($s);


foreach ($arr_sql as $val) {
	
	  $linkUrl='https://www.careco.es/nuestras-obras/'.createSlug($val['ubicacion'])."/".createSlug($val['title'])."/".$val['category_id'];
      $xml .= '
	  <url>
      <loc>'.$linkUrl.'</loc>
	  </url>';


}

$xml.="</urlset>";
  
 //hay que indicarle que tipo de archivo le estamos pasando
//para el robot esto es como si fuera una archivo xml fisico

header('Content-type:text/xml; charset:utf8');
echo $xml;



?>