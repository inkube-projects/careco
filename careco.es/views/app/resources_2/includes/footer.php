<section class="css-newsletter">
   <div class="container">
      <h2 class="css-marginB0 css-medium">
         <i class="fa fa-envelope css-fontSize22" style="font-weight: 900;"></i><br>
         Suscíbete a nuestra Newsletter
      </h2>
      <p class="css-fontSize14">Siempre estarás informado de todo lo que ocurre en Careco <br> (Noticias, Novedades, Consejos, Promociones, etc...)</p>

      <div class="input-group css-newsletter-container">
         <input type="text" class="form-control" placeholder="Introduce tu e-mail">

         <div class="input-group-btn">
            <button type="button" class="btn btn-default">Envíar</button>
         </div>
      </div>
   </div>
</section>

<section class="css-info">
   <div class="container wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
      <h2 class="text-center css-medium css-fontSize20 css-font-inherit css-text-black">Certificaciones y premios</h2>
      <img src="images/certifications.jpg" class="img-responsive css-noFloat center-block" alt="">
   </div>
</section>

<footer id="footer">
   <div class="footer-top wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
      <div class="container text-center">
         <h2 class="css-text-black css-semibold css-fontSize30 css-font-inherit">CARECO</h2>
         <p class="css-text-black">Copyright&copy; 2018 CARECO S.A. Carrer del Mar, 44  <br> 46003 Vléncia, Valencia</p>

         <div class="col-md-6 text-center css-noFloat center-block">
            <a href="#"><i class="fab fa-facebook-square"></i></a>
            <a href="#"><i class="fab fa-youtube"></i></a>
            <a href="#"><i class="fab fa-twitter-square"></i></a>
            <a href="#"><i class="fab fa-pinterest"></i></a>
            <a href="#"><i class="fab fa-linkedin"></i></a>
         </div>

         <div class="css-links-footer">
            <a href="current-promotion-sales.php">Promociones en curso</a> |
            <a href="next-promotions.php">Próximas promociones</a> |
            <a href="#">Careco</a> |
            <a href="#">Blog</a> |
            <a href="contact.php">Atención al cliente</a> |
            <a href="#">Legal</a>
         </div>
      </div>
   </div>
</footer>
