<nav class="css-nav-top">
   <div class="container">
      <div class="col-md-6">
         <i class="fas fa-phone"></i> +34 687 715 531
         <i class="fas fa-envelope css-marginL30"></i> careco@careco.es
      </div>

      <div class="col-md-6 text-right">
         <a href="#"><i class="fab fa-facebook-square"></i></a>
         <a href="#"><i class="fab fa-youtube"></i></a>
         <a href="#"><i class="fab fa-twitter-square"></i></a>
         <a href="#"><i class="fab fa-pinterest"></i></a>
         <a href="#"><i class="fab fa-linkedin"></i></a>
      </div>
   </div>
</nav>

<nav class="navbar navbar-default">
   <div class="container">
      <div class="container-fluid">
         <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">
               <img src="images/logo-high-n-01.png" alt="CARECO" width="120">
            </a>
         </div>

         <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
               <li>
                  <a href="current-promotion-sales.php">Promociones un curso <div class="css-nav-separator"></div></a>
               </li>
               <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Próximas promociones <div class="css-nav-separator"></div></a>
                  <ul class="dropdown-menu">
                     <li><a href="next-promotions.php"><i class="fa fa-chevron-right"></i> Avd. Francia (Alameda)</a></li>
                     <li><a href="next-promotions.php"><i class="fa fa-chevron-right"></i> Avd. Francia (Bolinches)</a></li>
                     <li><a href="next-promotions.php"><i class="fa fa-chevron-right"></i> Rocafort</a></li>
                     <li><a href="next-promotions.php"><i class="fa fa-chevron-right"></i> Náquera</a></li>
                     <li><a href="next-promotions.php"><i class="fa fa-chevron-right"></i> Picanya</a></li>
                  </ul>
               </li>
               <li><a href="about-us.php">Careco <div class="css-nav-separator"></div></a></li>
               <li><a href="#">Blog <div class="css-nav-separator"></div></a></li>
               <li><a href="contact.php">Atención al cliente <div class="css-nav-separator"></div></a></li>
            </ul>
         </div>
      </div>
   </div>
</nav>
