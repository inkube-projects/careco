// jQuery(function(e){
//    jQuery.validator.addMethod("noSpecialCharacters", function(value, element, arg){
//       var response = true;
//       if (value.match(arg)) {
//          response = false;
//       }
//       return response;
//    }, "No special characters allowed");
//
//    jQuery(".frm-promo").validate({
//       rules:{
//          "promo_email":{
//             required: true,
//             email: true
//          },
//          "promo_phone":{
//             required: true,
//             number: true,
//             rangelength: [9, 9]
//          },
//       },
//       messages: {
//          "promo_email":{
//             required: "Debes ingresar tu email",
//             email: "Debes ingresar un email válido"
//          },
//          "promo_phone":{
//             required: "Debes ingresar un número telefónico",
//             number: "Solo se permiten números sin signos ni espacios",
//             rangelength: "Debes ingresra un número telefónico válido"
//          },
//       },
//       errorPlacement: function (error, element) {
//          var name = jQuery(element).attr("id");
//          alert(name);
//          error.appendTo(jQuery("#" + name + "_validate"));
//       },
//       submitHandler: function(form) {
//          jQuery(".js-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Enviando...');
//          alert('aaa');
//          // persistMessage();
//          return false;
//       }
//    });
// });

function persistMessage(ID) {
   var i = jQuery("#frm-promo-" + ID).serialize();

   jQuery.ajax({
      type: 'POST',
      url: 'ajax.php?m=contact&acc=3&p=' + ID,
      data: i,
      dataType: 'json',
      success: function(r){
         if (r.status == 'OK') {
            jQuery(".contact-message").html("Su solicitud ha sido enviado correctamente");
         } else {
            jQuery(".contact-message").html(r.message);
         }

         jQuery("#mod-contact").modal();
         jQuery(".js-submit").attr("disabled", false).html('ESTOY INTERESADO');
      }
   });
}
