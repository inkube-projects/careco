jQuery(function(e){
   jQuery.validator.addMethod("noSpecialCharacters", function(value, element, arg){
      var response = true;
      if (value.match(arg)) {
         response = false;
      }
      return response;
   }, "No special characters allowed");

   jQuery("#frm-contact").validate({
      rules:{
          name:{
             required: true,
          },
          lastName:{
             required: true,
          },
         email:{
            required: true,
            email: true
         },
         phone:{
            required: true,
            number: true,
            rangelength: [9, 9]
         },
      },
      messages: {
          name:{
             required: "Debes ingresar nombre",
          },
          lastName:{
             required: "Debes ingresar apellido",
          },
         email:{
            required: "Debes ingresar email",
            email: "Debes ingresar un email válido"
         },
         phone:{
            required: "Debes ingresar un número telefónico",
            number: "Solo se permiten números sin signos ni espacios",
            rangelength: "Debes ingresra un número telefónico válido"
         },
      },
      errorPlacement: function (error, element) {
         var name = jQuery(element).attr("name");
         error.appendTo(jQuery("#" + name + "_validate"));
      },
      submitHandler: function(form) {
         jQuery(".js-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Enviando...');

         persistMessage();
         return false;
      }
   });
});

function persistMessage() {
   var i = jQuery("#frm-contact").serialize();

   jQuery.ajax({
      type: 'POST',
      url: 'ajax.php?m=contact&acc=2&p=' + jQuery("#key").data('id'),
      data: i,
      dataType: 'json',
      success: function(r){
         if (r.status == 'OK') {
            jQuery(".contact-message").html("Su solicitud ha sido enviado correctamente");
         } else {
            jQuery(".contact-message").html(r.message);
         }

         jQuery(".js-submit").attr("disabled", false).html('ESTOY INTERESADO');
      }

   });
}
