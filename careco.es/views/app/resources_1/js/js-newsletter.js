jQuery(function(e){
   jQuery.validator.addMethod("noSpecialCharacters", function(value, element, arg){
      var response = true;
      if (value.match(arg)) {
         response = false;
      }
      return response;
   }, "No special characters allowed");

   jQuery("#frm-newsletter").validate({
      rules:{
         newsletter_email:{
            required: true,
            email: true
         }
      },
      messages: {
         newsletter_email:{
            required: "Debes agregar un e-mail",
            email: "Debes agregar un a-mail válido"
         }
      },
      errorPlacement: function (error, element) {
         var name = jQuery(element).attr("name");
         error.appendTo(jQuery("#" + name + "_validate"));
      },
      submitHandler: function(form) {
         jQuery(".js-submit").attr("disabled", true).html('Cargando...');

         persistNewsletter();
         return false;
      }
   });
});

function persistNewsletter() {
   var i = jQuery("#frm-newsletter").serialize();

   jQuery.ajax({
      type: 'POST',
      url: 'ajax.php?m=newsletter',
      data: i,
      dataType: 'json',
      success: function(r){
         if (r.status == 'OK') {
            jQuery(".newsletter-message").html("El e-mail se ha guardado correctamente");
         } else {
            jQuery(".newsletter-message").html(r.message);
         }

         jQuery("#mod-newsletter").modal();
         jQuery(".js-submit").attr("disabled", false).html('Guardar');
      }

   });
}
