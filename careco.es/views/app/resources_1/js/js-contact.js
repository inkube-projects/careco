jQuery(function(e){
   jQuery.validator.addMethod("noSpecialCharacters", function(value, element, arg){
      var response = true;
      if (value.match(arg)) {
         response = false;
      }
      return response;
   }, "No special characters allowed");

   jQuery("#frm-contact").validate({
      rules:{
         contact_name:{
            required: true,
            noSpecialCharacters: /([*+^${}><|\[\]\/\\])/g
         },
         contact_last_name:{
            required: true,
            noSpecialCharacters: /([*+^${}><|\[\]\/\\])/g
         },
         contact_phone:{
            required: true,
            number: true,
            rangelength: [4, 9]
         },
         contact_email:{
            required: true,
            email: true
         },
         contact_message:{
            required: true,
         },
         regime:{
            required: true
         },
         temrs: {
            required: true
         }
      },
      messages: {
         contact_name:{
            required: "Debes ingresar tu nombre",
            noSpecialCharacters: "No se permiten caracteres especiales"
         },
         contact_last_name:{
            required: "Debes ingresar tu apellido",
            noSpecialCharacters: "No se permiten caracteres especiales"
         },
         contact_phone:{
            required: "Debes ingresar un número telefónico",
            number: "Solo se permiten números sin signos ni espacios",
            rangelength: "Debes ingresar un número telefónico válido"
         },
         contact_email:{
            required: "Debes ingresar tu email",
            email: "Debes ingresar un email válido"
         },
         contact_message:{
            required: "Debes ingresar tu mensaje",
         },
         regime:{
            required: "Debes seleccionar un departamento"
         },
         temrs: {
            required: "Debes aceptar los términos expuestos"
         }
      },
      errorPlacement: function (error, element) {
         var name = jQuery(element).attr("name");
         error.appendTo(jQuery("#" + name + "_validate"));
      },
      submitHandler: function(form) {
         jQuery(".js-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Enviando...');

         persistMessage();
         return false;
      }
   });
});

function persistMessage() {
   var i = jQuery("#frm-contact").serialize();

   jQuery.ajax({
      type: 'POST',
      url: 'ajax.php?m=contact&acc=1',
      data: i,
      dataType: 'json',
      success: function(r){
         if (r.status == 'OK') {
            jQuery(".contact-message").html("Su mensaje ha sido enviado correctamente");
         } else {
            jQuery(".contact-message").html(r.message);
         }

         jQuery("#mod-contact").modal();
         jQuery(".js-submit").attr("disabled", false).html('Enviar');
      }

   });
}
