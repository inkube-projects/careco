<?php
   require('core/core.php');
   
   if (isset($_GET['view'])) {
      if (file_exists("core/controller/".$_GET['view']."Controller.php")) {
         include("core/controller/".$_GET['view']."Controller.php");
      } else {
         include("core/controller/notFoundController.php");
      }
   } else {
      include("core/controller/indexController.php");
   }

?>
