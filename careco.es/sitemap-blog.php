<?php 

require('core/core.php');



// Etiqueta inicial XML
$xml = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:xhtml="http://www.w3.org/1999/xhtml">';


   
$db = new Connection();
  
 
$s = "SELECT * FROM blog_posts_tags WHERE NOT p_id = ''";
$s.= "  ORDER BY p_id DESC ";

$arr_sql = $db->fetchSQL($s);


foreach ($arr_sql as $val) {
	
	  $linkUrl='https://www.careco.es/blog/'.createSlug($val['p_title']).'/'.$val['p_id'];
      $xml .= '
	  <url>
      <loc>'.$linkUrl.'</loc>
	  </url>';


}

$xml.="</urlset>";
  
 //hay que indicarle que tipo de archivo le estamos pasando
//para el robot esto es como si fuera una archivo xml fisico

header('Content-type:text/xml; charset:utf8');
echo $xml;



?>