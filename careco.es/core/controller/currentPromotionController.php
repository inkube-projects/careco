<?php

$db = new Connection();

// se verifica si se envía una categoría de propiedad sino se le asigna el valor 1 [1: venta, 2: alquiler]
if (isset($_GET['c'])) {
   $cat = @number_format($_GET['c'],0,"","");

   if ($cat == 0 || $cat == "") {
      $cat = 1;
   }
} else {
   $cat = 1;
}

$cat_name = "Venta";

if ($cat_name == 2) {
   $cat_name = "Alquiler";
}

// Se verifica si se envía un id de promocion
if (isset($_GET['p'])) {
   $id = @number_format($_GET['p'],0,"","");
   $cnt_val = $db->getCount("promotions", "id='".$id."'");

   if ($cnt_val == 0) {
      $ini_promo = "SELECT * FROM promotions WHERE category='1' ORDER BY id ASC LIMIT 0,1";
   } else {
      $ini_promo = "SELECT * FROM promotions WHERE id='".$id."'";
   }
} else {
   $ini_promo = "SELECT * FROM promotions WHERE category='1' ORDER BY id ASC LIMIT 0,1";
}

$arr_sql = $db->fetchSQL($ini_promo);

if ($arr_sql) {
   $image = APP_IMG_NO_IMAGES."promo_no_image.jpg";

   if ($arr_sql[0]['image']) {
      $image = APP_IMG_PROMOTIONS."cover/".$arr_sql[0]['image'];
   }

   $promo_id = $arr_sql[0]['id'];
   $title = $arr_sql[0]['title'];
   $location = $arr_sql[0]['location'];
   $latitude = $arr_sql[0]['latitude'];
   $longitude = $arr_sql[0]['longitude'];

   // Se carga el resto de las promociones
   $s = "SELECT * FROM promotions WHERE NOT id = '".$promo_id."' AND category='1'";
   $arr_sql = $db->fetchSQL($s);
   $arr_promotions = array();

   foreach ($arr_sql as $val) {
      $arr_promotions[] = array(
         'id' => $val['id'],
         'title' => $val['title']
      );
   }

   // Se seleccionan las propiedades correspondientes
   if ($cat == 1) {
      $s = "SELECT * FROM properties WHERE promotion_id='".$promo_id."' AND cost <> '' AND status='1' ORDER BY id DESC";
   } else {
      $s = "SELECT * FROM properties WHERE promotion_id='".$promo_id."' AND monthly_cost <> '' AND status='1' ORDER BY id DESC";
   }

   $arr_sql = $db->fetchSQL($s);
   $arr_property = array();

   foreach ($arr_sql as $val) {
      // Se verifica si es una propiedad destacada
      $star = "";
      if ($val['outstanding'] == 1) {
         $star = '<i class="fas fa-star css-text-yellow css-fontSize20"></i>';
      }

      // seleccionar los iconos de cada propiedad
      $S_icon = "SELECT i.image, p.description FROM properties_descriptions p INNER JOIN icons i ON p.icon_id = i.id WHERE p.propertie_id = '".$val['id']."'";
      $arr_icons = $db->fetchSQL($S_icon);

      // Imágenes de galería
      $s_gal = "SELECT * FROM properties_images_gallery WHERE propertie_id='".$val['id']."'";
      $arr_gallery = $db->fetchSQL($s_gal);

      // if ($val['cost']) {
      //    $cost = $val['cost']." €";
      // } else {
      //    $cost = $val['monthly_cost']." € / mes";
      // }

      if ($cat == 1) {
         $cost = $val['cost']." €";
      } else {
         $cost = $val['monthly_cost']." € / mes";
      }

      $arr_property[] = array(
         'id' => $val['id'],
         'title' => $val['cover_title'],
         'cost' => $cost,
         'description' => substr($val['description'], 0, 380),
         'icons' => $arr_icons,
         'gallery' => $arr_gallery,
         'code' => $val['code'],
         'sold' => $val['sold'],
         'star' => $star
      );
   }
} else {
   $title = "";
   $arr_promotions = array();
   $arr_property = array();
}

include('html/promotion/current-promotion.php');

?>
