<?php

$db = new Connection();
$tag = @number_format($_GET['t'],0,"","");
$src = (isset($_GET['src_search']) && (!empty($_GET['src_search']))) ? sanitize($_GET['src_search']) : "" ;


$s = "SELECT count(p_id) AS total FROM blog_posts_tags WHERE NOT p_id = ''";

if (($tag != "") && ($tag != 0)) {
   $s .= " AND pt_tag_id='".$tag."'";
}
if ($src != "") {
   $s .= " AND p_title LIKE '%".$src."%'";
}

$s.= "  ORDER BY p_id DESC";
$arr_sql = $db->fetchSQL($s);

$num_total_registros = $arr_sql[0]['total'];
$tamano_pagina=12;
$pagina = (isset($_REQUEST["pagina"])) ? intval($_REQUEST["pagina"]) : 1;
if (!$pagina) { $inicio = 0; $pagina=1; }
else { $inicio = ($pagina - 1) * $tamano_pagina;}
$total_paginas = ceil($num_total_registros / $tamano_pagina);

//Limitamos resultados
$s = "SELECT * FROM blog_posts_tags WHERE NOT p_id = ''";

if ( ($tag != "") && ($tag != 0) ) {
	
	$s = "SELECT post.image AS p_image, post.id AS p_id, post.create_at AS p_create_at, post.title AS p_title, post.description AS p_description, pt.tag_id AS pt_tag_id  FROM post  INNER JOIN post_tags pt ON pt.post_id=post.id AND pt.tag_id=".$tag." WHERE NOT post.id = ''";

}

// busqueda por múltiples términos
if ($src != "") {
$arrWords=explode(" ", rawurldecode($src));
$nWords=count($arrWords);
$s .= " AND ( ";
for ($ws=0; $ws<$nWords; $ws++ ){
	if($ws>0) $s .= " OR ";
	$s .= " p_title LIKE '%".$arrWords[$ws]."%' OR p_description LIKE '%".$arrWords[$ws]."%' ";
	}
	
	$s .= " ) ";
	
}

$s.= "  ORDER BY p_id DESC LIMIT " . $inicio . "," . $tamano_pagina;

$arr_sql = $db->fetchSQL($s);


$arr_post = array();

foreach ($arr_sql as $val) {
   $date = datetime_format($val['p_create_at']);
   // $s_2 = "SELECT * FROM post_tags WHERE post_id='".$val['id']."' LIMIT 0, 1";
   // $arr_tag = $db->fetchSQL($s_2);

   $image = APP_IMG_NO_IMAGES."promo_no_image.jpg";

   if ($val['p_image']) {
      $image = APP_IMG_POST.$val['p_image'];
   }

   $arr_post[] = array(
      'id' => $val['p_id'],
      'title' => $val['p_title'],
      'description' => substr(sanitize($val['p_description']),0,300),
      'date' => $date['date'],
      'category' => $db->getValue("name", "blog_tags", "id='".$val['pt_tag_id']."'"),
      'image' => $image
   );
}

$s = "SELECT * FROM blog_tags";
$arr_tags = $db->fetchSQL($s);

// showArr($arr_post); exit;

include('html/blog/blog-list.php');

?>
