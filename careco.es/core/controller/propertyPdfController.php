<?php

$db = new Connection();

$id = @number_format($_GET['id'],0,"","");
$cnt_val = $db->getCount("properties", "id='".$id."'");

if ($cnt_val == 0) {
   header('location: index.php?view=nextPromotion'); exit;
}

/**
 * Clase para construir el pdf
 */
class PDF extends FPDF
{
    public $db;

    public $id;

    function __construct($db, $id)
    {
        parent::__construct();
        $this->id = $id;
        $this->db = $db;
    }

    /**
     * Cabecera
     * @return void
     */
    function Header()
    {
        $property = $this->getProperty();
        $this->SetTitle(mb_convert_case($property['cover_title'], MB_CASE_TITLE, "UTF-8"));

        // Logo
        $this->Image('views/images/logo-high-n-01.png',160,8,33);
        $this->Ln(10);
    }

    /**
     * Título
     * @param string $title Título de la pantalla pdf
     */
    function ChapterTitle($title)
    {
        $this->SetFont('Arial','B',11);
        $this->Cell(87);
        $this->Cell(30,0,mb_convert_case(utf8_decode($title), MB_CASE_TITLE, "UTF-8"),0,0,'R');
        // $this->MultiCell(30,0,mb_convert_case(utf8_decode($title), MB_CASE_TITLE, "UTF-8"), 0, 'R');
        $this->Ln(10);
    }

    /**
     * Cuerpo del PDF
     * @param array $property Arreglo de la propiedad
     */
    function ChapterBody($property)
    {
        $db = $this->db;
        $gallery_icons = "views/images/icons/";
        $gallery_path = "views/images/properties/property_".$property['id']."/images/gallery/";
        $desc = utf8_decode(sanitize($property['description']));
        $banner = $db->getValue("image", "properties_images_gallery", "propertie_id='".$property['id']."'");

        // Precios
        $this->SetFont('Arial','B',13);
        if ($property['monthly_cost']) { $this->Ln(10); $this->Cell(30,0,"Alquiler: ".$property['monthly_cost']." ".chr(128)."/MES"); }
        if ($property['cost']) { $this->Ln(10); $this->Cell(30,0,"Venta: ".$property['cost']." ".chr(128)); }

        // Descripcion
        $this->SetFont('Arial','',10);
        $this->Ln(10);
        $this->MultiCell(100, 5, $desc, 0, 'J');
        $this->Ln(10);

        // iconos
        $icons = $this->getIconDescription($property['id']);
        $cnt_x = 11;

        $after_desc_y = $this->GetY();
        foreach ($icons as $val) {
            if ($val['image']) {
                $this->Cell(90,0,$this->Image($gallery_icons.$val['image'],$cnt_x,$this->GetY(),6),$this->GetX(),$this->GetY(),90);
                // $this->Image($gallery_icons.$val['image'],$cnt_x,88,6);
                $cnt_x += 6;
            }
        }

        $this->SetFont('Arial','B',13);
        $this->SetY(37);
        $this->SetX(120);
        $this->MultiCell(120, 5, "SUPERFICIE", 0, 'J');

        // planos
        $blue_prints_path = "views/images/properties/property_".$property['id']."/images/";
        $this->Image($blue_prints_path.$property['blueprints'],152,37,45);

        // descripciones
        $this->SetFont('Arial','',9);

        foreach ($icons as $val) {
            $this->Ln(5);
            $this->SetX(119);
            $this->Cell(30,0,utf8_decode($val['description_2']." ".$val['description']));
        }

        $gallery = $this->getGalleryImage(0, 4);

        $this->Ln(5);
        $cnt = 0;
        $cnt_y = 120;
        // $this->AddPage();
        $total_images = count($gallery);
        $after_desc_y += 10;

        foreach ($gallery as $val) {
            $cnt++;

            if ($cnt == 1) {
                // showArr($after_desc_y); exit;
                $this->Cell(90,0,$this->Image($gallery_path.$val['image'],10,$after_desc_y,90),$this->GetX(),$after_desc_y,90);
                // $this->Image($gallery_path.$val['image'],10,$cnt_y,90);
            } elseif ($cnt == 2) {
                // $this->Image($gallery_path.$val['image'],107,$cnt_y,90);
                $this->Cell(90,0,$this->Image($gallery_path.$val['image'],10,$after_desc_y,90),$this->GetX(),$after_desc_y,90);
                $this->Ln(5);
                $cnt = 0;
                // $cnt_y += 80;
                $after_desc_y += 80;
            }

        }

        if ($total_images > 2) {
            $this->AddPage();

            $gallery = $this->getGalleryImage(4, 100);
            $cnt_y = 20;
            $cnt_images = 0;

            foreach ($gallery as $val) {
                $cnt++;
                $cnt_images++;

                if ($cnt == 1) {
                    $this->Image($gallery_path.$val['image'],10,$cnt_y,90);
                } elseif ($cnt == 2) {
                    $this->Image($gallery_path.$val['image'],107,$cnt_y,90);
                    $this->Ln(5);
                    $cnt = 0;
                    $cnt_y += 80;
                }

                if ($cnt_images == 6) {
                    $this->AddPage();
                    $cnt_images = 0;
                    $cnt_y = 40;
                }
            }
        }
    }

    /**
     * Pié de página
     * @return void
     */
    function Footer()
    {
        // Posición: a 1,5 cm del final
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Número de página
        $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().'/{nb}',0,0,'C');
    }

    /**
     * Obtiene la información de la propiedad
     * @return array
     */
    public function getProperty()
    {
        $db = $this->db;

        $s = "SELECT * FROM properties WHERE id='".$this->id."'";
        $arr_property = $db->fetchSQL($s);

        return $arr_property[0];
    }

    /**
     * Retorna los iconos de la propiedad
     * @param  integer $property_id [description]
     * @return array
     */
    public function getIconDescription($property_id)
    {
        $db = $this->db;
        $s = "SELECT i.image AS image, i.description AS description, pd.description AS description_2 FROM properties_descriptions pd INNER JOIN icons i ON pd.icon_id = i.id WHERE pd.propertie_id = '".$property_id."'";
        $arr_icons = $db->fetchSQL($s);

        return $arr_icons;
    }

    /**
     * Retorna las imagenes de galería
     * @return array
     */
    public function getGalleryImage($limit_i, $limit_f)
    {
        $db = $this->db;

        $s = "SELECT * FROM properties_images_gallery WHERE propertie_id='".$this->id."' LIMIT ".$limit_i.", ".$limit_f;
        $arr_gallery = $db->fetchSQL($s);

        return $arr_gallery;
    }
}

// Creación del objeto de la clase heredada
$pdf = new PDF($db, $id);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',12);

$arr_porperty = $pdf->getProperty();

$pdf->ChapterTitle($arr_porperty['title']);
$pdf->ChapterBody($arr_porperty);

$pdf->Output();

?>
