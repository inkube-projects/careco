<?php

$db = new Connection();

$s = "SELECT * FROM promotions WHERE category='2'";
$arr_sql = $db->fetchSQL($s);
$arr_promotion = array();

foreach ($arr_sql as $val) {
   // Imágenes de galería
   $s_gal = "SELECT * FROM promotions_images_gallery WHERE promotion_id='".$val['id']."'";
   $arr_gallery = $db->fetchSQL($s_gal);

   $arr_promotion[] = array(
      'id' => $val['id'],
      'title' => $val['title'],
      'description' => $val['description'],
      'location' => $val['location'],
      'latitude' => $val['latitude'],
      'longitude' => $val['longitude'],
      'gallery' => $arr_gallery,
      'image' => $val['image']
   );
}


if ($arr_promotion) {
   $rand = array_rand($arr_promotion);
   $image = APP_IMG_NO_IMAGES."promo_no_image.jpg";
   
   if ($arr_promotion[$rand]['image']) {
      $image = APP_IMG_PROMOTIONS.'cover/'.$arr_promotion[$rand]['image'];
   }
}

include('html/promotion/next-promotion.php');

?>
