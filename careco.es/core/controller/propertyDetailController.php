<?php

$db = new Connection();

$id = @number_format($_GET['id'],0,"","");
$cnt_val = $db->getCount("properties", "id='".$id."'");
$gallery_path = APP_IMG_PROPERTY."property_".$id."/images/gallery/";
$blue_prints_path = APP_IMG_PROPERTY."property_".$id."/images/";
$files_path = APP_IMG_PROPERTY."property_".$id."/PDF/";

if ($cnt_val == 0) {
   header('location: index.php?view=nextPromotion'); exit;
}

$s = "SELECT * FROM properties WHERE id='".$id."'";
$arr_property = $db->fetchSQL($s);

$s = "SELECT * FROM properties_images_gallery WHERE propertie_id='".$id."'";
$arr_gallery = $db->fetchSQL($s);

$s = "SELECT * FROM promotions WHERE id='".$arr_property[0]['promotion_id']."'";
$arr_promotion = $db->fetchSQL($s);

$category = ($arr_property == 1) ? "VENTA" : "ALQUILER";
$promotion = $db->getValue("title", "promotions", "id='".$arr_property[0]['promotion_id']."'");

$cost = "Contactar para mayor información";

if (($arr_property[0]['cost']) && ($arr_property[0]['cost'] != '')) {
   $cost = $arr_property[0]['cost']." &euro;";
} elseif (($arr_property[0]['monthly_cost']) && ($arr_property[0]['monthly_cost'] != "")) {
   $cost = $arr_property[0]['monthly_cost']." &euro; / Mes";
}

$s = "SELECT i.image AS image, i.description AS description, pd.description AS description_2 FROM properties_descriptions pd INNER JOIN icons i ON pd.icon_id = i.id WHERE pd.propertie_id = '".$id."'";
$arr_icons = $db->fetchSQL($s);

// showArr($arr_icons); exit;

include('html/property/property-detail.php');

?>
