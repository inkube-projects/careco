<?php

$db = new Connection();

$s = "SELECT * FROM portfolio_category";
$arr_category = $db->fetchSQL($s);

$s = "SELECT portfolio.*, pc.ubicacion FROM portfolio LEFT JOIN portfolio_category pc ON pc.id=portfolio.category_id";
$arr_portfolio = $db->fetchSQL($s);

$str_category = "";

foreach ($arr_category as $key => $val) {
   if ($key == 0) {
      $str_category .= "cat-".$val['id'];
   } else {
      $str_category .= ",cat-".$val['id'];
   }
}

$arr_class[0] = array('attachment-wide_tall size-wide_tall', 'wide_tall');
$arr_class[1] = array('attachment-tall size-tall', 'tall');
$arr_class[2] = array('attachment-regular size-regular', 'regular');
$arr_class[3] = array('attachment-regular size-regular', 'regular');
$arr_class[4] = array('attachment-wide size-wide', 'wide');
$arr_class[5] = array('attachment-wide size-wide', 'wide');
$arr_class[6] = array('attachment-tall size-tall', 'tall');
$arr_class[7] = array('attachment-regular size-regular', 'regular');
$arr_class[8] = array('attachment-wide size-wide', 'wide');
$arr_class[9] = array('attachment-tall size-tall', 'tall');
$arr_class[10] = array('attachment-wide size-wide', 'wide');
$arr_class[11] = array('attachment-tall size-tall', 'tall');
$arr_class[12] = array('attachment-wide size-wide', 'wide');
$arr_class[13] = array('attachment-regular size-regular', 'regular');
$arr_class[14] = array('attachment-regular size-regular', 'regular');
$arr_class[15] = array('attachment-tall size-tall', 'tall');
$arr_class[16] = array('attachment-regular size-regular', 'regular');
$arr_class[17] = array('attachment-tall size-tall', 'tall');
$arr_class[18] = array('attachment-regular size-regular', 'regular');
$arr_class[19] = array('attachment-tall size-tall', 'tall');
$arr_class[20] = array('attachment-tall size-tall', 'tall');
$arr_class[21] = array('attachment-wide_tall size-wide_tall', 'wide_tall');

include('html/about/our-works.php');
?>
