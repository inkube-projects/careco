<?php

$db = new Connection();

// Promociones en curso
$s = "SELECT * FROM promotions WHERE category='1' ORDER BY id DESC LIMIT 0, 2";
$arr_promotions = $db->fetchSQL($s);
$arr_current_promo = array();

foreach ($arr_promotions as $val) {
   $image = APP_IMG_NO_IMAGES."min_promo_no_image.jpg";

   if ($val['image']) {
      $image = APP_IMG_PROMOTIONS."cover/min_".$val['image'];
   }

   $arr_current_promo[] = array(
      'title' => $val['title'],
      'desc' => $val['description'],
	  'id' => $val['id'],
      'image' => $image
   );
}

// Proximas promociones
$s = "SELECT * FROM promotions WHERE category='2' AND destacado='1' ORDER BY id DESC LIMIT 0, 2";
$arr_promotions = $db->fetchSQL($s);
$arr_next_promo = array();

foreach ($arr_promotions as $val) {
   $image = APP_IMG_NO_IMAGES."min_promo_no_image.jpg";

   if ($val['image']) {
      $image = APP_IMG_PROMOTIONS."cover/min_".$val['image'];
   }

   $arr_next_promo[] = array(
   	   'id' => $val['id'],
      'title' => $val['title'],
      'desc' => $val['description'],
      'image' => $image
   );
}

// Opiniones
$s = "SELECT * FROM opinions";
$arr_sql = $db->fetchSQL($s);
$arr_opinions = array();

$cnt_item = ceil(count($arr_sql) / 3);

foreach ($arr_sql as $key => $val) {
   $image = APP_IMG_NO_IMAGES."min_promo_no_image.jpg";

   if ($val['image']) {
      $image = APP_IMG_OPINIONS."profile/".$val['image'];
   }

   $arr_opinions[] = array(
      'name' => mb_convert_case($val['name'], MB_CASE_TITLE, "UTF-8")." ".mb_convert_case($val['last_name'], MB_CASE_TITLE, "UTF-8"),
      'company' => $val['company'],
      'desc' => $val['description'],
      'image' => $image,
      'item' => ceil(($key + 1) / 3)
   );
}

// Entradas del blog
$s = "SELECT * FROM post ORDER BY id DESC LIMIT 0,4";
$arr_sql = $db->fetchSQL($s);
$arr_post = array();


foreach ($arr_sql as $val) {
   $date = datetime_format($val['create_at']);
   $image = APP_IMG_NO_IMAGES."blog_no_image.jpg";

   if ($val['image']) {
      $image = APP_IMG_POST.$val['image'];
   }

   $arr_post[] = array(
      'id' => $val['id'],
      'title' => $val['title'],
      'description' => substr(sanitize($val['description']),0,300),
      'date' => $date['date'],
      'image' => $image
   );
}

$s = "SELECT * FROM contact_department WHERE type = '1'";
$arr_departaments = $db->fetchSQL($s);

include('html/index/index.php');

?>
