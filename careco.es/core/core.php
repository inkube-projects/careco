<?php
// nucleo del sitio web
// ini_set('display_errors', 0);
// ini_set('display_startup_errors', 0);
// error_reporting(null);

@session_start();

define('DB_USER', 'root');
define('DB_PASS', 'AJF_19466118');
define('DB_DSN', 'mysql:dbname=careco_dev_bd;host=localhost;charset=utf8');

// ---- Preproducción
// define('DB_USER', 'qzg711');
// define('DB_PASS', 'Careco00');
// define('DB_DSN', 'mysql:dbname=qzg711;host=qzg711.careco.es;charset=utf8');

// define("MAINURL", "https://www.careco.es/careco.es/");
define("MAINURL", "http://localhost/careco/careco.es/");
define('HTML_DIR', 'html/');
define('APP_TITLE', 'CARECO');
define('APP_COPY', '<p class="css-text-black">Copyright&copy; '.date('Y').' CARECO S.A. Carrer del Mar, 44  <br> 46003 Vléncia, Valencia</p>');
define('APP_IMG', MAINURL.'views/images/');
define('APP_IMG_NO_IMAGES', MAINURL.'views/images/no_images/');
define('APP_IMG_PROMOTIONS', MAINURL.'views/images/promotions/');
define('APP_IMG_PROPERTY', MAINURL.'views/images/properties/');
define('APP_IMG_OPINIONS', MAINURL.'views/images/opinion/');
define('APP_IMG_ICONS', MAINURL.'views/images/icons/');
define('APP_IMG_ABOUT_US', MAINURL.'views/images/about_us/');
define('APP_IMG_MILESTONES', MAINURL.'views/images/milestones/');
define('APP_IMG_PORTFOLIO', MAINURL.'views/images/our-works/');
define('APP_IMG_POST', MAINURL.'views/images/post/');

// Datos de email
define('EMAIL_HOST', 'smtpout.secureserver.net');
define('EMAIL_SENDER', 'pruebas@prontopublicidad.com');
define('EMAIL_PASSWORD', 'pronto123');

setlocale(LC_TIME, 'es_VE');
date_default_timezone_set('Europe/Madrid');

require('core/model/classConnection.php');
require('core/bin/functions/main-lib.php');
require('vendor/autoload.php');

?>
