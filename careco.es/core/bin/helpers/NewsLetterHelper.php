<?php

/**
 * Calse que se encarga de guardar los emails de newsletter
 */
class NewsLetterHelper
{

   public $db;

   function __construct($db)
   {
      $this->db = $db;
   }


   public function persistEmail()
   {
      $email = mb_convert_case($_POST['newsletter_email'], MB_CASE_LOWER, "UTF-8");

      $result = $this->db->insertAction("newsletter", array('email', 'create_at'), array($email, date('Y-m-d H:i:s')));

      return $result;
   }
}


?>
