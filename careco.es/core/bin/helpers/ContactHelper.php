<?php

/**
* Calse que se encarga de guardar los emails de newsletter
*/
class ContactHelper
{

    public $db;

    function __construct($db)
    {
        $this->db = $db;
        // $this->admin_id = $_SESSION['ADMIN_SESSION_CARECO']['id'];
        // $this->admin_username = $_SESSION['ADMIN_SESSION_CARECO']['username'];
    }

    /**
    * Guarda un mensaje de la sección de contacto
    * @return boolean
    */
    public function persistMessage()
    {
        $db = $this->db;

        $arr_fields = array(
            'email',
            'department_id',
            'name',
            'last_name',
            'message',
            'phone',
            'status',
            'terms',
            'create_at',
        );

        $arr_values = array(
            mb_convert_case($_POST['contact_email'], MB_CASE_LOWER, "UTF-8"),
            $_POST['regime'],
            $_POST['contact_name'],
            $_POST['contact_last_name'],
            sanitize($_POST['contact_message']),
            $_POST['contact_phone'],
            1,
            1,
            date('Y-m-d H:i:s'),
        );

        $result = $db->insertAction("contact", $arr_fields, $arr_values);
        $this->persistUser();
        $this->SelectAction();

        return true;
    }

    /**
    * Guarda el interes de un usuario por una propiedad
    * @return void
    */
    public function persistContactProperty()
    {
        $db = $this->db;

        $arr_fields = array(
            'name',
            'last_name',
            'email',
            'department_id',
            'message',
            'phone',
            'status',
            'terms',
            'property_id',
            'create_at',
        );

        $arr_values = array(
            $_POST['name'],
            $_POST['lastName'],
            mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            5,
            "Este usuario esta interesado en la propiedad: <b>".$db->getValue("title", "properties", "id='".$_GET['p']."'")."</b><br><br>Observación del usuario: <b>".$_POST['observations']."</b>",
            $_POST['phone'],
            1,
            1,
            $_GET['p'],
            date('Y-m-d H:i:s'),
        );

        $db->insertAction("contact", $arr_fields, $arr_values);
        $this->persistUserPromo();
        $email = mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8");
        $this->createFromMessage($db->getMaxValue('id', 'contact', "email='".$email."'"));

    }

    /**
    * Guarda el interes de un usuario por una promoción
    * @return void
    */
    public function persistContactPromo()
    {
        $db = $this->db;

        $arr_fields = array(
            'name',
            'last_name',
            'email',
            'department_id',
            'message',
            'phone',
            'status',
            'terms',
            'promotion_id',
            'create_at',
        );

        $arr_values = array(
            $_POST['promo_name'],
            $_POST['promo_lastName'],
            mb_convert_case($_POST['promo_email'], MB_CASE_LOWER, "UTF-8"),
            4,
            "Este usuario esta interesado en la promoción: <b>".$db->getValue("title", "promotions", "id='".$_GET['p']."'")."</b><br><br>Observación del usuario: <b>".$_POST['promo_observations']."</b>",
            $_POST['promo_phone'],
            1,
            1,
            $_GET['p'],
            date('Y-m-d H:i:s'),
        );

        $db->insertAction("contact", $arr_fields, $arr_values);
        $this->persistUserPromo();
        $email = mb_convert_case($_POST['promo_email'], MB_CASE_LOWER, "UTF-8");
        $this->createFromMessage($db->getMaxValue('id', 'contact', "email='".$email."'"));
    }

    /**
     * Guarda el usuario si no existe en la bd
     * @return boolean
     */
    public function persistUserPromo()
    {
        $db = $this->db;

        if (isset($_POST['email'])){
            $email = mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8");
        }elseif (isset($_POST['promo_email'])) {
            $email = mb_convert_case($_POST['promo_email'], MB_CASE_LOWER, "UTF-8");
        }else{
            $email= '';
        }
        $cnt_val = $db->getCount("user", "email='".$email."'");
        $username = 'user_'.uniqid(rand(),true);

        if ($cnt_val > 0) {
            return false;
        }

        $arr_fields = array(
            'username',
            'email',
            'password',
            'role',
            'status',
            'username_canonical',
            'email_canonical',
            'name',
            'last_name',
            'create_at',
            'update_at',
        );

        $arr_values = array(
            $username,
            $email,
            hashPass($email, 12),
            3,
            4,
            $username,
            $email,
            mb_convert_case($_POST['name'], MB_CASE_UPPER, "UTF-8"),
            mb_convert_case($_POST['lastName'], MB_CASE_UPPER, "UTF-8"),
            date('Y-m-d H:i:s'),
            date('Y-m-d H:i:s'),
        );

        $result = $db->insertAction("user", $arr_fields, $arr_values);

        return true;
    }

    /**
     * Guarda el usuario si no existe en la bd
     * @return boolean
     */
    public function persistUser()
    {
        $db = $this->db;
        $email = mb_convert_case($_POST['contact_email'], MB_CASE_LOWER, "UTF-8");
        $cnt_val = $db->getCount("user", "email='".$email."'");
        $username = 'user_'.uniqid(rand(),true);

        if ($cnt_val > 0) {
            return false;
        }

        $arr_fields = array(
            'username',
            'email',
            'password',
            'role',
            'status',
            'username_canonical',
            'email_canonical',
            'name',
            'last_name',
            'create_at',
            'update_at',
        );

        $arr_values = array(
            $username,
            $email,
            hashPass($email, 12),
            3,
            4,
            $username,
            $email,
            mb_convert_case($_POST['contact_name'], MB_CASE_UPPER, "UTF-8"),
            mb_convert_case($_POST['contact_last_name'], MB_CASE_UPPER, "UTF-8"),
            date('Y-m-d H:i:s'),
            date('Y-m-d H:i:s'),
        );

        $result = $db->insertAction("user", $arr_fields, $arr_values);

        return true;
    }

    /**
     * Obtener el último contacto
     * @return array
     */
    public function SelectAction()
    {
        $db = $this->db;
        $email= mb_convert_case($_POST['contact_email'], MB_CASE_LOWER, "UTF-8");
        $c= "SELECT MAX(id) as id FROM contact WHERE email= '".$email."'";
        $arr_contact = $db->fetchSQL($c);
        $this->createFromMessage($arr_contact[0]['id']);
    }

    /**
     * Se crea un reportes desde un mensaje
     * @param  integer $contact_id ID del mensaje de contacto
     * @return boolean
     */
    public function createFromMessage($contact_id)
    {
        $db = $this->db;
        $s = "SELECT * FROM contact WHERE id='".$contact_id."'";
        $arr_contact = $db->fetchSQL($s);
        $user_id = $db->getValue("id", "user", "email='".$arr_contact[0]['email']."'");

        $arr_fields = array(
            'user_id',
            'user_description',
            'message_id',
            'create_at',
            'update_at',
        );
        $arr_values = array(
            $user_id,
            $arr_contact[0]['message'],
            $contact_id,
            date('Y-m-d H:i:s'),
            date('Y-m-d H:i:s'),
        );

        if ($arr_contact[0]['department_id']) {
            array_push($arr_fields, "departament_id");
            array_push($arr_values, $arr_contact[0]['department_id']);
        }

        $report = $db->insertAction("report", $arr_fields, $arr_values);
        return true;
    }

    /**
     * Crea los logs que relacionados con las acciones a los módulos de los usuarios
     * @param string $desc Descripcion del log
     */
    protected function addLogs($desc)
    {
       $ip_acces = @$_SERVER['REMOTE_ADDR'];
       $ip_proxy = @$_SERVER['HTTP_X_FORWARDED_FOR'];
       $ip_compa = @$_SERVER['HTTP_CLIENT_IP'];

       $arr_fields = array(
          'user_id',
          'username',
          'action',
          'ip_access',
          'ip_proxy',
          'ip_company',
          'create_at',
       );

       $arr_values = array(
          $this->admin_id,
          $this->admin_username,
          $desc,
          $ip_acces,
          $ip_proxy,
          $ip_compa,
          date('Y-m-d H:i:s'),
       );

       $result = $this->db->insertAction("user_logs", $arr_fields, $arr_values);

       return true;
    }


}
?>
