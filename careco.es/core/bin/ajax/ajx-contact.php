<?php

include('core/bin/helpers/ContactHelper.php');

$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$acc = @number_format($_GET['acc'],0,"","");

if ($_POST) {
   $contactHelper = new ContactHelper($db);

   switch ($acc) {
      case 1: // Contacto
         $db->beginTransaction();
         try {
            isRequiredValuesPost($_POST, array('regime', 'contact_name', 'contact_last_name', 'contact_phone', 'contact_email', 'contact_message'));
            isValidString($_POST['contact_name']);
            isValidString($_POST['contact_last_name']);
            isValidPhoneNumber($_POST['contact_phone']);
            isValidEmail($_POST['contact_email']);

            $db->existRecord("id='".$_POST['regime']."'", 'contact_department', 'El departamento no existe');

            $contactHelper->persistMessage();

            $arr_response = array('status' => 'OK', 'message' => 'Se ha guardado correctamente');
            $db->commit();
         } catch (\Exception $e) {
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            $db->rollBack();
         }
      break;

      case 2: // Propiedades
         $property_id = @number_format($_GET['p'],0,"","");

         $db->beginTransaction();
         try {
            isRequiredValuesPost($_POST, array('email', 'phone'));
            $db->existRecord("id='".$property_id."'", "properties", "La propiedad no existe");
            isValidEmail($_POST['email']);
            isValidPhoneNumber($_POST['phone']);

            $contactHelper->persistContactProperty();
            $arr_response = array('status' => 'OK', 'message' => 'Se ha guardado correctamente');
            $db->commit();
         } catch (\Exception $e) {
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            $db->rollBack();

         }
      break;

      case 3: // Promociones
         $promo_id = @number_format($_GET['p'],0,"","");

         $db->beginTransaction();
         try {
            isRequiredValuesPost($_POST, array('promo_email', 'promo_phone'));
            $db->existRecord("id='".$promo_id."'", "promotions", "La promoción no existe");
            isValidEmail($_POST['promo_email']);
            isValidPhoneNumber($_POST['promo_phone']);

            $contactHelper->persistContactPromo();
            $arr_response = array('status' => 'OK', 'message' => 'Se ha guardado correctamente');
            $db->commit();
         } catch (\Exception $e) {
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            $db->rollBack();

         }
      break;
   }
}

//------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
