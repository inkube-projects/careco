<?php

include('core/bin/helpers/NewsLetterHelper.php');

$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');

if ($_POST) {

   $db->beginTransaction();
   try {
      $newsLetterHelper = new NewsLetterHelper($db);
      isRequiredValuesPost($_POST, array('newsletter_email'));
      isValidEmail($_POST['newsletter_email']);

      existEmail($_POST['newsletter_email']);

      $newsLetterHelper->persistEmail();

      $arr_response = array('status' => 'OK', 'message' => 'Se ha guardado correctamente');
      $db->commit();
   } catch (\Exception $e) {
      $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
      $db->rollBack();
   }
}

//------------------------------------------------------------------------------------------------------------

/**
 * Verifica si el email existe
 * @param  string $email Email a verificar
 * @return boolean
 */
function existEmail($email)
{
   $db = new Connection();
   $cnt_val = $db->getCount("newsletter", "email='".$_POST['newsletter_email']."'");

   if ($cnt_val > 0) {
      throw new Exception("El email ya se encuentra registrado", 1);
   }

   return true;
}

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
