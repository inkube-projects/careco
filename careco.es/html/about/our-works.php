<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?=APP_TITLE?> | CASA COMO TÚ :: NUESTRAS OBRAS </title>
        <link href="views/app/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="views/app/resources_1/css/animate.min.css" rel="stylesheet">
        <link href="views/app/plugins/font-awesome/web-fonts-with-css/css/fontawesome-all.min.css" rel="stylesheet">
        <link href="views/app/resources_1/css/lightbox.css" rel="stylesheet">
        <link href="views/app/resources_1/css/main.css" rel="stylesheet">
        <link href="views/app/resources_1/css/presets/preset1.css" id="css-preset" rel="stylesheet">
        <link href="views/app/resources_1/css/responsive.css" rel="stylesheet">
        <link rel="canonical" href="https://www.careco.es/nuestras-obras" />
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        <link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" href="views/images/favicon.ico">
        <link href="views/app/resources_1/css/css-style.css" rel="stylesheet">
        <?php include('html/overall/analytics.php') ?>
        <style>
            .carousel-fade .carousel-inner .item {
                background-position: center;
                height: 331px;
            }


            /* project EFECT hover*/
            .transition {
                -webkit-transition: all .3s;
                -moz-transition: all .3s;
                -o-transition: all .3s;
                transition: all .3s;
            }


            .basic-widgets {
                padding-top: 0px;
                padding-bottom: 0px;
            }


            .project-hover {
                position: relative;
                overflow: hidden;
                background:#FFF;
            }
            .project-hover:hover .text-view {
                top: 40%;
                opacity: 1;
            }
            .project-hover:hover .btn-view {
                top: 60%;
                opacity: 1;
            }
            .project-hover:hover img {
                opacity: .2;
            }

            .project-hover .img-view {
                opacity: 1;
            }
            .project-hover .text-view {
                width: 95%;
                position: absolute;
                top: 35%;
                left: 50%;
                -webkit-transform: translate(-50%, -50%);
                -ms-transform: translate(-50%, -50%);
                transform: translate(-50%, -50%);
                opacity: 0;
                font-weight: 600;
                z-index:5;
                padding:20px;
            }
            .project-hover .text-view h3 {
                margin-bottom: 10px;
                font-weight: 600;
            }
            .project-hover .btn-view {
                width: 100%;
                position: absolute;
                top: 65%;
                left: 50%;
                -webkit-transform: translate(-50%, -50%);
                -ms-transform: translate(-50%, -50%);
                transform: translate(-50%, -50%);
                opacity: 0;
                z-index:6;
            }

            .basic-widgets .img-responsive{ width:100% !important;}

            .text-view span{ font-size:12px;}
            .btn-info {
                border: 1px solid #5E8F95;
                color: #000;
                background-color:#5E8F95;
                font-size:14px;
                font-weight:900;
                text-transform:lowercase;
            }


            /* end project EFECT hover*/

            .css-category {
                padding:0px 0 75px 0px;
            }

            .portfolio
            {
                margin: 48px 0;
            }

            .portfolio-sorting
            {
                text-transform: uppercase;
                font-size: 12px;
                margin-bottom: 36px;
                margin-top:0px;
                background: #7FA9AE;
                padding: 0px;
                line-height: 12px;
            }

            .portfolio-sorting li {
                margin:0;
                padding:0;
            }

            .portfolio-sorting li a {
                color: #FFF;
                text-decoration: none;
                padding: 16px 10px 16px 10px;
                float: left;
                background: #7FA9AE;
                text-align: left;
                line-height: 18px;
                border-right:solid 1px #b5decb;
            }
            .portfolio-sorting li a:hover,
            .portfolio-sorting li a.active
            {
                color: #b5decb;
                font-weight:900 !important;
                background: #62888c;
            }

            .portfolio-item
            {
                margin-bottom: 30px;
            }

            .project-hover:hover img {
                opacity: 0.6;
            }

            .project-hover {
                position: relative;
                overflow: hidden;
                background: #000;
            }

            .project-hover .text-view h3 {
                margin-bottom: 10px;
                font-weight: 900;
                color: #FFF;
                font-size: 22px;
            }
            .project-hover p{ color:#fff; font-size:16px;}


            .project-hover .css-overlayBrand {
                position: absolute;
                width: 100%;
                height: 100%;
                z-index: 3;
                background-color: #080d15;
                opacity: .0;
                top:0;
            }

            .project-hover:hover .css-overlayBrand {
                opacity: .6;
            }

            .cnt-css-overlayBrand {
                position: absolute;
                width: 100%;
                height: 100%;
            }
            .project-hover .btn-view {
                top: 65%;
            }

            .css-color-product {
                background-position: center;
                background-size: contain;
                background-repeat: no-repeat;
                width: 100%;
                height: 400px;
            }

            .cssPM0{ padding:0px !important; margin:0px !important;}

            ol, ul {
                margin-top: 0;
                margin-bottom: 0px;
            }

            .obrasCareco h1{ text-align:center; font-size:26px; }
        </style>
    </head>
    <!--/head-->

    <body>
        <!--.preloader-->
        <div class="preloader"> <i class="fas fa-circle-notch fa-spin"></i></div>
        <!--/.preloader-->

        <header id="home">
            <nav class="css-nav-top">
                <div class="container">
                    <div class="col-md-6">
                        <i class="fas fa-phone"></i> +34 687 715 531
                        <i class="fas fa-envelope css-marginL30"></i> careco@careco.es
                    </div>

                    <div class="col-md-6 text-right">
                        <a href="#"><i class="fab fa-linkedin"></i></a>
                        <a href="#"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </nav>

            <nav class="navbar navbar-default">
                <div class="container">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#">
                                <img src="views/images/logo-high-n-01.png" alt="CARECO" width="120">
                            </a>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Promociones en Valencia <div class="css-nav-separator"></div></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="https://www.careco.es/promociones-valencia/comprar-chalet-adosado/penya-roja/avenida-francia-alameda">Promociones en curso <i class="fas fa-angle-right css-text-green css-marginL10"></i></a></li>
                                        <li><a href="https://www.careco.es/proximas-promociones-valencia">Próximas promociones <i class="fas fa-angle-right css-text-green css-marginL10"></i></a></li>
                                        <li><a href="https://www.careco.es/nuestras-obras">Promociones realizadas<i class="fas fa-angle-right css-text-green css-marginL10"></i></a></li>
                                    </ul>
                                </li>
                                <li><a href="https://www.careco.es/asi-somos">Careco <div class="css-nav-separator"></div></a></li>
                                <li><a href="https://www.careco.es/blog">Blog <div class="css-nav-separator"></div></a></li>
                                <li><a href="https://www.careco.es/atencion-al-cliente">Atención al cliente <div class="css-nav-separator"></div></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <!--/#main-nav-->

            <div id="home-slider" class="carousel slide carousel-fade" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="item active" style="background-image: url('views/images/slider/6.jpg')">
                        <div class="caption">
                            <div class="container">
                                <div class="col-md-6 text-left">
                                    <a data-scroll class="btn btn-start btn-green animated fadeInUpBig btn-slide" href="#">PROMOCIONES REALIZADAS</a>
                                    <p class="animated fadeInRightBig css-text-black css-medium css-marginT20">Queremos estar a tú lado<br> en cada paso que des.</p>
                                </div>

                                <div class="col-md-6">
                                    <!-- <h1 class="animated fadeInLeftBig">CAR<span>ECO</span></h1> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!--SECTION CATÁLOGO -->
        <section class="css-category">
            <div class="container-fluid">
                <div class="row">
                    <!--LISTADO DE CATEGORIAS-->
                    <ul class="portfolio-sorting list-inline text-center">
                        <li><a href="#" data-group="all" class="active" >Ver ><br>Todas</a></li>
                        <?php foreach ($arr_category as $val): ?>

                            <li><a id="c-<?=$val['name_canonical']?>"   href="#" data-group="cat-<?=$val['id']?>"><?=$val['name']?></a></li>
                        <?php endforeach; ?>
                    </ul>
                    <!--END.-LISTADO DE CATEGORIAS-->

                    <div class="container">
                        <div class="col-xs-12 obrasCareco">
                            <h1 class="animated fadeInLeftBig"> Obra nueva y promociones construidas<br> <strong> por Careco en Valencia y Picanya</strong></h1>
                            <p style="text-align:center; padding-bottom:26px;">No hay mejor forma de conocer a una empresa que viendo el trabajo que realiza, <br>nuestros clientes están satisfechos por el cuidado, dedicación y calidad de cada obra construida.</p>
                        </div>
                    </div>

                    <!--LISTADO DE PRODUCTOS-->
                    <ul class="portfolio-items list-unstyled" id="grid">
                        <!--INTEM.01-->
                        <?php $cnt = 0; foreach ($arr_portfolio as $val):?>
                            <li class="col-md-3 col-sm-4 col-xs-12 cssPM0" data-groups='["cat-<?=$val['category_id']?>"]'>
                                <div class="basic-widgets">
                                    <div class="project-hover">
                                        <div class="css-color-product" style="background-image: url('<?=APP_IMG_PORTFOLIO.$val['image']?>'); background-size: cover;"></div>
                                        <div class="css-overlayBrand  transition"> </div>
                                        <div class="text-view transition text-center css-text-transformLO" style="position: absolute; z-index:999; color: #FFF;">
                                            <h3> <?=$val['description']?></h3>
                                        </div>
                                        <div class="btn-view transition text-center" style="position: absolute; z-index:999;">
                                            <a data-nu="" href="https://www.careco.es/nuestras-obras/<?=createSlug($val['ubicacion'])."/".createSlug($val['title'])."/".$val['category_id']?>" class="btn btn-info">+ INFO</a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php $cnt++; endforeach; ?>
                        <!--END.ITEM01-->

                    </ul>
                    <!--END.LISTADO DE PRODUCTOS-->

                </div>
            </div>
        </section>
        <!--/.CATÁLOGO END-->

        <?php include('html/includes/newsletter.php') ?>
        <?php include('html/includes/footer.php') ?>


        <script type="text/javascript" src="views/app/resources_1/js/jquery.js"></script>
        <script type="text/javascript" src="views/app/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="//maps.google.com/maps/api/js?key=AIzaSyAfoFMSNU6Vr-JaVW4BN86B_VGkCwowO_w&sensor=true"></script>
        <script type="text/javascript" src="views/app/resources_1/js/jquery.inview.min.js"></script>
        <script type="text/javascript" src="views/app/resources_1/js/wow.min.js"></script>
        <script type="text/javascript" src="views/app/resources_1/js/mousescroll.js"></script>
        <script type="text/javascript" src="views/app/resources_1/js/smoothscroll.js"></script>
        <script type="text/javascript" src="views/app/resources_1/js/jquery.countTo.js"></script>
        <script type="text/javascript" src="views/app/resources_1/js/lightbox.min.js"></script>
        <script type="text/javascript" src="views/app/resources_1/js/main.js"></script>
        <!-- jQuery validation -->
        <script type="text/javascript" src="views/app/plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="views/app/plugins/jquery-validation/additional-methods.js"></script>
        <!-- newsletter -->
        <script type="text/javascript" src="views/app/resources_1/js/js-newsletter.js"></script>

        <script type="text/javascript" src="views/app/resources_1/js/jquery.shuffle.min.js"></script>
        <script type="text/javascript" src="views/app/resources_1/js/custom.js"></script>
    </body>
</html>
