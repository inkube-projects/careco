<!doctype html>
<html lang="es-ES" prefix="og: http://ogp.me/ns# og: http://ogp.me/ns#" >
   <head>
      <!-- Meta Tags -->
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
      <!--Shortcut icon-->

      <title>NUESTROS HITOS - Careco</title>

      <meta property="og:title" content="NUESTROS HITOS" />
      <meta property="og:description" content="&hellip;" />
      <meta property="og:url" content="" /> <!-- URL Actual -->
      <meta property="og:type" content="article" />
      <meta property="og:site_name" content="Careco" />
      <meta property="article:published_time" content="2016-11-11T17:51:05+00:00" />
      <meta property="article:modified_time" content="2017-03-27T09:57:21+00:00" />
      <meta property="og:updated_time" content="2017-03-27T09:57:21+00:00" />

      <!-- This site is optimized with the Yoast SEO plugin v4.4 - https://yoast.com/wordpress/plugins/seo/ -->
      <link rel="canonical" href="" /> <!-- URL Actual -->
      <meta property="og:locale" content="es_ES" />
      <meta property="og:type" content="article" />
      <meta property="og:title" content="NUESTROS HITOS - Careco" />
      <meta property="og:url" content="" /> <!-- URL Actual -->
      <meta property="og:site_name" content="Careco" />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:title" content="NUESTROS HITOS - Careco" />
      <!-- / Yoast SEO plugin. -->

      <?php include('html/overall/header.php') ?>
      <link rel='stylesheet' id='nectar-portfolio-css'  href='views/app/resources_2/css/portfolio.7.6.css' type='text/css' media='all' />

      <style type="text/css">
         img.wp-smiley,
         img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
         }
      </style>
      <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="resources_2/css/vc_lte_ie9.min.css" media="screen"><![endif]-->
      <!--[if IE  8]><link rel="stylesheet" type="text/css" href="css\vc-ie8.min.css" media="screen"><![endif]-->
      <style type="text/css" data-type="vc_custom-css">
         .derechapad {
            padding-right:40%!important;
         }

         .rectangulo {
            min-height:125px!important;
            max-height:125px!important;
         }
      </style>
      <style type="text/css" data-type="vc_shortcodes-custom-css">
         .vc_custom_1481627786178{
            padding-top: 20px !important;
            padding-bottom: 20px !important;
         }

         .derechapad {
            padding-right:10%!important;
            padding-top:10%!important;
         }

         .derechapad2 {
            padding-right:10%!important;
            padding-top:10%!important;
         }

         .rectangulo {
            min-height:125px!important;
            max-height:125px!important;
         }

         .izquierdapad {
            padding-left:10%!important;
            padding-top:10%!important;
         }

         .filete .divider {
            border-right: 2px solid #fff;
         }

         img.wp-smiley,
         img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
         }
      </style>

      <noscript>
         <style type="text/css">
            .wpb_animate_when_almost_visible { opacity: 1; }
         </style>
      </noscript>
   </head>

   <body class="page-template-default page page-id-9256 ascend nectar-auto-lightbox wpb-js-composer js-comp-ver-4.12.1 vc_responsive"
   data-footer-reveal="false"
   data-header-format="default"
   data-footer-reveal-shadow="none"
   data-dropdown-style="classic"
   data-cae="linear"
   data-cad="500"
   data-aie="none"
   data-ls="pretty_photo"
   data-apte="standard"
   data-hhun="0"
   data-fancy-form-rcs="1"
   data-form-style="minimal"
   data-form-submit="default"
   data-is="minimal"
   data-button-style="default"
   data-header-inherit-rc="false"
   data-header-search="false"
   data-animated-anchors="true"
   data-ajax-transitions="true"
   data-full-width-header="true"
   data-slide-out-widget-area="true"
   data-slide-out-widget-area-style="fullscreen"
   data-user-set-ocm="1"
   data-loading-animation="none"
   data-bg-header="false"
   data-ext-responsive="false"
   data-header-resize="0"
   data-header-color="custom"
   data-transparent-header="false"
   data-smooth-scrolling="1"
   data-permanent-transparent="false"
   data-responsive="1">

   <div id="header-secondary-outer" data-full-width="true" data-permanent-transparent="false">
      <div class="container">
         <nav></nav>
      </div>
   </div>

   <?php include('html/overall/topnav.php'); ?>

   <div id="search-outer" class="nectar">
      <div id="search">
         <div class="container">
            <div id="search-box">
               <div class="col span_12">
                  <form action="http://www.careco.es" method="GET">
                     <input type="text" name="s" id="s" value="Start Typing..." data-placeholder="Start Typing..." />
                  </form>
               </div><!--/span_12-->
            </div><!--/search-box-->

            <div id="close">
               <a href="#">
                  <span class="icon-salient-x" aria-hidden="true"></span>
               </a>
            </div>
         </div><!--/container-->
      </div><!--/search-->
   </div><!--/search-outer-->

   <div id="mobile-menu" data-mobile-fixed="1">
      <div class="container">
         <ul>
            <li><a href="">No menu assigned!</a></li>
         </ul>
      </div>
   </div>

   <div id="ajax-loading-screen" data-disable-fade-on-click="0" data-effect="standard" data-method="standard">
      <div class="loading-icon none">
         <span class="default-loading-icon spin"></span>
      </div>
   </div>

   <div id="ajax-content-wrap">
      <div class="blurred-wrap">
         <div class="container-wrap">
            <div class="container main-content">
               <div class="row">
                  <p id="breadcrumbs">
                     <span xmlns:v="http://rdf.data-vocabulary.org/#">
                        <span typeof="v:Breadcrumb">
                           <a href="index.php" rel="v:url" property="v:title">Inicio</a> »
                           <span class="breadcrumb_last">NUESTRAS OBRAS</span>
                        </span>
                     </span>
                  </p>

                  <div id="fws_5a6e41487cbf7"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-content standard_section"  style="padding-top: 0px; padding-bottom: 0px; ">
                     <div class="row-bg-wrap">
                        <div class="row-bg using-image"  style="background-image: url(http://www.careco.es/wp-content/uploads/2016/11/nuestros_hitos.jpg); background-position: center center; background-repeat: no-repeat; " data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
                     </div>

                     <div class="col span_12 dark left">
                        <div  class="vc_col-sm-2 vc_hidden-sm vc_hidden-xs wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                           <div class="vc_column-inner">
                              <div class="wpb_wrapper"></div>
                           </div>
                        </div>

                        <div  class="vc_col-sm-2 vc_hidden-sm vc_hidden-xs wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                           <div class="vc_column-inner">
                              <div class="wpb_wrapper"></div>
                           </div>
                        </div>

                        <div  class="vc_col-sm-2 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                           <div class="vc_column-inner">
                              <div class="wpb_wrapper"></div>
                           </div>
                        </div>

                        <div  class="vc_col-sm-6 vc_hidden-sm vc_hidden-xs wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                           <div class="vc_column-inner">
                              <div class="wpb_wrapper">
                                 <div class="divider-wrap">
                                    <div style="height: 200px;" class="divider"></div>
                                 </div>

                                 <div class="wpb_text_column wpb_content_element  bloque-inicio" >
                                    <div class="wpb_wrapper">
                                       <h1>
                                          <a href="index.php">
                                             <span style="font-size: 32px; padding: 5px 10px 10px 0px; color: #fff;">CARECO</span>
                                          </a>
                                       </h1>

                                       <h1>
                                          <span style="font-size: 32px; background-color: #7fa9ae; padding: 5px 10px 5px 10px; color: #fff;">CASAS COMO TÚ</span>
                                       </h1>
                                    </div>
                                 </div>

                                 <div class="divider-wrap">
                                    <div style="height: 120px;" class="divider"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div id="fws_5a6e41487da6f"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-content  vc_row-o-equal-height vc_row-flex standard_section rectangulo hide" style="padding-top: 0px; padding-bottom: 0px; ">
                     <div class="row-bg-wrap">
                        <div class="row-bg  using-bg-color" style="background-color: #606060; " data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
                     </div>

                     <div class="col span_12 dark left">
                        <div  class="vc_col-sm-2 vc_hidden-xs wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                           <div class="vc_column-inner">
                              <div class="wpb_wrapper"></div>
                           </div>
                        </div>

                        <div  class="vc_col-sm-2 texto2 wpb_column column_container vc_column_container col padding-2-percent"  data-bg-cover="" data-padding-pos="left" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                           <div class="vc_column-inner">
                              <div class="wpb_wrapper"></div>
                           </div>
                        </div>

                        <div  class="vc_col-sm-2 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                           <div class="vc_column-inner">
                              <div class="wpb_wrapper"></div>
                           </div>
                        </div>

                        <div  class="vc_col-sm-2 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                           <div class="vc_column-inner">
                              <div class="wpb_wrapper"></div>
                           </div>
                        </div>

                        <div  class="vc_col-sm-2 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                           <div class="vc_column-inner">
                              <div class="wpb_wrapper"></div>
                           </div>
                        </div>

                        <div  class="vc_col-sm-2 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                           <div class="vc_column-inner">
                              <div class="wpb_wrapper"></div>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div id="fws_5a6e41487e523"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-content standard_section   "  style="padding-top: 0px; padding-bottom: 0px; ">
                     <div class="row-bg-wrap">
                        <div class="row-bg  using-bg-color"  style="background-color: #baddda; " data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
                     </div>

                     <div class="col span_12 dark left">
                        <div style="" class="vc_col-sm-4 vc_hidden-xs wpb_column column_container vc_column_container col no-extra-padding" data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-color="#cddddc" data-bg-opacity="1" data-hover-bg="#000000" data-hover-bg-opacity="0.7" data-animation="" data-delay="0">
                           <a class="column-link" href="index.php?view=aboutUs"></a>

                           <div class="vc_column-inner">
                              <div class="wpb_wrapper">
                                 <div class="divider-wrap">
                                    <div style="height: 20px;" class="divider"></div>
                                 </div>

                                 <div class="wpb_text_column wpb_content_element  right" >
                                    <div class="wpb_wrapper">
                                       <h4 style="text-align: right;">
                                          <span style="color: #a1bfbd;">ASÍ SOMOS <i class="fas fa-angle-right  css-marginL10 css-fontSize24"></i><span style="color: rgba(100, 100, 100, 0);"> ·</span></span>
                                       </h4>
                                    </div>
                                 </div>

                                 <div class="divider-wrap">
                                    <div style="height: 20px;" class="divider"></div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div style="" class="vc_col-sm-4 vc_hidden-xs wpb_column column_container vc_column_container col no-extra-padding" data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-color="#c7d2d6" data-bg-opacity="1" data-hover-bg="#000000" data-hover-bg-opacity="0.7" data-animation="" data-delay="0"><a class="column-link" href="index.php?view=ourMilestones"></a>
                           <div class="vc_column-inner">
                              <div class="wpb_wrapper">
                                 <div class="divider-wrap"><div style="height: 20px;" class="divider"></div></div>
                                 <div class="wpb_text_column wpb_content_element  right" >
                                    <div class="wpb_wrapper">
                                       <h4 style="text-align: center;"><span style="color: #a1bfbd;">NUESTROS HITOS <i class="fas fa-angle-right  css-marginL10 css-fontSize24"></i></span></h4>
                                    </div>
                                 </div>

                                 <div class="divider-wrap">
                                    <div style="height: 20px;" class="divider"></div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div style="" class="vc_col-sm-4 wpb_column column_container vc_column_container col no-extra-padding" data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-color="#adc0c0" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                           <div class="vc_column-inner">
                              <div class="wpb_wrapper">
                                 <div class="divider-wrap">
                                    <div style="height: 20px;" class="divider"></div>
                                 </div>

                                 <div class="wpb_text_column wpb_content_element">
                                    <div class="wpb_wrapper">
                                       <h4 style="text-align: left;">
                                          <span style="color: #a1bfbd;">
                                             <span style="color: rgba(100, 100, 100, 0);"> </span>
                                          </span>

                                          <span style="color: #ffffff;">NUESTRAS OBRAS <i class="fas fa-angle-down css-text-green css-marginL10 css-fontSize24"></i></span>
                                       </h4>
                                    </div>
                                 </div>

                                 <div class="divider-wrap">
                                    <div style="height: 20px;" class="divider"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div id="fws_5a6e41487f0dc"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row standard_section"  style="padding-top: 0px; padding-bottom: 0px;">
                     <div class="row-bg-wrap">
                        <div class="row-bg" style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
                     </div>

                     <div class="col span_12 dark left">
                        <div  class="vc_col-sm-12 portfolio-obras wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                           <div class="vc_column-inner">
                              <div class="wpb_wrapper">
                                 <div class="portfolio-filters-inline full-width-content " data-alignment="default" data-color-scheme="default">
                                    <div class="container">
                                       <span id="current-category">All</span>

                                       <ul>
                                          <li id="sort-label">Sort Portfolio:</li>
                                          <li><a href="#" data-filter="*">All</a></li>
                                          <?php foreach ($arr_category as $val): ?>
                                             <li style="display:block"><a href="#" data-filter=".cat-<?=$val['id']?>"><?=$val['name']?></a></li>
                                          <?php endforeach; ?>
                                          <!-- <li><a href="#" data-filter=".alquiler">ALQUILER</a>
                                             <ul class='children'>
                                                <li><a href="#" data-filter=".francia-alquiler">Francia</a></li>
                                                <li><a href="#" data-filter=".picanya-alquiler">Picanya</a></li>
                                             </ul>
                                          </li>
                                          <li><a href="#" data-filter=".edificio-aceiteras-casanova-avd-puerto">Edificio Aceiteras Casanova</a></li>
                                          <li><a href="#" data-filter=".edificio-azul-almazora">Edificio Azul</a></li>
                                          <li><a href="#" data-filter=".edificio-berlanga">Edificio Berlanga</a></li>
                                          <li><a href="#" data-filter=".edificio-miradores-calle-finlandia">Edificio Miradores</a></li>
                                          <li><a href="#" data-filter=".malilla">Malilla</a></li>
                                          <li><a href="#" data-filter=".masia-les-palmes-picanya">Masia Les Palmes</a></li>
                                          <li><a href="#" data-filter=".urbanizacion-la-mezquida-javea">Urbanización La Mezquida</a></li>
                                          <li><a href="#" data-filter=".venta">VENTA</a>
                                             <ul class='children'>
                                                <li><a href="#" data-filter=".francia">Francia</a></li>
                                                <li><a href="#" data-filter=".picanya">Picanya</a></li>
                                             </ul>
                                          </li> -->
                                       </ul>

                                       <div class="clear"></div>
                                    </div>
                                 </div>

                                 <div class="portfolio-wrap">
                                    <span class="portfolio-loading  none"></span>
                                    <div class="row portfolio-items masonry-items" data-masonry-type="default" data-ps="2" data-starting-filter="default" data-gutter="5px" data-categories-to-show="<?=$str_category?>,edificio-aceiteras-casanova-avd-puerto" data-col-num="elastic">
                                       <?php $cnt = 0; foreach ($arr_portfolio as $val):?>
                                          <div class="col elastic-portfolio-item <?=$arr_class[$cnt][1]?> element cat-<?=$val['category_id']?>"  data-project-cat="cat-<?=$val['category_id']?>" data-default-color="true" data-title-color="" data-subtitle-color="">
                                             <div class="inner-wrap animated" data-animation="fade_in">
                                                <div class="work-item style-2" data-custom-content="off">
                                                   <?php if ($cnt >= 21) { $cnt = 0; } ?>
                                                   <img width="1000" height="1000" src="<?=APP_IMG_PORTFOLIO.$val['image']?>" class="<?=$arr_class[$cnt][0]?> wp-post-image" alt="" title="" />
                                                   <div class="work-info-bg"></div>

                                                   <div class="work-info">
                                                      <a href="<?=APP_IMG_PORTFOLIO.$val['image']?>" class="pretty_photo"></a>
                                                      <div class="vert-center">
                                                         <h3><?=$val['title']?></h3>
                                                      </div>
                                                   </div>
                                                </div><!--work-item-->
                                             </div><!--/inner-wrap-->
                                          </div><!--/col-->
                                       <?php $cnt++; endforeach; ?>
                                    </div><!--/portfolio-->
                                 </div><!--/portfolio wrap-->
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div><!--/row-->
            </div><!--/container-->
         </div><!--/container-wrap-->
         <?php include('html/overall/footer.php') ?>
      </div><!--blurred-wrap-->

      <a id="to-top" class=""><i class="fas fa-angle-up"></i></a>
      <?php include('html/overall/js.php') ?>
      <script type='text/javascript' src='views/app/resources_2/js/isotope.min.7.6.js'></script>
   </body>
</html>
