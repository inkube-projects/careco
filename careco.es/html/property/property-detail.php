<!doctype html>
<html lang="es-ES" prefix="og: http://ogp.me/ns# og: http://ogp.me/ns#" >
	<head>
		<base href="<?=MAINURL?>">
		<!-- Meta Tags -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
		<!--Shortcut icon-->
		<title><?=mb_convert_case($arr_property[0]['cover_title'], MB_CASE_TITLE, "UTF-8")?> - Careco</title>
	
		<link rel='dns-prefetch' href='//fonts.googleapis.com' />
		<link rel='dns-prefetch' href='//s.w.org' />
		<link rel="alternate" type="application/rss+xml" title="Careco &raquo; Feed" href="http://www.careco.es/?feed=rss2" />
		
		<style type="text/css">
			img.wp-smiley, img.emoji {
				display: inline !important;
				border: none !important;
				box-shadow: none !important;
				height: 1em !important;
				width: 1em !important;
				margin: 0 .07em !important;
				vertical-align: -0.1em !important;
				background: none !important;
				padding: 0 !important;
			}
			.btn-submit {
    display: block;
    padding: 12px;
    width: 100%;
    color: #fff;
    border: 0;
    margin-top: 40px;
}
.btn-submit {
    width: 150px;
    background-color: #000;
}
.precioA{font-size:18px !important; font-weight:600 !important;}

		</style>
		<link rel='stylesheet' id='contact-form-7-css'  href='<?=MAINURL?>views/app/resources_2/css/styles.4.6.1.css' type='text/css' media='all' />
		<link rel='stylesheet' id='mediaelement-css'  href='<?=MAINURL?>views/app/resources_2/css/mediaelementplayer.min.2.22.0.css' type='text/css' media='all' />
		<link rel='stylesheet' id='wp-mediaelement-css'  href='<?=MAINURL?>views/app/resources_2/css/wp-mediaelement.min.4.7.css' type='text/css' media='all' />
		<link rel='stylesheet' id='rgs-css'  href='<?=MAINURL?>views/app/resources_2/css/rgs.6.0.1.css' type='text/css' media='all' />
		<link rel='stylesheet' id='font-awesome-css'  href='<?=MAINURL?>views/app/plugins/font-awesome/web-fonts-with-css/css/fontawesome-all.min.css' type='text/css' media='all' />
		<link rel='stylesheet' id='main-styles-css'  href='<?=MAINURL?>views/app/resources_2/css/style.7.6.css' type='text/css' media='all' />
		<link rel='stylesheet' id='pretty_photo-css'  href='<?=MAINURL?>views/app/resources_2/css/prettyPhoto.7.0.1.css' type='text/css' media='all' />
		<!--[if lt IE 9]>
		<link rel='stylesheet' id='nectar-ie8-css'  href='http://www.careco.es/wp-content/themes/salient/css/ie8.css?ver=4.7' type='text/css' media='all' />
		<![endif]-->
		<link rel='stylesheet' id='responsive-css'  href='<?=MAINURL?>views/app/resources_2/css/responsive.7.6.css' type='text/css' media='all' />
		<link rel='stylesheet' id='nectarslider-css'  href='<?=MAINURL?>views/app/resources_2/css/nectar-slider4.7.css' type='text/css' media='all' />
		<link rel='stylesheet' id='nectar-portfolio-css'  href='<?=MAINURL?>views/app/resources_2/css/portfolio.7.6.css' type='text/css' media='all' />
		<link rel='stylesheet' id='select2-css'  href='<?=MAINURL?>views/app/resources_2/css/select2-6.2.css' type='text/css' media='all' />
		<link rel='stylesheet' id='skin-ascend-css'  href='<?=MAINURL?>views/app/resources_2/css/ascend7.6.css' type='text/css' media='all' />
		<link rel='stylesheet' id='easy-social-share-buttons-css'  href='<?=MAINURL?>views/app/resources_2/css/easy-social-share-buttons.min.4.1.css' type='text/css' media='all' />
		<link rel='stylesheet' id='redux-google-fonts-salient_redux-css'  href='//fonts.googleapis.com/css?family=Montserrat%3A400%2C700%7CLato%3A900%2C400&#038;subset=latin&#038;ver=1509443950' type='text/css' media='all' />
		<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/jquery.1.12.4.js'></script>
		<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/jquery-migrate.min.1.4.1.js'></script>
		<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/modernizr.2.6.2.js'></script>
		<link rel='https://api.w.org/' href='http://www.careco.es/?rest_route=/' />
		<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.careco.es/xmlrpc.php?rsd" />
		<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.careco.es/wp-includes/wlwmanifest.xml" />
		<meta name="generator" content="WordPress 4.7" />
		<link rel='shortlink' href='http://www.careco.es/?p=9178' />
		<link rel="alternate" type="application/json+oembed" href="http://www.careco.es/?rest_route=%2Foembed%2F1.0%2Fembed&#038;url=http%3A%2F%2Fwww.careco.es%2F%3Fportfolio%3Davda-francia-comercial" />
		<link rel="alternate" type="text/xml+oembed" href="http://www.careco.es/?rest_route=%2Foembed%2F1.0%2Fembed&#038;url=http%3A%2F%2Fwww.careco.es%2F%3Fportfolio%3Davda-francia-comercial&#038;format=xml" />
		<link rel="stylesheet" href="<?=MAINURL?>views/app/resources_2/css/main-property.css">
		<link rel="stylesheet" href="<?=MAINURL?>views/app/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?=MAINURL?>views/app/resources_2/css/css-style.css">
		<script type="text/javascript">
			var essb_settings = {
				"ajax_url":"http:\/\/www.careco.es\/wp-admin\/admin-ajax.php",
				"essb3_nonce":"777d0930da",
				"essb3_plugin_url":"http:\/\/www.careco.es\/wp-content\/plugins\/easy-social-share-buttons3",
				"essb3_facebook_total":true,
				"essb3_admin_ajax":false,
				"essb3_internal_counter":false,
				"essb3_stats":false,
				"essb3_ga":false,
				"essb3_ga_mode":"simple",
				"essb3_counter_button_min":0,
				"essb3_counter_total_min":0,
				"blog_url":"http:\/\/www.careco.es\/",
				"ajax_type":"wp",
				"essb3_postfloat_stay":false,
				"essb3_no_counter_mailprint":false,
				"essb3_single_ajax":false,
				"twitter_counter":"self",
				"post_id":9178
			};
		</script>
		<meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
			<!--[if lte IE 9]>
				<link rel="stylesheet" type="text/css" href="http://www.careco.es/wp-content/plugins/js_composer_salient/assets/css/vc_lte_ie9.min.css" media="screen">
			<![endif]--><!--[if IE  8]>
				<link rel="stylesheet" type="text/css" href="http://www.careco.es/wp-content/plugins/js_composer_salient/assets/css/vc-ie8.min.css" media="screen">
			<![endif]-->
		<style type="text/css" data-type="vc_custom-css">
			#full_width_portfolio #portfolio-extra .clear {
	    		padding-bottom: 0;
			}
			.cuadro {
				min-height:400px!important;
				max-height:400px!important;
			}
			.rectangulo {
				min-height:400px!important;
				max-height:400px!important;
			}
			.textopading25 {
				padding-left: 15%;
				padding-right: 15%;
			}
			.foto {
				padding-right: 25%;
			}
			.padr {
				padding-right: 25%;
			}
			.mapa {
				padding-left:25%;
			}
			body[data-form-style="minimal"] .minimal-form-input {
				padding-top: 10px;
			}
			body[data-form-style="minimal"] input[type="email"] {
				background-color: #eee !important;
				padding-top: 7px !important;
				padding-bottom: 7px !important;
				padding-left: 7px !important;
				padding-right: 7px !important;
			}
			.ascend .container-wrap input[type="submit"], .ascend .container-wrap button[type="submit"] {
				padding: 8px !important;
				font-size: 14px !important;
			}
			.titemail {
				font-size: 12px;
			}
			body[data-form-style="minimal"] input[type="submit"], body[data-form-style="minimal"] button[type="submit"] {
				background-color: #697F7F !important;
				font-size: 12px;
				color:#FFF !important;
			}
		 .ascend .container-wrap input[type="submit"], .ascend .container-wrap button[type="submit"] {
			 padding: 8px !important;
			 font-size: 14px !important;
		 }
		</style>
		<style type="text/css" data-type="vc_shortcodes-custom-css">
			.vc_custom_1488884339684 {
				background-color: #1aa8b2 !important;
			}
			.vc_custom_1488878968715 {
				background-color: #1aa8b2 !important;
			}
			.vc_custom_1481627956764{
				padding-top: 20px !important;
				padding-bottom: 20px !important;
			}
		</style>
		<noscript>
			<style type="text/css">
				.wpb_animate_when_almost_visible { opacity: 1; }
			</style>
		</noscript>
	</head>

	<body class="portfolio-template-default single single-portfolio postid-9178 ascend nectar-auto-lightbox wpb-js-composer js-comp-ver-4.12.1 vc_responsive"
	data-footer-reveal="false"
	data-header-format="default"
	data-footer-reveal-shadow="none"
	data-dropdown-style="classic"
	data-cae="linear"
	data-cad="500"
	data-aie="none"
	data-ls="pretty_photo"
	data-apte="standard"
	data-hhun="0"
	data-fancy-form-rcs="1"
	data-form-style="minimal"
	data-form-submit="default"
	data-is="minimal"
	data-button-style="default"
	data-header-inherit-rc="false"
	data-header-search="false"
	data-animated-anchors="true"
	data-ajax-transitions="true"
	data-full-width-header="true"
	data-slide-out-widget-area="true"
	data-slide-out-widget-area-style="fullscreen"
	data-user-set-ocm="1"
	data-loading-animation="none"
	data-bg-header="false"
	data-ext-responsive="false"
	data-header-resize="0"
	data-header-color="custom"
	data-transparent-header="false"
	data-smooth-scrolling="1"
	data-permanent-transparent="false"
	data-responsive="1" >

	<div id="header-secondary-outer" data-full-width="true" data-permanent-transparent="false" >
		<div class="container">
			<nav></nav>
		</div>
	</div>

	<? include('html/overall/topnav.php'); ?>

	<div id="ajax-loading-screen" data-disable-fade-on-click="0" data-effect="standard" data-method="standard">
		<div class="loading-icon none">
			<span class="default-loading-icon spin"></span>
		</div>
	</div>

	<div id="ajax-content-wrap">
		<div class="blurred-wrap">
			<div id="full_width_portfolio" data-featured-img="http://www.careco.es/wp-content/uploads/2016/11/37MG_5798.jpg" >
				<div class="row project-title">
					<div class="container">
						<div class="title-wrap">
							<div class="col span_12 section-title no-date">
								<h1>Avda. Francia Bajo Comercial</h1>

								<div id="portfolio-nav">
									<ul>
										<li id="all-items"><a href="http://www.careco.es/?page_id=9178" title="Back to all projects"><i class="icon-salient-back-to-all"></i></a></li>
									</ul>

									<ul class="controls">
										<li id="prev-link"><a href="http://www.careco.es/?portfolio=avda-francia-p36-2" rel="next"><i class="icon-salient-left-arrow-thin"></i></a></li>
										<li id="next-link"><a href="http://www.careco.es/?portfolio=avda-francia-bajo-comercial" rel="prev"><i class="icon-salient-right-arrow-thin"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div><!--/row-->

				<div class="container-wrap" data-nav-pos="in_header">
					<div class="container main-content">
						<div class="row">
							<p id="breadcrumbs">
								<span xmlns:v="http://rdf.data-vocabulary.org/#">
									<span typeof="v:Breadcrumb">
										<a href="http://www.careco.es/" rel="v:url" property="v:title">Inicio</a> »
										<span rel="v:child" typeof="v:Breadcrumb">
											<!--<a href="index.php?view=currentPromotion&c=<?//=$arr_property[0]['category']?>" rel="v:url" property="v:title"><?//=$category?></a> »-->
											<span class="breadcrumb_last"><?=$promotion?></span>
										</span>
									</span>
								</span>
							</p>

							<div id="post-area" class="col span_12">
								<div id="portfolio-extra">
									<div id="fws_5b28785c25335"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-section  vc_row-o-equal-height vc_row-flex standard_section" style="padding-top: 0px; padding-bottom: 1%; ">
										<div class="row-bg-wrap">
											<div class="row-bg  using-bg-color" style="background-color: #ffffff; " data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
										</div>

										<div class="col span_12 dark left">
											<div class="vc_col-sm-12 wpb_column column_container vc_column_container col no-extra-padding" data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
												<div class="vc_column-inner">
													<div class="wpb_wrapper">
														<div class="wpb_gallery wpb_content_element clearfix">
															<div class="wpb_wrapper">
																<div class="wpb_gallery_slidesnectarslider_style" data-interval="5">
																	<div class="nectar-slider-wrap" style="height:  450px" data-flexible-height="" data-fullscreen="false"  data-full-width="false" data-parallax="false" data-autorotate="5500" id="ns-id-5b28785c2e21f">
																		<div class="swiper-container" style="height:  450px"  data-loop="false" data-height=" 450" data-bullets="true" data-bullet_style="see_through" data-arrows="true" data-bullets="false" data-desktop-swipe="false" data-settings="">
																			<div class="swiper-wrapper">
																				<?php foreach ($arr_gallery as $val): ?>
																					<div class="swiper-slide" data-bg-alignment="center" data-color-scheme="light" data-x-pos="centered" data-y-pos="middle">
																						<div class="image-bg" style="background-image: url('<?=$gallery_path.$val['image']?>');"></div>
																						<a class="entire-slide-link" href="<?=$gallery_path.$val['image']?>"></a>
																						<span class="ie-fix"></span>
																					</div>
																				<?php endforeach; ?>
																			</div>

																			<a href="" class="slider-prev">
																				<i class="icon-salient-left-arrow"></i>
																				<div class="slide-count">
																					<span class="slide-current">1</span>
																					<i class="icon-salient-right-line"></i>
																					<span class="slide-total"></span>
																				</div>
																			</a>

																			<a href="" class="slider-next">
																				<i class="icon-salient-right-arrow"></i>
																				<div class="slide-count">
																					<span class="slide-current">1</span>
																					<i class="icon-salient-right-line"></i>
																					<span class="slide-total"></span>
																				</div>
																			</a>

																			<div class="slider-pagination"></div>
																			<div class="nectar-slider-loading"></div>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<div class="divider-wrap">
															<div style="height: 10px;" class="divider"></div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div id="fws_5b28785c3d92b"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-section  vc_row-o-equal-height vc_row-flex standard_section" style="padding-top: 0px; padding-bottom: 50pxpx; ">
										<div class="row-bg-wrap">
											<div class="row-bg  using-bg-color" style="background-color: #ffffff; " data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
										</div>

										<div class="col span_12 dark left">
											<div  class="vc_col-sm-6 wpb_column column_container vc_column_container col padding-3-percent"  data-bg-cover="" data-padding-pos="right" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
												<div class="vc_column-inner">
													<div class="wpb_wrapper">
														<div class="divider-wrap">
															<div style="height: 24px;" class="divider"></div>
														</div>
														<div class="wpb_text_column wpb_content_element " >
															<div class="wpb_wrapper">
																<p style="text-align: left;"><?=$arr_property[0]['code']?>
																	<a href="javascript:history.back(1)">&lt; Volver</a>
																</p>
																<h4 style="text-align: left; line-height: 28px;"><strong><?=mb_convert_case($arr_property[0]['title'], MB_CASE_UPPER, "UTF-8")?></strong></h4>

																<h6 style="text-align: left;" class="css-bold">

                                                            <?php
															$addVentaTxt='';
															if(strlen($arr_property[0]['monthly_cost']>1)) { echo "<span class='precioA'>Alquiler: </span>".$arr_property[0]['monthly_cost'].'<span class="precioA">€/MES<br></span>'; $addVentaTxt='<span class="precioA">Venta: </span>';}
															if(strlen($arr_property[0]['cost']>1)) echo $addVentaTxt.$arr_property[0]['cost'].' <span class="precioA">€ </span>';
															?>





                                                                </h6>


															</div>
														</div>

														<div id="fws_5b28785c3eea7" data-midnight="" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row standard_section" style="padding-top: 0px; padding-bottom: 0px; ">
															<div class="row-bg-wrap">
																<div class="row-bg" style=""></div>
															</div>

															<div class="col span_12  left">
																<div class="vc_col-sm-12 wpb_column column_container vc_column_container col padding-1-percent"  data-bg-cover="" data-padding-pos="top-bottom" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
																	<div class="vc_column-inner">
																		<div class="wpb_wrapper">
																			<div class="img-with-aniamtion-wrap" data-max-width="100%">
																				<div class="inner">
																					<?php foreach ($arr_icons as $icon): ?>
			                                                         <?php if ($icon['image']): ?>
			                                                            <div class="pull-left css-marginR5">
			                                                               <span class="css-icon-description" style="line-height: 3;"><?=$icon['description_2']?></span>
			                                                               <img src="<?=APP_IMG_ICONS.$icon['image']?>" class="pull-right css-icon" alt="">
			                                                            </div>
			                                                         <?php endif; ?>
			                                                      <?php endforeach; ?>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<div class="wpb_text_column wpb_content_element">
															<div class="wpb_wrapper">
																<p class="css-gray-2 css-fontSize13" style="line-height: 21px;"><?=nl2br($arr_property[0]['description'])?></p>
															</div>
														</div>

														<div class="divider-wrap">
															<div style="height: 24px;" class="divider"></div>
														</div>
														<div role="form" class="wpcf7" id="wpcf7-f9409-p9178-o1" lang="es-ES" dir="ltr">
															<div class="screen-reader-response"></div>
															<form id="frm-contact" name="frm-contact" method="post">
																<div style="display: none;">
																	<input type="hidden" name="_wpcf7" value="9409" />
																	<input type="hidden" name="_wpcf7_version" value="4.6.1" />
																	<input type="hidden" name="_wpcf7_locale" value="es_ES" />
																	<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f9409-p9178-o1" />
																	<input type="hidden" name="_wpnonce" value="62a7f888a0" />
																</div>

																<span class="titemail">TU NOMBRE (OBLIGATORIO)</span><br />
			                                                    <span class="wpcf7-form-control-wrap">
			                                                       <input type="text" name="name" id="name" size="40" class="wpcf7-form-control wpcf7-text wpcf7-name wpcf7-validates-as-required wpcf7-validates-as-name" aria-required="true" aria-invalid="false" />
			                                                       <div class="text-danger" id="name_validate"></div>
			                                                    </span>
			                                                    <br>
			                                                    <span class="titemail">TU APELLIDO (OBLIGATORIO)</span><br />
			                                                    <span class="wpcf7-form-control-wrap">
			                                                       <input type="text" name="lastName" id="lastName" size="40" class="wpcf7-form-control wpcf7-text wpcf7-lastName wpcf7-validates-as-required wpcf7-validates-as-lastName" aria-required="true" aria-invalid="false" />
			                                                       <div class="text-danger" id="lastName_validate"></div>
			                                                    </span>
			                                                    <br>
																<span class="titemail">TU EMAIL (OBLIGATORIO)</span><br />
																<span class="wpcf7-form-control-wrap your-email">
																	<input type="text" name="email" id="email" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" />
																	<div class="text-danger" id="email_validate"></div>
																</span>
																<br>
																<span class="titemail">TU TELÉFONO</span><br />
																<span class="wpcf7-form-control-wrap tel-999">
																	<input type="text" name="phone" id="phone" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel" aria-invalid="false" />
																	<div class="text-danger" id="phone_validate"></div>
																</span>
																<br>
				                                                <span class="titemail">OBSERVACIONES</span><br />
				                                                <span class="wpcf7-form-control-wrap">
				                                                   <input type="text" id="observations" name="observations" size="40" class="wpcf7-form-control wpcf7-text wpcf7-observations wpcf7-validates-as-required wpcf7-validates-as-observations" aria-required="true" aria-invalid="false" />
				                                                   <div class="text-danger" id="observations_validate"></div>
				                                                </span>

																<p>
																	<button type="submit" class="wpcf7-form-control wpcf7-submit js-submit css-marginT20">ESTOY INTERESADO</button>
																</p>

																<p class="css-noPadding contact-message"></p>

																<div class="wpcf7-response-output wpcf7-display-none"></div>
															</form>
														</div>

														<div class="divider-wrap">
															<div style="height: 84px;" class="divider"></div>
														</div>
													</div>
												</div>
											</div>

											<div class="vc_col-sm-2 wpb_column column_container vc_column_container col no-extra-padding" data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
												<div class="vc_column-inner">
													<div class="wpb_wrapper">
														<div class="divider-wrap">
															<div style="height: 26px;" class="divider"></div>
														</div>

														<div class="wpb_text_column wpb_content_element">
															<div class="wpb_wrapper">
																<h4 style="text-align: right;">SUPERFICIE</h4>
																<p style="text-align: right;" class="css-gray-2 css-fontSize13">
																	<?php foreach ($arr_icons as $val): ?>
																		<?=$val['description_2']." ".$val['description']?><br>
																	<?php endforeach; ?>
																</p>
															</div>
														</div>
													</div>
												</div>
											</div>

											<div class="vc_col-sm-4 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
												<div class="vc_column-inner">
													<div class="wpb_wrapper">

														<div class="divider-wrap">
															<div style="height: 21px;" class="divider"></div>
														</div>

														<div class="img-with-aniamtion-wrap right" data-max-width="100%">
															<div class="inner">
																<a href="<?=$blue_prints_path.$arr_property[0]['blueprints']?>" class="pp right">
																	<img data-shadow="none" data-shadow-direction="middle" class="img-with-animation " data-delay="0" height="700" width="600" data-animation="none" src="<?=$blue_prints_path.$arr_property[0]['blueprints']?>" alt="" />
																</a>
															</div>
														</div>

                                                        <div class="wpb_text_column wpb_content_element ">
																<div class="wpb_wrapper">

																		<p class="css-text-green css-marginB20">Compártelo</p>
                    													 <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b2110a6329f7e46"></script>
                    													 <div class="addthis_inline_share_toolbox"></div>

																</div>
															</div>



													</div>
												</div>

											</div>



										</div>
									</div>

									<div id="fws_5b28785c47297"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-content standard_section cuadro" style="padding-top: 0px; padding-bottom: 0px; ">
										<div class="row-bg-wrap">
											<div class="row-bg  using-bg-color" style="background-color: #5b4254; " data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
										</div>

										<div class="col span_12 dark left">
											<div class="vc_col-sm-4 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
												<div class="vc_column-inner">
													<div class="wpb_wrapper">
														<div id="fws_5b28785c4779c" data-midnight="" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row  vc_row-o-equal-height vc_row-flex standard_section rectangulo" style="padding-top: 0px; padding-bottom: 0px; ">
															<div class="row-bg-wrap">
																<div class="row-bg" style=""></div>
															</div>

															<div class="col span_12  left">
																<div style="" class="vc_col-sm-12 rectangulo wpb_column column_container vc_column_container col centered-text padding-10-percent" data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-color="#e2e2e2" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
																	<div class="vc_column-inner">
																		<div class="wpb_wrapper">
																			<div class="divider-wrap">
																				<div style="height: 60px;" class="divider"></div>
																			</div>

																			<div class="divider-wrap">
																				<div style="margin-top: 12px; height: 1px; margin-bottom: 12px;" data-width="100%" data-animate="" data-animation-delay="" data-color="default" class="divider-border"></div>
																			</div>

																			<div class="wpb_text_column wpb_content_element">
																				<div class="wpb_wrapper">
																					<h5 style="text-align: center;">
																						<span style="color: #444444;">
																							<strong>PERSONALIDAD</strong>
																						</span>
																						<br />
																						<span style="color: #444444;">CASAS COMO TÚ</span></h5>
																					</div>
																				</div>

																				<div class="divider-wrap">
																					<div style="margin-top: 12px; height: 1px; margin-bottom: 12px;" data-width="100%" data-animate="" data-animation-delay="" data-color="default" class="divider-border"></div>
																				</div>

																				<div class="divider-wrap">
																					<div style="height: 10px;" class="divider"></div>
																				</div>

																				<div class="wpb_text_column wpb_content_element  textopading25" >
																					<div class="wpb_wrapper">
																						<p style="text-align: center;">
																							<span style="color: #444444;"> Calidad, innovación y cercanía, tres grandes conceptos que combinados nos han convertido en una de las empresas del sector con los clientes más satisfechos.</span>
																						</p>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>

												<div  class="vc_col-sm-8 cuadro wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
													<div class="vc_column-inner">
														<div class="wpb_wrapper">
															<div class="wpb_gallery wpb_content_element clearfix">
																<div class="wpb_wrapper">
																	<div class="wpb_gallery_slidesnectarslider_style" data-interval="5">
																		<div class="nectar-slider-wrap" style="height:  400px" data-flexible-height="" data-fullscreen="false"  data-full-width="false" data-parallax="false" data-autorotate="5500" id="ns-id-5b28785c4816d">
																			<div class="swiper-container" style="height:  400px"  data-loop="false" data-height=" 400" data-bullets="true" data-bullet_style="see_through" data-arrows="true" data-bullets="false" data-desktop-swipe="false" data-settings="">
																				<div class="swiper-wrapper">
																					<?php foreach ($arr_gallery as $val): ?>
																						<div class="swiper-slide" data-bg-alignment="center" data-color-scheme="light" data-x-pos="centered" data-y-pos="middle">
																							<div class="image-bg" style="background-image: url('<?=$gallery_path.$val['image']?>');"></div>
																							<a class="entire-slide-link" href="<?=$gallery_path.$val['image']?>"></a>
																							<span class="ie-fix"></span>
																						</div>
																					<?php endforeach; ?>
																				</div>

																				<a href="" class="slider-prev">
																					<i class="icon-salient-left-arrow"></i>
																					<div class="slide-count">
																						<span class="slide-current">1</span>
																						<i class="icon-salient-right-line"></i>
																						<span class="slide-total"></span>
																					</div>
																				</a>

																				<a href="" class="slider-next">
																					<i class="icon-salient-right-arrow"></i>
																					<div class="slide-count">
																						<span class="slide-current">1</span>
																						<i class="icon-salient-right-line"></i>
																						<span class="slide-total"></span>
																					</div>
																				</a>

																				<div class="slider-pagination"></div>

																				<div class="nectar-slider-loading"></div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>

										<div id="fws_5b28785c5bc84"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-content standard_section" style="padding-top: 80px; padding-bottom: 80px; ">
											<div class="row-bg-wrap">
												<div class="row-bg"  style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
											</div>

											<div class="col span_12 dark left">
												<div  class="vc_col-sm-2 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
													<div class="vc_column-inner">
														<div class="wpb_wrapper"></div>
													</div>
												</div>

												<div class="vc_col-sm-8 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
													<div class="vc_column-inner">
														<div class="wpb_wrapper">
															<div id="fws_5b28785c5c285" data-midnight="" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row standard_section   "  style="padding-top: 0px; padding-bottom: 0px; ">
																<div class="row-bg-wrap">
																	<div class="row-bg" style=""></div>
																</div>

																<div class="col span_12  left">
                                                                <?php if(strlen($arr_property[0]['quality_memories'])>1){ ?>
																	<div  class="vc_col-sm-3 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
																		<a class="column-link" href="<?=$files_path.$arr_property[0]['quality_memories']?>"></a>
																		<div class="vc_column-inner">
																			<div class="wpb_wrapper">
																				<div class="iwithtext">
																					<div class="iwt-icon">
                                                                                    <i class="icon-default-style fa fa-arrow-circle-down" accent-color aria-hidden="true"></i>
																					</div>

																					<div class="iwt-text">
																						<a href="<?=$files_path.$arr_property[0]['quality_memories']?>">
																							<span style="color: #5b4254;">MEMORIA</span><br />
																							<span style="color: #5b4254;">DE CALIDADES</span>
																						</a>
																					</div>

																					<div class="clear"></div>
																				</div>
																			</div>
																		</div>
																	</div>
<?php } if(strlen($arr_property[0]['blueprints_doc'])>1){ ?>
																	<div  class="vc_col-sm-3 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
																		<a class="column-link" href="<?=$files_path.$arr_property[0]['blueprints_doc']?>"></a>
																		<div class="vc_column-inner">
																			<div class="wpb_wrapper">
																				<div class="iwithtext">
																					<div class="iwt-icon">
																						<i class="icon-default-style fa fa-arrow-circle-down" accent-color aria-hidden="true"></i>
																					</div>

																					<div class="iwt-text">
																						<span style="color: #5b4254;">PLANOS</span>
																					</div>

																					<div class="clear"></div>
																				</div>
																			</div>
																		</div>
																	</div>
																<?php } if(strlen($arr_property[0]['cee'])>1){ ?>
																	<div  class="vc_col-sm-3 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
																		<a class="column-link" href="<?=$files_path.$arr_property[0]['cee']?>"></a>
																		<div class="vc_column-inner">
																			<div class="wpb_wrapper">
																				<div class="iwithtext">
																					<div class="iwt-icon">
																						<i class="icon-default-style fa fa-arrow-circle-down" accent-color aria-hidden="true"></i>
																					</div>

																					<div class="iwt-text">
																						<a href="<?=$files_path.$arr_property[0]['cee']?>">
																							<span style="color: #5b4254;">C.E.E.</span>
																						</a>
																					</div>

																					<div class="clear"></div>
																				</div>
																			</div>
																		</div>
																	</div>
																<?php } /* if(strlen($arr_property[0]['pdf_sheet']) > 1) { */ ?>
																	<div  class="vc_col-sm-3 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
																		<!-- <a class="column-link" href="<?=$files_path.$arr_property[0]['pdf_sheet']?>"></a> -->
																		<a class="column-link" href="index.php?view=propertyPdf&id=<?=$id?>"></a>
																		<div class="vc_column-inner">
																			<div class="wpb_wrapper">
																				<div class="iwithtext">
																					<div class="iwt-icon">
																						<i class="icon-default-style fa fa-arrow-circle-down" accent-color aria-hidden="true"></i>
																					</div>

																					<div class="iwt-text">
																						<span style="color: #5b4254;">FICHA PDF</span>
																					</div>

																					<div class="clear"></div>
																				</div>
																			</div>
																		</div>
																	</div>
																<?php /* } */ ?>
															</div>
														</div>
													</div>
												</div>
											</div>

											<div  class="vc_col-sm-2 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
												<div class="vc_column-inner">
													<div class="wpb_wrapper"></div>
												</div>
											</div>
										</div>
									</div>

										<div id="fws_5b28785c5cd85"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-content  vc_row-o-equal-height vc_row-flex standard_section" style="padding-top: 0px; padding-bottom: 0px; ">
											<div class="row-bg-wrap">
												<div class="row-bg  using-bg-color" style="background-color: #adc0c0; " data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
											</div>

											<div class="col span_12 dark left">
												<div  class="vc_col-sm-8 mapa wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="left" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
													<div class="vc_column-inner">
														<div class="wpb_wrapper">
															<div id="map_5a6fd1b8d1c41" style="height: 400px;" class="nectar-google-map" data-dark-color-scheme="" data-ultra-flat="" data-greyscale="" data-extra-color="" data-enable-animation="false" data-enable-zoom="" data-zoom-level="14" data-center-lat="<?=$arr_promotion[0]['latitude']?>" data-center-lng="<?=$arr_promotion[0]['longitude']?>" data-marker-img=""></div>
			                                    <div class="map_5a6fd1b8d1c41 map-marker-list">
			                                       <div class="map-marker" data-lat="<?=$arr_promotion[0]['latitude']?>" data-lng="<?=$arr_promotion[0]['longitude']?>" data-mapinfo=""></div>
			                                    </div>
														</div>
													</div>
												</div>

												<div  class="vc_col-sm-4 padr wpb_column column_container vc_column_container col padding-4-percent"  data-bg-cover="" data-padding-pos="left" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
													<div class="vc_column-inner">
														<div class="wpb_wrapper">
															<div class="divider-wrap">
																<div style="height: 20px;" class="divider"></div></div>
																<div class="wpb_text_column wpb_content_element padr">
																	<div class="wpb_wrapper css-location-promo">
																		<?=$arr_promotion[0]['location']?>
																	</div>
																</div>

																<div class="divider-wrap">
																	<div style="height: 15px;" class="divider"></div>
																</div>

																<div id="fws_5b28785c5d85a" data-midnight="" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row standard_section"  style="padding-top: 0px; padding-bottom: 0px; ">
																	<div class="row-bg-wrap">
																		<div class="row-bg" style=""></div>
																	</div>

																	<div class="col span_12  left">
																		<div  class="vc_col-sm-12 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
																			<div class="vc_column-inner">
																				<div class="wpb_wrapper">
																					<div class="divider-wrap">
																						<div style="height: 25px;" class="divider"></div>
																					</div>

																					<!-- <div class="wpb_text_column wpb_content_element">
																						<div class="wpb_wrapper css-location-promo">
											                                    <?=$arr_promotion[0]['location']?>
											                                 </div>
																					</div> -->
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>

											<div id="fws_5b28785c5ddab" data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-content standard_section vertically-align-columns" data-using-ctc="true" style="padding-top: 0px; padding-bottom: 0px; color: #ffffff; ">
												<div class="row-bg-wrap">
													<div class="row-bg using-bg-color" style="background-color: #779c9c; " data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
												</div>
											</div>
										</div>
									</div><!--/post-area-->
								</div>
							</div><!--/container-->
						</div><!--/container-wrap-->
					</div><!--/if portfolio fullwidth-->

					<?php include('html/overall/footer.php') ?>
				</div><!--blurred-wrap-->
				<div id="slide-out-widget-area-bg" class="fullscreen dark"></div>
			</div> <!--/ajax-content-wrap-->

			<div id="key" data-id="<?=$id?>"></div>

			<a id="to-top" class=""><i class="fa fa-angle-up"></i></a>
			<script type="text/javascript">
				function showhide_toggle(a,b,d,f){var e=jQuery("#"+a+"-link-"+b),c=jQuery("a",e),g=jQuery("#"+a+"-content-"+b);a=jQuery("#"+a+"-toggle-"+b);e.toggleClass("sh-show sh-hide");g.toggleClass("sh-show sh-hide").toggle();"true"===c.attr("aria-expanded")?c.attr("aria-expanded","false"):c.attr("aria-expanded","true");a.text()===d?a.text(f):a.text(d)};
			</script>
			<link rel='stylesheet' id='js_composer_front-css'  href='<?=MAINURL?>views/app/resources_2/css/js_composer.min.4.12.1.css' type='text/css' media='all' />
			<link rel='stylesheet' id='sbvcbgslider-style-css'  href='<?=MAINURL?>views/app/resources_2/css/style.4.7.css' type='text/css' media='all' />
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/core.min.1.11.4.js'></script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/widget.min.1.11.4.js'></script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/position.min.1.11.4.js'></script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/menu.min.1.11.4.js'></script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/wp-a11y.min.4.7.js'></script>
			<script type="text/javascript" src="views/app/plugins/bootstrap/js/bootstrap.js"></script>
			<script type='text/javascript'>
				/* <![CDATA[ */
					var uiAutocompleteL10n = {
						"noResults":"Sin resultados.",
						"oneResult":"1 resultado encontrado. Utiliza las teclas de flecha arriba y abajo para navegar.",
						"manyResults":"%d resultados encontrados. Utiliza las teclas arriba y abajo para navegar.",
						"itemSelected":"Elemento seleccionado."
					};
				/* ]]> */
			</script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/autocomplete.min.1.11.4.js'></script>
			<script type='text/javascript'>
				/* <![CDATA[ */
					var MyAcSearch = {"url":"http:\/\/www.careco.es\/wp-admin\/admin-ajax.php"};
				/* ]]> */
			</script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/wpss-search-suggest.js'></script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/jquery.form.min.3.51.0-2014.06.20.js'></script>
			<script type='text/javascript'>
				/* <![CDATA[ */
					var _wpcf7 = {"recaptcha":{"messages":{"empty":"Por favor, prueba que no eres un robot."}}};
				/* ]]> */
			</script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/scripts.4.6.1.js'></script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/nicescroll.3.5.4.js'></script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/sticky.1.0.js'></script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/prettyPhoto.7.0.1.js'></script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/midnight.1.0.js'></script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/superfish.1.4.8.js'></script>
			<script type='text/javascript'>
				/* <![CDATA[ */
					var nectarLove = {"ajaxurl":"http:\/\/www.careco.es\/wp-admin\/admin-ajax.php","postID":"9178","rooturl":"http:\/\/www.careco.es","pluginPages":[],"disqusComments":"false","loveNonce":"a50a0f106b","mapApiKey":"AIzaSyA44aW6ukgUoBzZd5Dn5bsJloe5v0u-5rw"};
				/* ]]> */
			</script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/init.7.6.js'></script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/infinitescroll.1.1.js'></script>
			<script type='text/javascript'>
				/* <![CDATA[ */
					var mejsL10n = {"language":"es-ES","strings":{"Close":"Cerrar","Fullscreen":"Pantalla completa","Turn off Fullscreen":"Salir de pantalla completa","Go Fullscreen":"Ver en pantalla completa","Download File":"Descargar archivo","Download Video":"Descargar v\u00eddeo","Play":"Reproducir","Pause":"Pausa","Captions\/Subtitles":"Pies de foto \/ Subt\u00edtulos","None":"None","Time Slider":"Control de tiempo","Skip back %1 seconds":"Retroceder %1 segundos","Video Player":"Reproductor de v\u00eddeo","Audio Player":"Reproductor de audio","Volume Slider":"Control de volumen","Mute Toggle":"Desactivar sonido","Unmute":"Activar sonido","Mute":"Silenciar","Use Up\/Down Arrow keys to increase or decrease volume.":"Utiliza las teclas de flecha arriba\/abajo para aumentar o disminuir el volumen.","Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.":"Usa las teclas de direcci\u00f3n izquierda\/derecha para avanzar un segundo, y las flechas arriba\/abajo para avanzar diez segundos."}};

					var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/"};
				/* ]]> */
			</script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/mediaelement-and-player.min.2.22.0.js'></script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/wp-mediaelement.min.4.7.js'></script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/flickity.min.1.1.1.js'></script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/nectar-slider.7.6.js'></script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/select2.min.3.5.2.js'></script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/wp-embed.min.4.7.js'></script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/js_composer_front.min.4.12.1.js'></script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/jquery.backstretch.min.1.1.js'></script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/jquery.flexverticalcenter.1.1.js'></script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/script.1.1.js'></script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/map.7.0.7.js'></script>
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/map.7.0.7.js'></script>
			<!-- jQuery validation -->
	      <script type="text/javascript" src="views/app/plugins/jquery-validation/jquery.validate.js"></script>
	      <script type="text/javascript" src="views/app/plugins/jquery-validation/additional-methods.js"></script>
			<!-- Custom JS -->
			<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/js-property-detail.js'></script>
		</body>
</html>
