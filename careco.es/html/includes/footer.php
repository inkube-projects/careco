 <footer id="footer">
         <div class="footer-top wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
            <div class="container text-center">
               <h2 class="css-text-black css-semibold fontSize30">CARECO</h2>
               <p class="css-text-black">Copyright&copy; 2018 CARECO S.A. Carrer del Mar, 44  <br> 46003 Vléncia, Valencia</p>

               <div class="col-md-6 text-center css-noFloat center-block">
                  <a href="#"><i class="fab fa-facebook-square"></i></a>
                  <a href="#"><i class="fab fa-youtube"></i></a>
                  <a href="#"><i class="fab fa-twitter-square"></i></a>
                  <a href="#"><i class="fab fa-pinterest"></i></a>
                  <a href="#"><i class="fab fa-linkedin"></i></a>
               </div>

               <div class="css-links-footer">
                  <a href="https://www.careco.es/promociones-valencia/venta/penya-roja/avenida-francia-alameda">Promociones en curso</a> |
                  <a href="https://www.careco.es/proximas-promociones-valencia">Próximas promociones</a> |
                  <a href="https://www.careco.es/asi-somos">Careco</a> |
                  <a href="https://www.careco.es/blog">Blog</a> |
                  <a href="https://www.careco.es/atencion-al-cliente">Atención al cliente</a> |
                  <a href="https://www.careco.es/legal">Legal</a>
               </div>
            </div>
         </div>
      </footer>