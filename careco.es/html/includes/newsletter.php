      <section class="css-newsletter">
         <div class="container">
            <h2 class="css-marginB0 css-medium">
               <i class="fa fa-envelope"></i><br>
               Suscíbete a nuestra Newsletter
            </h2>
            <p class="css-fontSize14">Siempre estarás informado de todo lo que ocurre en Careco <br> (Noticias, Novedades, Consejos, Promociones, etc...)</p>

            <form name="frm-newsletter" id="frm-newsletter" method="post" action="">
               <div class="input-group css-newsletter-container">
                  <input type="text" id="newsletter_email" name="newsletter_email" class="form-control" placeholder="Introduce tu e-mail">

                  <div class="input-group-btn">
                     <button type="submit" class="btn btn-default js-submit">Envíar</button>
                  </div>
               </div>
               <div class="text-danger" id="newsletter_email_validate"></div>
            </form>
         </div>
      </section>

      <section class="css-info">
         <div class="container wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
            <h2 class="text-center css-medium css-fontSize20">Certificaciones y premios</h2>
            <img src="views/images/certifications.jpg" class="img-responsive css-noFloat center-block" alt="">
         </div>
      </section>
      
            <!-- Modal newsletter -->
      <div class="modal fade" tabindex="-1" role="dialog" id="mod-newsletter">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Newsletter</h4>
               </div>
               <div class="modal-body">
                  <p class="newsletter-message"></p>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
               </div>
            </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->