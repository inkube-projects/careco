<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="title" content="CARECO | Legal">
      <title>CARECO | Legal</title>
      <meta name="robots" content="noindex">
      <link href="views/app/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="views/app/resources_1/css/animate.min.css" rel="stylesheet">
      <link href="views/app/plugins/font-awesome/web-fonts-with-css/css/fontawesome-all.min.css" rel="stylesheet">
      <link href="views/app/resources_1/css/lightbox.css" rel="stylesheet">
      <link href="views/app/resources_1/css/main.css" rel="stylesheet">
      <link href="views/app/resources_1/css/presets/preset1.css" id="css-preset" rel="stylesheet">
      <link href="views/app/resources_1/css/responsive.css" rel="stylesheet">
      <!--[if lt IE 9]>
         <script src="js/html5shiv.js"></script>
         <script src="js/respond.min.js"></script>
      <![endif]-->
      <link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
      <link rel="shortcut icon" href="views/images/favicon.ico">
      <link href="views/app/resources_1/css/css-style.css" rel="stylesheet">
      <style>
	  	.carousel-fade .carousel-inner .item {
    background-position: center;
    height: 331px;
}
	  </style>
   </head>
   <!--/head-->

   <body>
      <!--.preloader-->
      <div class="preloader"> <i class="fas fa-circle-notch fa-spin"></i></div>
      <!--/.preloader-->

      <header id="home">
         <nav class="css-nav-top">
            <div class="container">
               <div class="col-md-6">
                  <i class="fas fa-phone"></i> +34 687 715 531
                  <i class="fas fa-envelope css-marginL30"></i> careco@careco.es
               </div>

               <div class="col-md-6 text-right">
                  <a href="#"><i class="fab fa-linkedin"></i></a>
                  <a href="#"><i class="fab fa-instagram"></i></a>
               </div>
            </div>
         </nav>

         <nav class="navbar navbar-default">
            <div class="container">
               <div class="container-fluid">
                  <div class="navbar-header">
                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                     </button>
                     <a class="navbar-brand" href="https://www.careco.es/">
                        <img src="views/images/logo-high-n-01.png" alt="CARECO" width="120">
                     </a>
                  </div>

                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav navbar-right">
                        <li><a href="https://www.careco.es/promociones-valencia/comprar-chalet-adosado/penya-roja/avenida-francia-alameda">Promociones en curso <div class="css-nav-separator"></div></a></li>
                        <li><a href="https://www.careco.es/proximas-promociones-valencia">Próximas promociones <div class="css-nav-separator"></div></a></li>
                        <li><a href="https://www.careco.es/asi-somos">Careco <div class="css-nav-separator"></div></a></li>
                        <li><a href="https://www.careco.es/blog">Blog <div class="css-nav-separator"></div></a></li>
                        <li><a href="https://www.careco.es/atencion-al-cliente">Atención al cliente <div class="css-nav-separator"></div></a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </nav>
         <!--/#main-nav-->

         <div id="home-slider" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
               <div class="item active" style="background-image: url(views/images/slider/2.jpg)">
                  <div class="caption">
                     <div class="container">
                        <div class="col-md-6 text-left">
                           <a data-scroll class="btn btn-start btn-green animated fadeInUpBig btn-slide" href="#">CONSTRUIMOS CONTIGO</a>
                           <p class="animated fadeInRightBig css-text-black css-medium css-marginT20">Queremos estar a tú lado<br> en cada paso que des.</p>
                        </div>

                        <div class="col-md-6">
                           <!-- <h1 class="animated fadeInLeftBig">CAR<span>ECO</span></h1> -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- <a class="left-control" href="#home-slider" data-slide="prev"><i class="fa fa-angle-left"></i></a>
            <a class="right-control" href="#home-slider" data-slide="next"><i class="fa fa-angle-right"></i></a> -->
            <a id="tohash" href="#portfolio"><i class="fa fa-angle-down css-text-black"></i></a>
         </div>
      </header>

		<section id="legal">
        	<div class="container">
            	<div class="col-xs-12">
                
               
               
               <H3> CONDICIONES GENERALES DE USO DEL SITIO WEB WWW.CARECO.ES</H3>

<h3>1.	Introducción</h3></h3>
El presente documento tiene como finalidad el establecer y regular las normas de uso del Sitio www.careco.es (en adelante el "Sitio"), entendiendo por Sitio todas las páginas y sus contenidos propiedad de CARECO S.A. a las cuales se accede a través del dominio www.careco.es y sus subdominios. La utilización del Sitio atribuye la condición de usuario del mismo e implica la aceptación de todas las condiciones incluidas en el presente Aviso Legal. El usuario se compromete a leer atentamente el presente Aviso Legal en cada una de las ocasiones en que se proponga utilizar el Sitio, ya que éste y sus condiciones de uso recogidas en el presente Aviso Legal pueden sufrir modificaciones.

<h3>2.	Titularidad del Sitio Web</h3>
El titular del presente Sitio es Construcciones CARECO S.A. con domicilio en Carrer del Mar, 44, 46003 València - España.<br>
Email: careco@careco.es<br>
Teléfono: 96 315 43 80 <br>


<h3>3.	Política de protección de datos personales</h3>
La Política de Privacidad forma parte de las Condiciones Generales que rigen la Página Web www.careco.es, junto con la Política de Cookies y el Aviso Legal. Careco se reserva el derecho a modificar o adaptar la presente Política de Privacidad en todo momento. Por lo tanto, le recomendamos que revise la misma cada vez que acceda a la Página Web. En el supuesto de que usuario se haya registrado en la página web y acceda a su cuenta o perfil, al acceder a la misma, se le informará en el supuesto de que haya habido modificaciones sustanciales en relación con el tratamiento de sus datos personales.<br><br>

<h5 style="margin-right:40px;">3.1.	¿Quién es el RESPONSABLE DEL TRATAMIENTO?</h5>
Los datos que se recojan o nos facilites voluntariamente por medio de la Página Web, ya sea por la navegación en la misma, así como todos aquellos que nos puedas facilitar en los formularios de contacto, vía email o por teléfono, serán recabados y tratados por Careco, cuyos datos se indican a continuación: Identidad Construcciones CARECO S.A. con domicilio en Carrer del Mar, 44, 46003 València - España. Teléfono: 96 315 43 80  Dirección de correo electrónico careco@careco.es Información Delegado de Protección de datos Si, por cualquier motivo, quieres ponerte en contacto con nosotros en cualquier cuestión relacionada con el tratamiento de sus datos personales o privacidad (con nuestro Delegado de Protección de Datos), puedes hacerlo a través de cualquiera de los medios indicados anteriormente.<br><br>

<h5 style="margin-right:40px;">3.2.	¿Cuáles son tus DERECHOS en relación con el tratamiento de tus datos?</h5>
Cualquier persona tiene derecho a obtener información sobre la existencia del tratamiento de sus datos personales, acceder a la información y datos personales que Careco tiene, solicitar la rectificación de los datos inexactos o, en su caso, solicitar la supresión, cuando, entre otros motivos, los datos ya no sean necesarios para los fines para los que fueron recogidos o el interesado retire el consentimiento que haya otorgado. En determinados supuestos, el interesado podrá solicitar la limitación del tratamiento de sus datos, en cuyo caso, solo los conservaremos de acuerdo con la normativa vigente. En algunos casos, puede ejercitar su derecho a la portabilidad de los datos, que serán entregados en un formato adecuado, de uso común o lectura mecánica a usted o el nuevo responsable del tratamiento que designe. Tiene derecho a revocar en cualquier momento el consentimiento para cualquiera de los tratamientos para los que lo has otorgado. Careco dispone de formularios para el ejercicio de cualquiera de los citados derechos, para lo que deberá ponerse en contacto con nosotros en la siguiente dirección de correo electrónica careco@careco.es, solicitando el modelo que necesite. Del mismo modo, puedes utilizar los que la Agencia Española de Protección de Datos o terceros ponen a disposición de los interesados. Estos formularios deberán ir firmados electrónicamente o ir acompañados de fotocopia del DNI. Si se actúa por medio de representante, además, deberá ir acompañado de su copia de DNI o firma electrónica. Los formularios deberán ser presentados presencialmente en el domicilio de Careco indicado anteriormente o, en su caso, enviados por correo electrónico. Tiene derecho a presentar una reclamación ante la Agencia Española de Protección de Datos, en el supuesto de que consideres que no se ha atendido convenientemente la solicitud de tus derechos. El plazo máximo para resolver por Careco es de un mes, a contar desde la efectiva recepción de tu solicitud por nuestra parte.
<br><br>
<h5 style="margin-right:40px;">3.3.	¿Qué Datos RECOPILAMOS a través de la Página Web?</h5>
<h5 style="margin-right:80px;">3.3.1.	Datos almacenados por la navegación y uso de la Página Web</h5>
Por el simple hecho de navegar en la Página Web, Careco recopilará información referente a: -	Dirección IP -	Versión del navegador -	Sistema operativo - Duración de la visita o navegación por la Página web Tal información se almacena mediante Google Analytics, por lo que nos remitimos a la Política de Privacidad de Google, ya que éste recaba y trata tal información.<br>
<br>
 http://www.google.com/intl/en/policies/privacy/ Del mismo modo, en la Página Web se facilita la utilidad de Google Maps, la cual puede tener acceso a tu ubicación, en el supuesto de que se lo permitas, con el fin de facilitarte una mayor especificidad sobre la distancia y/o caminos nuestras sedes. A este respecto, nos remitimos a la Política de Privacidad utilizada por Google Maps, con el fin de conocer el uso y tratamiento de tales datos http://www.google.com/intl/en/policies/privacy/ La información que nosotros manejamos, no estará relacionada con un usuario concreto y se almacenará en nuestras bases de datos, con la finalidad de realizar análisis estadísticos, mejoras en la Página Web, sobre nuestros productos y/o servicios y nos ayudará a mejorar nuestra estrategia comercial. Los datos no serán comunicados a terceros.
<br><br>
<h5 style="margin-right:80px;">3.3.2.	Registro del usuario en la Página Web</h5>
Para acceder a determinados productos y/o servicios, es preciso que el usuario se registre en la Página Web. Para ello, en el formulario de registro, se solicitan una serie de datos personales. Los datos necesario y obligatorios a facilitar por el usuario para llevar a cabo tal registro, se encuentran marcados con el símbolo *. En el caso de no facilitar tales campos, no se llevará a cabo el registro. El usuario y la contraseña generados son personales e intransferibles, siendo el usuario responsable de su custodia. No recomendamos que la apunte en algún sitio y tampoco que la notifique terceros. Tenga en cuenta que el sistema tiene una funcionalidad para restablecer contraseña que puede ser usado tantas veces como lo necesite. Si no recuerdas tu contraseña la puede recuperar desde el área de acceso a tu cuenta, pulsando el enlace, "Has olvidao tu contraseña? y seguir las instrucciones. En este caso, los datos de navegación, se asociarán a los de registro del usuario, identificando al mismo usuario concreto que navega por la Página Web. De esta forma, personalizar la oferta de productos y/o servicios que, a nuestro criterio, más se ajuste al usuario, así como recomendarte determinados productos y/o servicios. Los datos de registro de cada usuario se incorporarán a las bases de datos de Careco junto con el historial de operaciones realizadas por el mismo, y se almacenarán en las mismas mientras no se elimine la cuenta del usuario registrado. Una vez eliminada tal cuenta, dichos datos serán apartados de nuestras bases de datos, manteniéndolos apartados durante 10 años aquellos datos relativos a las transacciones realizadas, sin que se accedan o alteren los mismos, con el fin de dar cumplimiento a los plazos legalmente vigentes. Los datos que no se encuentren vinculados a las transacciones realizadas se mantendrán durante un periodo de 5, años salvo que retire el consentimiento en cuyo caso serán eliminados inmediatamente. La base legal para el tratamiento de sus datos personales es la ejecución de un contrato entre las partes, en la que el usuario solicita de Careco una serie de productos/ servicios/ utilidades... La base legal para el tratamiento de sus datos es la ejecución de un contrato consistente en una plataforma destinada a la comunicación de novedades y noticias de interés. En relación con la remisión de comunicaciones y promociones por vía electrónica y la respuesta a las solicitudes de información, la legitimación del tratamiento es el consentimiento del afectado Las finalidades del tratamiento, serán las siguientes: a)	Gestionar el alta en el área de registro de usuario y su acceso a la Página Web b)	Gestionar la compra de los productos y/o servicios puestos a tu disposición a través de la Página Web c)	Mantenerle informado de la tramitación y estado de sus compras d)	responder a su solicitud de información e)	Gestionar todas las utilidades que ofrece la plataforma a alumnos, asociados, colegiados… Así, le informamos que podrá recibir comunicaciones vía email y/o en su teléfono, con el fin de informarle de posibles incidencias, errores, problemas y/o estado de sus pedidos. Sus datos personales podrán ser facilitados a terceras personas, con el fin de llevar a cabo determinadas gestiones y/o servicios solicitados por el usuario: Supuesto	Terceros a los que se comunica o facilitan los datos En el supuesto de realizar un pedido a través de la página web	- Empresas de reparto - Plataforma de pago - Posibles intermediarios - Empresa de mantenimiento web - Empresa de hosting - Transferencias internacionales
<br><br>
Para el envío de comunicaciones comerciales, se solicitará el consentimiento expreso del usuario a la hora de realizar el registro. A este respecto, el usuario podrá revocar el consentimiento prestado, dirigiéndose a Careco haciendo uso de los medios indicados anteriormente. En cualquier caso, en cada comunicación comercial, se le dará la posibilidad de darse de baja en la recepción de las mismas, ya sea por medio de un link y/o dirección de correo electrónico.
<br><br>
<h5 style="margin-right:80px;">3.3.3.	Datos facilitados en el formulario de contacto</h5>
Pueden ponerse en contacto directamente con nosotros a través del apartado "Contacta con Nosotros" facilitados en la Página Web. Para ello, es necesario que el Usuario se identifique e indique sus datos personales, con el fin de que Careco pueda contactar con el usuario, en caso de que sea necesario para atender su solicitud de información. Los datos necesarios y obligatorios a facilitar por el usuario para llevar a cabo tal registro, se encuentran marcados con el símbolo *. En el caso de no facilitar tales campos, no se permitirá el envío de su petición. Tales datos personales, serán incorporados a las bases de datos de Careco, que conservará los mismos para contestar a su solicitud y/o petición de información y, tras ello, serán eliminados en el plazo de 5 años. La base legal para el tratamiento de tales datos es el consentimiento del usuario que solicita información, o tiene la necesidad de mantener contacto con Careco Las finalidades del tratamiento serán las siguientes: a) Gestionar las consultas o solicitudes de información que nos envíes a través de la Página Web, correo electrónico o teléfono b) Envío de comunicaciones, promociones especiales, noticias o acciones que resulten de tu interés o nos solicites incluso por medios electrónicos. Al tratarse de una finalidad accesoria a la principal, deberás marcar la casilla habilitada a tal efecto. Los datos personales que nos facilites por este medio, no serán comunicados a terceros, siendo Careco quien dé, directamente, respuesta a este tipo de consultas. Por otra parte, se informa de que pueden tener acceso a tus datos personales: Supuesto	Terceros a los que se comunica o pueden tener acceso a los datos Empresa de Manto y Soporte de la Página Web	Servicio de Hosting	Transferencias internacionales
<br><br>
<h5 style="margin-right:80px;">3.3.4.	Envío de Newsletter</h5>
En la Página Web, se permite la opción de suscribirse a la Newsletter de Careco. Para ello, es necesario que nos facilite una dirección de correo electrónico a la que se remitirá la misma. Tal información será almacenada en una base de datos de Careco en la cual quedará registrada hasta que el interesado solicite la baja de la misma o, en su caso, se cese por parte de Careco del envío de la misma. La base legal para el tratamiento de estos datos personales es el consentimiento expreso prestado por todos aquellos interesados que se suscriban en este servicio marcando la casilla destinada a tal efecto. Los datos de los correos electrónicos serán únicamente tratados y almacenados con la finalidad de gestionar el envío de la Newsletter por parte de los usuarios que soliciten la misma. Para ello, se hace uso de una aplicación para el envío automático de la Newsletter, teniendo la misma acceso a las direcciones de correo electrónico: Nombre de la aplicación	Titular de la aplicación /País.
<br><br>
Para el envío de la Newsletter, se solicitará el consentimiento expreso del usuario a la hora de realizar el registro en ella marcando la casilla destinada a tal efecto. A este respecto, el usuario podrá revocar el consentimiento prestado, dirigiéndose Careco haciendo uso de los medios indicados anteriormente. En cualquier caso, en cada comunicación, se le dará la posibilidad de darse de baja en la recepción de las mismas, ya sea por medio de un link y/o dirección de correo electrónico. El envío de la newsletter se realiza utilizando la plataforma propia intregada en el servidor de careco.es, radicada en EEUU por lo que se produce una transferencia internacional de datos amparada por el acuerdo Unión Europea – Estados Unidos denominado Privacy Shield https://www.privacyshield.gov/list
<br><br>
<h5 style="margin-right:80px;">3.3.5.	"Cookies"</h5>
Este Sitio Web utiliza "cookies". Las "cookies" son unos pequeños ficheros de datos que el servidor de Internet remite al navegador y que le son devueltos posteriormente en cada nueva petición. Estos ficheros se almacenan en el ordenador del usuario y permiten al sistema recordar características o preferencias de navegación de sesiones anteriores. La finalidad de estas cookies es mejorar el servicio que ofrecemos a los Usuarios. Las "cookies" de esta Web no son invasivas ni nocivas. Más información en: Política de cookies
<br><br>
<h5 style="margin-right:40px;">3.4.	Redes sociales</h5>
Al hacerte fan, seguidor o análogos de nuestra empresa en la vertiente de las distintas redes sociales, en el contexto de este tratamiento debes tener en cuenta que Careco únicamente puede consultar o dar de baja tus datos de forma restringida al tener un perfil específico. Cualquier rectificación de tus datos o restricción de información o de publicaciones debes realizarla a través de la configuración de tu perfil o usuario en la propia red social. Por defecto consientes: a) El tratamiento de tus datos personales en el entorno de dicha red social y conforme a sus políticas de Privacidad: <br>
<br>
Facebook http://www.facebook.com/policy.php?ref=pf <br>
<br>
Twitter http://twitter.com/privacy <br>
<br>
Linkedin	http://www.linkedin.com/legal/privacy-policy?trk=hb_ft_priv <br>
<br>
Pinterest	https://about.pinterest.com/es/privacy-policy <br>
<br>
Google* http://www.google.com/intl/es/policies/privacy/ *(Google+ y Youtube) b) el acceso de Careco a los datos contenidos en tu perfil o biografía, dependiendo de la configuración que tengas de tu privacidad en cada red, estos serán más o menos amplios. c) A que las noticias publicadas sobre nuestros eventos, o nuestros comentarios pueda aparecer en tu muro o biografía. d) A recibir comunicaciones sobre nuestros productos/eventos. Si quieres dejar de seguirnos, sólo tienes que pinchar la opción "Dejar de ser fan" o “dejar de seguir”. Puedes ejercer los derechos de acceso, rectificación, cancelación y oposición en cualquier momento, mediante escrito, dirigido a la dirección que aparece arriba o enviando un e-mail a careco@careco.es
<br><br>
<h3>4.	Propiedad intelectual e industrial</h3>
Los derechos de propiedad intelectual de este sitio, son titularidad de COSMECEUTICAL CENTER SLU (en adelante “Careco”). La reproducción, distribución, comercialización o transformación, total o parcial, no autorizadas del contenido del Sitio, a no ser que sea para uso personal y privado, constituye una infracción de los derechos de propiedad intelectual de Careco Igualmente, todas las marcas o signos distintivos de cualquier clase contenidos en el Sitio están protegidos por Ley. La utilización no autorizada de la información contenida en este Sitio, así como los perjuicios ocasionados en los derechos de propiedad intelectual e industrial de Careco, pueden dar lugar al ejercicio de las acciones que legalmente correspondan y, si procede, a las responsabilidades que de dicho ejercicio se deriven.
<br><br>
<h3>5.	Exclusión de responsabilidad</h3>
El contenido, información y/o consejos expresados en este Sitio deben entenderse como simplemente orientativos. Careco no responde de ninguna forma de la efectividad o exactitud de los mismos, quedando exenta de cualquier responsabilidad contractual o extracontractual con los usuarios que haga uso de ellos, ya que son éstas las que deberán decidir según su criterio la oportunidad de los mismos. En este Sitio se pueden publicar contenidos aportados por terceras personas o empresas, Careco no responde de la veracidad y exactitud de los mismos, quedando exenta de cualquier responsabilidad contractual o extracontractual con los usuarios que hagan uso de ellos. Careco se reserva el derecho de modificar el contenido del Sitio sin previo aviso y sin ningún tipo de limitación. Careco declina cualquier responsabilidad por los eventuales daños y perjuicios que puedan ocasionarse por la falta de disponibilidad y/o continuidad de este Sitio y de los servicios que se ofrecen en él. Careco no garantiza la ausencia de virus ni de otros elementos en la Web que puedan producir alteraciones en su sistema informático. Careco declina cualquier responsabilidad contractual o extracontractual con los usuarios que hagan uso de ello y tuviera perjuicios de cualquier naturaleza ocasionados por virus informáticos o por elementos informáticos de cualquier índole. Careco declina cualquier responsabilidad por los servicios que eventualmente pudieran prestarse en el Sitio por parte de terceros. Careco declina cualquier responsabilidad por los servicios y/o información que se preste en otros Sitios enlazados con este. Careco no controla ni ejerce ningún tipo de supervisión en Sitios Webs de terceros. Aconsejamos a los usuarios de los mismos a actuar con prudencia y consultar las eventuales condiciones legales que se expongan en dichas Webs. Los Usuarios que remitan cualquier tipo de información a Careco se comprometen a que la misma sea veraz y que no vulnere cualquier derecho de terceros ni la legalidad vigente.
<br><br>
<h3>6.	Condiciones de uso del portal para los usuarios</h3>
El acceso al presente Sitio es gratuito salvo en lo relativo al coste de la conexión a través de la red de telecomunicaciones suministrada por el proveedor de acceso contratado por los usuarios. Careco actualmente no tiene conocimiento efectivo de que la actividad o la información a la que remiten o recomiendan es ilícita o de que lesiona bienes o derechos de un tercero susceptibles de indemnización. Careco si tuviera conocimiento de que alguno de los enlaces es ilícito, actuaría con diligencia para suprimir o inutilizar el enlace correspondiente. En este sentido ningún órgano competente ha declarado la ilicitud de los datos, ordenado su retirada o imposibilitado su acceso. Queda expresamente prohibido el uso del Sitio con fines lesivos de bienes o intereses de Careco o de terceros o que de cualquier otra forma sobrecarguen, dañen o inutilicen las redes, servidores y demás equipos informáticos (hardware) o productos y aplicaciones informáticas (software) de Careco o de terceros. En el caso de que el usuario tuviera conocimiento de que los Sitios enlazados remiten a páginas cuyos contenidos o servicios son ilícitos, nocivos, denigrantes, violentos o contrarios a la moral le agradeceríamos que se pusiera en contacto con Careco.
<br><br>
<h3>7.	Foros, comentarios y opiniones. EN SU CASO</h3>
Las opiniones expresadas en los foros de discusión, y/o comentarios y opiniones son formuladas por personas ajenas a Careco bajo su única y exclusiva responsabilidad. Los participantes en los foros se comprometen a utilizar los mismos en conformidad con la ley, las condiciones generales reflejadas en este Aviso Legal, así como con la moral y buenas costumbres generalmente aceptadas. Los participantes de los foros se obligan a indemnizar a Careco por cualquier daño o perjuicio que pudiera ocasionar por el uso de los mismos, infringiendo estas condiciones generales y/o la legalidad vigente. Careco no responde de la veracidad y exactitud de las opiniones expresadas en los foros, quedando exenta de cualquier responsabilidad contractual o extracontractual con la persona o empresa que hagan uso de ellos. Se prohíbe el envío de cualquier contenido u opinión que vulnere la legalidad vigente y/o derechos legítimos de otras personas. Careco se reserva el derecho de adoptar las medidas legales que considere oportunas. Careco se reserva el derecho de admitir y/o dar de baja cualquier opinión y/o usuario, sin previo aviso y sin ningún tipo de justificación o compensación de cualquier tipo. Careco se reserva el derecho de modificar el texto de las opiniones expresadas para adaptarlos a las características de cada foro en concreto. Careco se reserva el derecho de asignar la opinión a la sección y/o subsección que estime más conveniente. Si vd. cree que cualquier contenido y/o información de este Sitio vulnera un derecho legítimo o la legalidad vigente le agradeceríamos que se pusiera en contacto con Careco para que podamos tomar las medidas oportunas.
<br>
<h3>8.	Recomendaciones de visualización</h3>
La web está adaptada para una correcta visualización en diferentes dispositivos y móviles que cumplan con los estandarés webs y en versiones modernas de navegadores webs.
<br><br>
<h3>9.	Legislación</h3>
El presente Aviso Legal se rige en todos y cada uno de sus extremos por la Legislación española.
<br><br>
<h3>10.	Contacte con nosotros</h3>
Si tiene Ud. cualquier pregunta sobre las condiciones reflejadas en este Aviso Legal o si desea hacer cualquier sugerencia o recomendación, por favor diríjase a nosotros a través de la siguiente dirección de correo electrónico: careco@careco.es.
                
                </div>
            </div>
        </section>



       <?php include('html/overall/contact.php') ?>
      <?php include('html/includes/newsletter.php') ?>
      <?php include('html/includes/footer.php') ?>


      <script type="text/javascript" src="views/app/resources_1/js/jquery.js"></script>
      <script type="text/javascript" src="views/app/plugins/bootstrap/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="//maps.google.com/maps/api/js?key=AIzaSyAfoFMSNU6Vr-JaVW4BN86B_VGkCwowO_w&sensor=true"></script>
      <script type="text/javascript" src="views/app/resources_1/js/jquery.inview.min.js"></script>
      <script type="text/javascript" src="views/app/resources_1/js/wow.min.js"></script>
      <script type="text/javascript" src="views/app/resources_1/js/mousescroll.js"></script>
      <script type="text/javascript" src="views/app/resources_1/js/smoothscroll.js"></script>
      <script type="text/javascript" src="views/app/resources_1/js/jquery.countTo.js"></script>
      <script type="text/javascript" src="views/app/resources_1/js/lightbox.min.js"></script>
      <script type="text/javascript" src="views/app/resources_1/js/main.js"></script>
      <!-- jQuery validation -->
      <script type="text/javascript" src="views/app/plugins/jquery-validation/jquery.validate.js"></script>
      <script type="text/javascript" src="views/app/plugins/jquery-validation/additional-methods.js"></script>
      <!-- newsletter -->
      <script type="text/javascript" src="views/app/resources_1/js/js-newsletter.js"></script>
      <script type="text/javascript" src="views/app/resources_1/js/js-contact.js"></script>
   </body>
</html>
