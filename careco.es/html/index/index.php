<?php
require_once 'html/overall/Mobile_Detect.php';
$detect = new Mobile_Detect;
?>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?=APP_TITLE?> | CASA COMO TÚ :: HOME</title>
        <link href="views/app/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="views/app/resources_1/css/animate.min.css" rel="stylesheet">
        <link href="views/app/plugins/font-awesome/web-fonts-with-css/css/fontawesome-all.min.css" rel="stylesheet">
        <link href="views/app/resources_1/css/lightbox.css" rel="stylesheet">
        <link href="views/app/resources_1/css/main.css" rel="stylesheet">
        <link href="views/app/resources_1/css/presets/preset1.css" id="css-preset" rel="stylesheet">
        <link href="views/app/resources_1/css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        <link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" href="views/images/favicon.ico">
        <link href="views/app/resources_1/css/css-style.css" rel="stylesheet">
        <?php include('html/overall/analytics.php') ?>
    </head>
    <!--/head-->

    <body>
        <!--.preloader-->
        <div class="preloader"> <i class="fas fa-circle-notch fa-spin"></i></div>
        <!--/.preloader-->

        <header id="home">
            <nav class="css-nav-top">
                <div class="container">
                    <div class="col-md-6">
                        <i class="fas fa-phone"></i> +34 687 715 531
                        <i class="fas fa-envelope css-marginL30"></i> careco@careco.es
                    </div>

                    <div class="col-md-6 text-right">
                        <a href="#"><i class="fab fa-linkedin"></i></a>
                        <a href="#"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </nav>

            <nav class="navbar navbar-default main-nav">
                <div class="container">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#">
                                <img src="views/images/logo-high-n-01.png" alt="CARECO" width="120">
                            </a>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <!--<li><a href="index.php?view=currentPromotion">Promociones en curso <div class="css-nav-separator"></div></a></li>
                                <li><a href="index.php?view=nextPromotion">Próximas promociones <div class="css-nav-separator"></div></a></li>
                                <li><a href="index.php?view=ourWork">Promociones realizadas<div class="css-nav-separator"></div></a></li>-->
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Promociones en Valencia <div class="css-nav-separator"></div></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="https://www.careco.es/promociones-valencia/comprar-chalet-adosado/penya-roja/avenida-francia-alameda">Promociones en curso <i class="fas fa-angle-right css-text-green css-marginL10"></i></a></li>
                                        <li><a href="https://www.careco.es/proximas-promociones-valencia">Próximas promociones <i class="fas fa-angle-right css-text-green css-marginL10"></i></a></li>
                                        <li><a href="https://www.careco.es/nuestras-obras">Promociones realizadas<i class="fas fa-angle-right css-text-green css-marginL10"></i></a></li>
                                    </ul>
                                </li>
                                <li><a href="https://www.careco.es/asi-somos">Careco <div class="css-nav-separator"></div></a></li>
                                <li><a href="https://www.careco.es/blog">Blog <div class="css-nav-separator"></div></a></li>
                                <li><a href="https://www.careco.es/atencion-al-cliente">Atención al cliente <div class="css-nav-separator"></div></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <!--/#main-nav-->

            <?php
            // Móvil Exclude tablets.
            if( $detect->isMobile() && !$detect->isTablet() ){ include ("html/overall/slider_movil.php"); }
            else { include ("html/overall/slider_pc.php"); }
            ?>
        </header>

        <section id="portfolio">
            <div class="container">
                <div class="col-md-6 css-borde-right">
                    <div class="text-right">
                        <h3 class="css-promotion-title"><span class="css-text-black">PROMOCIONES</span> EN CURSO<i class="fas fa-angle-down css-text-green css-marginL10"></i></h3>
                        <p>En careco contamos con variedad de promociones <br> en zonas de Valencia y en poblaciones de <br> alrededores.</p>
                        <a href="https://www.careco.es/promociones-valencia/comprar-chalet-adosado/picanya" class="btn btn-start css-marginR10">Venta</a>
                        <a href="https://www.careco.es/promociones-valencia/alquiler/penya-roja/avenida-francia-alameda" class="btn btn-start">Alquiler</a>
                    </div>

                    <div class="row css-marginT30">
                        <?php foreach ($arr_current_promo as $val):
                            if($val['id'] == 15) $url2 = 'penya-roja/avenida-francia-alameda'; else if($val['id'] == 16) $url2 = 'picanya'; ?>

                            <div class="col-md-6">
                                <div class="folio-item wow fadeInRightBig" data-wow-duration="1000ms" data-wow-delay="300ms">
                                    <div class="folio-image css-promotion-panel" style="background-image: url('<?=$val['image']?>')">
                                        <!-- <div class="css-tag">Venta</div> -->
                                    </div>

                                    <div class="overlay">
                                        <a href="https://www.careco.es/promociones-valencia/comprar-chalet-adosado/<?=$url2?>" >
                                            <div class="overlay-content">
                                                <div class="overlay-text">
                                                    <div class="folio-info">
                                                        <h3 class="css-marginB0 css-semibold css-marginT20">Promoción de viviendas</h3>
                                                        <p class="css-marginT0 css-semibold"><?=$val['title']?></p>
                                                        <p class="pull-left"><?=substr($val['desc'],0,45)?>...</p>
                                                        <i class="fas fa-angle-right pull-right css-fontSize20"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <div class="row fadeInRight" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="col-md-6">
                            <p class="css-text-black">
                                <span class="css-bold"><?=@$arr_current_promo[0]['title']?></span> <!-- <span class="css-text-green">(Valencia)</span> -->
                            </p>
                        </div>

                        <div class="col-md-6">
                            <p class="css-text-black"><span class="css-bold"><?=@$arr_current_promo[1]['title']?></span> <!-- <span class="css-text-green">(Valencia)</span> --></p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="text-left">
                        <h3 class="css-promotion-title"><i class="fas fa-angle-down css-text-green css-marginR10"></i> PRÓXIMAS <span class="css-text-black">PROMOCIONES</span></h3>
                        <p>En careco nos gusta ir un paso por delante y anticiparnos <br> para ofrecerte promociones futuras <br> en las mejores zonas de Valencia.</p>
                        <a href="https://www.careco.es/proximas-promociones-valencia" class="btn btn-start">Ver todas</a>
                    </div>

                    <div class="row css-marginT30">
                        <?php foreach ($arr_next_promo as $val): ?>
                            <div class="col-md-6">
                                <div class="folio-item wow fadeInRightBig" data-wow-duration="1000ms" data-wow-delay="300ms">
                                    <div class="folio-image css-promotion-panel" style="background-image: url('<?=$val['image']?>')">
                                        <!-- <div class="css-tag">Venta</div> -->
                                    </div>

                                    <div class="overlay">
                                        <?php
                                            if($val['id'] == 12) $url = 'venta-naquera';
                                            else if($val['id'] == 11) $url = 'venta-rocafort';
                                        ?>

                                        <a href="https://www.careco.es/proximas-promociones-valencia/<?=$url?>" >
                                            <div class="overlay-content">
                                                <div class="overlay-text">
                                                    <div class="folio-info">
                                                        <h3 class="css-marginB0 css-semibold css-marginT20">Próximas promociones</h3>
                                                        <p class="css-marginT0 css-semibold"><?=$val['title']?></p>
                                                        <p class="pull-left"><?=substr($val['desc'],0,45)?>...</p>
                                                        <i class="fas fa-angle-right pull-right css-fontSize20"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <div class="row fadeInRight" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="col-md-6">
                            <p class="css-text-black"><span class="css-bold"><?=@$arr_next_promo[0]['title']?></span> <!-- <span class="css-text-green">(Valencia)</span> --></p>
                        </div>

                        <div class="col-md-6">
                            <p class="css-text-black"><span class="css-bold"><?=@$arr_next_promo[1]['title']?></span> <!-- <span class="css-text-green">(Valencia)</span> --></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="features">
            <div class="">
                <div class="container">
                    <h2 class="css-semibold">CARECO</h2>
                    <p>
                        <i class="fas fa-quote-left"></i>
                        Llevamos trabajando casi 70 años, <br> para ser un referente de calidad en todos nuestros proyectos
                        <i class="fas fa-quote-right"></i>
                    </p>
                </div>
            </div>
        </section>

        <section id="services">
            <div class="container">
                <div class="text-center our-services">
                    <div class="row">
                        <div class="col-sm-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                            <div class="service-icon">
                                <i class="fas fa-home"></i>
                            </div>
                            <div class="service-info">
                                <h3 class="css-text-black css-semibold">Careco</h3>
                                <p>En CARECO pensamos que la experiencia y la fiabilidad son los mejores avales en una empresa de construcción como la nuestra.</p>
                                <a href="https://www.careco.es/asi-somos" class="btn btn-start">+ Info</a>
                            </div>
                        </div>

                        <div class="col-sm-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                            <div class="service-icon">
                                <i class="fas fa-sitemap"></i>
                            </div>
                            <div class="service-info">
                                <h3 class="css-text-black css-semibold">Nuestros hitos</h3>
                                <p>Desde 1947 la familia Casanova ha sido un referente en el mercado de la construcción en Valencia. Conoce los principales hitos e innovaciones que nos han hecho diferentes.</p>
                                <a href="https://www.careco.es/nuestros-hitos" class="btn btn-start">+ Info</a>
                            </div>
                        </div>

                        <div class="col-sm-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                            <div class="service-icon">
                                <i class="fas fa-building"></i>
                            </div>
                            <div class="service-info">
                                <h3 class="css-text-black css-semibold">Nuestras obras</h3>
                                <p>Nuestras promociones son un referente en Valencia y en el mercado inmobiliario, tanto en innovación como en acabados o las zonas de construcción elegidas.</p>
                                <a href="https://www.careco.es/nuestras-obras" class="btn btn-start">+ Info</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!--/#services-->

        <section id="client">
            <div>
                <a class="twitter-left-control" href="#client-carousel" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                <a class="twitter-right-control" href="#client-carousel" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
                <div class="container">
                    <div class="row">
                        <div class="col-md-11 css-noFloat center-block">
                            <div class="twitter-icon text-center">
                                <h4 class="css-marginB0 css-semibold">Que opinan nuestros clientes</h4>
                                <p>Las opniones de nuestros clientes es importante</p>
                            </div>

                            <div id="client-carousel" class="carousel slide wow fadeIn" data-ride="carousel" data-wow-duration="1000ms" data-wow-delay="300ms">
                                <div class="carousel-inner">
                                    <?php for ($i = 1; $i <= $cnt_item; $i++) { ?>
                                        <?php if ($i == 1) { $active = "active"; } else { $active = ""; } ?>
                                        <div class="item <?=$active?>">
                                            <?php for ($j = 0; $j < count($arr_opinions); $j++) { ?>
                                                <?php if ($arr_opinions[$j]['item'] == $i): ?>
                                                    <div class="col-md-4">
                                                        <img src="<?=$arr_opinions[$j]['image']?>" class="img-circle img-thumbnail center-block" width="80" alt="">
                                                        <div class="text-center">
                                                            <p class="css-text-green css-marginB0 css-bold"><?=$arr_opinions[$j]['name']?></p>
                                                            <p><?=$arr_opinions[$j]['company']?></p>
                                                        </div>
                                                        <div class="css-client-panel">
                                                            <p>
                                                                <i class="fas fa-quote-left css-text-green"></i>
                                                                <?=$arr_opinions[$j]['desc']?>
                                                                <i class="fas fa-quote-right css-text-green"></i>
                                                            </p>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>

                                <ol class="carousel-indicators">
                                    <?php for ($i = 0; $i < $cnt_item; $i++) { ?>
                                        <?php if ($i == 0) { $active = "active"; } else { $active = ""; } ?>
                                        <li data-target="#client-carousel" data-slide-to="<?=$i?>" class="<?=$active?>"></li>
                                    <?php } ?>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/#twitter-->

        <section id="blog">
            <div class="container">
                <div class="row">
                    <div class="heading text-center col-sm-8 col-sm-offset-2 wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="300ms">
                        <h2 class="css-semibold css-fontSize20 css-text-black"> <a href="https://www.careco.es/blog">Blog de Careco </a></h2>
                        <p>Si estás pensado en comprar o alquilar un inmueble, casa o piso, en nuestro blog encontrarás información actual sobre la venta de obra nueva, de segunda mano y de alquileres. También contarás con temas legales y ayudas a la compra de vivienda y alquileres.</p>
                    </div>
                </div>
                <div class="blog-posts">
                    <div class="row">
                        <?php foreach ($arr_post as $val):
                            $linkUrl='https://www.careco.es/blog/'.createSlug($val['title']).'/'.$val['id'];
                            ?>
                            <div class="col-sm-3 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="400ms">
                                <div class="post-thumb">
                                    <a href="#">
                                        <img class="img-responsive" src="<?=$val['image']?>" alt="" height="175">
                                    </a>
                                </div>
                                <div class="entry-header">
                                    <span class="date"><?=$val['date']?></span>
                                    <h3 class="css-marginT0 css-fontSize16"><a href="<?=$linkUrl?>"><?=$val['title']?></a></h3>
                                </div>
                                <div class="entry-content">
                                    <p class="css-justify" style="height:210px;"><?=$val['description']?>...</p>
                                    <a href="<?=$linkUrl?>" class="btn btn-start css-paddingL40 css-paddingR40"> leer más</a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <div class="load-more wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="500ms">
                        <a href="https://www.careco.es/blog" class="btn-loadmore"><i class="fa fa-repeat"></i> Visitar el Blog</a>
                    </div>
                </div>
            </div>
        </section>
        <!--/#blog-->

        <?php include('html/overall/contact.php') ?>
        <?php include('html/overall/footer.php') ?>

        <script type="text/javascript" src="views/app/resources_1/js/jquery.js"></script>
        <script type="text/javascript" src="views/app/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="//maps.google.com/maps/api/js?key=AIzaSyAfoFMSNU6Vr-JaVW4BN86B_VGkCwowO_w&sensor=true"></script>
        <script type="text/javascript" src="views/app/resources_1/js/jquery.inview.min.js"></script>
        <script type="text/javascript" src="views/app/resources_1/js/wow.min.js"></script>
        <script type="text/javascript" src="views/app/resources_1/js/mousescroll.js"></script>
        <script type="text/javascript" src="views/app/resources_1/js/smoothscroll.js"></script>
        <script type="text/javascript" src="views/app/resources_1/js/jquery.countTo.js"></script>
        <script type="text/javascript" src="views/app/resources_1/js/lightbox.min.js"></script>
        <script type="text/javascript" src="views/app/resources_1/js/main.js"></script>
        <!-- jQuery validation -->
        <script type="text/javascript" src="views/app/plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="views/app/plugins/jquery-validation/additional-methods.js"></script>
        <!-- newsletter -->
        <script type="text/javascript" src="views/app/resources_1/js/js-newsletter.js"></script>
        <!-- Contact -->
        <script type="text/javascript" src="views/app/resources_1/js/js-contact.js"></script>
    </body>
</html>
