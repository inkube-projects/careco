<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>CARECO | Contacto</title>
       <meta name="robots" content="noindex">
      <link href="views/app/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="views/app/resources_1/css/animate.min.css" rel="stylesheet">
      <link href="views/app/plugins/font-awesome/web-fonts-with-css/css/fontawesome-all.min.css" rel="stylesheet">
      <link href="views/app/resources_1/css/lightbox.css" rel="stylesheet">
      <link href="views/app/resources_1/css/main.css" rel="stylesheet">
      <link href="views/app/resources_1/css/presets/preset1.css" id="css-preset" rel="stylesheet">
      <link href="views/app/resources_1/css/responsive.css" rel="stylesheet">
      <!--[if lt IE 9]>
         <script src="js/html5shiv.js"></script>
         <script src="js/respond.min.js"></script>
      <![endif]-->
      <link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
      <link rel="shortcut icon" href="views/images/favicon.ico">
      <link href="views/app/resources_1/css/css-style.css" rel="stylesheet">
      <style>
	  	.carousel-fade .carousel-inner .item {
    background-position: center;
    height: 331px;
}
	  </style>
   </head>
   <!--/head-->

   <body>
      <!--.preloader-->
      <div class="preloader"> <i class="fas fa-circle-notch fa-spin"></i></div>
      <!--/.preloader-->

      <header id="home">
         <nav class="css-nav-top">
            <div class="container">
               <div class="col-md-6">
                  <i class="fas fa-phone"></i> +34 687 715 531
                  <i class="fas fa-envelope css-marginL30"></i> careco@careco.es
               </div>

               <div class="col-md-6 text-right">
                  <a href="#"><i class="fab fa-linkedin"></i></a>
                  <a href="#"><i class="fab fa-instagram"></i></a>
               </div>
            </div>
         </nav>

         <nav class="navbar navbar-default">
            <div class="container">
               <div class="container-fluid">
                  <div class="navbar-header">
                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                     </button>
                     <a class="navbar-brand" href="#">
                        <img src="views/images/logo-high-n-01.png" alt="CARECO" width="120">
                     </a>
                  </div>

                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Promociones en Valencia <div class="css-nav-separator"></div></a>
                           <ul class="dropdown-menu">
                              <li><a href="https://www.careco.es/promociones-valencia/comprar-chalet-adosado/penya-roja/avenida-francia-alameda">Promociones en curso <i class="fas fa-angle-right css-text-green css-marginL10"></i></a></li>
                       		  <li><a href="https://www.careco.es/proximas-promociones-valencia">Próximas promociones <i class="fas fa-angle-right css-text-green css-marginL10"></i></a></li>
                        	  <li><a href="https://www.careco.es/nuestras-obras">Promociones realizadas<i class="fas fa-angle-right css-text-green css-marginL10"></i></a></li>
                           </ul>
                        </li>
               <li><a href="https://www.careco.es/asi-somos">Careco <div class="css-nav-separator"></div></a></li>
               <li><a href="https://www.careco.es/blog">Blog <div class="css-nav-separator"></div></a></li>
               <li><a href="https://www.careco.es/atencion-al-cliente">Atención al cliente <div class="css-nav-separator"></div></a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </nav>
         <!--/#main-nav-->

         <div id="home-slider" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
               <div class="item active" style="background-image: url(views/images/slider/2.jpg)">
                  <div class="caption">
                     <div class="container">
                        <div class="col-md-6 text-left">
                           <a data-scroll class="btn btn-start btn-green animated fadeInUpBig btn-slide" href="#">CONSTRUIMOS CONTIGO</a>
                           <p class="animated fadeInRightBig css-text-black css-medium css-marginT20">Queremos estar a tú lado<br> en cada paso que des.</p>
                        </div>

                        <div class="col-md-6">
                           <!-- <h1 class="animated fadeInLeftBig">CAR<span>ECO</span></h1> -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- <a class="left-control" href="#home-slider" data-slide="prev"><i class="fa fa-angle-left"></i></a>
            <a class="right-control" href="#home-slider" data-slide="next"><i class="fa fa-angle-right"></i></a> -->
            <a id="tohash" href="#portfolio"><i class="fa fa-angle-down css-text-black"></i></a>
         </div>
      </header>






       <?php include('html/overall/contact.php') ?>
      <?php include('html/includes/newsletter.php') ?>
      <?php include('html/includes/footer.php') ?>


      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/jquery.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/plugins/bootstrap/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="//maps.google.com/maps/api/js?key=AIzaSyAfoFMSNU6Vr-JaVW4BN86B_VGkCwowO_w&sensor=true"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/jquery.inview.min.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/wow.min.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/mousescroll.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/smoothscroll.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/jquery.countTo.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/lightbox.min.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/main.js"></script>
      <!-- jQuery validation -->
      <script type="text/javascript" src="<?=MAINURL?>views/app/plugins/jquery-validation/jquery.validate.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/plugins/jquery-validation/additional-methods.js"></script>
      <!-- newsletter -->
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/js-newsletter.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/js-contact.js"></script>
   </body>
</html>
