<!doctype html>
<html lang="es-ES" prefix="og: http://ogp.me/ns# og: http://ogp.me/ns#" >
   <head>
      <!-- Meta Tags -->
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
      <!--Shortcut icon-->
      <title>ASI SOMOS - Careco</title>
      <meta property="og:title" content="ASI SOMOS" />
      <meta property="og:description" content="&hellip;" />
      <meta property="og:url" content="" /> <!-- Direccion actual -->
      <meta property="og:type" content="article" />
      <meta property="og:site_name" content="Careco" />
      <meta property="article:published_time" content="2016-09-27T07:57:49+00:00" />
      <meta property="article:modified_time" content="2017-03-27T09:57:27+00:00" />
      <meta property="og:updated_time" content="2017-03-27T09:57:27+00:00" />

      <link rel="canonical" href="" /> <!-- Direccion actual -->
      <meta property="og:locale" content="es_ES" />
      <meta property="og:type" content="article" />
      <meta property="og:title" content="ASI SOMOS - Careco" />
      <meta property="og:url" content="" /> <!-- Direccion actual -->
      <meta property="og:site_name" content="Careco" />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:title" content="ASI SOMOS - Careco" />
      
      

      <?php include('html/overall/header.php') ?>
      


		<style type="text/css">
         img.wp-smiley,
         img.emoji {
         	display: inline !important;
         	border: none !important;
         	box-shadow: none !important;
         	height: 1em !important;
         	width: 1em !important;
         	margin: 0 .07em !important;
         	vertical-align: -0.1em !important;
         	background: none !important;
         	padding: 0 !important;
         }
      </style>

      <style type="text/css" data-type="vc_custom-css">
         .texto {
            padding-right: 40%;
            padding-left: 5%;
            padding-top:1%;
            vertical-align: middle!important;
         }
         .rectangulo {
            min-height:100px!important;
            max-height:100px!important;
         }
         .fotocuadrada {
            min-height:300px!important;
            max-height:300px!important;
         }
         .cuadro {
            min-height:440px!important;
            max-height:440px!important;
            vertical-align: middle!important;
         }

         .bloquehorizontal {
            min-height:335px!important;
            max-height:335px!important;
         }
      </style>
      <style type="text/css" data-type="vc_shortcodes-custom-css">
         .vc_custom_1481708416896{
            margin-top: 10px !important;
            margin-bottom: 20px !important;
         }

         .vc_custom_1481627786178{
            padding-top: 20px !important;
            padding-bottom: 20px !important;
         }
      </style>
      <noscript>
         <style type="text/css">.wpb_animate_when_almost_visible { opacity: 1; }</style>
      </noscript>
      <link href="views/app/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="views/app/resources_1/css/animate.min.css" rel="stylesheet">
      <link href="views/app/plugins/font-awesome/web-fonts-with-css/css/fontawesome-all.min.css" rel="stylesheet">
      <link href="views/app/resources_1/css/lightbox.css" rel="stylesheet">
      <link href="views/app/resources_1/css/main.css" rel="stylesheet">
      <link href="views/app/resources_1/css/presets/preset1.css" id="css-preset" rel="stylesheet">
      <link href="views/app/resources_1/css/responsive.css" rel="stylesheet">
   </head>

   <body class="page-template-default page page-id-7121 ascend nectar-auto-lightbox wpb-js-composer js-comp-ver-4.12.1 vc_responsive"
      data-footer-reveal="false"
      data-header-format="default"
      data-footer-reveal-shadow="none"
      data-dropdown-style="classic"
      data-cae="linear"
      data-cad="500"
      data-aie="none"
      data-ls="pretty_photo"
      data-apte="standard"
      data-hhun="0"
      data-fancy-form-rcs="1"
      data-form-style="minimal"
      data-form-submit="default"
      data-is="minimal"
      data-button-style="default"
      data-header-inherit-rc="false"
      data-header-search="false"
      data-animated-anchors="true"
      data-ajax-transitions="true"
      data-full-width-header="true"
      data-slide-out-widget-area="true"
      data-slide-out-widget-area-style="fullscreen"
      data-user-set-ocm="1"
      data-loading-animation="none"
      data-bg-header="false"
      data-ext-responsive="false"
      data-header-resize="0"
      data-header-color="custom"
      data-transparent-header="false"
      data-smooth-scrolling="1"
      data-permanent-transparent="false"
      data-responsive="1" >

      <div id="header-secondary-outer" data-full-width="true" data-permanent-transparent="false" >
         <div class="container">
            <nav></nav>
         </div>
      </div>

    

      <?php include('html/overall/topnav.php'); ?>

      <div id="search-outer" class="nectar">
         <div id="search">
            <div class="container">
               <div id="search-box">
                  <div class="col span_12">
                     <form action="http://www.careco.es" method="GET">
                        <input type="text" name="s" id="s" value="Start Typing..." data-placeholder="Start Typing..." />
                     </form>
                  </div><!--/span_12-->
               </div><!--/search-box-->

               <div id="close">
                  <a href="#">
                     <span class="icon-salient-x" aria-hidden="true"></span>
                  </a>
               </div>
            </div><!--/container-->
         </div><!--/search-->
      </div><!--/search-outer-->

      <div id="ajax-loading-screen" data-disable-fade-on-click="0" data-effect="standard" data-method="standard">
         <div class="loading-icon none">
            <span class="default-loading-icon spin"></span>
         </div>
      </div>

      <div id="ajax-content-wrap">
         <div class="blurred-wrap">
            <div class="container-wrap">
               <div class="container main-content">
                  <div class="row">
                     <p id="breadcrumbs">
                        <span xmlns:v="http://rdf.data-vocabulary.org/#">
                           <span typeof="v:Breadcrumb">
                              <a href="index.php" rel="v:url" property="v:title">Inicio</a> »
                              <span class="breadcrumb_last">ATENCIÓN AL CLIENTE</span>
                           </span>
                        </span>
                     </p>

                     <div id="fws_5a6a68952676d" data-midnight="custom" data-bg-mobile-hidden="true" class="wpb_row vc_row-fluid vc_row full-width-content  vc_row-o-equal-height vc_row-flex standard_section  bloquehorizontal" style="padding-top: 100px; padding-bottom: 0px; ">
                        <div class="row-bg-wrap">
                           <div class="row-bg using-image" style="background-image: url(views/images/about-us-banner.jpg); background-position: left top; background-repeat: no-repeat; " data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.5" data-enable_gradient="false"></div>
                        </div>

                        <div class="col span_12 custom left">
                           <div  class="vc_col-sm-12 textobajo wpb_column column_container vc_column_container col has-animation" data-bg-cover="" data-padding-pos="left" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="reveal-from-right" data-delay="0">
                              <div class="column-inner-wrap">
                                 <div  data-bg-cover="" class="column-inner no-extra-padding">
                                    <div class="wpb_wrapper">
                                       <div id="fws_5a6a689527566" data-midnight="" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row  vc_row-o-equal-height vc_row-flex standard_section" style="padding-top: 0px; padding-bottom: 0px; ">
                                          <div class="row-bg-wrap">
                                             <div class="row-bg" style=""></div>
                                          </div>

                                          <div class="col span_12  left">
                                             <div class="vc_col-sm-4 wpb_column column_container vc_column_container col no-extra-padding" data-bg-cover="" data-padding-pos="bottom-left" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                                                <div class="vc_column-inner">
                                                   <div class="wpb_wrapper"></div>
                                                </div>
                                             </div>

                                             <div class="vc_col-sm-8 wpb_column column_container vc_column_container col no-extra-padding" data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                                                <div class="vc_column-inner">
                                                   <div class="wpb_wrapper">
                                                      <div class="img-with-aniamtion-wrap " data-max-width="100%">
                                                         <div class="inner">
                                                            <a href="index.php" target="_self" class="">
                                                               <img data-shadow="none" data-shadow-direction="middle" class="img-with-animation bloque-inicio" data-delay="0" height="22" width="127" data-animation="grow-in" src="images/logo-medium-n-01.png" alt="" />
                                                            </a>
                                                         </div>
                                                      </div>

                                                      <div class="wpb_text_column wpb_content_element  vc_custom_1481708416896 bloque-inicio" >
                                                         <div class="wpb_wrapper">
                                                            <h1>
                                                               <span style="font-size: 30px; background-color: #7fa9ae; padding: 5px 10px 5px 10px; color: #fff;">CASAS COMO TÚ</span>
                                                            </h1>
                                                         </div>
                                                      </div>

                                                      <div class="wpb_text_column wpb_content_element  bloque-inicio" >
                                                         <div class="wpb_wrapper">
                                                            <p style="font-size: 16px;">Atención al cliente</p>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     
				<?php include('html/overall/contact.php') ?>
                     

                  
            </div><!--/row-->
         </div><!--/container-->
      </div><!--/container-wrap-->
   </div><!--blurred-wrap-->
</div>
   <?php include('html/overall/footer.php') ?>

   <div id="slide-out-widget-area-bg" class="fullscreen dark"></div>

   <a id="to-top" class=""><i class="fas fa-angle-up"></i></a>
   <?php  include('html/overall/js.php') ?>
</body>
</html>
