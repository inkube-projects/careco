<!doctype html>
<html lang="es-ES" prefix="og: http://ogp.me/ns# og: http://ogp.me/ns#">
   <head>
      <!-- Meta Tags -->
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
      <!--Shortcut icon-->
      <title><?=APP_TITLE?> | Próximas promociones</title>

      <meta property="og:title" content="Promociones en curso" />
      <meta property="og:description" content="&hellip;" />
      <meta property="og:url" content="" /> <!-- pagina actual-->
      <meta property="og:type" content="article" />
      <meta property="og:site_name" content="Careco" />
      <meta property="article:published_time" content="2016-11-10T16:47:07+00:00" />
      <meta property="article:modified_time" content="2017-12-15T07:57:59+00:00" />
      <meta property="og:updated_time" content="2017-12-15T07:57:59+00:00" />

      <!-- This site is optimized with the Yoast SEO plugin v4.4 - https://yoast.com/wordpress/plugins/seo/ -->
      <link rel="canonical" href="" /> <!-- pagina actual-->
      <meta property="og:locale" content="es_ES" />
      <meta property="og:type" content="article" />
      <meta property="og:title" content="Promociones en curso - Careco" />
      <meta property="og:url" content="" /> <!-- pagina actual-->
      <meta property="og:site_name" content="Careco" />
      <meta property="og:image" content="images/iconos-localidad-1.png" />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:title" content="Promociones en curso - Careco" />
      <meta name="twitter:image" content="images/iconos-localidad-1.png" />
      <!-- / Yoast SEO plugin. -->

      <?php include('html/overall/header.php') ?>

      <style type="text/css" data-type="vc_custom-css">
         .rectangulo {
            min-height:400px!important;
            max-height:400px!important;
            padding-right:12.6%!important;
            padding-left:12%!important;
         }
         .pad {
            padding-left:20%!important;
         }
         .padr {
            padding-right:10%!important;
         }
         .block {
            display:block!important;
         }
         .promo .wpb_wrapper > div {
            margin-bottom: 0px;
         }
         div#links-link-7471, div#links2-link-7471 {
            margin-bottom: 10px;
         }
         .vc_col-sm-4.textobajo.wpb_column.column_container.col.centered-text.padding-4-percent.instance-0 {
            padding: 0%;
         }
         .tres {
            min-height:210px!important;
            max-height:210px!important;
         }
         body[data-form-style="minimal"] .minimal-form-input {
            padding-top: 10px;
         }
         body[data-form-style="minimal"] input[type="email"] {
            background-color: #D4DDDD !important;
            padding-top: 7px !important;
            padding-bottom: 7px !important;
            padding-left: 7px !important;
            padding-right: 7px !important;
         }
         .titemail {
            font-size: 12px;
         }
         body[data-form-style="minimal"] input[type="submit"] {
            background-color: #697F7F !important;
            font-size: 12px;
         }
		 .futuras {
    color: #6B6C6B !important;
}
      </style>
      <style type="text/css" data-type="vc_shortcodes-custom-css">
         .vc_custom_1482397502146{
            padding-top: 30px !important;
            padding-bottom: 15px !important;
            background-color: #e2e2e2 !important;
         }
         .vc_custom_1481797530421{
            margin-top: 10px !important;
            padding-bottom: 20px !important;
         }
         .vc_custom_1481627956764{
            padding-top: 20px !important;
            padding-bottom: 20px !important;
         }
         .css-gallery-image {
            height:306px
         }
         .ascend .container-wrap input[type="submit"], .ascend .container-wrap button[type="button"] {
				padding: 8px !important;
				font-size: 14px !important;
			}
         body[data-form-style="minimal"] input[type="submit"], body[data-form-style="minimal"] button[type="button"] {
            background-color: #697F7F !important;
            font-size: 12px;
            color:#FFF !important;
         }
         .ascend .container-wrap input[type="submit"], .ascend .container-wrap button[type="button"] {
            padding: 8px !important;
            font-size: 14px !important;
         }
         p {
            padding-bottom: 0;
         }
      </style>
      <noscript>
         <style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style>
      </noscript>
   </head>

   <body class="page-template-default page page-id-7576 ascend nectar-auto-lightbox wpb-js-composer js-comp-ver-4.12.1 vc_responsive"
   data-footer-reveal="false"
   data-header-format="default"
   data-footer-reveal-shadow="none"
   data-dropdown-style="classic"
   data-cae="linear"
   data-cad="500"
   data-aie="none"
   data-ls="pretty_photo"
   data-apte="standard"
   data-hhun="0"
   data-fancy-form-rcs="1"
   data-form-style="minimal"
   data-form-submit="default"
   data-is="minimal"
   data-button-style="default"
   data-header-inherit-rc="false"
   data-header-search="false"
   data-animated-anchors="true"
   data-ajax-transitions="true"
   data-full-width-header="true"
   data-slide-out-widget-area="true"
   data-slide-out-widget-area-style="fullscreen"
   data-user-set-ocm="1"
   data-loading-animation="none"
   data-bg-header="false"
   data-ext-responsive="false"
   data-header-resize="0"
   data-header-color="custom"
   data-transparent-header="false"
   data-smooth-scrolling="1"
   data-permanent-transparent="false"
   data-responsive="1" >

      <div id="header-secondary-outer" data-full-width="true" data-permanent-transparent="false" >
         <div class="container">
            <nav></nav>
         </div>
      </div>

      <?php include('html/overall/topnav.php'); ?>

      <div id="ajax-loading-screen" data-disable-fade-on-click="0" data-effect="standard" data-method="standard">
         <div class="loading-icon none">
            <span class="default-loading-icon spin"></span>
         </div>
      </div>

      <div id="ajax-content-wrap">
         <div class="blurred-wrap">
            <div class="container-wrap">
               <div class="container main-content">
                  <div class="row">
                     <p id="breadcrumbs">
                        <span xmlns:v="http://rdf.data-vocabulary.org/#">
                           <span typeof="v:Breadcrumb">
                              <a href="index.php" rel="v:url" property="v:title" class="css-fontSize12">Inicio</a> »
                              <span class="breadcrumb_last css-fontSize12 css-gray-2">Futuras promociones</span>
                           </span>
                        </span>
                     </p>

                     <div id="fws_5a7745e425e50"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-section  vc_row-o-equal-height vc_row-flex standard_section" data-using-ctc="true" style="padding-top: 0px; padding-bottom: 0px; color: #ffffff; ">
                        <div class="row-bg-wrap">
                           <div class="row-bg" style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
                        </div>

                        <div class="col span_12 custom left">
                           <div style="" class="vc_col-sm-4 textobajo wpb_column column_container vc_column_container col centered-text padding-4-percent" data-using-bg="true" data-bg-cover="" data-padding-pos="right" data-has-bg-color="true" data-bg-color="#eeeeee" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                              <div class="vc_column-inner">
                                 <div class="wpb_wrapper css-promo-list">
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1482397502146 pad" >
                                       <div class="wpb_wrapper">
                                          <h2 style="text-align: left;" class="css-noMargin"><a href="index.php?view=nextPromotion"><span style="color: #6b6c6b;">PRÓXIMAS <br class="css-marginB10"> PROMOCIONES</span></a></h2>
                                       </div>
                                    </div>

                                    <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                                       <div class="wpb_wrapper">
                                          <div class="arrow"></div>
                                       </div>
                                    </div>

                                    <div class="divider-wrap">
                                       <div class="divider css-marginB30"></div>
                                    </div>

                                    <div class="wpb_text_column wpb_content_element  pad" >
                                       <div class="wpb_wrapper">
                                          <?php foreach ($arr_promotion as $promo): ?>
                                             <p class="futuras css-noMargin"><span style="cursor:pointer" class="jumpTo" href="#promo_<?=$promo['id']?>"><?=$promo['title']?></span></p>
                                          <?php endforeach; ?>
                                       </div>
                                    </div>


                                    <div class="wpb_text_column wpb_content_element  vc_custom_1481797530421 pad" >
                                       <div class="wpb_wrapper">
                                          <h2 style="text-align: left; font-size: 14px;">
                                             <span style="color: #ffffff;">
                                                <a style="color: #ffffff;" href="https://www.careco.es">&lt; Volver</a>
                                             </span>
                                          </h2>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>

                           <div  class="vc_col-sm-8 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                              <div class="vc_column-inner">
                                 <div class="css-promotion-cover-image" style="background-image: url('<?=$image?>')"></div>

                                 <!-- <div class="wpb_wrapper">
                                    <div id="fws_5a7745e4320b2" data-midnight="" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row  vc_row-o-equal-height vc_row-flex  vc_row-o-content-middle standard_section   "  style="padding-top: 0px; padding-bottom: 0px; ">
                                       <div class="row-bg-wrap">
                                          <div class="row-bg" style=""></div>
                                       </div>

                                       <div class="col span_12 left">
                                          <div  class="vc_col-sm-12 wpb_column column_container vc_column_container col padding-3-percent"  data-bg-cover="" data-padding-pos="right" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                                             <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                   <div class="wpb_gallery wpb_content_element clearfix">
                                                      <div class="wpb_wrapper">
                                                         <div class="wpb_gallery_slidesnectarslider_style" data-interval="5">
                                                            <div class="nectar-slider-wrap" style="height: 378px" data-flexible-height="true" data-fullscreen="false"  data-full-width="false" data-parallax="false" data-autorotate="5500" id="ns-id-5a7745e432eea">
                                                               <div class="swiper-container" style="height: 378px"  data-loop="true" data-height="378" data-bullets="" data-bullet_style="see_through" data-arrows="false" data-bullets="false" data-desktop-swipe="true" data-settings="">
                                                                  <div class="swiper-wrapper">
                                                                     <div class="swiper-slide" data-bg-alignment="center" data-color-scheme="light" data-x-pos="centered" data-y-pos="middle">
                                                                        <div class="image-bg" style="background-image: url('<?=$image?>');"></div>
                                                                        <span class="ie-fix"></span>
                                                                     </div>
                                                                  </div>

                                                                  <div class="nectar-slider-loading"></div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div> -->
                              </div>
                           </div>
                        </div>
                     </div>

                     <div id="fws_5a7745e433be0"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row  vc_row-o-equal-height vc_row-flex standard_section" style="padding-top: 0px; padding-bottom: 0px; ">
                        <div class="row-bg-wrap">
                           <div class="row-bg" style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
                        </div>

                        <div class="col span_12 dark center">
                           <div style="" class="vc_col-sm-4 wpb_column column_container vc_column_container col no-extra-padding" data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-color="#cddddc" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                              <div class="vc_column-inner">
                                 <div class="wpb_wrapper"></div>
                              </div>
                           </div>

                           <div style="" class="vc_col-sm-4 wpb_column column_container vc_column_container col no-extra-padding" data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-color="#cddddc" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                              <div class="vc_column-inner">
                                 <div class="wpb_wrapper">
                                    <div class="divider-wrap">
                                       <div style="height: 70px;" class="divider"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>

                           <div style="" class="vc_col-sm-4 wpb_column column_container vc_column_container col padding-1-percent" data-using-bg="true" data-bg-cover="" data-padding-pos="right" data-has-bg-color="true" data-bg-color="#cddddc" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                              <div class="vc_column-inner">
                                 <div class="wpb_wrapper"></div>
                              </div>
                           </div>
                        </div>
                     </div>


                     <?php foreach ($arr_promotion as $key => $val): ?>

                        <div id="next_promo_<?=$val['id']?>" data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row standard_section"  style="padding-top: 0px; padding-bottom: 0px;">
                           <div class="row-bg-wrap">
                              <div class="row-bg"  style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
                           </div>

                           <div class="col span_12 dark left">
                              <div  class="vc_col-sm-12 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                                 <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                       <div class="divider-wrap">
                                          <div style="height: 40px;" class="divider"></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div id="promo_<?=$val['id']?>"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row standard_section  tres "  style="padding-top: 0px; padding-bottom: 0px; ">
                           <div class="row-bg-wrap">
                              <div class="row-bg"  style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
                           </div>

                           <div class="col span_12 dark left">
                              <div  class="vc_col-sm-4 wpb_column column_container vc_column_container col padding-1-percent"  data-bg-cover="" data-padding-pos="right" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                                 <div class="vc_column-inner">
                                    <div class="wpb_wrapper">

                                       <div class="wpb_text_column wpb_content_element">
                                          <div class="wpb_wrapper">
                                             <h4><?=$val['title']?></h4>
                                          </div>
                                       </div>

                                       <div class="divider-wrap">
                                          <div style="height: 8px;" class="divider"></div>
                                       </div>

                                       <div class="wpb_text_column wpb_content_element  padr" >
                                          <div class="wpb_wrapper">
                                             <p style="text-align: left; color: #658181;" class="css-fontSize13"><?=$val['description']?></p>
                                          </div>
                                       </div>

                                       <div class="divider-wrap">
                                          <div style="height: 20px;" class="divider"></div>
                                       </div>

                                       <div role="form" class="wpcf7" id="wpcf7-f9409-p7576-o1" lang="es-ES" dir="ltr">
                                          <div class="screen-reader-response"></div>
                                          <form name="frm-promo" id="frm-promo-<?=$val['id']?>" class="frm-promo" method="post" action="">
                                             <div style="display: none;">
                                                <input type="hidden" name="_wpcf7" value="9409" />
                                                <input type="hidden" name="_wpcf7_version" value="4.6.1" />
                                                <input type="hidden" name="_wpcf7_locale" value="es_ES" />
                                                <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f9409-p7576-o1" />
                                                <input type="hidden" name="_wpnonce" value="62a7f888a0" />
                                                <input type="hidden" name="promo_id" value="<?=$val['id']?>">
                                             </div>

                                             <p>
                                                 <span class="titemail">TU NOMBRE (OBLIGATORIO)</span><br />
                                                 <span class="wpcf7-form-control-wrap">
                                                    <input type="text" id="name_<?=$val['id']?>" name="promo_name" size="40" class="wpcf7-form-control wpcf7-text wpcf7-name wpcf7-validates-as-required wpcf7-validates-as-name" aria-required="true" aria-invalid="false" />
                                                    <div class="text-danger" id="name_<?=$val['id']?>_validate"></div>
                                                 </span>
                                                 <br>
                                                 <span class="titemail">TU APELLIDO (OBLIGATORIO)</span><br />
                                                 <span class="wpcf7-form-control-wrap">
                                                    <input type="text" id="lastName_<?=$val['id']?>" name="promo_lastName" size="40" class="wpcf7-form-control wpcf7-text wpcf7-lastName wpcf7-validates-as-required wpcf7-validates-as-lastName" aria-required="true" aria-invalid="false" />
                                                    <div class="text-danger" id="lastName_<?=$val['id']?>_validate"></div>
                                                 </span>
                                                 <br>
                                                <span class="titemail">TU EMAIL (OBLIGATORIO)</span><br />
                                                <span class="wpcf7-form-control-wrap your-email">
                                                   <input type="text" id="email_<?=$val['id']?>" name="promo_email" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" />
                                                   <div class="text-danger" id="email_<?=$val['id']?>_validate"></div>
                                                </span>
                                                <br>

                                                <span class="titemail">TU TELÉFONO</span><br />
                                                <span class="wpcf7-form-control-wrap tel-999">
                                                   <input type="text" id="phone_<?=$val['id']?>" name="promo_phone" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel" aria-invalid="false" />
                                                   <div class="text-danger" id="phone_<?=$val['id']?>_validate"></div>
                                               </span>
                                               <br>
                                               <span class="titemail">OBSERVACIONES</span><br />
                                               <span class="wpcf7-form-control-wrap">
                                                  <input type="text" id="observations_<?=$val['id']?>" name="promo_observations" size="40" class="wpcf7-form-control wpcf7-text wpcf7-observations wpcf7-validates-as-required wpcf7-validates-as-observations" aria-required="true" aria-invalid="false" />
                                                  <div class="text-danger" id="observations_<?=$val['id']?>_validate"></div>
                                               </span>
                                             </p>

                                             <input type="hidden" name="hidden-537" value="" class="wpcf7-form-control wpcf7-hidden" />
                                             <input type="hidden" name="hidden-656" value="index.php?view=nextPromotion" class="wpcf7-form-control wpcf7-hidden" />
                                             <p>
                                                <button type="button" onclick="persistMessage('<?=$val['id']?>')" class="js-submit css-marginT20">ESTOY INTERESADO</button>
                                             </p>
                                             <div class="wpcf7-response-output wpcf7-display-none"></div>
                                          </form>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <div  class="vc_col-sm-4 block wpb_column column_container vc_column_container col padding-4-percent"  data-bg-cover="" data-padding-pos="left" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                                 <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                       <div class="wpb_gallery wpb_content_element clearfix">
                                          <div class="wpb_wrapper">
                                             <div class="wpb_gallery_slides wpb_flexslider flex-gallery flexslider" data-interval="5" data-flex_fx="fade">
                                                <ul class="slides">
                                                   <?php foreach ($val['gallery'] as $gal): ?>
                                                      <li><div class="css-gallery-image" style="background-image: url(<?=APP_IMG_PROMOTIONS."gallery/min_".$gal['image']?>)"></div></li>
                                                   <?php endforeach; ?>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <div  class="vc_col-sm-4 wpb_column column_container vc_column_container col padding-3-percent"  data-bg-cover="" data-padding-pos="left" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                                 <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                       <div id="map_<?=$val['id']?>" style="height: 307px;" class="nectar-google-map" data-dark-color-scheme="" data-ultra-flat="" data-greyscale="" data-extra-color="" data-enable-animation="false" data-enable-zoom="" data-zoom-level="13" data-center-lat="<?=$val['latitude']?>" data-center-lng="<?=$val['longitude']?>" data-marker-img=""></div>
                                       <div class="map_<?=$val['id']?> map-marker-list">
                                          <div class="map-marker" data-lat="<?=$val['latitude']?>" data-lng="<?=$val['longitude']?>" data-mapinfo=""></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     <?php endforeach; ?>


                     <div id="fws_5a7745e440fce"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row standard_section"  style="padding-top: 0px; padding-bottom: 0px; ">
                        <div class="row-bg-wrap">
                           <div class="row-bg"  style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
                        </div>

                        <div class="col span_12 dark left">
                           <div  class="vc_col-sm-12 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                              <div class="vc_column-inner">
                                 <div class="wpb_wrapper">
                                    <div class="divider-wrap">
                                       <div style="height: 40px;" class="divider"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div><!--/row-->
               </div><!--/container-->
            </div><!--/container-wrap-->

            <?php include('html/overall/footer.php') ?>
         </div><!--blurred-wrap-->

         <div id="slide-out-widget-area-bg" class="fullscreen dark"></div>
      </div> <!--/ajax-content-wrap-->

      <a id="to-top" class=""><i class="fa fa-angle-up"></i></a>

      <!-- Modal contact -->
      <div class="modal fade" tabindex="-1" role="dialog" id="mod-contact">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Contacto</h4>
               </div>
               <div class="modal-body">
                  <p class="contact-message"></p>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
               </div>
            </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->

      <?php include('html/overall/js.php');
	  if(isset($_REQUEST['promotionId'])){ echo ' <script type="text/javascript"> goToPromotion='.$_REQUEST['promotionId'].';</script>'; }
	   ?>
      <!-- Custom JS -->
      <script type='text/javascript' src='views/app/resources_2/js/js-next-promotion.js'></script>
      <script type="text/javascript">


	  jQuery(document).ready(function() {

           jQuery('.jumpTo').click(function(e){
			  e.preventDefault();
			   var ref = jQuery(this).attr('href').substring(1);
			   //jQuery(document).scrollTo("#"+ref);
			   jQuery('html,body').animate({scrollTop: jQuery("#"+ref).offset().top},'slow');
		 });


		 if (typeof(goToPromotion) !== 'undefined') {
			 jQuery('html,body').animate({scrollTop: jQuery("#promo_"+goToPromotion).offset().top},'slow');
			 }

       });

      </script>


   </body>
</html>
