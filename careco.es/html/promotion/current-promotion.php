<!doctype html>
   <html lang="es-ES" prefix="og: http://ogp.me/ns# og: http://ogp.me/ns#" >
      <head>
         <!-- Meta Tags -->
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
         <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
         <!--Shortcut icon-->
         
         <?php
		 if($_REQUEST['p']==16 && $_REQUEST['c']==1){  $title2 = "Comprar chalet adosado en Picanya";  }
		 else { $title2 = "Promociones en curso ".$title." ".$cat_name; }
		 
		  ?>
         
         <title> <?=$title2?> - Careco</title>

         <!-- This site is optimized with the Yoast SEO plugin v4.4 - https://yoast.com/wordpress/plugins/seo/ -->
         <link rel="canonical" href="" /> <!-- pagina actual-->
         <meta property="og:locale" content="es_ES" />
         <meta property="og:type" content="article" />
         <meta property="og:title" content="<?=$title2?> - Careco" />
         <meta property="og:url" content="" /> <!-- pagina actual-->
         <meta property="og:site_name" content="Careco" />
         <meta property="og:image" content="<?=MAINURL?>views/images/iconos-localidad-1.png" />
         <meta name="twitter:card" content="summary" />
         <meta name="twitter:title" content="Promociones en curso <?=$title?> <?=$cat_name?> - Careco" />
         <meta name="twitter:image" content="images/iconos-localidad-1.png" />
        

         <?php include('html/overall/header.php') ?>

         <style media="screen">
            <?php if ($cat == 2): ?>
               .wpb_column.instance-3::before {
                  background-color: #cddddc!important;
                  opacity: 1;
               }
               .wpb_column.instance-4::before {
                   /* background-color: #cddddc!important; */
                   opacity: 1!important;
               }
               .wpb_column  h4{
                   color: #a1bfbd;
               }
            <?php elseif ($cat == 1): ?>
               .wpb_column.instance-5::before {
                   background-color: #cddddc!important;
                   opacity: 1!important;
               }
               .wpb_column  h4{
                   color: #a1bfbd;
               }
               .vc_custom_1481628275977 {
                  padding-top: 9px !important;
               }

            <?php endif; ?>
         </style>
         <style>
		 			   .txtP{ text-align:left; color:#FFF !important; font-weight:300; font-size:14px; float:left; width:100%;}
.txtP_Bold{ text-align:left; color:#FFF !important; font-weight:bold; font-size:20px; float:left; width:100%;}
.imgPromo{ float:left; width:100%; height:auto; margin:15px 0 15px 0px;}
.futuras {
    color: #6B6C6B !important;
}
.promo h2 {
    line-height: 22px;
	font-family: Montserrat;
    letter-spacing: 0;
    font-size: 20px;
    font-weight: 700 !important;
}
		 </style>
      </head>

      <body class="page-template-default page page-id-7471 ascend nectar-auto-lightbox wpb-js-composer js-comp-ver-4.12.1 vc_responsive"
         data-footer-reveal="false"
         data-header-format="default"
         data-footer-reveal-shadow="none"
         data-dropdown-style="classic"
         data-cae="linear"
         data-cad="500"
         data-aie="none"
         data-ls="pretty_photo"
         data-apte="standard"
         data-hhun="0"
         data-fancy-form-rcs="1"
         data-form-style="minimal"
         data-form-submit="default"
         data-is="minimal"
         data-button-style="default"
         data-header-inherit-rc="false"
         data-header-search="false"
         data-animated-anchors="true"
         data-ajax-transitions="true"
         data-full-width-header="true"
         data-slide-out-widget-area="true"
         data-slide-out-widget-area-style="fullscreen"
         data-user-set-ocm="1"
         data-loading-animation="none"
         data-bg-header="false"
         data-ext-responsive="false"
         data-header-resize="0"
         data-header-color="custom"
         data-transparent-header="false"
         data-smooth-scrolling="1"
         data-permanent-transparent="false"
         data-responsive="1">

         <?php include('html/overall/topnav.php'); ?>

         <div id="ajax-loading-screen" data-disable-fade-on-click="0" data-effect="standard" data-method="standard">
            <div class="loading-icon none">
               <span class="default-loading-icon spin"></span>
            </div>
         </div>

         <div id="ajax-content-wrap">
            <div class="blurred-wrap">
               <div class="container-wrap">
                  <div class="container main-content">
                     <div class="row">
                        <p id="breadcrumbs">
                           <span xmlns:v="http://rdf.data-vocabulary.org/#">
                              <span typeof="v:Breadcrumb">
                                 <a href="index.php" rel="v:url" property="v:title" class="css-fontSize12">Inicio</a> »
                                 <span class="breadcrumb_last css-fontSize12 css-gray-2">Promociones en curso <?=$title?> <?=$cat_name?></span>
                              </span>
                           </span>
                        </p>
                        <div id="fws_5a6fd1b8c4cb1"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-section  vc_row-o-equal-height vc_row-flex standard_section  promo " data-using-ctc="true" style="padding-top: 0px; padding-bottom: 0px; color: #ffffff; ">
                           <div class="row-bg-wrap">
                              <div class="row-bg" style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
                           </div>

                           <div class="col span_12 custom left">
                              <div style="" class="vc_col-sm-4 textobajo wpb_column column_container vc_column_container col centered-text padding-4-percent" data-using-bg="true" data-bg-cover="" data-padding-pos="right" data-has-bg-color="true" data-bg-color="#eeeeee" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                                 <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                       <div class="wpb_text_column wpb_content_element  vc_custom_1481628275977 pad" >
                                          <div class="wpb_wrapper">
                                             <h2 style="text-align: left;"><a href="index.php?view=currentPromotion" style="text-decoration: none;"><span style="color: #6b6c6b;">PROMOCIONES <br class="css-marginB10"> EN CURSO</span></a></h2>
                                          </div>
                                       </div>

                                       <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                                          <div class="wpb_wrapper">
                                             <div class="arrow"></div>
                                          </div>
                                       </div>

                                       <div class="divider-wrap"><div style="height: 45px;" class="divider"></div></div>

                                       <div class="wpb_text_column wpb_content_element  vc_custom_1481742481819 pad activo" >
                                          <div class="wpb_wrapper">
                                             <p class="activo css-noMargin"><?=$title?></p>
                                             
                                             
                                 
                                             
                                             <?php 
											 
											 
									if($_REQUEST['p']==15) $url='penya-roja/avenida-francia-alameda';
									if($_REQUEST['p']==16) $url='picanya';
									
											 
											 foreach ($arr_promotions as $val): 
											 	if($val['id']==15) $url2='penya-roja/avenida-francia-alameda';
									            if($val['id']==16) $url2='picanya';
									
											 ?>
                                                <p class="css-noMargin">
                                                   <strong>
                                                      <div id="links2-link-7471" class="sh-link links2-link sh-hide">
                                                         <a href="https://www.careco.es/promociones-valencia/comprar-chalet-adosado/<?=$url2?>" class="css-noDecoration" onclick="showhide_toggle('links2', 7471, '<?=$val['title']?>', '<?=$val['title']?>'); return false;" aria-expanded="false">
                                                            <span id="links2-toggle-7471"><?=$val['title']?> <i class="fa fa-angle-right" aria-hidden="true"></i></span>
                                                         </a>
                                                      </div>
                                                   </strong>
                                                </p>
                                             <?php endforeach; ?>
                                          </div>
                                       </div>

                                       <div class="wpb_text_column wpb_content_element  vc_custom_1482324452088 pad" >
                                          <div class="wpb_wrapper">
                                             <h2 style="text-align: left; font-size: 14px;"><span style="color: #ffffff;"><a style="color: #ffffff;" href="https://www.careco.es">&lt; Volver</a></span></h2>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                                    <div  class="vc_col-sm-8 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                                       <div class="vc_column-inner">
                                          <div class="css-promotion-cover-image" style="background-image: url('<?=$image?>')"></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <div id="fws_5a6fd1b8c744e"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row  vc_row-o-equal-height vc_row-flex standard_section" style="padding-top: 0px; padding-bottom: 0px; ">
                                 <div class="row-bg-wrap">
                                    <div class="row-bg" style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
                                 </div>

                                 <div class="col span_12 dark center">
                                    <div style="" class="vc_col-sm-4 wpb_column column_container vc_column_container col no-extra-padding" data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-color="#cddddc" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                                       <div class="vc_column-inner">
                                          <div class="wpb_wrapper"> <p class="activoA2 css-noMargin"><?=$title?> <i class="fa fa-angle-down css-font24" aria-hidden="true"></i> </p></div>
                                       </div>
                                    </div>

                                    <?php ($cat == 1) ? $active = "css-promo-active" : $active = "";
									
									
									 
									
									 ?>

                                    <div style="" class="vc_col-sm-4 wpb_column column_container vc_column_container col no-extra-padding <?=$active?>" data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-color="#adc0c0" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                                       <a class="column-link" href="https://www.careco.es/promociones-valencia/comprar-chalet-adosado/<?=$url?>"></a>
                                          <div class="vc_column-inner css-paddingTB22">
                                             <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element " >
                                                   <div class="wpb_wrapper">
                                                      <h4 style="text-align: center;" class="css-lato css-noMargin">
                                                         EN VENTA
                                                      </h4>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </a>
                                    </div>

                                    <?php ($cat == 2) ? $active = "css-promo-active" : $active = ""; ?>

                                    <div style="" class="vc_col-sm-4 wpb_column column_container vc_column_container col no-extra-padding <?=$active?>" data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-color="#cddddc" data-bg-opacity="1" data-hover-bg="#000000" data-hover-bg-opacity="0.7" data-animation="" data-delay="0">
                                       <a class="column-link" href="https://www.careco.es/promociones-valencia/alquiler/<?=$url?>"></a>

                                       <div class="vc_column-inner css-paddingTB22">
                                          <div class="wpb_wrapper">
                                             <div class="wpb_text_column wpb_content_element " >
                                                <div class="wpb_wrapper">
                                                   <h4 style="text-align: center;" class="css-lato css-noMargin">
                                                      EN ALQUILER
                                                   </h4>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <div id="fws_5a6fd1b8c7e7e"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row standard_section" style="padding-top: 0px; padding-bottom: 0px; ">
                                 <div class="row-bg-wrap">
                                    <div class="row-bg" style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
                                 </div>

                                 <div class="col span_12 dark left">
                                    <div  class="vc_col-sm-12 wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                                       <div class="vc_column-inner">
                                          <div class="wpb_wrapper">
                                             <div class="divider-wrap"><div style="height: 40px;" class="divider"><h1 style="font-size:24px"><?=$title2?></h1></div><div>
              <?php if($_REQUEST['p']==16){ ?>       
                                         <p>    Grupo Careco en Picanya<br><br>

Grupo Careco es un referente en el mercado de la construcción en Valencia desde su creación en el año 1.947.<br><br>

Los principios básicos son calidad e innovación. Dos conceptos que están presentes en cada una de nuestras obras y que nos han llevado a ser galardonados con los premios más importantes del sector: La estrella de Oro a la Calidad, el Oro a la calidad en el Certamen Nacional Aqua, así como varios diplomas al mérito inmobiliario en Valencia. Además, nuestro grupo cuenta con el sello de calidad AENOR, que garantiza el control de calidad de todos los procesos y el acabado final de nuestras viviendas. <a href="#leerMas"> leer m&aacute;s </a> 
</p> <?php } ?>
                                             
                                             
                                             </div></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <?php foreach ($arr_property as $val): ?>
                                 <div id="fws_5a6fd1b8c8379"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row standard_section css-marginB90" style="padding-top: 0px; padding-bottom: 0px; ">
                                    <div class="row-bg-wrap">
                                       <div class="row-bg" style="" data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
                                    </div>

                                    <div class="col span_12 dark left">
                                       <div  class="vc_col-sm-4 wpb_column column_container vc_column_container col padding-4-percent" data-bg-cover="" data-padding-pos="right" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element " >
                                                   <div class="wpb_wrapper">
                                                      <h6 class="css-bold"><?=$val['cost']?></h6>
                                                      <h4><strong class="css-dark-gray-2 css-uppercase css-semibold" > <a style="color:#000 !important;" href="<?=$uFull?>" target="_self"><?=$val['title']?></a></strong> <?=$val['star']?></h4>
                                                      <p class="css-noPadding"><?=$val['code']?></p>

                                                      <?php if ($val['sold'] == 1): ?>
                                                         <p class="css-sold">Vendida</p>
                                                      <?php endif; ?>
                                                   </div>
                                                </div>

                                                <div class="img-with-aniamtion-wrap css-marginT10" data-max-width="100%">
                                                   <div class="inner">
                                                      <!-- Iconos -->
                                                      <?php foreach ($val['icons'] as $icon): ?>
                                                         <?php if ($icon['image']): ?>
                                                            <div class="pull-left css-marginR5">
                                                               <span class="css-icon-description" style="line-height: 3;"><?=$icon['description']?></span>
                                                               <img src="<?=APP_IMG_ICONS.$icon['image']?>" class="pull-right css-icon" alt="">
                                                            </div>
                                                         <?php endif; ?>
                                                      <?php endforeach; ?>

                                                      <div class="clearfix css-marginB10"></div>
                                                   </div>
                                                </div>

                                                <div class="divider-wrap">
                                                   <div style="height: 8px;" class="divider"></div>
                                                </div>
    <?php
                                                   $uFull= 'https://www.careco.es'.$_SERVER['REQUEST_URI'].'/'.createSlug($val['title']).'/'.$val['id'];
											//index.php?view=propertyDetail&id=<?=$val['id']?	 
												   
												   
												   ?>
                                                <div class="img-with-aniamtion-wrap " data-max-width="100%">
                                                   <div class="inner">
                                                      <a href="<?=$uFull?>" target="_self">
                                                         <img data-shadow="none" data-shadow-direction="middle" class="img-with-animation " data-delay="0" height="38" width="166" data-animation="none" src="https://www.careco.es/careco.es/views/images/properties/btn_info.jpg" alt="" />
                                                      </a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>

                                       <div  class="vc_col-sm-4 wpb_column column_container vc_column_container col padding-4-percent"  data-bg-cover="" data-padding-pos="right" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element " >
                                                   <div class="wpb_wrapper">
                                                      <h4 class="css-dark-gray-2 css-semibold">CARACTERÍSTICAS</h4>
                                                   </div>
                                                </div>

                                                <div class="divider-wrap">
                                                   <div style="height: 8px;" class="divider"></div>
                                                </div>

                                                <div class="wpb_text_column wpb_content_element  padr" >
                                                   <div class="wpb_wrapper">
                                                   
                                                 
                                                   
                                                   
                                                      <p class="css-fontSize13 css-gray-2 css-lineH"><?=$val['description']?>... [<a href="<?=$uFull?>">Más info</a>]</p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>

                                       <div  class="vc_col-sm-4 block wpb_column column_container vc_column_container col padding-4-percent"  data-bg-cover="" data-padding-pos="left" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                                          <div class="vc_column-inner">
                                             <div class="wpb_wrapper">
                                                <div class="wpb_gallery wpb_content_element clearfix">
                                                   <div class="wpb_wrapper">
                                                      <div class="wpb_gallery_slides wpb_flexslider flex-gallery flexslider" data-interval="5" data-flex_fx="fade">
                                                         <ul class="slides">
                                                            <?php foreach ($val['gallery'] as $gallery): ?>
                                                               <li>
                                                                  <div class="css-gallery-image" style="background-image: url('<?=APP_IMG_PROPERTY."property_".$val['id']."/images/gallery/min_".$gallery['image']?>')"></div>
                                                               </li>
                                                            <?php endforeach; ?>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              <?php endforeach; ?>
                              
                              
                              
                             <div>
                             
                              <?php if($_REQUEST['p']==16){ ?>       
                                         <p > <a name="leerMas"></a><br> Somos pioneros en la Comunidad Valenciana en la incorporación de nuevas tecnologías, como la instalación de sistemas domóticos, paneles de energía solar, sistema de aspiración centralizada como solución antialérgica de ácaros y polvo, control de calidad del aire en la vivienda asegurando un sistema de renovación de aire automática, sistemas de ahorro energético, así como la concepción del aislamiento acústico y térmico a partir de un estudio integral de todos los elementos constructivos que componen la vivienda.</p> 
<p>
Grupo Careco está construyendo en la zona Masía Les Palmes de Picanya, chalets adosados y pisos desde el año 2.001.
</p><p>
Picanya reúne todas las necesidades que podemos buscar a la hora de elegir nuestro futuro hogar: Zonas verdes, colegios, instalaciones deportivas, comerciales, a un paso de Valencia y con una excelente comunicación.
</p><p>
Cada chalet adosado en Picanya es un espacio de bienestar único y personal en el que no se ha dejado nada al azar. Todo está pensado para disfrutar el mayor confort, la tecnología más innovadora, el mejor aislamiento, la mejor seguridad, la facilidad en las comunicaciones, el ahorro energético y el más alto nivel de calidad. Vivir en un adosado en Picanya es descubrir la calidad de vida de una vivienda bien equilibrada.
</p><p>
Los chalets unifamiliares cuentan con garaje privado para varios vehículos, terraza con posibilidad de disfrutar de piscina, zona verde o huerta, cocina con terraza y lavadero, opción de barbacoa y paellero, buhardilla con posibilidad de hacer otro dormitorio, sala de juegos, home cinema, estudio, biblioteca, etc…. Viviendas preparadas para instalación de ascensor, placas solares para agua caliente sanitaria, ventanales con doble acristalamiento Climalit, aislamiento acústico superando la normativa.</p><p>
Pero si prefieres vivir en un piso, porque eres joven, vives sólo o simplemente no necesitas tanto espacio, también puedes disfrutar de las ventajas de vivir en Picanya. Disponemos de pisos, tanto en alquiler como en venta. Nuestros edificios son de poca altura, por lo que se encuentran totalmente integrados en el entorno residencial de la zona. Las viviendas en planta baja cuentan con terraza o jardín, también disponemos de áticos con amplias terrazas. Y como siempre, avalados por la calidad e innovación que te proporciona Grupo Careco.<br><br><br>

</p> <?php } ?>

                             </div> 

                     <div id="fws_5a6fd1b8d17ef"  data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-content  vc_row-o-equal-height vc_row-flex standard_section" style="padding-top: 0px; padding-bottom: 0px; ">
                        <div class="row-bg-wrap">
                           <div class="row-bg  using-bg-color" style="background-color: #adc0c0; " data-color_overlay="" data-color_overlay_2="" data-gradient_direction="" data-overlay_strength="0.3" data-enable_gradient="false"></div>
                        </div>

                        <div class="col span_12 dark left">
                           <div  class="vc_col-sm-8 mapa wpb_column column_container vc_column_container col no-extra-padding"  data-bg-cover="" data-padding-pos="left" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                              <div class="vc_column-inner">
                                 <div class="wpb_wrapper">
                                    <div id="map_5a6fd1b8d1c41" style="height: 400px;" class="nectar-google-map" data-dark-color-scheme="" data-ultra-flat="" data-greyscale="" data-extra-color="" data-enable-animation="false" data-enable-zoom="" data-zoom-level="14" data-center-lat="<?=$latitude?>" data-center-lng="<?=$longitude?>" data-marker-img=""></div><div class="map_5a6fd1b8d1c41 map-marker-list"><div class="map-marker" data-lat="<?=$latitude?>" data-lng="<?=$longitude?>" data-mapinfo=""></div></div>
                                 </div>
                              </div>
                           </div>

                           <div  class="vc_col-sm-4 padr wpb_column column_container vc_column_container col padding-4-percent"  data-bg-cover="" data-padding-pos="left" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                              <div class="vc_column-inner">
                                 <div class="wpb_wrapper css-location-promo">
                                    <?=$location?>                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php include('html/overall/footer.php') ?>
         </div>

         <div id="slide-out-widget-area-bg" class="fullscreen dark"></div>
      </div> <!--/ajax-content-wrap-->

      <a id="to-top" class=""><i class="fas fa-angle-up"></i></a>
      <?php include('html/overall/js.php') ?>
   </body>
</html>
