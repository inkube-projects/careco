<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>CARECO | CASA COMO TÚ :: <?=$arr_post[0]['p_title']?></title>
      <link href="<?=MAINURL?>views/app/resources_1/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?=MAINURL?>views/app/resources_1/css/animate.min.css" rel="stylesheet">
      <link href="<?=MAINURL?>views/app/plugins/font-awesome/web-fonts-with-css/css/fontawesome-all.min.css" rel="stylesheet">
      <link href="<?=MAINURL?>views/app/resources_1/css/lightbox.css" rel="stylesheet">
      <link href="<?=MAINURL?>views/app/resources_1/css/main.css" rel="stylesheet">
      <link href="<?=MAINURL?>views/app/resources_1/css/presets/preset1.css" id="css-preset" rel="stylesheet">
      <link href="<?=MAINURL?>views/app/resources_1/css/responsive.css" rel="stylesheet">
       <link rel="canonical" href="https://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" />
      <!--[if lt IE 9]>
         <script src="js/html5shiv.js"></script>
         <script src="js/respond.min.js"></script>
      <![endif]-->
      <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
      <link rel="shortcut icon" href="images/favicon.ico">
      <link href="<?=MAINURL?>views/app/resources_1/css/css-style.css" rel="stylesheet">
      <style media="screen">
         .carousel-fade .carousel-inner .item {
            height: 210px;
         }
         .navbar-default .navbar-nav > li > a {
            font-size: 24px;
            color: #7fa9ae;
			padding-top:5px;
         }
         .navbar-nav > li {
            padding: 7px 0;
         }
         .css-nav-separator {
            margin-top: 4px;
            height: 18px;
            border-right: 1px solid #7fa9ae;
         }
         .dropdown-search {
            width: 330px;
            padding: 20px;
         }
         .dropdown-search input {
            background-color: transparent;
            border-radius: 0;
            border: 1px solid #CCC;
         }
         .entry-header .date::after {
            bottom: 0;
            display: none;
         }
         .load-more {
            text-align: right;
         }
      </style>
   </head>
   <!--/head-->

   <body>
      <!--.preloader-->
      <div class="preloader"> <i class="fas fa-circle-notch fa-spin"></i></div>
      <!--/.preloader-->

      <header id="home">
         <nav class="css-nav-top">
            <div class="container">
               <div class="col-md-6">
                  <i class="fas fa-phone"></i> +34 687 715 531
                  <i class="fas fa-envelope css-marginL30"></i> careco@careco.es
               </div>

               <div class="col-md-6 text-right css-nav-top2">
                  <a href="https://www.careco.es"><i class="fas fa-arrow-circle-left css-text-green"></i>
                  <span class="css-text-black css-semibold">Volver a la web</span></a>
                  &nbsp; &nbsp;  | &nbsp; &nbsp; 
                 
                  <a href="#"><i class="fab fa-linkedin"></i></a>
                  <a href="#"><i class="fab fa-instagram"></i></a>
               </div>
            </div>
         </nav>

         <nav class="navbar navbar-default">
            <div class="container">
               <div class="container-fluid">
                  <div class="navbar-header">
                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                     </button>
                     <a class="navbar-brand" href="https://www.careco.es/blog">
                        <img src="<?=MAINURL?>views/images/blog/logo-blog.png" alt="CARECO" width="200">
                     </a>
                  </div>

                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-bars"></i> <div class="css-nav-separator"></div></a>
                           <ul class="dropdown-menu">
                              <li><a href="index.php?view=blog"><i class="fa fa-chevron-right css-text-green"></i> Todas</a></li>
                              <?php foreach ($arr_tags as $val): ?>
                                 <li><a href="https://www.careco.es/blog/categoria/<?=createSlug($val['name'])."/".$val['id']?>"> <i class="fa fa-chevron-right css-text-green"></i> <?=$val['name']?></a></li>
                              <?php endforeach; ?>
                              <li><a href="https://www.careco.es"><i class="fa fa-chevron-right css-text-green"></i> Volver a la web</a></li>
                           </ul>
                        </li>
                        <li class="dropdown">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-search"></i></a>
                           <ul class="dropdown-menu dropdown-search">
                              <li>
                                 <form name="frm-search" action="javascript:;" id="frm-search" method="post">
                                    <input type="hidden" name="view" id="view" value="blog">
                                    <div class="form-group css-noMargin">
                                       <div class="input-group">
                                          <input type="text" id="src_search" name="src_search" class="form-control" placeholder="¿Qué buscas?">
                                          <span class="input-group-btn">
                                             <button id="search1" class="btn btn-green css-text-white" type="submit">Buscar</button>
                                          </span>
                                       </div>
                                    </div>
                                 </form>
                              </li>
                           </ul>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </nav>
         <!--/#main-nav-->

         <div id="home-slider" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
               <div class="item active" style="background-image: url(<?=MAINURL?>views/images/blog/banner02.jpg)">
                  <div class="caption">

                  </div>
               </div>
            </div>
            <a id="tohash" href="#portfolio"><i class="fa fa-angle-down css-text-black"></i></a>
         </div>
      </header>

      <section id="blog">
         <div class="container">
            <div class="blog-posts">
               <div class="row">
                  <!-- Detalle del post -->
                  <div class="col-md-8 css-marginT40">
                     <a class="btn btn-green animated fadeInUpBig css-text-white" href="javascript:history.back()">Volver</a>
                  <!--   <a class="btn btn-green animated fadeInUpBig css-text-white pull-right" href="#">Siguiente <i class="fas fa-arrow-circle-right"></i></a>
                     <a class="btn btn-green animated fadeInUpBig css-text-white pull-right css-marginR20" href="#"><i class="fas fa-arrow-circle-left"></i> Anterior</a> -->

                     <div class="clearfix css-marginB30"></div>
                     <div class="css-underline-title css-marginB40"></div>

                     <h1 class="css-blogtitleD"><?=$arr_post[0]['p_title']?></h1>

                     <p class="css-text-green pull-left"><i class="far fa-calendar"></i> <?=$date['date']?></p>
                     <p class="css-text-green pull-right"><i class="far fa-comments"></i> 12</p>

                     <div class="clearfix css-marginB30"></div>

                     <img src="<?=$image?>" class="img-responsive" alt="<?=$arr_post[0]['p_title']?>">
                     <div class="clearfix css-marginB20"></div>

                     <?php foreach ($arr_post_tag as $val): ?>
                        <a class="btn btn-green animated fadeInUpBig css-text-white" href="#"><i class="fas fa-tag"></i> <?=$val['bt_name']?></a>
                     <?php endforeach; ?>

                     <div class="css-blog-description">
                        <?=$arr_post[0]['p_description']?>
                     </div>

                     <hr>

                     <p class="css-text-green css-marginB20">Compártelo</p>
                     <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b2110a6329f7e46"></script>
                     <div class="addthis_inline_share_toolbox"></div>

                     <!--<a class="btn css-btn-linkedin animated fadeInUpBig css-text-white" href="#"><i class="fab fa-linkedin-in"></i> LinkedIn</a>
                     <a class="btn css-btn-facebook animated fadeInUpBig css-text-white" href="#"><i class="fab fa-facebook-f"></i> Facebook</a>
                     <a class="btn css-btn-twitter animated fadeInUpBig css-text-white" href="#"><i class="fab fa-twitter"></i> Twitter</a>
                     <a class="btn css-btn-mail animated fadeInUpBig css-text-white" href="#"><i class="fas fa-envelope"></i> Correo</a>
                     <a class="btn css-btn-other animated fadeInUpBig css-text-white" href="#"><i class="fas fa-plus"></i> Más...</a>-->
                     
                     <p><hr></p>
                     
              

                  </div>
                  
                  
                  
               
                  
                  

                  <!-- Categorias -->
                  <div class="col-md-4 css-marginT40 displayNonePhone">
                     <h3 class="css-marginT0">Buscar</h3>
                     <div class="css-underline-title css-marginB20 css-marginT10"></div>

                     <form name="frm-search2" action="javascript:;" id="frm-search2" method="post">
                        <input type="hidden" name="view" id="view-2" value="blog">
                        <div class="form-group css-noMargin">
                           <div class="input-group">
                              <input type="text" id="src_search_2" name="src_search" class="form-control css-input-green" placeholder="¿Qué buscas?">
                              <span class="input-group-btn">
                                 <button id="search2" class="btn btn-green css-text-white" type="submit">Buscar</button>
                              </span>
                           </div>
                        </div>
                     </form>

                     <h3>Categorías</h3>
                     <div class="css-underline-title css-marginB20 css-marginT10"></div>

                     <ul class="css-ul-post">
                        <?php foreach ($arr_tags as $val): ?>
                           <li><a href="https://www.careco.es/blog/categoria/<?=createSlug($val['name'])."/".$val['id']?>"><?=$val['name']?></a></li>
                        <?php endforeach; ?>
                     </ul>
                  </div>
                  
                 
                  
                  
                  
               </div>
            </div>
         </div>
      </section>
      <!--/#blog-->
      
      
         <!-- Page Content -->
    <div class="container">
        <div class="row">
        
            
            
        </div>
    </div>
    <!-- /.container -->
      


    
      <?php include('html/includes/newsletter.php') ?>
      <?php include('html/includes/footer.php') ?>

      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/jquery.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="//maps.google.com/maps/api/js?key=AIzaSyAfoFMSNU6Vr-JaVW4BN86B_VGkCwowO_w&sensor=true"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/jquery.inview.min.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/wow.min.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/mousescroll.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/smoothscroll.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/jquery.countTo.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/lightbox.min.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/main.js"></script>
      <!-- jQuery validation -->
      <script type="text/javascript" src="<?=MAINURL?>views/app/plugins/jquery-validation/jquery.validate.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/plugins/jquery-validation/additional-methods.js"></script>
      <!-- newsletter -->
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/js-newsletter.js"></script>
      <script type="text/javascript">
      $(function(e){
         // Evita que se cierre el dropdown al darle click al formulario de busqueda
         $('.dropdown-menu').find('form').click(function (e) {
            e.stopPropagation();
         });
		 
		 
		 $('#search1').click(function(e){
			 e.preventDefault();
			 term=encodeURI($('#src_search').val().replace(/(\s+)/g, '+'));
			 if(term.length>2){
				 window.location.href="https://www.careco.es/blog/busqueda/"+encodeURI(term);
				 }
			 
			 })
			 
			 $('#search2').click(function(e){
			 e.preventDefault();
			 term=encodeURI($('#src_search_2').val().replace(/(\s+)/g, '+'));
			 if(term.length>2){
				 window.location.href="https://www.careco.es/blog/busqueda/"+encodeURI(term);
				 }
			 
			 })
		 
      });
      </script>
      
      
   </body>
</html>
