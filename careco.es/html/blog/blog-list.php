
 <?php 
 
 $categoriaActual=array('name'=>"Bienvenidos al Blog de CARECO","description"=>"");
  foreach ($arr_tags as $val){
   if( isset($_GET['t']) && $val['id']==$_GET['t'] )$categoriaActual=$val;
   }
   ?>
<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">
      <?php if($pagina>1) echo  '<meta name="robots" content="noindex, follow">'; ?>
      <title>CARECO | CASA COMO TÚ :: <?=$categoriaActual['name']?></title>
      <link href="<?=MAINURL?>views/app/resources_1/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?=MAINURL?>views/app/resources_1/css/animate.min.css" rel="stylesheet">
      <link href="<?=MAINURL?>views/app/plugins/font-awesome/web-fonts-with-css/css/fontawesome-all.min.css" rel="stylesheet">
      <link href="<?=MAINURL?>views/app/resources_1/css/lightbox.css" rel="stylesheet">
      <link href="<?=MAINURL?>views/app/resources_1/css/main.css" rel="stylesheet">
      <link href="<?=MAINURL?>views/app/resources_1/css/presets/preset1.css" id="css-preset" rel="stylesheet">
      <link href="<?=MAINURL?>views/app/resources_1/css/responsive.css" rel="stylesheet">
      <link rel="canonical" href="https://www.careco.es/blog" />
      <!--[if lt IE 9]>
         <script src="js/html5shiv.js"></script>
         <script src="js/respond.min.js"></script>
      <![endif]-->
      <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
      <link rel="shortcut icon" href="images/favicon.ico">
      <link href="<?=MAINURL?>views/app/resources_1/css/css-style.css" rel="stylesheet">
      
      <style media="screen">
         .carousel-fade .carousel-inner .item {
            height: 450px;
         }
           .navbar-default .navbar-nav > li > a {
            font-size: 24px;
            color: #7fa9ae;
			padding-top:5px;
         }
         .navbar-nav > li {
            padding: 7px 0;
         }
         .css-nav-separator {
            margin-top: 4px;
            height: 18px;
            border-right: 1px solid #7fa9ae;
         }
         .dropdown-search {
            width: 330px;
            padding: 20px;
         }
         .dropdown-search input {
            background-color: transparent;
            border-radius: 0;
            border: 1px solid #CCC;
         }
         .entry-header .date::after {
            bottom: 0;
            display: none;
         }
         .load-more {
            text-align: right;
         }
      </style>
   </head>
   <!--/head-->

   <body>
      <!--.preloader-->
      <div class="preloader"> <i class="fas fa-circle-notch fa-spin"></i></div>
      <!--/.preloader-->

      <header id="home">
         <nav class="css-nav-top">
            <div class="container">
               <div class="col-md-6">
                  <i class="fas fa-phone"></i> +34 687 715 531
                  <i class="fas fa-envelope css-marginL30"></i> careco@careco.es
               </div>

               <div class="col-md-6 text-right css-nav-top2">
                  <a href="https://www.careco.es"><i class="fas fa-arrow-circle-left css-text-green"></i>
                  <span class="css-text-black css-semibold">Volver a la web</span></a>
                  &nbsp; &nbsp;  | &nbsp; &nbsp; 
                 
                  <a href="#"><i class="fab fa-linkedin"></i></a>
                  <a href="#"><i class="fab fa-instagram"></i></a>
               </div>
            </div>
         </nav>

         <nav class="navbar navbar-default">
            <div class="container">
               <div class="container-fluid">
                  <div class="navbar-header">
                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                     </button>
                     <a class="navbar-brand" href="https://www.careco.es/blog">
                        <img src="<?=MAINURL?>views/images/blog/logo-blog.png" alt="CARECO" width="200">
                     </a>
                  </div>

                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span style=" font-size:18px;  color:#000;">CATEGORIAS &nbsp; &nbsp;</span><i class="fas fa-bars"></i> <div class="css-nav-separator"></div></a>
                           <ul class="dropdown-menu">
                              <li><a href="https://www.careco.es/blog"><i class="fa fa-chevron-right css-text-green"></i> Todas</a></li>
                              <?php
							  
							  
							   $categoriaActual=array('name'=>"Bienvenidos al Blog de CARECO","description"=>"");
							   foreach ($arr_tags as $val): 
							        if( isset($_GET['t']) && $val['id']==$_GET['t'] ){
										$categoriaActual=$val;
										}
							  
							  ?>
                                 <li><a href="https://www.careco.es/blog/categoria/<?=createSlug($val['name'])."/".$val['id']?>"><i class="fa fa-chevron-right css-text-green"></i> <?=$val['name']?></a></li>
                              <?php endforeach; ?>
                              <li><a href="#"><i class="fa fa-chevron-right css-text-green"></i> Volver a la web</a></li>
                           </ul>
                        </li>
                        <li class="dropdown">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-search"></i></a>
                           <ul class="dropdown-menu dropdown-search">
                              <li>
                                  <form name="frm-search" action="javascript:;" id="frm-search" method="get">
                                    <input type="hidden" name="view" id="view" value="blog">
                                    <div class="form-group css-noMargin">
                                       <div class="input-group">
                                          <input type="text" id="src_search" name="src_search" class="form-control" placeholder="¿Qué buscas?">
                                          <span class="input-group-btn">
                                             <button id="search1" class="btn btn-green css-text-white" type="submit">Buscar</button>
                                          </span>
                                       </div>
                                    </div>
                                 </form>
                              </li>
                           </ul>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </nav>
         <!--/#main-nav-->


         <?php
		 
		 $plusPagination="";
		  if (!isset($_GET['t'])){ ?>
         
		<!--ACCESO POR CATEGORÍAS CAMBIAR SLIDER01 POR SLIDER 02-->
         <div id="home-slider" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
               <div class="item active" style="background-image: url(<?=MAINURL?>views/images/blog/banner.jpg)">
                  <div class="caption">
                     <div class="container">
                        <div class="col-md-6 text-left">
                           <a data-scroll class="btn btn-start btn-green animated fadeInUpBig btn-slide" href="#">UN <span class="css-bold">BLOG</span> COMO TÚ</a>
                           <p class="animated fadeInRightBig css-text-black css-medium css-marginT20">La información del sector inmobiliario <br> de Valencia para tí y como tú.</p>
                        </div>

                        <div class="col-md-6">
                           <!-- <h1 class="animated fadeInLeftBig">CAR<span>ECO</span></h1> -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <a id="tohash" href="#portfolio"><i class="fa fa-angle-down css-text-black"></i></a>
         </div>
         <!--END.SLIDER 01..-->
         <?php } else { // hay categoria 
		 
		 $plusPagination = "categoria/".createSlug($categoriaActual['name'])."/".$_GET['t']."/";
		 ?> 
         
         <!--SLIDER 02-->
         <div id="home-slider" class="carousel slide carousel-fade" data-ride="carousel" >
            <div class="carousel-inner">
               <div class="item active" style="background-image: url(<?=MAINURL?>views/images/blog/banner02.jpg); height:210px !important">
                  <div class="caption">

                  </div>
               </div>
            </div>
            <a id="tohash" href="#portfolio"><i class="fa fa-angle-down css-text-black"></i></a>
         </div>
         <!--END SLIDER 02-->
         <?php } ?>
         
      </header>

      <section id="blog">
         <div class="container">
            <div class="heading wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="300ms">
               <h1 class="css-fontSize25 css-text-black css-blog-title" style="text-transform:uppercase;"><?=$categoriaActual['name']?></h1>
               <div class="css-underline-title"></div>
             <?php  if(count($arr_post)>0) { ?> <p class="css-marginT30"><?=cut_text($categoriaActual['description'], 250)?></p> <?php } ?>
            </div>
            

            <div class="blog-posts">
            
            	<!--olcultar al llegar con la categoría-->
                <?php if (!isset($_GET['t'])){ ?>
               <p class="css-semibold css-fontSize15 css-text-black css-marginB30 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="400ms">LO ÚLTIMO <i class="fas fa-angle-down"></i></p>
               <?php } ?>
				<!--end-->
                
               <div class="row">
                  <?php foreach ($arr_post as $val):
				     $linkUrl='https://www.careco.es/blog/'.createSlug($val['title']).'/'.$val['id'];
				   ?>
                     <div class="col-sm-4 wow fadeInUp css-marginB40" data-wow-duration="1000ms" data-wow-delay="400ms">
                        <div class="post-thumb">
                           <a href="<?=$linkUrl?>">
                              <div class="css-blog-entry-image" style="background-image: url('<?=$val['image']?>')">
                                 <!--<div class="css-blog-category">
                                    <i class="fas fa-tag"></i>
                                    <?//=$val['category']?>
                                 </div>-->
                              </div>
                           </a>
                        </div>
                        <div class="entry-header">
                           <span class="date css-marginT10 css-marginB10"><i class="far fa-calendar"></i> <?=$val['date']?></span>
                           <hr class="css-marginT10 css-marginB10">
                           <h3 class="css-marginT0 css-fontSize16"><a href="<?=$linkUrl?>"><?=$val['title']?></a></h3>
                        </div>
                        <div class="entry-content">
                           <p class="css-justify" style="height:200px"><?=$val['description']?>...</p>
                           <hr class="css-marginT10 css-marginB10">
                           <div class="css-blog-icon css-semibold pull-left">
                              <!--<i class="far fa-comments"></i> 12-->
                           </div>

                           <div class="css-blog-icon css-semibold pull-right">
                            <a href="<?=$linkUrl?>"> Leer más <i class="fas fa-chevron-right"></i></a>
                           </div>
                           <div class="clearfix"></div>

                           <hr class="css-marginT10 css-marginB10">
                        </div>
                     </div>
                  <?php endforeach; ?>
               </div>
                
            </div>
            
           <div  class="col-sm-12"  id="page-selection"></div>
            <div  class="col-sm-12">
            <?=$categoriaActual['description']?>  
            </div>
          
         </div>
      </section>
      <!--/#blog-->

 
	   <?php include('html/includes/newsletter.php') ?>
      <?php include('html/includes/footer.php') ?>

      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/jquery.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/bootstrap.min.js"></script>
      //pagination
      <script src="<?=MAINURL?>views/app/resources_1/js/jquery.bootpag.min.js"></script>
      
      <script type="text/javascript" src="//maps.google.com/maps/api/js?key=AIzaSyAfoFMSNU6Vr-JaVW4BN86B_VGkCwowO_w&sensor=true"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/jquery.inview.min.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/wow.min.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/mousescroll.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/smoothscroll.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/jquery.countTo.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/lightbox.min.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/main.js"></script>
             <!-- jQuery validation -->
      <script type="text/javascript" src="<?=MAINURL?>views/app/plugins/jquery-validation/jquery.validate.js"></script>
      <script type="text/javascript" src="<?=MAINURL?>views/app/plugins/jquery-validation/additional-methods.js"></script>
      <!-- newsletter -->
      <script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/js-newsletter.js"></script>
      <script type="text/javascript">
      $(function(e){
         // Evita que se cierre el dropdown al darle click al formulario de busqueda
         $('.dropdown-menu').find('form').click(function (e) {
            e.stopPropagation();
         });
		 
		 $('#page-selection').bootpag({
            total: <?=$total_paginas?>, // total pages
			page: <?=$pagina?>,// default page
			maxVisible: 10,     // visible pagination
			leaps: true         // next/prev leaps through maxVisible
        }).on("page", function(event, num){
			top.location.href="https://www.careco.es/blog/<?=$plusPagination?>"+num;
			
		}); 
		
		$('#search1').click(function(e){
			 e.preventDefault();
			 term=encodeURI($('#src_search').val().replace(/(\s+)/g, '+'));
			 if(term.length>2){
				 top.location.href="https://www.careco.es/blog/busqueda/"+encodeURI(term);
				 }
			 
			 })
		 
		 
		 
      });
      </script>
   </body>
</html>
