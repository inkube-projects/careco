<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">
      <title><?=APP_TITLE?> | CASA COMO TÚ :: Configurador</title>
      <link href="views/app/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="views/app/resources_1/css/animate.min.css" rel="stylesheet">
      <link href="views/app/plugins/font-awesome/web-fonts-with-css/css/fontawesome-all.min.css" rel="stylesheet">
      <link href="views/app/resources_1/css/lightbox.css" rel="stylesheet">
      <link href="views/app/resources_1/css/main.css" rel="stylesheet">
      <link href="views/app/resources_1/css/presets/preset1.css" id="css-preset" rel="stylesheet">
      <link href="views/app/resources_1/css/responsive.css" rel="stylesheet">
      <link rel="canonical" href=" https://www.careco.es/CONFIGURADOR-PISOS-CHALETS-ADOSADOS-VALENCIA" />
      <!--[if lt IE 9]>
         <script src="js/html5shiv.js"></script>
         <script src="js/respond.min.js"></script>
      <![endif]-->
      <link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
      <link rel="shortcut icon" href="views/images/favicon.ico">
      <link href="views/app/resources_1/css/css-style.css" rel="stylesheet">
      
      
		<style>
		@import url('https://fonts.googleapis.com/css?family=Montserrat');
		</style>
		<style>
			body,html{background:#969696;font-family: 'Montserrat', sans-serif;text-transform:uppercase;font-weight:bold;font-size:16px;}
			#contenedor{width:1600px;height:610px;margin:auto;}
			#parametros{float:left;height:500px;padding:0px;width:560px;position:absolute;background:#969696;}
			#imagenes{width:1046px;float:right;height:100%;position:relative;background:#969696;}
			#imagenes img{width:100%;position:absolute;}
			.botonparedes,.botonsuelo,.botonencimera,.botonfrentes{border:0px solid white;width:33%;height:112px;float:left;overflow:hidden;}
			hr{clear:both;margin-top:20px;padding-top:6px;border:0px;}
			.imagenboton{width:100%;height:100%;}
			.imglogo{width:100%;}
			#botonera{
				width:100%;
				margin-bottom:48px;
			}
			#botonera .boton{width:33%;border:0px;margin:0px;padding:0px;height:48px;line-height:48px;text-align:center;text-transform:uppercase;border-radius:0px;background:white;color:#4a4a4a;font-weight:bold;float:left;transition:all 1s;}
			.botonseleccionado{background:#4e4e4e !important;color:white !important;}
			#overlay{width:100%;height:100%;position:absolute;top:0px;left:0px;background:url(pattern.png);opacity:0.5;}
			.tituloseccion{width:100%;height:40px;line-height:40px;padding-left:20px;color:white;font-size:18px;}
			#titulo{color:white;padding:20px;font-size:25px;}
			.botoncalidadesseleccionado{border:4px solid #81a7b0;    width: 31%;height:56px !important;
			    height: 105px;background:url(views/images/configurador/Capa_Alfa.png);
			    }
			    .oscuro{
			    -webkit-filter: brightness(0.7);
			    -moz-filter: brightness(0.7);
			    -o-filter: brightness(0.7);
			    -ms-filter: brightness(0.7);
			    filter: brightness(0.7);
			    }
			    #botoneracocina{display:none;}
			    #botonerabuhardilla{display:none;}
			    
			    @media screen and (min-width:3000px){
			   	#parametros{		width:666px;  	}
			   	#imagenes{		width:2372px;  	}
			   	#contenedor{		width:3038px;  	}
			   	.botoncalidadesseleccionado{border:4px solid #81a7b0;    width: 31%;height:104px !important;
			    height: 105px;background:url(views/images/configurador/Capa_Alfa.png);
			    }
			   	
			   }
			   @media screen and (min-width:1800px) and (max-width:3001px){
			   	#parametros{		width:620px;  	}
			   	#imagenes{		width:1230px;  	}
			   	#contenedor{		width:1850px;  	}
			   	.botoncalidadesseleccionado{border:4px solid #81a7b0;    width: 31%;height:104px !important;
			    height: 105px;background:url(views/images/configurador/Capa_Alfa.png);
			    }
			   	
			   }
			   @media screen and (min-width:1200px) and (max-width:1800px){
			   	#parametros{		width:320px;  	}
			   	#imagenes{		width:880px;  	}
			   	#contenedor{		width:1200px;  	}
			   	#titulo{font-size:14px;}
			   	#botonera .boton{font-size:12px;}
			   	.tituloseccion{font-size:12px;}
			   	.botoncalidades{height:64px;}
			   	.botoncalidadesseleccionado{border:4px solid #81a7b0;    width: 31%;height:56px !important;
			    height: 105px;background:url(views/images/configurador/Capa_Alfa.png);
			    }
			   }
			   @media screen and (min-width:900px) and (max-width:1200px){
			   	#parametros{		width:320px;  	}
			   	#imagenes{		width:580px;  	}
			   	#contenedor{		width:900px;  	}
			   	#titulo{font-size:14px;}
			   	#botonera .boton{font-size:12px;}
			   	.tituloseccion{font-size:12px;}
			   	.botoncalidades{height:64px;}
			   	.botoncalidadesseleccionado{border:4px solid #81a7b0;    width: 31%;height:56px !important;
			    height: 105px;background:url(views/images/configurador/Capa_Alfa.png);
			    }
			   }
			   @media screen and (min-width:600px) and (max-width:900px){
			   	#parametros{		width:320px;height:427px;  	}
			   	#imagenes{		width:600px;margin-top:426px;  	}
			   	#contenedor{		width:600px;  	}
			   	#titulo{font-size:14px;}
			   	#botonera .boton{font-size:12px;}
			   	.tituloseccion{font-size:12px;}
			   	.botoncalidades{height:64px;}
			   	.botoncalidadesseleccionado{border:4px solid #81a7b0;    width: 31%;height:56px !important;
			    height: 105px;background:url(views/images/configurador/Capa_Alfa.png);
			    }
			   	
			   }
			   @media screen and (max-width:600px){
			   	#parametros{		width:320px;height:427px;  	}
			   	#imagenes{		width:320px;margin-top:426px;  	}
			   	#contenedor{		width:320px;  	}
			   	#titulo{font-size:14px;}
			   	#botonera .boton{font-size:12px;}
			   	.tituloseccion{font-size:12px;}
			   	.botoncalidades{height:64px;}
			   	.botoncalidadesseleccionado{border:4px solid #81a7b0;    width: 31%;height:56px !important;
			    height: 105px;background:url(views/images/configurador/Capa_Alfa.png);
			    }
			   }
		</style>

   </head>
   <!--/head-->

   <body>
      <!--.preloader-->
      <div class="preloader"> <i class="fas fa-circle-notch fa-spin"></i></div>
      <!--/.preloader-->

      <header id="home">
         <nav class="css-nav-top">
            <div class="container">
               <div class="col-md-6">
                  <i class="fas fa-phone"></i> +34 687 715 531
                  <i class="fas fa-envelope css-marginL30"></i> careco@careco.es
               </div>

               <div class="col-md-6 text-right">
                  <a href="#"><i class="fab fa-linkedin"></i></a>
                  <a href="#"><i class="fab fa-instagram"></i></a>
               </div>
            </div>
         </nav>

         <nav class="navbar navbar-default">
            <div class="container">
               <div class="container-fluid">
                  <div class="navbar-header">
                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                     </button>
                     <a class="navbar-brand" href="#">
                        <img src="views/images/logo-high-n-01.png" alt="CARECO" width="120">
                     </a>
                  </div>

                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Promociones en Valencia <div class="css-nav-separator"></div></a>
                           <ul class="dropdown-menu">
                              <li><a href="https://www.careco.es/promociones-valencia/comprar-chalet-adosado/penya-roja/avenida-francia-alameda">Promociones en curso <i class="fas fa-angle-right css-text-green css-marginL10"></i></a></li>
                       		  <li><a href="https://www.careco.es/proximas-promociones-valencia">Próximas promociones <i class="fas fa-angle-right css-text-green css-marginL10"></i></a></li>
                        	  <li><a href="https://www.careco.es/nuestras-obras">Promociones realizadas<i class="fas fa-angle-right css-text-green css-marginL10"></i></a></li>
                           </ul>
                        </li>
                        <li><a href="https://www.careco.es/asi-somos">Careco <div class="css-nav-separator"></div></a></li>
                        <li><a href="https://www.careco.es/blog">Blog <div class="css-nav-separator"></div></a></li>
                        <li><a href="https://www.careco.es/atencion-al-cliente">Atención al cliente <div class="css-nav-separator"></div></a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </nav>
         <!--/#main-nav-->

 
      </header>



<div id="contenedor">
			<div id="parametros">
			<div id="titulo">
			Configurador de la vivienda
			</div>
			<div id="botonera">
				<div id="botonsalon" class="boton botonseleccionado">Salon</div>
				<div id="botoncocina" class="boton">Cocina</div>
				<div id="botonbuhardilla" class="boton">Buhardilla</div>
			</div>
			
			<div id="botonerasalon" class="secciones">
				<div id="suelos" style="color:rgb(103, 103, 103);">
				<div class="tituloseccion">
					Acabados suelo
					</div>
					<div id="V1" class="botonsuelo botoncalidades grupo1"><img src="views/images/configurador/Suelo_V1.png" class="imagenboton"></div>
					<div id="V2" class="botonsuelo botoncalidades grupo1"><img src="views/images/configurador/Suelo_V2.png" class="imagenboton"></div>
					<div id="V3" class="botonsuelo botoncalidades grupo1"><img src="views/images/configurador/Suelo_V3.png" class="imagenboton"></div>
					<div style="clear:both;"></div>
				</div>
				
				<div id="paredes" style="color:rgb(103, 103, 103);">
				<div class="tituloseccion">
					Acabados paredes</div>
					<div id="BR" class="botonparedes botoncalidades grupo2"><img src="views/images/configurador/Pared_BR.png" class="imagenboton"></div>
					<div id="Marfil" class="botonparedes botoncalidades grupo2"><img src="views/images/configurador/Pared_Marfil.png" class="imagenboton"></div>
					<div id="Marron" class="botonparedes botoncalidades grupo2"><img src="views/images/configurador/Pared_Marron.png" class="imagenboton"></div>
					<div style="clear:both;"></div>
				</div>
			</div>
			
			<div id="botoneracocina" class="secciones">
				<div id="suelos" style="color:rgb(103, 103, 103);">
					<div class="tituloseccion">
					Acabados suelo
					</div>
					<div id="Blanco" class="botonsuelo botoncalidades grupo1"><img src="views/images/configurador/Cocina_Suelo_Blanco.jpg" class="imagenboton"></div>
					<div id="Gris" class="botonsuelo botoncalidades grupo1"><img src="views/images/configurador/Cocina_Suelo_Gris.jpg" class="imagenboton"></div>
					<div id="Negro" class="botonsuelo botoncalidades grupo1"><img src="views/images/configurador/Cocina_Suelo_Negro.jpg" class="imagenboton"></div>
					<div style="clear:both;"></div>
				</div>
				
				
				<div id="frentes" style="color:rgb(103, 103, 103);">
					<div class="tituloseccion">
					Acabados encimera
					</div>
					<div id="Blanca" class="botonfrentes botoncalidades grupo2"><img src="views/images/configurador/Blanco-Maple.jpg" class="imagenboton"></div>
					<div id="Gris" class="botonfrentes botoncalidades grupo2"><img src="views/images/configurador/Bancada-Cocina-GRIS-EXPO.jpg" class="imagenboton"></div>
					<div id="Naranja" class="botonfrentes botoncalidades grupo2"><img src="views/images/configurador/Ss_Life_Naranja-Cool.jpg" class="imagenboton"></div>
					<div style="clear:both;"></div>
					
				</div>
				<div id="encimeras" style="color:rgb(103, 103, 103);">
					<div class="tituloseccion">
					Acabados frentes armariadas
					</div>
					<div id="Beige" class="botonencimera botoncalidades grupo3"><img src="views/images/configurador/Cocina_Armarios_Beige.jpg" class="imagenboton"></div>
					<div id="Blancos" class="botonencimera botoncalidades grupo3"><img src="views/images/configurador/Cocina_Armarios_Blancos.jpg" class="imagenboton"></div>
					<div id="Grises" class="botonencimera botoncalidades grupo3"><img src="views/images/configurador/Cocina_Armarios_Gris.jpg" class="imagenboton"></div>
					<div style="clear:both;"></div>
				</div>
			</div>
			
			<div id="botonerabuhardilla" class="secciones">
				<div id="suelos" style="color:rgb(103, 103, 103);">
				<div class="tituloseccion">
					Acabados suelo
					</div>
					<div id="V1" class="botonsuelo botoncalidades grupo1"><img src="views/images/configurador/Suelo_V1.png" class="imagenboton"></div>
					<div id="V2" class="botonsuelo botoncalidades grupo1"><img src="views/images/configurador/Suelo_V2.png" class="imagenboton"></div>
					<div id="V3" class="botonsuelo botoncalidades grupo1"><img src="views/images/configurador/Suelo_V3.png" class="imagenboton"></div>
					<div style="clear:both;"></div>
				</div>
				
				<div id="paredes" style="color:rgb(103, 103, 103);">
				<div class="tituloseccion">
					Acabados paredes</div>
					<div id="BR" class="botonparedes botoncalidades grupo2"><img src="views/images/configurador/Pared_BR.png" class="imagenboton"></div>
					<div id="Marfil" class="botonparedes botoncalidades grupo2"><img src="views/images/configurador/Pared_Marfil.png" class="imagenboton"></div>
					<div id="Marron" class="botonparedes botoncalidades grupo2"><img src="views/images/configurador/Pared_Marron.png" class="imagenboton"></div>
					<div style="clear:both;"></div>
				</div>
			</div>
			</div>
			
			<div id="imagenes">
				
				<img src="views/images/configurador/Salon_Suelo_V1_Pared_BR.jpg">
				
			</div>
		</div>
	</body>


       <?php include('html/overall/contact.php') ?>
      <?php include('html/includes/newsletter.php') ?>
      <?php include('html/includes/footer.php') ?>


      <script type="text/javascript" src="views/app/resources_1/js/jquery.js"></script>
      <script type="text/javascript" src="views/app/plugins/bootstrap/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="//maps.google.com/maps/api/js?key=AIzaSyAfoFMSNU6Vr-JaVW4BN86B_VGkCwowO_w&sensor=true"></script>
      <script type="text/javascript" src="views/app/resources_1/js/jquery.inview.min.js"></script>
      <script type="text/javascript" src="views/app/resources_1/js/wow.min.js"></script>
      <script type="text/javascript" src="views/app/resources_1/js/mousescroll.js"></script>
      <script type="text/javascript" src="views/app/resources_1/js/smoothscroll.js"></script>
      <script type="text/javascript" src="views/app/resources_1/js/jquery.countTo.js"></script>
      <script type="text/javascript" src="views/app/resources_1/js/lightbox.min.js"></script>
      <script type="text/javascript" src="views/app/resources_1/js/main.js"></script>
      <!-- jQuery validation -->
      <script type="text/javascript" src="views/app/plugins/jquery-validation/jquery.validate.js"></script>
      <script type="text/javascript" src="views/app/plugins/jquery-validation/additional-methods.js"></script>
      <!-- newsletter -->
      <script type="text/javascript" src="views/app/resources_1/js/js-newsletter.js"></script>
      <script type="text/javascript" src="views/app/resources_1/js/js-contact.js"></script>
      <script>
			var suelo = "V1";
			var pared = "BR";
			var suelococina = "Blanco";
			var encimera = "Beige";
			var frente = "Blanca";
			$(document).ready(function(){
				$( window ).resize(function() {
				if(window.innerWidth < 900){
					$("#imagenes").css("margin-top","426px")
				}else{
					$("#imagenes").css("margin-top","0px")
				}
				})
				$(".boton").click(function(){
					if(($(this).attr("id") == "botonsalon" || $(this).attr("id") == "botonbuhardilla") && window.innerWidth < 900){
						$("#imagenes").css("margin-top","331px")
					}
					if(($(this).attr("id") == "botoncocina") && window.innerWidth < 900){
						$("#imagenes").css("margin-top","426px")
					}
				})
				// SALON
				$("#botonerasalon .botonsuelo").click(function(){
					
					suelo = $(this).attr("id")
					$("#imagenes img").attr("src","views/images/configurador/Salon_Suelo_"+suelo+"_Pared_"+pared+".jpg")
					console.log("views/images/configurador/Salon_Suelo_"+suelo+"_Pared_"+pared+".jpg")
					
				})
				$("#botonerasalon .botonparedes").click(function(){
					pared = $(this).attr("id")
					$("#imagenes img").attr("src","views/images/configurador/Salon_Suelo_"+suelo+"_Pared_"+pared+".jpg")
					
				})
				// BUHARDILLA
				$("#botonerabuhardilla .botonsuelo").click(function(){
					
					suelo = $(this).attr("id")
					$("#imagenes img").attr("src","views/images/configurador/Buhardilla_Suelo_"+suelo+"_Pared_"+pared+".jpg")
					console.log("views/images/configurador/Salon_Suelo_"+suelo+"_Pared_"+pared+".jpg")
					
				})
				$("#botonerabuhardilla .botonparedes").click(function(){
					pared = $(this).attr("id")
					$("#imagenes img").attr("src","views/images/configurador/Buhardilla_Suelo_"+suelo+"_Pared_"+pared+".jpg")
					
				})
				// COCINA
				$("#botoneracocina .botonsuelo").click(function(){
					
					suelococina = $(this).attr("id")
					$("#imagenes img").attr("src","views/images/configurador/Cocina_S_"+suelococina+"_M_"+encimera+"_B_"+frente+".jpg")
					console.log("views/images/configurador/Salon_Suelo_"+suelo+"_Pared_"+pared+".jpg")
					
				})
				$("#botoneracocina .botonencimera").click(function(){
					encimera = $(this).attr("id")
					$("#imagenes img").attr("src","views/images/configurador/Cocina_S_"+suelococina+"_M_"+encimera+"_B_"+frente+".jpg")
					
				})
				$("#botoneracocina .botonfrentes").click(function(){
					frente = $(this).attr("id")
					$("#imagenes img").attr("src","views/images/configurador/Cocina_S_"+suelococina+"_M_"+encimera+"_B_"+frente+".jpg")
					
				})
				// COCINA////////////////////////////////////////////
				
				$("#botonera .boton").click(function(){
					$("#botonera .boton").removeClass("botonseleccionado")
					$(this).addClass("botonseleccionado")
				})
				
				$(".grupo1").click(function(){
					$(".grupo1").removeClass("botoncalidadesseleccionado")
					$(this).addClass("botoncalidadesseleccionado")
				})
				$(".grupo2").click(function(){
					$(".grupo2").removeClass("botoncalidadesseleccionado")
					$(this).addClass("botoncalidadesseleccionado")
				})
				$(".grupo3").click(function(){
					$(".grupo3").removeClass("botoncalidadesseleccionado")
					$(this).addClass("botoncalidadesseleccionado")
				})
				
				/*
				$(".botoncalidades").click(function(){
					$(".botoncalidades").removeClass("botoncalidadesseleccionado")
					$(this).addClass("botoncalidadesseleccionado")
				})
				*/
				
				$("#botoncocina").click(function(){
					$(".secciones").hide()
					$("#botoneracocina").show()
					$("#imagenes img").attr("src","views/images/configurador/Cocina_S_"+suelococina+"_M_"+encimera+"_B_"+frente+".jpg")
				})
				$("#botonsalon").click(function(){
					$(".secciones").hide()
					$("#botonerasalon").show()
					$("#imagenes img").attr("src","views/images/configurador/Salon_Suelo_"+suelo+"_Pared_"+pared+".jpg")
				})
				$("#botonbuhardilla").click(function(){
					$(".secciones").hide()
					$("#botonerabuhardilla").show()
					$("#imagenes img").attr("src","views/images/configurador/Buhardilla_Suelo_"+suelo+"_Pared_"+pared+".jpg")
				
				})
				$(".botoncalidades").hover(function(){$(this).addClass("oscuro")},function(){$(this).removeClass("oscuro")})
			})
		</script>
   </body>
</html>
