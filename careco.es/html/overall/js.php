
<script type="text/javascript">function showhide_toggle(a,b,d,f){var e=jQuery("#"+a+"-link-"+b),c=jQuery("a",e),g=jQuery("#"+a+"-content-"+b);a=jQuery("#"+a+"-toggle-"+b);e.toggleClass("sh-show sh-hide");g.toggleClass("sh-show sh-hide").toggle();"true"===c.attr("aria-expanded")?c.attr("aria-expanded","false"):c.attr("aria-expanded","true");a.text()===d?a.text(f):a.text(d)};</script>
<link rel='stylesheet' id='sbvcbgslider-style-css'  href='<?=MAINURL?>views/app/resources_2/css/style.4.7.css' type='text/css' media='all' />
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/core.min.1.11.4.js'></script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/widget.min.1.11.4.js'></script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/position.min.1.11.4.js'></script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/menu.min.1.11.4.js'></script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/wp-a11y.min.4.7.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var uiAutocompleteL10n = {
   "noResults":"Sin resultados.",
   "oneResult":"1 resultado encontrado. Utiliza las teclas de flecha arriba y abajo para navegar.",
   "manyResults":"%d resultados encontrados. Utiliza las teclas arriba y abajo para navegar.",
   "itemSelected":"Elemento seleccionado."};
/* ]]> */
</script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/autocomplete.min.1.11.4.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var MyAcSearch = {
   "url":"http:\/\/www.careco.es\/wp-admin\/admin-ajax.php"
};
/* ]]> */
</script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/wpss-search-suggest.js'></script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/jquery.form.min.3.51.0-2014.06.20.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpcf7 = {
   "recaptcha":{
      "messages":{
         "empty":"Por favor, prueba que no eres un robot."
      }
   }
};
/* ]]> */
</script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/scripts.4.6.1.js'></script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/nicescroll.3.5.4.js'></script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/prettyPhoto.7.0.1.js'></script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/midnight.1.0.js'></script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/superfish.1.4.8.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var nectarLove = {
   "ajaxurl":"http:\/\/www.careco.es\/wp-admin\/admin-ajax.php",
   "postID":"7471",
   "rooturl":"http:\/\/www.careco.es",
   "pluginPages":[],
   "disqusComments":"false",
   "loveNonce":"5725bf779a",
   "mapApiKey":"AIzaSyA44aW6ukgUoBzZd5Dn5bsJloe5v0u-5rw"
};
/* ]]> */
</script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/init.7.6.js'></script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/infinitescroll.1.1.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var mejsL10n = {
   "language":"es-ES",
   "strings":{
      "Close":"Cerrar",
      "Fullscreen":"Pantalla completa",
      "Turn off Fullscreen":"Salir de pantalla completa",
      "Go Fullscreen":"Ver en pantalla completa",
      "Download File":"Descargar archivo",
      "Download Video":"Descargar v\u00eddeo",
      "Play":"Reproducir",
      "Pause":"Pausa",
      "Captions\/Subtitles":"Pies de foto \/ Subt\u00edtulos",
      "None":"None",
      "Time Slider":"Control de tiempo",
      "Skip back %1 seconds":"Retroceder %1 segundos",
      "Video Player":"Reproductor de v\u00eddeo",
      "Audio Player":"Reproductor de audio",
      "Volume Slider":"Control de volumen",
      "Mute Toggle":"Desactivar sonido",
      "Unmute":"Activar sonido",
      "Mute":"Silenciar",
      "Use Up\/Down Arrow keys to increase or decrease volume.":"Utiliza las teclas de flecha arriba\/abajo para aumentar o disminuir el volumen.",
      "Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.":"Usa las teclas de direcci\u00f3n izquierda\/derecha para avanzar un segundo, y las flechas arriba\/abajo para avanzar diez segundos."
   }
};
var _wpmejsSettings = {
   "pluginPath":"\/wp-includes\/js\/mediaelement\/"
};
/* ]]> */
</script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/mediaelement-and-player.min.2.22.0.js'></script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/wp-mediaelement.min.4.7.js'></script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/flickity.min.1.1.1.js'></script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/nectar-slider.7.6.js'></script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/select2.min.3.5.2.js'></script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/wp-embed.min.4.7.js'></script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/js_composer_front.min.4.12.1.js'></script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/jquery.backstretch.min.1.1.js'></script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/jquery.flexverticalcenter.1.1.js'></script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/script.1.1.js'></script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/jquery.flexslider-min.4.12.1.js'></script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/map.7.0.7.js'></script>
<script type="text/javascript" src="<?=MAINURL?>views/app/plugins/bootstrap/js/bootstrap.js"></script>
<div class="essb_mailform">
   <div class="essb_mailform_content">
      <p>Send this to friend</p>
      <label class="essb_mailform_content_label">Your email</label>
      <input type="text" id="essb_mailform_from" class="essb_mailform_content_input" placeholder="Your email"/>
      <label class="essb_mailform_content_label">Recipient email</label>
      <input type="text" id="essb_mailform_to" class="essb_mailform_content_input" placeholder="Recipient email"/>
      <div class="essb_mailform_content_buttons">
         <button id="essb_mailform_btn_submit" class="essb_mailform_content_button" onclick="essb_mailform_send();">Send</button>
         <button id="essb_mailform_btn_cancel" class="essb_mailform_content_button" onclick="essb_close_mailform(); return false;">Cancel</button>
      </div>
      <input type="hidden" id="essb_mail_salt" value="1362929168"/>
      <input type="hidden" id="essb_mail_instance" value=""/>
      <input type="hidden" id="essb_mail_post" value=""/>
   </div>
</div>
<div class="essb_mailform_shadow"></div>
<link rel="stylesheet" id="essb-cct-style" href="//www.careco.es/wp-content/plugins/easy-social-share-buttons3/lib/modules/click-to-tweet/assets/css/styles.css" type="text/css" media="all" />

<!-- jQuery validation -->
<script type="text/javascript" src="<?=MAINURL?>views/app/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="<?=MAINURL?>views/app/plugins/jquery-validation/additional-methods.js"></script>
<!-- newsletter -->
<script type="text/javascript" src="<?=MAINURL?>views/app/resources_1/js/js-newsletter.js"></script>
