<section id="contact">
   <div class="col-md-6 css-paddingL90 css-paddingR70 css-background-green css-paddingT50 css-paddingB30 css-min-height950">
      <h2 class="css-bold css-text-green css-fontSize22">ATENCIÓN AL CLIENTE</h2>
      <p class="css-text-black css-justify">En Careco llevamos desde 1947 anticipándonos al cambio. Creemos que sólo así se puede alcanzar la calidad de vida, por eso, diseñamos, construimos y promovemos viviendas y construcciones tecnológicamente avanzadas que han sido pensadas para disfrutar del mañana. <span class="css-bolditalic">Hoy por hoy, vamos por delante.</span></p>

      <h4 class="css-bold css-text-green css-marginT40 css-fontSize16">SEDE CENTRAL</h4>
      <p class="css-text-black">
         Carrer del Mar, 44 <br>
         46003 Valéncia, Valencia
      </p>

      <p class="css-text-black">
         T. +34 96 315 43 80 <br>
         M. +34 687 715 531 <br>
         <b>Email: careco@careco.es</b>
      </p>

      <div class="col-md-10 css-noPadding css-marginT50">
         <div id="google-map" class="wow fadeIn" data-latitude="39.4734651" data-longitude="-0.373807" data-wow-duration="1000ms" data-wow-delay="400ms"></div>
      </div>
   </div>

   <div class="col-md-6 css-background-gray css-paddingT50 css-min-height950 css-paddingL70 css-paddingR90">
      <form name="frm-contact" id="frm-contact" method="post" action="">
         <i class="fas fa-comments css-text-green css-fontSize35 css-marginB30"></i>
         <div class="row  wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
            <div class="col-sm-6">
               <div class="form-group">
                  <label>¿Qué te interesa?</label>
                  <select class="form-control" name="regime" id="regime">
                  		<option value="0">Seleccione una opción</option>
                     <?php foreach ($arr_departaments as $val): ?>
                        <option value="<?=$val['id']?>"><?=$val['name']?></option>
                     <?php endforeach; ?>
                  </select>
                  <div class="text-danger" id="regime_validate"></div>
               </div>
            </div>

            <div class="col-sm-6">
               <div class="form-group">
                  <label>Nombre(s)*</label>
                  <input type="text" class="form-control" name="contact_name" id="contact_name">
                  <div class="text-danger" id="contact_name_validate"></div>
               </div>
            </div>

            <div class="col-sm-6">
               <div class="form-group">
                  <label>Apellido(s)*</label>
                  <input type="text" class="form-control" name="contact_last_name" id="contact_last_name">
                  <div class="text-danger" id="contact_last_name_validate"></div>
               </div>
            </div>

            <div class="col-sm-6">
               <div class="form-group">
                  <label>Teléfono*</label>
                  <input type="text" class="form-control" name="contact_phone" id="contact_phone">
                  <div class="text-danger" id="contact_phone_validate"></div>
               </div>
            </div>

            <div class="col-sm-6">
               <div class="form-group">
                  <label>E-mail*</label>
                  <input type="text" class="form-control" name="contact_email" id="contact_email">
                  <div class="text-danger" id="contact_email_validate"></div>
               </div>
            </div>
         </div>

         <div class="form-group">
            <label>Cuéntanos</label>
            <textarea name="contact_message" id="contact_message" class="form-control" rows="4"></textarea>
            <div class="text-danger" id="contact_message_validate"></div>
         </div>

         <div class="form-group">
            <button type="submit" class="btn-submit js-submit">Enviar</button>
         </div>

         <div class="checkbox">
            <label class="css-regular">
               <input type="checkbox" name="temrs" id="temrs" value="1"> Acepto los términos expuestos:
            </label>

            <div class="text-danger" id="temrs_validate"></div>
         </div>

         <p class="css-text-terms">Los datos de carácter personal que se faciliten mediante este formulario quedarán registrados en un fichero de titularidad privada, debidamente inscrito en el RGPD, cuyo responsable es CARECO S.A. y serán utilizados para contestar a las consultas planteadas. Ud. puede ejercitar los derechos de acceso, rectificación, cancelación  y oposición según lo establecido en el Título III del Reglamento de desarrollo de la LOPD (RD 1720/2007 de 21 de diciembre) <br> visite nuestro <a href="https://www.careco.es/legal"> Aviso Legal y Política de Privacidad</a></p>
      </form>
   </div>

   <div class="clearfix"></div>
</section>

<!-- Modal contact -->
<div class="modal fade" tabindex="-1" role="dialog" id="mod-contact">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Contacto</h4>
         </div>
         <div class="modal-body">
            <p class="contact-message"></p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
