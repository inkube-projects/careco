<nav class="css-nav-top">
   <div class="container">
      <div class="col-md-6">
         <i class="fas fa-phone"></i> +34 687 715 531
         <i class="fas fa-envelope css-marginL30"></i> careco@careco.es
      </div>

      <div class="col-md-6 text-right">
      	  <a href="#"><i class="fab fa-linkedin"></i></a>
          <a href="#"><i class="fab fa-instagram"></i></a>
        <!-- <a href="#"><i class="fab fa-facebook-square"></i></a>
         <a href="#"><i class="fab fa-youtube"></i></a>
         <a href="#"><i class="fab fa-twitter-square"></i></a>
         <a href="#"><i class="fab fa-pinterest"></i></a>
         <a href="#"><i class="fab fa-linkedin"></i></a>-->
      </div>
   </div>
</nav>

<nav class="navbar navbar-default">
   <div class="container">
      <div class="container-fluid">
         <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="https://www.careco.es/">
               <img src="<?=MAINURL?>views/images/logo-high-n-01.png" alt="CARECO" width="120">
            </a>
         </div>

         <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
               <li class="dropdown">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Promociones en Valencia <div class="css-nav-separator"></div></a>
                           <ul class="dropdown-menu">
                              <li><a href="https://www.careco.es/promociones-valencia/comprar-chalet-adosado/penya-roja/avenida-francia-alameda">Promociones en curso <i class="fas fa-angle-right css-text-green css-marginL10"></i></a></li>
                       		  <li><a href="https://www.careco.es/proximas-promociones-valencia">Próximas promociones <i class="fas fa-angle-right css-text-green css-marginL10"></i></a></li>
                        	  <li><a href="https://www.careco.es/nuestras-obras">Promociones realizadas<i class="fas fa-angle-right css-text-green css-marginL10"></i></a></li>
                           </ul>
                        </li>
               <li><a href="https://www.careco.es/asi-somos">Careco <div class="css-nav-separator"></div></a></li>
               <li><a href="https://www.careco.es/blog">Blog <div class="css-nav-separator"></div></a></li>
               <li><a href="https://www.careco.es/atencion-al-cliente">Atención al cliente <div class="css-nav-separator"></div></a></li>
            </ul>
         </div>
      </div>
   </div>
</nav>
