
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Careco &raquo; Feed" href="https://www.careco.es/?feed=rss2" />
   <script type="text/javascript">
   window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.careco.es\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.7"}};
   !function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
</script>
<style type="text/css">
   img.wp-smiley,
   img.emoji {
      display: inline !important;
      border: none !important;
      box-shadow: none !important;
      height: 1em !important;
      width: 1em !important;
      margin: 0 .07em !important;
      vertical-align: -0.1em !important;
      background: none !important;
      padding: 0 !important;
   }
</style>
<link rel='stylesheet' id='contact-form-7-css'  href='<?=MAINURL?>views/app/resources_2/css/styles.4.6.1.css' type='text/css' media='all' />
<link rel='stylesheet' id='mediaelement-css'  href='<?=MAINURL?>views/app/resources_2/css/mediaelementplayer.min.2.22.0.css' type='text/css' media='all' />
<link rel='stylesheet' id='wp-mediaelement-css'  href='<?=MAINURL?>views/app/resources_2/css/wp-mediaelement.min.4.7.css' type='text/css' media='all' />
<link rel='stylesheet' id='rgs-css'  href='<?=MAINURL?>views/app/resources_2/css/rgs.6.0.1.css' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='<?=MAINURL?>views/app/plugins/font-awesome/web-fonts-with-css/css/fontawesome-all.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='main-styles-css'  href='<?=MAINURL?>views/app/resources_2/css/style.7.6.css' type='text/css' media='all' />
<link rel='stylesheet' id='pretty_photo-css'  href='<?=MAINURL?>views/app/resources_2/css/prettyPhoto.7.0.1.css' type='text/css' media='all' />
<!--[if lt IE 9]>
   <link rel='stylesheet' id='nectar-ie8-css'  href='<?=MAINURL?>css\ie8.4.7.css' type='text/css' media='all' />
<![endif]-->
<link rel='stylesheet' id='responsive-css'  href='<?=MAINURL?>views/app/resources_2/css/responsive.7.6.css' type='text/css' media='all' />
<link rel='stylesheet' id='nectarslider-css'  href='<?=MAINURL?>views/app/resources_2/css/nectar-slider4.7.css' type='text/css' media='all' />
<link rel='stylesheet' id='select2-css'  href='<?=MAINURL?>views/app/resources_2/css/select2-6.2.css' type='text/css' media='all' />
<link rel='stylesheet' id='skin-ascend-css'  href='<?=MAINURL?>views/app/resources_2/css/ascend7.6.css' type='text/css' media='all' />
<link rel='stylesheet' id='easy-social-share-buttons-css'  href='<?=MAINURL?>views/app/resources_2/css/easy-social-share-buttons.min.4.1.css' type='text/css' media='all' />
<link rel='stylesheet' id='js_composer_front-css'  href='<?=MAINURL?>views/app/resources_2/css/js_composer.min.4.12.1.css' type='text/css' media='all' />
<link rel='stylesheet' id='redux-google-fonts-salient_redux-css'  href='//fonts.googleapis.com/css?family=Montserrat%3A400%2C700%7CLato%3A900%2C400&#038;subset=latin&#038;ver=1509443950' type='text/css' media='all' />
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/jquery.1.12.4.js'></script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/jquery-migrate.min.1.4.1.js'></script>
<script type='text/javascript' src='<?=MAINURL?>views/app/resources_2/js/modernizr.2.6.2.js'></script>
<link rel='https://api.w.org/' href='https://www.careco.es/?rest_route=/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.careco.es/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.careco.es/wp-includes/wlwmanifest.xml" />
<meta name="generator" content="WordPress 4.7" />
<link rel='shortlink' href='<?=MAINURL?>' /> <!-- Pagina actual -->
<link rel="alternate" type="application/json+oembed" href="https://www.careco.es/?rest_route=%2Foembed%2F1.0%2Fembed&#038;url=http%3A%2F%2Fwww.careco.es%2F%3Fpage_id%3D7471" />
<link rel="alternate" type="text/xml+oembed" href="https://www.careco.es/?rest_route=%2Foembed%2F1.0%2Fembed&#038;url=http%3A%2F%2Fwww.careco.es%2F%3Fpage_id%3D7471&#038;format=xml" />
<link rel="stylesheet" href="<?=MAINURL?>views/app/resources_2/css/main-promotion-sale.css">
<link rel="stylesheet" href="<?=MAINURL?>views/app/resources_2/css/css-style.css">
<script type="text/javascript">
   var essb_settings = {
      "ajax_url":"https:\/\/www.careco.es\/wp-admin\/admin-ajax.php",
      "essb3_nonce":"dd1aeca4cb",
      "essb3_plugin_url":"https:\/\/www.careco.es\/wp-content\/plugins\/easy-social-share-buttons3",
      "essb3_facebook_total":true,
      "essb3_admin_ajax":false,
      "essb3_internal_counter":false,
      "essb3_stats":false,
      "essb3_ga":false,
      "essb3_ga_mode":"simple",
      "essb3_counter_button_min":0,
      "essb3_counter_total_min":0,
      "blog_url":"https:\/\/www.careco.es\/",
      "ajax_type":"wp",
      "essb3_postfloat_stay":false,
      "essb3_no_counter_mailprint":false,
      "essb3_single_ajax":false,
      "twitter_counter":"self",
      "post_id":7471
   };
</script>
<meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]>
   <link rel="stylesheet" type="text/css" href="views/app/resources_2/css/vc_lte_ie9.min.css" media="screen">
<![endif]-->
<!--[if IE  8]>
   <link rel="stylesheet" type="text/css" href="views/app/resources_2/css/vc-ie8.min.css" media="screen">
<![endif]-->
<style type="text/css" data-type="vc_custom-css">
   .rectangulo {
      min-height:400px!important;
      max-height:400px!important;
      padding-right:12.6%!important;
      padding-left:12%!important;

   }
   .pad {
      padding-left:20%!important;

   }
   .padr {
      padding-right:10%!important;
   }

   .block {
      display:block!important;
   }
   .promo .wpb_wrapper > div {margin-bottom: 0px;}
   div#links-link-7471, div#links2-link-7471 {
      margin-bottom: 10px;
   }
   .vc_col-sm-4.textobajo.wpb_column.column_container.col.centered-text.padding-4-percent.instance-0 {
      padding: 0%;
   }
</style>
<style type="text/css" data-type="vc_shortcodes-custom-css">
   .vc_custom_1481628275977{
      padding-top: 30px !important;
      padding-bottom: 15px !important;
      background-color: #e2e2e2 !important;
   }
   .vc_custom_1481742481819{
      padding-bottom: 20px !important;
   }
   .vc_custom_1482324452088{
      margin-top: 45px !important;
      padding-bottom: 23px !important;
   }
   .vc_custom_1481627956764{
      padding-top: 20px !important;
      padding-bottom: 20px !important;
   }
</style>
<noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
<link rel="stylesheet" href="<?=MAINURL?>views/app/plugins/bootstrap/css/bootstrap.min.css">
