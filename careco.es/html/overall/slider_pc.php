  <div id="home-slider" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">

               <div class="item active" style="background-image: url(views/images/slider/1.jpg)">
                  <div class="caption">
                     <div class="container">
                        <div class="col-md-6 text-left">
                           <a data-scroll class="btn btn-start btn-green animated fadeInUpBig btn-slide" href="#">CASAS CÓMO TÚ</a>
                           <p class="animated fadeInRightBig css-text-black css-medium css-marginT20">Sea como sea tu vida, <br> en CARECO tenemos tu casa</p>
                        </div>

                        <div class="col-md-6">
                           <!-- <h1 class="animated fadeInLeftBig">CAR<span>ECO</span></h1> -->
                        </div>
                     </div>
                  </div>
               </div>
               
               
                <div class="item " style="background-image: url(views/images/slider/3.jpg)">
                  <div class="caption">
                     <div class="container">
                        <!--<div class="col-md-6 text-left">
                           <a data-scroll class="btn btn-start btn-green animated fadeInUpBig btn-slide" href="http://www.careco.es/?page_id=8004">CARECO PICANYA</strong></strong></a>
                           <p class="animated fadeInRightBig css-text-black css-medium css-marginT20">Nueva promoción <br> Residencial en Picanya (Valencia).</p>
                        </div>-->

                        <div class="col-md-6">
                           <!-- <h1 class="animated fadeInLeftBig">CAR<span>ECO</span></h1> -->
                        </div>
                     </div>
                  </div>
               </div>
               
               
               
                <div class="item " style="background-image: url(views/images/slider/4.jpg)">
                  <div class="caption">
                     <div class="container">
                        <!--<div class="col-md-6 text-left">
                           <a data-scroll class="btn btn-start btn-green animated fadeInUpBig btn-slide" href="http://www.careco.es/?page_id=8004">CARECO PICANYA</strong></strong></a>
                           <p class="animated fadeInRightBig css-text-black css-medium css-marginT20">Nueva promoción <br> Residencial en Picanya (Valencia).</p>
                        </div>-->

                        <div class="col-md-6">
                           <!-- <h1 class="animated fadeInLeftBig">CAR<span>ECO</span></h1> -->
                        </div>
                     </div>
                  </div>
               </div>

               <div class="item " style="background-image: url(views/images/slider/5.jpg)">
                  <div class="caption">
                     <div class="container">
                        <!--<div class="col-md-6 text-left">
                           <a data-scroll class="btn btn-start btn-green animated fadeInUpBig btn-slide" href="http://www.careco.es/?page_id=8004">CARECO PICANYA</strong></strong></a>
                           <p class="animated fadeInRightBig css-text-black css-medium css-marginT20">Nueva promoción <br> Residencial en Picanya (Valencia).</p>
                        </div>-->

                        <div class="col-md-6">
                           <!-- <h1 class="animated fadeInLeftBig">CAR<span>ECO</span></h1> -->
                        </div>
                     </div>
                  </div>
               </div>


              



            </div>
   			<a class="left-control" href="#home-slider" data-slide="prev"><i class="fa fa-angle-left"></i></a>
            <a class="right-control" href="#home-slider" data-slide="next"><i class="fa fa-angle-right"></i></a>

            <a id="tohash" href="#portfolio"><i class="fa fa-angle-down css-text-black"></i></a>
         </div>