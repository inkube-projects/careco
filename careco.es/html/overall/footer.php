<section class="css-newsletter">
   <div class="container">
      <h2 class="css-marginB0 css-medium">
         <i class="fa fa-envelope css-fontSize22" style="font-weight: 900;"></i><br>
         Suscíbete a nuestra Newsletter
      </h2>
      <p class="css-fontSize14">Siempre estarás informado de todo lo que ocurre en Careco <br> (Noticias, Novedades, Consejos, Promociones, etc...)</p>

      <form name="frm-newsletter" id="frm-newsletter" method="post" action="">
         <div class="input-group css-newsletter-container">
            <input type="text" id="newsletter_email" name="newsletter_email" class="form-control" placeholder="Introduce tu e-mail">

            <div class="input-group-btn">
               <button type="submit" class="btn btn-default js-submit">Envíar</button>
            </div>
         </div>
         <!--<label class="css-regular">
               <input type="checkbox" name="temrs" id="temrs" value="1"> Acepto los términos expuestos:
            </label>-->
               <p><br>Soy mayor de 14 años y ACEPTO las condiciones establecidas en <a href="https://www.careco.es/legal">Aviso Legal y Política de Privacidad</a><br>
 Suscribirme a la lista de correo para conocer las novedades de CARECO - nuevos inmubles, información del sector inmobiliario, eventos y ferias.</p>
         <div class="text-danger" id="newsletter_email_validate"></div>
      </form>
   </div>
</section>

<section class="css-info">
   <div class="container wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
      <h2 class="text-center css-medium css-fontSize20 css-font-inherit css-text-black">Certificaciones y premios</h2>
      <img src="<?=MAINURL?>views/images/certifications.jpg" class="img-responsive css-noFloat center-block" alt="">
   </div>
</section>

 <footer id="footer">
         <div class="footer-top wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
            <div class="container text-center">
               <h2 class="css-text-black css-semibold fontSize30">CARECO</h2>
               <p class="css-text-black">Copyright&copy; 2018 CARECO S.A. Carrer del Mar, 44  <br> 46003 Vléncia, Valencia</p>

               <div class="col-md-6 text-center css-noFloat center-block">
                  <a href="#"><i class="fab fa-linkedin"></i></a>
                  <a href="#"><i class="fab fa-instagram"></i></a>
               </div>

               <div class="css-links-footer">
                <a href="https://www.careco.es/promociones-valencia/comprar-chalet-adosado/penya-roja/avenida-francia-alameda">Promociones en curso</a> |
                <a href="https://www.careco.es/proximas-promociones-valencia">Próximas promociones</a> |
                <a href="https://www.careco.es/asi-somos">Careco</a> |
                <a href="https://www.careco.es/blog">Blog</a> |
                <a href="https://www.careco.es/atencion-al-cliente">Atención al cliente</a> |
                <a href="https://www.careco.es/legal">Legal</a>
               </div>
            </div>
         </div>
  </footer>

<!-- Modal newsletter -->
<div class="modal fade" tabindex="-1" role="dialog" id="mod-newsletter">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Newsletter</h4>
         </div>
         <div class="modal-body">
            <p class="newsletter-message"></p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
